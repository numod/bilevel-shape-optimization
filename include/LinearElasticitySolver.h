//
// Created by josua on 03.06.20.
//

#ifndef BILEVELSHAPEOPT_LINEARELASTICITYSOLVER_H
#define BILEVELSHAPEOPT_LINEARELASTICITYSOLVER_H

#include <goast/Core/FEM.h>
#include <goast/DiscreteShells.h>

#include <utility>

#include "ShapeOptimization/StateEquationInterface.h"
#include "NodalAreas.h"

template<typename ConfiguratorType, class DeformationType>
class LinearElasticitySolver : public StateEquationSolverBase<ConfiguratorType> {

public:

  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using VecType = typename ConfiguratorType::VecType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

//  using ShellDeformationType =  ShellDeformation<ConfiguratorType, NonlinearMembraneDeformation<ConfiguratorType>, SimpleBendingDeformation<ConfiguratorType> >;

public:
  const RealType updateTolerance = 1e-10;

  // Underlying mesh and deformation model etc.
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;

  mutable DeformationType m_W;
  const std::vector<int> &m_dirichletBoundary;
  mutable MatrixType m_Hessian;
  mutable LinearSolver<ConfiguratorType> m_HessianSolver;

  // additional helpers
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_vertexForces;
  mutable VectorType m_faceMaterial;

  // (cached) output
  mutable VectorType m_defDisplacement;
  mutable bool m_solvedAccurately;
  mutable MatrixType m_mixedDerivative;

  //
  mutable bool m_updateDeformation = true;
  mutable bool m_updateHessian = true;
  mutable bool m_updateMixedDerviative = true;


  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginForceControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = true;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginForceParameter, m_beginMaterialParameter;
public:
  LinearElasticitySolver( const MeshTopologySaver &Topology,
                          const VectorType &refGeometry,
                          const std::vector<int> &dirichletBoundary,
                          const VectorType undefDisplacement,
                          const VectorType vertexForces,
                          const VectorType faceMaterial,
                          const bool UndefIsControl = true,
                          const bool ForceIsControl = false,
                          const bool MaterialIsControl = false,
                          const bool UndefIsParameter = false,
                          const bool ForceIsParameter = false,
                          const bool MaterialIsParameter = false )
          : m_Topology( Topology ), m_refGeometry( refGeometry ), m_dirichletBoundary( dirichletBoundary ),
            m_undefDisplacement( std::move( undefDisplacement )), m_vertexForces( std::move( vertexForces )),
            m_faceMaterial( std::move( faceMaterial )), m_W( Topology, faceMaterial ),
            m_defDisplacement( 3 * Topology.getNumVertices()), m_solvedAccurately( false ),
            m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
            m_MaterialIsControl( MaterialIsControl ),
            m_UndefIsParameter( UndefIsParameter ), m_ForceIsParameter( ForceIsParameter ),
            m_MaterialIsParameter( MaterialIsParameter ),
            m_numVertices( Topology.getNumVertices()),
            m_numFaces( Topology.getNumFaces()),
            m_HessianSolver( ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

//    if ( m_MaterialIsControl )
//      throw std::runtime_error( "LinearElasticitySolver: Material as control is currently not supported!" );

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginForceParameter = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginForceParameter += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "LinearElasticitySolver: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "LinearElasticitySolver: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "LinearElasticitySolver: Material distribution can be either control or parameter, not both!" );

    m_mixedDerivative.resize( 3 * m_numVertices, m_numControls );

    // Initial solution
    buildHessian();
    computeDeformation();
  }

  bool updateControl( const VectorType &Control ) {
    bool updated = false;
    if ( m_UndefIsControl )
      updated = updateUndeformed( Control.segment( m_beginUndefControl, 3 * m_numVertices )) || updated;
    if ( m_ForceIsControl )
      updated = updateForces( Control.segment( m_beginForceControl, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsControl ) {
      bool materialUpdated = updateMaterial( Control.segment( m_beginMaterialControl, m_numFaces ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation;

    return updated;
  }

  bool updateParameters( const VectorType &Parameters ) override {
    bool updated = false;
    if ( m_UndefIsParameter )
      updated = updateUndeformed( Parameters.segment( m_beginUndefParameter, 3 * m_numVertices )) || updated;
    if ( m_ForceIsParameter )
      updated = updateForces( Parameters.segment( m_beginForceParameter, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsParameter ) {
      bool materialUpdated = updateMaterial( Parameters.segment( m_beginMaterialParameter, m_numFaces ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation;

    return updated;
  }


  VectorType solve( const VectorType &Arg ) override {
    updateControl( Arg );

    if ( m_updateHessian )
      buildHessian();

    if ( m_updateDeformation )
      computeDeformation();

    return m_defDisplacement;
  }


  VectorType solveAdjoint( const VectorType &Arg, const VectorType &rhs ) override {
    updateControl( Arg );

    if ( m_updateHessian )
      buildHessian();

    VectorType maskedRhs = rhs;
    applyMaskToVector( m_dirichletBoundary, maskedRhs );

    VectorType adjointSolution( 3 * m_numVertices );
    m_HessianSolver.backSubstitute( maskedRhs, adjointSolution );

    return adjointSolution;
  }

  /**
   * \todo Implement proper caching
   */
  MatrixType mixedDerivative( const VectorType &Arg ) override {
    updateControl( Arg );

    if ( m_updateMixedDerviative ) {
      TripletListType tripletList;

      // dUndef dDef
      if ( m_UndefIsControl ) {
        VectorType undefGeometry = m_refGeometry + m_undefDisplacement;
        VectorType defGeometry = m_refGeometry + m_defDisplacement;
        m_W.pushTripletsDefHessian( m_refGeometry, m_refGeometry, tripletList, 0, m_beginUndefControl, -1. );
        computeNodalAreaForceGradientMatrix_Negative_Transposed<ConfiguratorType>( m_Topology, m_vertexForces ).pushTriplets(
                undefGeometry, tripletList, 0, m_beginUndefControl );
      }

      // dF dDef
      if ( m_ForceIsControl ) {
        VectorType nodalAreas( m_numVertices );
        computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

        for ( int i = 0; i < m_numVertices; i++ )
          for ( const int j : { 0, 1, 2 } )
            tripletList.emplace_back( i + j * m_numVertices, i + j * m_numVertices + m_beginForceControl,
                                      -nodalAreas[i] );
      }

      // dMaterial dDef
      if ( m_MaterialIsControl ) {
        VectorType defDisplacementMinusDisplacement = m_defDisplacement - m_undefDisplacement;

        std::vector<TripletListType> mdTriplets;
        m_W.pushTripletsThirdDerivativeDefParam(m_refGeometry, m_refGeometry, mdTriplets, 1.);

        for ( int i = 0; i < 3 * m_numVertices; i++ ) {
          for ( auto triplet : mdTriplets[i])
            tripletList.emplace_back(triplet.row(), triplet.col()  + m_beginMaterialControl, defDisplacementMinusDisplacement[i] * triplet.value());
        }

      }

      // Assembly
      m_mixedDerivative.setZero();
      m_mixedDerivative.setFromTriplets( tripletList.begin(), tripletList.end());

      m_updateMixedDerviative = false;
    }

    return m_mixedDerivative;
  }

  bool lastSolveSuccessful() const override {
    return m_solvedAccurately;
  }


protected:
  void computeDeformation() const {
    // Build right hand side given by weighted forces
    VectorType nodalAreas( m_numVertices );
    computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

    VectorType rhs = m_vertexForces;
    for ( const int i : { 0, 1, 2 } )
      rhs.segment( i * m_numVertices, m_numVertices ).array() *= nodalAreas.array();

    applyMaskToVector( m_dirichletBoundary, rhs );

    VectorType defDisplacementMinusDisplacement( 3 * m_numVertices );
    m_HessianSolver.backSubstitute( rhs, defDisplacementMinusDisplacement );

    m_defDisplacement = defDisplacementMinusDisplacement + m_undefDisplacement;
    m_solvedAccurately = true;
    m_updateDeformation = false;
  }

  /**
   * \brief Compute linearization of the state equation
   */
  void buildHessian() {
    m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    applyMaskToSymmetricMatrix( m_dirichletBoundary, m_Hessian );
    m_HessianSolver.prepareSolver( m_Hessian );

    m_updateHessian = false;
  }

  bool updateUndeformed( const VectorType &undefDisplacement ) const {
    if (( m_undefDisplacement - undefDisplacement ).norm() > updateTolerance ) {
      m_undefDisplacement = undefDisplacement;
      return true;
    }
    else {
      return false;
    }

  }

  bool updateForces( const VectorType &vertexForces ) const {
    if (( m_vertexForces - vertexForces ).norm() > updateTolerance ) {
      m_vertexForces = vertexForces;
      return true;
    }
    else {
      return false;
    }

  }

  bool updateMaterial( const VectorType &faceMaterial ) const {
    if (( m_faceMaterial - faceMaterial ).norm() > updateTolerance ) {
      m_faceMaterial = faceMaterial;
      m_W.setParameters( m_faceMaterial );
      return true;
    }
    else {
      return false;
    }

  }

};


#endif //BILEVELSHAPEOPT_LINEARELASTICITYSOLVER_H
