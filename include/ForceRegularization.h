//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_FORCEREGULARIZATION_H
#define BILEVELSHAPEOPT_FORCEREGULARIZATION_H



template<typename ConfiguratorType>
class ForceL2Regularization
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const int m_numVertices;
  const int m_numFaces;

  mutable MatrixType m_massMatrix;

  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:

  ForceL2Regularization( const MeshTopologySaver &Topology,
                            const VectorType &refGeometry,
                            const bool UndefIsControl = true,
                            const bool ForceIsControl = false,
                            const bool MaterialIsControl = false,
                            const bool UndefIsParameter = false,
                            const bool ForceIsParameter = false,
                            const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceL2Regularization: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceL2Regularization: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceL2Regularization: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceL2Regularization::apply: wrong dimension of argument!" );

    if ( m_ForceIsControl )
      Dest = 0.5 * Arg.segment( m_beginForceControl, 3 * m_numVertices ).transpose() * m_massMatrix * Arg.segment( m_beginForceControl, 3 * m_numVertices );
    else
      Dest = 0.;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ForceL2Regularization::setParameters: Invalid number of parameters!" );
    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class ForceL2RegularizationGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_massMatrix;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ForceL2RegularizationGradient( const MeshTopologySaver &Topology,
                                    const VectorType &refGeometry,
                                    const bool UndefIsControl = true,
                                    const bool ForceIsControl = false,
                                    const bool MaterialIsControl = false,
                                    const bool UndefIsParameter = false,
                                    const bool ForceIsParameter = false,
                                    const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()),m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceL2RegularizationGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceL2RegularizationGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceL2RegularizationGradient: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceL2RegularizationGradient::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    if ( m_ForceIsControl ) {
      Dest.segment( m_beginForceControl, 3 * m_numVertices ) = m_massMatrix * Arg.segment( m_beginForceControl, 3 * m_numVertices );
    }
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ForceL2RegularizationGradient::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_numControls + 3 * m_numVertices;
  }


};



template<typename ConfiguratorType>
class ForceL2RegularizationHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_massMatrix;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ForceL2RegularizationHessian( const MeshTopologySaver &Topology,
                                 const VectorType &refGeometry,
                                 const bool UndefIsControl = true,
                                 const bool ForceIsControl = false,
                                 const bool MaterialIsControl = false,
                                 const bool UndefIsParameter = false,
                                 const bool ForceIsParameter = false,
                                 const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()),m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceL2RegularizationHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceL2RegularizationHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceL2RegularizationHessian: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceL2RegularizationHessian::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    TripletListType triplets;

    if ( m_ForceIsControl ) {
      for ( int k = 0; k < m_massMatrix.outerSize(); ++k )
        for ( typename MatrixType::InnerIterator it( m_massMatrix, k ); it; ++it )
          triplets.emplace_back( m_beginForceControl + it.row(), m_beginForceControl + it.col(), it.value());
    }

    Dest.setFromTriplets(triplets.begin(), triplets.end());
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ForceL2RegularizationHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_numControls + 3 * m_numVertices;
  }


};


template<typename ConfiguratorType>
class ForceL2LogBarrier
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const int m_numVertices;
  const int m_numFaces;

  mutable MatrixType m_massMatrix;

  mutable VectorType m_Parameters;

  const RealType m_barrierValue;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:

  ForceL2LogBarrier( const MeshTopologySaver &Topology,
                         const VectorType &refGeometry,
                         const RealType barrierValue,
                         const bool UndefIsControl = true,
                         const bool ForceIsControl = false,
                         const bool MaterialIsControl = false,
                         const bool UndefIsParameter = false,
                         const bool ForceIsParameter = false,
                         const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()),m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrierValue( barrierValue * barrierValue ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceL2LogBarrier: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceL2LogBarrier: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceL2LogBarrier: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceL2LogBarrier::apply: wrong dimension of argument!" );

    if ( m_ForceIsControl ) {
      RealType L2 = Arg.segment( m_beginForceControl, 3 * m_numVertices ).transpose() * m_massMatrix *
                    Arg.segment( m_beginForceControl, 3 * m_numVertices );
      Dest = -std::log(m_barrierValue - L2);
    }
    else
      Dest = 0.;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ForceL2LogBarrier::setParameters: Invalid number of parameters!" );
    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class ForceL2LogBarrierGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_massMatrix;
  const RealType m_barrierValue;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ForceL2LogBarrierGradient( const MeshTopologySaver &Topology,
                                 const VectorType &refGeometry,
                             const RealType barrierValue,
                                 const bool UndefIsControl = true,
                                 const bool ForceIsControl = false,
                                 const bool MaterialIsControl = false,
                                 const bool UndefIsParameter = false,
                                 const bool ForceIsParameter = false,
                                 const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()),m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrierValue( barrierValue * barrierValue ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceL2LogBarrierGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceL2LogBarrierGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceL2LogBarrierGradient: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceL2LogBarrierGradient::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    if ( m_ForceIsControl ) {
      RealType L2 = Arg.segment( m_beginForceControl, 3 * m_numVertices ).transpose() * m_massMatrix *
                    Arg.segment( m_beginForceControl, 3 * m_numVertices );
      RealType logTerm = -std::log(m_barrierValue - L2);

      Dest.segment( m_beginForceControl, 3 * m_numVertices ) = 2 * m_massMatrix * Arg.segment( m_beginForceControl, 3 * m_numVertices ) / (m_barrierValue - L2);
    }
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ForceL2LogBarrierGradient::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_numControls + 3 * m_numVertices;
  }


};



template<typename ConfiguratorType>
class ForceL2LogBarrierHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_massMatrix;
  const RealType m_barrierValue;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ForceL2LogBarrierHessian( const MeshTopologySaver &Topology,
                                const VectorType &refGeometry,
                            const RealType barrierValue,
                                const bool UndefIsControl = true,
                                const bool ForceIsControl = false,
                                const bool MaterialIsControl = false,
                                const bool UndefIsParameter = false,
                                const bool ForceIsParameter = false,
                                const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrierValue( barrierValue * barrierValue ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceL2RegularizationHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceL2RegularizationHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceL2RegularizationHessian: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceL2RegularizationHessian::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    TripletListType triplets;

    if ( m_ForceIsControl ) {
      RealType L2 = Arg.segment( m_beginForceControl, 3 * m_numVertices ).transpose() * m_massMatrix *
                    Arg.segment( m_beginForceControl, 3 * m_numVertices );
      RealType logTerm = -std::log(m_barrierValue - L2);

      VectorType L2grad= 2 * m_massMatrix * Arg.segment( m_beginForceControl, 3 * m_numVertices );

      MatrixType localHessian = ( 2 * m_massMatrix ) / ( m_barrierValue - L2 ) +
                                (L2grad * L2grad.transpose()) / (( m_barrierValue - L2 ) * ( m_barrierValue - L2 ));


      for ( int k = 0; k < localHessian.outerSize(); ++k )
        for ( typename MatrixType::InnerIterator it( localHessian, k ); it; ++it )
//          if (std::abs(it.value()) > 1e-12)
            triplets.emplace_back( m_beginForceControl + it.row(), m_beginForceControl + it.col(), it.value());
    }

    Dest.setFromTriplets(triplets.begin(), triplets.end());
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ForceL2RegularizationHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_numControls + 3 * m_numVertices;
  }


};



template<typename ConfiguratorType>
class ForceCylinderLogBarrier
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const int m_numVertices;
  const int m_numFaces;

  mutable VectorType m_nodalAreas;

  mutable VectorType m_Parameters;

  const RealType m_radius;
  const RealType m_height;
  const int m_cylinderDim;
  std::array<int, 2> m_radialDim{ -1, -1 };

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:

  ForceCylinderLogBarrier( const MeshTopologySaver &Topology,
                           const VectorType &refGeometry,
                           int cylinderDim,
                           const RealType radius,
                           const RealType height,
                           const bool UndefIsControl = true,
                           const bool ForceIsControl = false,
                           const bool MaterialIsControl = false,
                           const bool UndefIsParameter = false,
                           const bool ForceIsParameter = false,
                           const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_radius( radius * radius ), m_height( height * height ), m_cylinderDim( cylinderDim ), m_radialDim{ -1, -1 } {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceCylinderLogBarrier: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceCylinderLogBarrier: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceCylinderLogBarrier: Material distribution can be either control or parameter, not both!" );

    if ( cylinderDim == 0 )
      m_radialDim = { 1, 2 };
    else if ( cylinderDim == 1 )
      m_radialDim = { 0, 2 };
    else if ( cylinderDim == 2 )
      m_radialDim = { 0, 1 };
    else
      throw std::runtime_error( "ForceCylinderLogBarrier: Invalid cylinder dim." );

    computeNodalAreas<ConfiguratorType>( Topology, refGeometry, m_nodalAreas );

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceCylinderLogBarrier::apply: wrong dimension of argument!" );

    if ( m_ForceIsControl ) {
      VectorType hForce = Arg.segment( m_beginForceControl + m_cylinderDim * m_numVertices, m_numVertices );
      VectorType rForce( 2 * m_numVertices );
      rForce.head( m_numVertices ) = Arg.segment( m_beginForceControl + m_radialDim[0] * m_numVertices, m_numVertices );
      rForce.tail( m_numVertices ) = Arg.segment( m_beginForceControl + m_radialDim[1] * m_numVertices, m_numVertices );

      RealType L2h = hForce.array().pow( 2 ).matrix().dot( m_nodalAreas );
      RealType L2r = rForce.head( m_numVertices ).array().pow( 2 ).matrix().dot( m_nodalAreas ) +
                     rForce.tail( m_numVertices ).array().pow( 2 ).matrix().dot( m_nodalAreas );

      Dest =  -std::log( m_height - L2h ) - std::log( m_radius - L2r );
    }
    else
      Dest = 0.;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ForceCylinderLogBarrier::setParameters: Invalid number of parameters!" );
    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class ForceCylinderLogBarrierGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable VectorType m_nodalAreas;

  const RealType m_radius;
  const RealType m_height;
  const int m_cylinderDim;
  std::array<int, 2> m_radialDim{ -1, -1 };

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ForceCylinderLogBarrierGradient( const MeshTopologySaver &Topology,
                                   const VectorType &refGeometry,
                                   int cylinderDim,
                                   const RealType radius,
                                   const RealType height,
                                   const bool UndefIsControl = true,
                                   const bool ForceIsControl = false,
                                   const bool MaterialIsControl = false,
                                   const bool UndefIsParameter = false,
                                   const bool ForceIsParameter = false,
                                   const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_radius( radius * radius ), m_height( height * height ), m_cylinderDim( cylinderDim ), m_radialDim{ -1, -1 } {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceCylinderLogBarrierGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceCylinderLogBarrierGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceCylinderLogBarrierGradient: Material distribution can be either control or parameter, not both!" );

    if ( cylinderDim == 0 )
      m_radialDim = { 1, 2 };
    else if ( cylinderDim == 1 )
      m_radialDim = { 0, 2 };
    else if ( cylinderDim == 2 )
      m_radialDim = { 0, 1 };
    else
      throw std::runtime_error( "ForceCylinderLogBarrierGradient: Invalid cylinder dim." );

    computeNodalAreas<ConfiguratorType>(Topology, refGeometry, m_nodalAreas);

  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceCylinderLogBarrierGradient::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    if ( m_ForceIsControl ) {
      VectorType hForce = Arg.segment( m_beginForceControl + m_cylinderDim * m_numVertices, m_numVertices );
      VectorType rForce( 2 * m_numVertices );
      rForce.head( m_numVertices ) = Arg.segment( m_beginForceControl + m_radialDim[0] * m_numVertices, m_numVertices );
      rForce.tail( m_numVertices ) = Arg.segment( m_beginForceControl + m_radialDim[1] * m_numVertices, m_numVertices );

      RealType L2h = hForce.array().pow( 2 ).matrix().dot( m_nodalAreas );
      RealType L2r = rForce.head( m_numVertices ).array().pow( 2 ).matrix().dot( m_nodalAreas ) +
                     rForce.tail( m_numVertices ).array().pow( 2 ).matrix().dot( m_nodalAreas );

      RealType logTermH = -std::log( m_height - L2h );
      RealType logTermR = -std::log( m_radius - L2r );

      Dest.segment( m_beginForceControl + m_radialDim[0] * m_numVertices, m_numVertices ) =
              2 * m_nodalAreas.cwiseProduct( rForce.head( m_numVertices )) / ( m_radius - L2r );
      Dest.segment( m_beginForceControl + m_radialDim[1] * m_numVertices, m_numVertices ) =
              2 * m_nodalAreas.cwiseProduct( rForce.tail( m_numVertices )) / ( m_radius - L2r );

      Dest.segment( m_beginForceControl + m_cylinderDim * m_numVertices, m_numVertices ) =
              2 * m_nodalAreas.cwiseProduct( hForce ) / ( m_height - L2h );
    }
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ForceCylinderLogBarrierGradient::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_numControls + 3 * m_numVertices;
  }


};



template<typename ConfiguratorType>
class ForceCylinderLogBarrierHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable VectorType m_nodalAreas;
  mutable MatrixType m_massMatrixH;
  mutable MatrixType m_massMatrixR;

  const RealType m_radius;
  const RealType m_height;
  const int m_cylinderDim;
  std::array<int, 2> m_radialDim{ -1, -1 };

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ForceCylinderLogBarrierHessian( const MeshTopologySaver &Topology,
                                  const VectorType &refGeometry,
                                  int cylinderDim,
                                  const RealType radius,
                                  const RealType height,
                                  const bool UndefIsControl = true,
                                  const bool ForceIsControl = false,
                                  const bool MaterialIsControl = false,
                                  const bool UndefIsParameter = false,
                                  const bool ForceIsParameter = false,
                                  const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_radius( radius * radius ), m_height( height * height ), m_cylinderDim( cylinderDim ), m_radialDim{ -1, -1 } {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ForceCylinderLogBarrierHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ForceCylinderLogBarrierHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ForceCylinderLogBarrierHessian: Material distribution can be either control or parameter, not both!" );


    if ( cylinderDim == 0 )
      m_radialDim = { 1, 2 };
    else if ( cylinderDim == 1 )
      m_radialDim = { 0, 2 };
    else if ( cylinderDim == 2 )
      m_radialDim = { 0, 1 };
    else
      throw std::runtime_error( "ForceCylinderLogBarrierGradient: Invalid cylinder dim." );

    computeNodalAreas<ConfiguratorType>( Topology, refGeometry, m_nodalAreas );

    m_massMatrixH.resize( 3 * m_numVertices, 3 * m_numVertices );
    m_massMatrixR.resize( 3 * m_numVertices, 3 * m_numVertices );
    TripletListType tripletsH, tripletsR;
    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ ) {
      tripletsH.emplace_back( m_cylinderDim * m_numVertices + vertexIdx, m_cylinderDim * m_numVertices + vertexIdx,
                              m_nodalAreas[vertexIdx] );
      tripletsR.emplace_back( m_radialDim[0] * m_numVertices + vertexIdx, m_radialDim[0] * m_numVertices + vertexIdx,
                              m_nodalAreas[vertexIdx] );
      tripletsR.emplace_back( m_radialDim[1] * m_numVertices + vertexIdx, m_radialDim[1] * m_numVertices + vertexIdx,
                              m_nodalAreas[vertexIdx] );
    }
    m_massMatrixH.setFromTriplets( tripletsH.begin(), tripletsH.end());
    m_massMatrixR.setFromTriplets( tripletsR.begin(), tripletsR.end());
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ForceCylinderLogBarrierHessian::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    TripletListType triplets;

    if ( m_ForceIsControl ) {
      VectorType hForce = Arg.segment( m_beginForceControl + m_cylinderDim * m_numVertices, m_numVertices );
      VectorType rForce( 2 * m_numVertices );
      rForce.head( m_numVertices ) = Arg.segment( m_beginForceControl + m_radialDim[0] * m_numVertices, m_numVertices );
      rForce.tail( m_numVertices ) = Arg.segment( m_beginForceControl + m_radialDim[1] * m_numVertices, m_numVertices );

      RealType L2h = hForce.array().pow( 2 ).matrix().dot( m_nodalAreas );
      RealType L2r = rForce.head( m_numVertices ).array().pow( 2 ).matrix().dot( m_nodalAreas ) +
                     rForce.tail( m_numVertices ).array().pow( 2 ).matrix().dot( m_nodalAreas );

      RealType logTermH = -std::log( m_height - L2h );
      RealType logTermR = -std::log( m_radius - L2r );

      VectorType L2gradR = VectorType::Zero( 3 * m_numVertices );
      L2gradR.segment( m_radialDim[0] * m_numVertices, m_numVertices ) =
              2 * m_nodalAreas.cwiseProduct( rForce.head( m_numVertices ));
      L2gradR.segment( m_radialDim[1] * m_numVertices, m_numVertices ) =
              2 * m_nodalAreas.cwiseProduct( rForce.tail( m_numVertices ));

      VectorType L2gradH = VectorType::Zero( 3 * m_numVertices );
      L2gradH.segment( m_cylinderDim * m_numVertices, m_numVertices ) =
              2 * m_nodalAreas.cwiseProduct( hForce );

      MatrixType localHessianR = ( 2 * m_massMatrixR ) / ( m_radius - L2r ) +
                                 ( L2gradR * L2gradR.transpose()) / (( m_radius - L2r ) * ( m_radius - L2r ));

      MatrixType localHessianH = ( 2 * m_massMatrixH ) / ( m_height - L2h ) +
                                 ( L2gradH * L2gradH.transpose()) / (( m_height - L2h ) * ( m_height - L2h ));


      for ( int k = 0; k < localHessianH.outerSize(); ++k )
        for ( typename MatrixType::InnerIterator it( localHessianH, k ); it; ++it )
          triplets.emplace_back( m_beginForceControl + it.row(), m_beginForceControl + it.col(), it.value());

      for ( int k = 0; k < localHessianR.outerSize(); ++k )
        for ( typename MatrixType::InnerIterator it( localHessianR, k ); it; ++it )
          triplets.emplace_back( m_beginForceControl + it.row(), m_beginForceControl + it.col(), it.value());
    }

    Dest.setFromTriplets(triplets.begin(), triplets.end());
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ForceCylinderLogBarrierHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_numControls + 3 * m_numVertices;
  }


};

#endif //BILEVELSHAPEOPT_FORCEREGULARIZATION_H
