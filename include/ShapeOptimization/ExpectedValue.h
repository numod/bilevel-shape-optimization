/**
 * \file
 * \brief Expected value of parametrized functions
 * \author Sassen
 */

#ifndef BILEVELSHAPEOPT_EXPECTEDVALUE_H
#define BILEVELSHAPEOPT_EXPECTEDVALUE_H

#include "Optimization/StochasticOperators.h"

/**
 * \brief Monte Carlo approximation of the expected value of a parametrized function
 * \author Sassen
 * \tparam FunctionalType Parametrized function
 * \tparam GeneratorType Generator of the samples for the Monte Carlo approximation
 *
 * This implements the expected value \f$\mathbb{E}_{\mu \tilde \mathcal{D}}[f_\mu(\cdot)]\f$ of a parametrized function
 * \f$f_\mu\f$, where the distribution \f$\mathcal D\f$ is determined by the used generator.
 * Concretely it draws samples \f$\mu_1,\ldots,\mu_N\f$ from the distribution and the value of the function is then
 * given as
 * \f[ \sum_{i=1}^N f_{\mu_i}(\cdot)\f]
 */
template<typename FunctionalType>
class SampledExpectedValue
        : public SampledBaseOp<typename FunctionalType::DomainType, typename FunctionalType::RangeType, typename FunctionalType::ParameterType> {
public:
  using SampleType = typename FunctionalType::ParameterType;
  using DomainType = typename FunctionalType::DomainType;
  using RangeType = typename FunctionalType::RangeType;
protected:
  // Wrapped function and used generator
  FunctionalType &m_F;
  const std::function<SampleType()> m_Gen;

  const int m_numSamples;

  // Stored samples
  std::vector<SampleType> m_Samples;

public:
  SampledExpectedValue( FunctionalType &F, const std::function<SampleType()> &Gen, const int numSamples )
          : m_F( F ), m_Gen( Gen ), m_numSamples( numSamples ) {
    computeSamples();
  }

  void computeSamples() override {
    if ( m_Samples.size() != m_numSamples )
      m_Samples.resize( m_numSamples );

    for ( auto &sample : m_Samples )
      sample = m_Gen();
  }

  void setSamples( const std::vector<SampleType> &Samples ) override {
    assert( Samples.size() == m_numSamples && "SampledExpectedValue::setSamples: Wrong number of samples!" );
    m_Samples = Samples;
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Samples;
  }

  void apply( const DomainType &Arg, RangeType &Dest ) const override {
    // first value
    m_F.setParameters( m_Samples[0] );
    Dest = m_F( Arg );

    // other values
    for ( int i = 1; i < m_numSamples; i++ ) {
      m_F.setParameters( m_Samples[i] );
      Dest += m_F( Arg );
    }

    Dest /= m_numSamples;
  }
};


#endif //BILEVELSHAPEOPT_EXPECTEDVALUE_H
