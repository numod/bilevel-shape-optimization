//
// Created by josua on 03.06.20.
//

#ifndef BILEVELSHAPEOPT_STATEEQUATIONINTERFACE_H
#define BILEVELSHAPEOPT_STATEEQUATIONINTERFACE_H

#include <goast/Core/Configurators.h>

template<typename ConfiguratorType=DefaultConfigurator>
class StateEquationSolverBase {
protected:
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;

public:
  StateEquationSolverBase() = default;

  // Destroy polymorphic StateEquationSolverBase correctly, important!
  virtual ~StateEquationSolverBase() = default;

  virtual bool updateParameters ( const VectorType &Parameters ) = 0;

  virtual VectorType solve( const VectorType &Arg ) = 0;

  virtual VectorType solveAdjoint( const VectorType &Arg, const VectorType &rhs ) = 0;

  virtual MatrixType mixedDerivative( const VectorType &Arg ) = 0;

  virtual bool lastSolveSuccessful() const = 0;

};

#endif //BILEVELSHAPEOPT_STATEEQUATIONINTERFACE_H
