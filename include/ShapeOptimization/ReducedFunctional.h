//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_REDUCEDFUNCTIONAL_H
#define BILEVELSHAPEOPT_REDUCEDFUNCTIONAL_H

#include <goast/Optimization/Objectives.h>

#include "StateEquationInterface.h"
#include "Optimization/StochasticOperators.h"

template<typename ConfiguratorType>
class ReducedPDECFunctional
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;

  const BaseOp<VectorType, RealType> &m_J;
  StateEquationSolverBase<ConfiguratorType> &m_stateEqSolver;
public:
  ReducedPDECFunctional( const BaseOp<VectorType, RealType> &J,
                         StateEquationSolverBase<ConfiguratorType> &stateEqSolver ) : m_J( J ),
                                                                                      m_stateEqSolver( stateEqSolver ) {

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    VectorType Solution = m_stateEqSolver.solve( Arg );

    VectorType combinedVector( Arg.size() + Solution.size());
    combinedVector.head( Arg.size()) = Arg;
    combinedVector.tail( Solution.size()) = Solution;

    if ( m_stateEqSolver.lastSolveSuccessful())
      Dest = m_J( combinedVector );
    else
      Dest = std::numeric_limits<RealType>::infinity();

  }

  int getTargetDimension() const {
    return 1;
  }
};

template<typename ConfiguratorType>
class ReducedPDECGradient
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;

  const BaseOp<VectorType, RealType> &m_J;
  const BaseOp<VectorType, VectorType> &m_DJ;
  StateEquationSolverBase<ConfiguratorType> &m_stateEqSolver;
  const std::vector<int> &m_dirichletBoundary;
public:
  ReducedPDECGradient( const BaseOp<VectorType, RealType> &J,
                       const BaseOp<VectorType, VectorType> &DJ,
                       StateEquationSolverBase<ConfiguratorType> &stateEqSolver,
                       const std::vector<int> &dirichletBoundary ) : m_J( J ),
                                                                     m_DJ( DJ ),
                                                                     m_stateEqSolver( stateEqSolver ),
                                                                     m_dirichletBoundary( dirichletBoundary ) {

  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();

    VectorType Solution = m_stateEqSolver.solve( Arg );

    VectorType combinedVector( Arg.size() + Solution.size());
    combinedVector.head( Arg.size()) = Arg;
    combinedVector.tail( Solution.size()) = Solution;

    if ( !m_stateEqSolver.lastSolveSuccessful()) {
      Dest.setConstant( std::numeric_limits<RealType>::infinity());
    }
    else {
      // Gradient of J
      VectorType costGradient = m_DJ( combinedVector );

      VectorType solGradient = -costGradient.tail( Solution.size());

      // Adjoint problem
      VectorType adjointSolution = m_stateEqSolver.solveAdjoint( Arg, solGradient );
//      applyMaskToVector(_dirichletBoundary, costGradient);


      // Complete gradient
      MatrixType mixedDerivative = m_stateEqSolver.mixedDerivative( Arg );
//      applyMaskToMatrix(_dirichletBoundary, mixedDerivative);
      Dest = costGradient.head( Arg.size()) + mixedDerivative.transpose() * adjointSolution;
    }
  }

  int getTargetDimension() const {
    return 1;
  }
};

template<typename ConfiguratorType>
class ReducedPDECParametrizedFunctional
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;

  ParametrizedBaseOp<VectorType, RealType, VectorType> &m_J;
  StateEquationSolverBase<ConfiguratorType> &m_stateEqSolver;
public:
  ReducedPDECParametrizedFunctional( ParametrizedBaseOp<VectorType, RealType, VectorType> &J,
                                     StateEquationSolverBase<ConfiguratorType> &stateEqSolver ) : m_J( J ),
                                                                                                  m_stateEqSolver(
                                                                                                          stateEqSolver ) {

  }

  void setParameters( const VectorType &parameters ) override {
    m_J.setParameters( parameters );
    m_stateEqSolver.updateParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_J.getParameters();
  }

  void apply( const VectorType &Arg, RealType &Dest ) const {
    VectorType Solution = m_stateEqSolver.solve( Arg );

    VectorType combinedVector( Arg.size() + Solution.size());
    combinedVector.head( Arg.size()) = Arg;
    combinedVector.tail( Solution.size()) = Solution;

    if ( m_stateEqSolver.lastSolveSuccessful())
      Dest = m_J( combinedVector );
    else
      Dest = std::numeric_limits<RealType>::infinity();

  }

  int getTargetDimension() const {
    return 1;
  }
};

template<typename ConfiguratorType>
class ReducedPDECParametrizedGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;

  ParametrizedBaseOp<VectorType, RealType, VectorType> &m_J;
  ParametrizedBaseOp<VectorType, VectorType, VectorType> &m_DJ;
  StateEquationSolverBase<ConfiguratorType> &m_stateEqSolver;
  const std::vector<int> &m_dirichletBoundary;
public:
  ReducedPDECParametrizedGradient( ParametrizedBaseOp<VectorType, RealType, VectorType> &J,
                                   ParametrizedBaseOp<VectorType, VectorType, VectorType> &DJ,
                                   StateEquationSolverBase<ConfiguratorType> &stateEqSolver,
                                   const std::vector<int> &dirichletBoundary ) : m_J( J ),
                                                                                 m_DJ( DJ ),
                                                                                 m_stateEqSolver( stateEqSolver ),
                                                                                 m_dirichletBoundary(
                                                                                         dirichletBoundary ) {

  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();

    VectorType Solution = m_stateEqSolver.solve( Arg );

    VectorType combinedVector( Arg.size() + Solution.size());
    combinedVector.head( Arg.size()) = Arg;
    combinedVector.tail( Solution.size()) = Solution;

    if ( !m_stateEqSolver.lastSolveSuccessful()) {
      Dest.setConstant( std::numeric_limits<RealType>::infinity());
    }
    else {
      // Gradient of J
      VectorType costGradient = m_DJ( combinedVector );

      VectorType solGradient = -costGradient.tail( Solution.size());

      // Adjoint problem
      VectorType adjointSolution = m_stateEqSolver.solveAdjoint( Arg, solGradient );
//      applyMaskToVector(_dirichletBoundary, costGradient);


      // Complete gradient
      MatrixType mixedDerivative = m_stateEqSolver.mixedDerivative( Arg );
//      applyMaskToMatrix(_dirichletBoundary, mixedDerivative);
      Dest = costGradient.head( Arg.size()) + mixedDerivative.transpose() * adjointSolution;
    }
  }

  void setParameters( const VectorType &parameters ) override {
    m_J.setParameters( parameters );
    m_DJ.setParameters( parameters );
    m_stateEqSolver.updateParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_J.getParameters();
  }

  int getTargetDimension() const {
    return 1;
  }
};


#endif //BILEVELSHAPEOPT_REDUCEDFUNCTIONAL_H
