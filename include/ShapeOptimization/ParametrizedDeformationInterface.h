//
// Created by josua on 13.10.20.
//

#ifndef BILEVELSHAPEOPT_PARAMETRIZEDDEFORMATIONINTERFACE_H
#define BILEVELSHAPEOPT_PARAMETRIZEDDEFORMATIONINTERFACE_H

#include <goast/Core/DeformationInterface.h>

/**
 * \brief A parametrized function \f$f_\mu \colon U \to V\f$ where \f$\mu\f$ is the parameter
 * \author Sassen
 * \tparam _DomainType Type of domain elements
 * \tparam _RangeType Type of range elements
 * \tparam _ParameterType Type of a parameter
 */
template<typename ConfiguratorType>
class ParametrizedDeformationBase : public DeformationBase<ConfiguratorType> {
public:
  using VectorType = typename ConfiguratorType::VectorType;
  using ParameterType = VectorType;
  using RealType = typename ConfiguratorType::RealType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TensorType = typename ConfiguratorType::TensorType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

  virtual void applyParameterGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                       VectorType &Dest ) const = 0;

  void applyAddParameterGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                  Eigen::Ref<VectorType> Dest, RealType factor = 1.0 ) const {
    VectorType Temp( UndeformedGeom.size() );
    applyParameterGradient( UndeformedGeom, DeformedGeom, Temp );
    Dest += factor * Temp;
  }

  virtual void pushTripletsParameterHessian( const VectorType &UndefGeom, const VectorType &DefGeom,
                                             TripletListType &triplets, int rowOffset, int colOffset,
                                             RealType factor = 1.0 ) const {
    throw std::logic_error( "pushTripletsParameterHessian needs to be implemented in derived class." );
  }

  virtual void pushTripletsMixedHessianUndefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                                   TripletListType &triplets, int rowOffset, int colOffset,
                                                   RealType factor = 1.0 ) const {
    throw std::logic_error( "pushTripletsMixedHessianUndefParam needs to be implemented in derived class." );
  }

  virtual void pushTripletsMixedHessianDefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                                 TripletListType &triplets, int rowOffset, int colOffset,
                                                 RealType factor = 1.0 ) const = 0;

  void applyMixedHessianDefParam( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, MatrixType &Hessian,
                                  RealType factor = 1.0 ) const {
    int dofs = DeformedGeom.size();
    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != numParameters()))
      Hessian.resize( dofs, numParameters());
    Hessian.setZero();

    // set up triplet list
    TripletListType tripletList;
    pushTripletsMixedHessianDefParam( UndeformedGeom, DeformedGeom, tripletList, 0, 0, factor );

    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.begin(), tripletList.end());
  }

  void applyMixedHessianUndefParam( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                    MatrixType &Hessian, RealType factor = 1.0 ) const {
    int dofs = UndeformedGeom.size();
    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != numParameters()))
      Hessian.resize( dofs, numParameters());
    Hessian.setZero();

    // set up triplet list
    TripletListType tripletList;
    pushTripletsMixedHessianUndefParam( UndeformedGeom, DeformedGeom, tripletList, 0, 0, factor );

    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.begin(), tripletList.end());
  }

  void applyParameterHessian( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, MatrixType &Hessian,
                              RealType factor = 1.0 ) const {
    if (( Hessian.rows() != numParameters()) || ( Hessian.cols() != numParameters()))
      Hessian.resize( numParameters(), numParameters());
    Hessian.setZero();

    // set up triplet list
    TripletListType tripletList;
    pushTripletsParameterHessian( UndeformedGeom, DeformedGeom, tripletList, 0, 0, factor );

    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.begin(), tripletList.end());
  }

  virtual void pushTripletsThirdDerivativeDefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                                    std::vector<TripletListType> &triplets,
                                                    RealType factor = 1.0 ) const {
    throw std::logic_error( "pushTripletsThirdDerivativeDefParam needs to be implemented in derived class." );
  }

  void applyThirdDerivativeDefParam( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                     TensorType &Derivative, RealType factor = 1.0 ) const {
    int dofs = DeformedGeom.size();
    Derivative.resize( dofs, dofs, numParameters());
    Derivative.setZero();

    // set up triplet list
    std::vector<TripletListType> tripletList;
    pushTripletsThirdDerivativeDefParam( UndeformedGeom, DeformedGeom, tripletList, factor );

    // fill matrix from triplets
    Derivative.setFromTriplets( tripletList );
  }

  virtual void setParameters( const ParameterType &parameters ) = 0;

  virtual const ParameterType &getParameters() = 0;

  virtual int numParameters() const = 0;
};


#endif //BILEVELSHAPEOPT_PARAMETRIZEDDEFORMATIONINTERFACE_H
