/**
 * \file
 * \brief Expected value of parametrized functions
 * \author Sassen
 */

#ifndef BILEVELSHAPEOPT_EXPECTEDEXCESS_H
#define BILEVELSHAPEOPT_EXPECTEDEXCESS_H

#include "Optimization/StochasticOperators.h"

/**
 * \brief Monte Carlo approximation of the smoothed expected excess of a parametrized function
 * \author Sassen
 * \tparam ConfiguratorType Container with data types
 * \tparam GeneratorType Generator of the samples for the Monte Carlo approximation
 *
 * This implements the smoothed expected value \f$\mathbb{E}_{\mu \tilde \mathcal{D}}[q^\varepsilon(f_\mu(\cdot))]\f$ of a
 * parametrized function \f$f_\mu\f$, where the distribution \f$\mathcal D\f$ is determined by the used generator and
 * \f$[q^\varepsilon(t) = \frac{1}{2}( \sqrt{(t-\eta)^2+\varepsilon} + (t-\eta) ) \f$ as smooth approximation
 * of the max-function.
 * Concretely it draws samples \f$\mu_1,\ldots,\mu_N\f$ from the distribution and the value of the function is then
 * given as
 * \f[ \sum_{i=1}^N q^\varepsilon(f_{\mu_i}(\cdot))\f]
 */
template<typename ConfiguratorType>
class SampledExpectedExcess
        : public SampledBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using SampleType = typename ConfiguratorType::VectorType;
  using DomainType = typename ConfiguratorType::VectorType;
  using RangeType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using RealType = typename ConfiguratorType::RealType;
protected:
  // Wrapped function and used generator
  ParametrizedBaseOp<DomainType, RangeType, SampleType> &m_F;
  const std::function<SampleType()> m_Gen;
  const int m_numSamples;

  // Parameters
  const RealType m_eps;
  const RealType m_eta;

  // Stored samples
  std::vector<SampleType> m_Samples;

public:
  SampledExpectedExcess( ParametrizedBaseOp<DomainType, RangeType, SampleType> &F,
                         const std::function<SampleType()> &Gen,
                         const int numSamples, RealType eps, RealType eta )
          : m_F( F ), m_Gen( Gen ), m_numSamples( numSamples ), m_eps( eps ), m_eta( eta ) {

    computeSamples();
  }

  void computeSamples() override {
    if ( m_Samples.size() != m_numSamples )
      m_Samples.resize( m_numSamples );

    for ( auto &sample : m_Samples )
      sample = m_Gen();
  }

  void setSamples( const std::vector<SampleType> &Samples ) override {
    assert( Samples.size() == m_numSamples && "SampledExpectedValue::setSamples: Wrong number of samples!" );
    m_Samples = Samples;
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Samples;
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    // first value
    m_F.setParameters( m_Samples[0] );
    Dest = q( m_eps, m_eta, m_F( Arg ));

    // other values
    for ( int i = 1; i < m_numSamples; i++ ) {
      m_F.setParameters( m_Samples[i] );
      Dest += q( m_eps, m_eta, m_F( Arg ));
    }

    Dest /= m_numSamples;
  }

protected:
  static RealType q( RealType eps, RealType eta, RealType t ) {
    return 0.5 * ( std::sqrt(( t - eta ) * ( t - eta ) + eps ) + ( t - eta ));
  }
};

/**
 * \brief
 * \tparam ConfiguatorType
 * \tparam GeneratorType
 */
template<typename ConfiguatorType>
class SampledExpectedExcessGradient
        : public SampledBaseOp<typename ConfiguatorType::VectorType, typename ConfiguatorType::VectorType, typename ConfiguatorType::VectorType> {
public:
  using SampleType = typename ConfiguatorType::VectorType;
  using VectorType = typename ConfiguatorType::VectorType;
  using RealType = typename ConfiguatorType::RealType;
protected:
  // Wrapped function and used generator
  ParametrizedBaseOp<VectorType, RealType, SampleType> &m_F;
  ParametrizedBaseOp<VectorType, VectorType, SampleType> &m_DF;
  const std::function<SampleType()> m_Gen;
  const int m_numSamples;

  // Parameters
  const RealType m_eps;
  const RealType m_eta;

  // Stored samples
  std::vector<SampleType> m_Samples;

public:
  SampledExpectedExcessGradient( ParametrizedBaseOp<VectorType, RealType, SampleType> &F,
                                 ParametrizedBaseOp<VectorType, VectorType, SampleType> &DF,
                                 const std::function<SampleType()> &Gen,
                                 const int numSamples, RealType eps, RealType eta )
          : m_F( F ), m_DF( DF ), m_Gen( Gen ), m_numSamples( numSamples ), m_eps( eps ), m_eta( eta ) {

    computeSamples();
  }

  void computeSamples() override {
    if ( m_Samples.size() != m_numSamples )
      m_Samples.resize( m_numSamples );

    for ( auto &sample : m_Samples )
      sample = m_Gen();
  }

  void setSamples( const std::vector<SampleType> &Samples ) override {
    assert( Samples.size() == m_numSamples && "SampledExpectedValue::setSamples: Wrong number of samples!" );
    m_Samples = Samples;
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Samples;
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    // first value
    m_F.setParameters( m_Samples[0] );
    m_DF.setParameters( m_Samples[0] );
    Dest = m_DF( Arg ) * Dq( m_eps, m_eta, m_F( Arg ));

    // other values
    for ( int i = 1; i < m_numSamples; i++ ) {
      m_F.setParameters( m_Samples[i] );
      m_DF.setParameters( m_Samples[i] );
      Dest += m_DF( Arg ) * Dq( m_eps, m_eta, m_F( Arg ));
    }

    Dest /= m_numSamples;
  }

protected:
  static RealType Dq( RealType eps, RealType eta, RealType t ) {
    return 0.5 * (( t - eta ) / std::sqrt(( t - eta ) * ( t - eta ) + eps ) + 1 );
  }
};


#endif //BILEVELSHAPEOPT_EXPECTEDEXCESS_H
