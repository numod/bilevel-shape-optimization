//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_ENERGYREGULARIZATION_H
#define BILEVELSHAPEOPT_ENERGYREGULARIZATION_H

template<typename ConfiguratorType>
class EnergyRegularizationTerm
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {
protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;

  const VectorType &_refGeometry;
  const DeformationBase<ConfiguratorType> &_W;
  const int _numVertices;

public:

  EnergyRegularizationTerm( const MeshTopologySaver &topology,
                            const VectorType &refGeometry,
                            const DeformationBase<ConfiguratorType> &W ) :
          _W( W ),
          _refGeometry( refGeometry ),
          _numVertices( topology.getNumVertices()) {

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < 3 * _numVertices )
      throw std::length_error( "EnergyRegularizationTerm: wrong dimension of argument!" );

    VectorType Geometry = _refGeometry + Arg.head( 3 * _numVertices );

    _W.applyEnergy( _refGeometry, Geometry, Dest );
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class EnergyRegularizationTermGradient : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;

  const VectorType &_refGeometry;
  const DeformationBase<ConfiguratorType> &_W;
  const int _numVertices;


public:

  EnergyRegularizationTermGradient( const MeshTopologySaver &topology,
                                    const VectorType &refGeometry,
                                    const DeformationBase<ConfiguratorType> &W ) :
          _W( W ),
          _refGeometry( refGeometry ),
          _numVertices( topology.getNumVertices()) {

  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() < 3 * _numVertices )
      throw std::length_error( "EnergyRegularizationTermGradient: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    VectorType Geometry = _refGeometry + Arg.head( 3 * _numVertices );

    // Calculate undeformed Gradient
    VectorType energyGradient_undef( 3 * _numVertices );
    _W.applyDefGradient( _refGeometry, Geometry, energyGradient_undef );

    Dest.head( 3 * _numVertices ) = energyGradient_undef;
  }

  int getTargetDimension() const {
    return 3 * _numVertices;
  }


};

#endif //BILEVELSHAPEOPT_ENERGYREGULARIZATION_H
