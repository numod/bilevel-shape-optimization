//=============================================================================
//
//  CLASS MaterialDeformationEnergies
//
//=============================================================================


#ifndef BILEVELSHAPEOPT_MATERIALDEFORMATIONENERGIES_H
#define BILEVELSHAPEOPT_MATERIALDEFORMATIONENERGIES_H


//== INCLUDES =================================================================
#include <goast/Core/Auxiliary.h>
#include <goast/Core/LocalMeshGeometry.h>
#include <goast/Core/Topology.h>
#include <goast/Core/DeformationInterface.h>

#include "ShapeOptimization/ParametrizedDeformationInterface.h"
#include "materialBendingDeformations.h"
#include "materialMembraneDeformations.h"


//!==========================================================================================================
//! DEFORMATION ENERGIES
//!==========================================================================================================
//! \brief Abstract base class to prescribe structure of deformation energies.
//! \author Heeren
//! \coauthor Echelmeyer, modified for changing vertex weights
//!
//! Deformation energy  \f$ E \f$  is considered as some functional  \f$ (S_1, S_2) \mapsto E[S_1, S_2] \f$ ,
//! where  \f$ S_1 \f$  and  \f$ S_2 \f$  are refered to as undeformed and deformed configuration, respectively.
//!
//! Several functions are supposed to be provided by derived classes:
//! - applyEnergy()
//! - applyUndefGradient (), i.e. D_1 E[S_1, S_2]
//! - applyDefGradient (), i.e. D_2 E[S_1, S_2]
//! - pushTripletsDefHessian(), i.e. to assemble D_2^2 E[S_1, S_2]
//! - pushTripletsUndefHessian(), i.e. to assemble  D_1^2 E[S_1, S_2]
//! - pushTripletsMixedHessian(), i.e. to assemble D_1 D_2 E[S_1, S_2]
//! where D_i denotes the derivative w.r.t. the ith argument.


//!==========================================================================================================
template<typename ConfiguratorType>
class MaterialNonlinearMembraneDeformation : public ParametrizedDeformationBase<ConfiguratorType> {

public:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef std::vector<TripletType> TripletListType;
  using ParameterType = VectorType;

protected:
  const MeshTopologySaver &m_Topology;
  VectorType m_faceWeights;

public:
  MaterialNonlinearMembraneDeformation( const MeshTopologySaver &Topology, const VectorType faceWeights )
          : m_Topology( Topology ), m_faceWeights( faceWeights ) {

  }

  void applyEnergy( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, RealType &Dest ) const override {
    materialNonlinearMembraneEnergy<ConfiguratorType>( m_Topology, UndeformedGeom, true, m_faceWeights ).apply(
            DeformedGeom, Dest );
  }

  void applyUndefGradient( const VectorType &UndeformedGeom,
                           const VectorType &DeformedGeom,
                           VectorType &Dest ) const override {
    materialNonlinearMembraneGradientUndef<ConfiguratorType>( m_Topology, DeformedGeom, m_faceWeights ).apply(
            UndeformedGeom, Dest );
  }

  void applyDefGradient( const VectorType &UndeformedGeom,
                         const VectorType &DeformedGeom,
                         VectorType &Dest ) const override {
    materialNonlinearMembraneGradientDef<ConfiguratorType>( m_Topology, UndeformedGeom, m_faceWeights ).apply(
            DeformedGeom, Dest );
  }

  void applyParameterGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                               VectorType &Dest ) const override {
    materialNonlinearMembraneEnergyGradientMat<ConfiguratorType>( m_Topology, UndeformedGeom, DeformedGeom ).apply(
            m_faceWeights, Dest );
  }

  void pushTripletsDefHessian( const VectorType &UndefGeom, const VectorType &DefGeom, TripletListType &triplets,
                               int rowOffset, int colOffset, RealType factor = 1.0 ) const override {
    materialNonlinearMembraneHessianDef<ConfiguratorType>( m_Topology, UndefGeom, m_faceWeights, rowOffset,
                                                           colOffset ).pushTriplets( DefGeom, triplets, factor );
  }

  void pushTripletsUndefHessian( const VectorType &UndefGeom, const VectorType &DefGeom, TripletListType &triplets,
                                 int rowOffset, int colOffset, RealType factor = 1.0 ) const override {
    materialNonlinearMembraneHessianUndef<ConfiguratorType>( m_Topology, DefGeom, m_faceWeights, rowOffset,
                                                             colOffset ).pushTriplets( UndefGeom, triplets, factor );
  }

  // mixed second derivative of deformation energy E[S_1, S_2], i.e. if "FirstDerivWRTDef" we have D_1 D_2 E[.,.], otherwise D_2 D_1 E[.,.]
  void pushTripletsMixedHessian( const VectorType &UndefGeom, const VectorType &DefGeom, TripletListType &triplets,
                                 int rowOffset, int colOffset, const bool FirstDerivWRTDef,
                                 RealType factor = 1.0 ) const override {
    materialNonlinearMembraneHessianMixed<ConfiguratorType>( m_Topology, UndefGeom, true, FirstDerivWRTDef,
                                                             m_faceWeights, rowOffset, colOffset ).pushTriplets(
            DefGeom, triplets, factor );
  }

  void pushTripletsMixedHessianDefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                         TripletListType &triplets, int rowOffset, int colOffset,
                                         RealType factor = 1.0 ) const override {
    materialNonlinearMembraneHessianMixedDefMat<ConfiguratorType>( m_Topology, UndefGeom, DefGeom, rowOffset,
                                                                   colOffset ).pushTriplets( m_faceWeights,
                                                                                             triplets, factor );
  }


  void pushTripletsThirdDerivativeDefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                            std::vector<TripletListType> &triplets,
                                            RealType factor = 1.0 ) const {
    materialNonlinearMembraneThirdDerivDefMat<ConfiguratorType>( m_Topology, UndefGeom, m_faceWeights ).pushTriplets( DefGeom, triplets, factor );
  }

  int numOfNonZeroHessianEntries() const {
    // per face we have 3 active vertices, i.e. 9 combinations each producing a 3x3-matrix
    return 9 * 9 * m_Topology.getNumFaces();
  }

  void setParameters( const ParameterType &parameters ) override {
    assert( parameters.size() == m_Topology.getNumFaces());
    m_faceWeights = parameters;
  }

  const ParameterType &getParameters() override {
    return m_faceWeights;
  }

  int numParameters() const override {
    return m_Topology.getNumVertices();
  }

};

//!==========================================================================================================
template<typename ConfiguratorType>
class MaterialSimpleBendingDeformation : public ParametrizedDeformationBase<ConfiguratorType> {

public:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef std::vector<TripletType> TripletListType;
  using ParameterType = VectorType;

protected:
  const MeshTopologySaver &m_Topology;
  VectorType m_faceWeights;

public:
  MaterialSimpleBendingDeformation( const MeshTopologySaver &Topology,
                                    const VectorType faceWeights ) :
          m_Topology( Topology ), m_faceWeights( faceWeights ) {

  }

  void applyEnergy( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, RealType &Dest ) const override {
    materialSimpleBendingEnergy<ConfiguratorType>( m_Topology, UndeformedGeom, true, m_faceWeights ).apply(
            DeformedGeom, Dest );
  }

  void applyUndefGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                           VectorType &Dest ) const override {
    materialSimpleBendingGradientUndef<ConfiguratorType>( m_Topology, DeformedGeom, m_faceWeights ).apply(
            UndeformedGeom, Dest );
  }

  void applyDefGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                         VectorType &Dest ) const override {
    materialSimpleBendingGradientDef<ConfiguratorType>( m_Topology, UndeformedGeom, m_faceWeights ).apply(
            DeformedGeom, Dest );
  }

  void applyParameterGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                               VectorType &Dest ) const override {
    materialSimpleBendingEnergyGradientMat<ConfiguratorType>( m_Topology, UndeformedGeom, DeformedGeom ).apply(
            m_faceWeights, Dest );
  }

  void pushTripletsDefHessian( const VectorType &UndefGeom, const VectorType &DefGeom, TripletListType &triplets,
                               int rowOffset, int colOffset, RealType factor = 1.0 ) const override {
    materialSimpleBendingHessianDef<ConfiguratorType>( m_Topology, UndefGeom, m_faceWeights, rowOffset,
                                                       colOffset ).pushTriplets( DefGeom, triplets, factor );
  }

  void pushTripletsUndefHessian( const VectorType &UndefGeom, const VectorType &DefGeom, TripletListType &triplets,
                                 int rowOffset, int colOffset, RealType factor = 1.0 ) const override {
    materialSimpleBendingHessianUndef<ConfiguratorType>( m_Topology, DefGeom, m_faceWeights, rowOffset,
                                                         colOffset ).pushTriplets( UndefGeom, triplets, factor );
  }

  // mixed second derivative of deformation energy E[S_1, S_2], i.e. if "FirstDerivWRTDef" we have D_1 D_2 E[.,.], otherwise D_2 D_1 E[.,.]
  void pushTripletsMixedHessian( const VectorType &UndefGeom, const VectorType &DefGeom, TripletListType &triplets,
                                 int rowOffset, int colOffset, const bool FirstDerivWRTDef,
                                 RealType factor = 1.0 ) const override {
    materialSimpleBendingHessianMixed<ConfiguratorType>( m_Topology, UndefGeom, true, FirstDerivWRTDef, m_faceWeights,
                                                         rowOffset, colOffset ).pushTriplets( DefGeom, triplets, factor );
  }


  void pushTripletsMixedHessianDefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                         TripletListType &triplets, int rowOffset, int colOffset,
                                         RealType factor = 1.0 ) const override {
    materialSimpleBendingHessianMixedDefMat<ConfiguratorType>( m_Topology, UndefGeom, DefGeom, rowOffset,
                                                               colOffset ).pushTriplets( m_faceWeights, triplets, factor );
  }

  void pushTripletsThirdDerivativeDefParam( const VectorType &UndefGeom, const VectorType &DefGeom,
                                            std::vector<TripletListType> &triplets,
                                            RealType factor = 1.0 ) const {
    materialSimpleBendingThirdDerivDefMat<ConfiguratorType>( m_Topology, UndefGeom, m_faceWeights, 0, 0 ).pushTriplets( DefGeom, triplets, factor );
  }

  int numOfNonZeroHessianEntries() const {
    // per edge we have 4 active vertices, i.e. 16 combinations each producing a 3x3-matrix
    return 16 * 9 * m_Topology.getNumEdges();
  }

  void setParameters( const ParameterType &parameters ) override {
    assert( parameters.size() == m_Topology.getNumFaces());
    m_faceWeights = parameters;
  }

  const ParameterType &getParameters() override {
    return m_faceWeights;
  }

  int numParameters() const override {
    return m_Topology.getNumFaces();
  }

};

//!==========================================================================================================
template<typename ConfiguratorType, typename MaterialMembraneDeformationType, typename MaterialBendingDeformationType>
class MaterialShellDeformation : public ParametrizedDeformationBase<ConfiguratorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;
  using ParameterType = VectorType;

protected:
  const MeshTopologySaver &m_Topology;
  VectorType m_faceWeights;

  MaterialMembraneDeformationType W_mem;
  MaterialBendingDeformationType W_bend;

public:
  MaterialShellDeformation( const MeshTopologySaver &Topology, VectorType faceWeights )
          : m_Topology( Topology ), m_faceWeights( faceWeights ), W_mem( m_Topology, m_faceWeights ),
            W_bend( m_Topology, m_faceWeights ) {}

  void applyEnergy( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, RealType &Dest ) const {
    W_mem.applyEnergy( UndeformedGeom, DeformedGeom, Dest );
    W_bend.applyAddEnergy( UndeformedGeom, DeformedGeom, Dest );
  }

  void applyUndefGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, VectorType &Dest ) const {
    W_mem.applyUndefGradient( UndeformedGeom, DeformedGeom, Dest );
    W_bend.applyAddUndefGradient( UndeformedGeom, DeformedGeom, Dest );
  }

  void applyDefGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom, VectorType &Dest ) const {
    W_mem.applyDefGradient( UndeformedGeom, DeformedGeom, Dest );
    W_bend.applyAddDefGradient( UndeformedGeom, DeformedGeom, Dest );
  }

  void applyParameterGradient( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                               VectorType &Dest ) const override {
    W_mem.applyParameterGradient( UndeformedGeom, DeformedGeom, Dest );
    W_bend.applyAddParameterGradient( UndeformedGeom, DeformedGeom, Dest );
  }

  void pushTripletsDefHessian( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                               TripletListType &triplets, int rowOffset, int colOffset, RealType factor = 1.0 ) const {
    //TODO parallelize?!
    W_mem.pushTripletsDefHessian( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, factor );
    W_bend.pushTripletsDefHessian( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, factor );
  }

  void pushTripletsUndefHessian( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                 TripletListType &triplets, int rowOffset, int colOffset,
                                 RealType factor = 1.0 ) const {
    //TODO parallelize?!
    W_mem.pushTripletsUndefHessian( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, factor );
    W_bend.pushTripletsUndefHessian( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, factor );
  }

  // mixed second derivative of deformation energy E[S_1, S_2], i.e. if "FirstDerivWRTDef" we have D_1 D_2 E[.,.], otherwise D_2 D_1 E[.,.]
  void pushTripletsMixedHessian( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                 TripletListType &triplets, int rowOffset, int colOffset, const bool FirstDerivWRTDef,
                                 RealType factor = 1.0 ) const {
    //TODO parallelize?!
    W_mem.pushTripletsMixedHessian( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, FirstDerivWRTDef, factor );
    W_bend.pushTripletsMixedHessian( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, FirstDerivWRTDef, factor );
  }

  // mixed second derivative of energy E[delta, u_def], D delta D u_def E [delta, u_def], i.e. first derived wrt deformed geometry then wrt vertex weights 
  void pushTripletsMixedHessianDefParam( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                         TripletListType &triplets, int rowOffset, int colOffset,
                                         RealType factor = 1. ) const override {
    //TODO parallelize?!
    W_mem.pushTripletsMixedHessianDefParam( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, factor );
    W_bend.pushTripletsMixedHessianDefParam( UndeformedGeom, DeformedGeom, triplets, rowOffset, colOffset, factor );
  }

  void pushTripletsThirdDerivativeDefParam( const VectorType &UndeformedGeom, const VectorType &DeformedGeom,
                                            std::vector<TripletListType> &triplets,
                                            RealType factor = 1.0 ) const {
    W_mem.pushTripletsThirdDerivativeDefParam( UndeformedGeom, DeformedGeom, triplets, factor );
    W_bend.pushTripletsThirdDerivativeDefParam( UndeformedGeom, DeformedGeom, triplets, factor );
  }

  int numOfNonZeroHessianEntries() const override {
    return W_mem.numOfNonZeroHessianEntries() + W_bend.numOfNonZeroHessianEntries();
  }

  RealType getBendingWeight() const {
    return -100.;
  }

  void setParameters( const ParameterType &parameters ) override {
    assert( parameters.size() == m_Topology.getNumFaces());
    m_faceWeights = parameters;
    W_mem.setParameters( m_faceWeights );
    W_bend.setParameters( m_faceWeights );
  }

  const ParameterType &getParameters() override {
    return m_faceWeights;
  }

  int numParameters() const override {
    return m_Topology.getNumFaces();
  }

};

#endif
