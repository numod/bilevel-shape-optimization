//=============================================================================
//
//  CLASS MaterialShellDeformations
//
//=============================================================================


#ifndef MATERIALBENDINGDEFORMATIONS_HH
#define MATERIALBENDINGDEFORMATIONS_HH


//== INCLUDES =================================================================
#include <goast/Core/Auxiliary.h>
#include <goast/Core/LocalMeshGeometry.h>
#include <goast/Core/Topology.h>
#include <goast/Core/BaseOpInterface.h>

//#define DEBUGMODE

//!==========================================================================================================
//! DISCRETE SHELLS BENDING ENERGY (BASED ON GRINSPUN ET AL. 2003)
//!==========================================================================================================

//! \brief Discrete bending energy between two thin shells given as triangular meshes.
//! \author Heeren
//! \coauthor Echelmeyer, modified for changing vertex weights
//!
//! Smooth bending energy between two shells \f$ S \f$ and \f$ \tilde S \f$ is given by \f$ E[S, \tilde S] = \int_S ( h - h_\phi])^2 da \f$, 
//! where \f$ h \f$ and \f$ h_\phi \f$ denote the mean curvature on \f$ S \f$ and \f$ \tilde S = \phi(S) \f$, respectively.
//! Discrete bending energy is taken from "Discrete shells" paper by Grinspun et al., 2003.
//! Here \f$ E[S, \tilde S] = \sum_e \frac{(\theta_e - \theta_{\phi(e)})^2 |e|^2}{A_e} \f$, 
//! where the sum is over all edges \f$ e \in S \f$, where \f$ \theta_e \f$ is the dihedral angle at edge \f$ e \f$ and \f$ A_e = |T_1| + |T_2| \f$, if \f$ T_i \f$  are the triangles sharing edge \f$ e \f$.
//!
//! Note that the energy might either be thought of as \f$ S \mapsto E[S, \tilde S] \f$ (active shell is undeformed shell) or \f$ \tilde S \mapsto E[S, \tilde S] \f$ (active shell is deformed shell).
//! The active shell is considered the argument whereas the inactive shell is given in the constructor.
template<typename ConfiguratorType>
class materialSimpleBendingEnergy
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_inactiveGeometry;
  const bool _activeShellIsDeformed;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;

public:

  materialSimpleBendingEnergy( const MeshTopologySaver &topology,
                               const VectorType &InactiveGeometry,
                               const bool ActiveShellIsDeformed,
                               const VectorType &faceWeights )
          : _topology( topology ),
            _inactiveGeometry( InactiveGeometry ),
            _activeShellIsDeformed( ActiveShellIsDeformed ),
            _faceWeights( faceWeights ),
            _edgeWeights( setEdgeWeights()) {

  }

  // energy evaluation
  void apply( const VectorType &ActiveGeometry, RealType &Dest ) const {

    if ( ActiveGeometry.size() != _inactiveGeometry.size()) {
      std::cerr << "size of active = " << ActiveGeometry.size() << " vs. size of inactive = "
                << _inactiveGeometry.size() << std::endl;
      throw BasicException( "SimpleBendingEnergy::apply(): sizes dont match!" );
    }

    const VectorType *defShellP = _activeShellIsDeformed ? &ActiveGeometry : &_inactiveGeometry;
    const VectorType *undefShellP = _activeShellIsDeformed ? &_inactiveGeometry : &ActiveGeometry;

    Dest = 0.;

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // set up vertices and edges
      VecType Pi, Pj, Pk, Pl, temp;

      // get deformed geometry
      getXYZCoord<VectorType, VecType>( *defShellP, Pi, pi );
      getXYZCoord<VectorType, VecType>( *defShellP, Pj, pj );
      getXYZCoord<VectorType, VecType>( *defShellP, Pk, pk );
      getXYZCoord<VectorType, VecType>( *defShellP, Pl, pl );

      // compute deformed dihedral angle
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );

      // get undeformed geometry
      getXYZCoord<VectorType, VecType>( *undefShellP, Pi, pi );
      getXYZCoord<VectorType, VecType>( *undefShellP, Pj, pj );
      getXYZCoord<VectorType, VecType>( *undefShellP, Pk, pk );
      getXYZCoord<VectorType, VecType>( *undefShellP, Pl, pl );

      // compute volume, length of edge and theta difference
      // A_e = h_e * l_e = (|T_1| + |T_2|)/3 if T_1 and T_2 share edge e
      // Furthermore, |T| = 0.5 * |e_1 x e_2|, if e_1 and e_2 are edges of T
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      // CAUTION We omitted a factor 3 here!
      Dest += _edgeWeights[edgeIdx] * delTheta * delTheta * elengthSqr / vol;

#ifdef DEBUGMODE
                                                                                                                              if( std::isnan( Dest[0] ) ){
          std::cerr << "NaN in simple bending energy in edge " << edgeIdx << "! " << std::endl;
          if( hasNanEntries(ActiveGeometry) ){
            std::cerr << "Argument has NaN entries! " << std::endl;
          }
          else{
            std::cerr << "delTheta = " << delTheta << std::endl;
            std::cerr << "elengthSqr = " << elengthSqr << std::endl;
            std::cerr << "vol = " << vol << std::endl;
          }
          throw BasicException("SimpleBendingEnergy::apply(): NaN Error!");
      }
#endif
    }
  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if (fi == -1 || fj == -1)
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }


};


//material as input
template<typename ConfiguratorType>
class materialInputSimpleBendingEnergy
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;


public:

  materialInputSimpleBendingEnergy( const MeshTopologySaver &topology,
                                    const VectorType &undefShell,
                                    const VectorType &defShell )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ) {

  }

  // energy evaluation
  void apply( const VectorType &faceWeights, RealType &Dest ) const {

    if ( faceWeights.size() != _topology.getNumFaces()) {
      throw std::length_error( "materialInputSimpleBendingEnergy::apply(): wrong size of weight vector" );
    }

    VectorType edgeWeights( _topology.getNumEdges());
    setEdgeWeights( faceWeights, edgeWeights );

    Dest.setZero();

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // set up vertices and edges
      VecType Pi, Pj, Pk, Pl, temp;

      // get deformed geometry
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );

      // compute deformed dihedral angle
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );

      // get undeformed geometry
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );

      // compute volume, length of edge and theta difference
      // A_e = h_e * l_e = (|T_1| + |T_2|)/3 if T_1 and T_2 share edge e
      // Furthermore, |T| = 0.5 * |e_1 x e_2|, if e_1 and e_2 are edges of T
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      // CAUTION We omitted a factor 3 here!
      Dest[0] += edgeWeights[edgeIdx] * delTheta * delTheta * elengthSqr / vol;

#ifdef DEBUGMODE
                                                                                                                              if( std::isnan( Dest[0] ) ){
          std::cerr << "NaN in simple bending energy in edge " << edgeIdx << "! " << std::endl;
          if( hasNanEntries(ActiveGeometry) ){
            std::cerr << "Argument has NaN entries! " << std::endl;
          }
          else{
            std::cerr << "delTheta = " << delTheta << std::endl;
            std::cerr << "elengthSqr = " << elengthSqr << std::endl;
            std::cerr << "vol = " << vol << std::endl;
          }
          throw BasicException("SimpleBendingEnergy::apply(): NaN Error!");
      }
#endif
    }
  }

  //bending energy scales with delta^3
  void setEdgeWeights( const VectorType &faceWeights, VectorType &edgeWeights ) const {
    edgeWeights.resize(_topology.getNumFaces());
    edgeWeights.setZero();

    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }
  }


};

// // ==========================================================================================================
// // ! \brief First derivative of simple bending energy w.r.t. the material weights (cf. class materialSimpleBendingEnergy above).
// // ! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingEnergyGradientMat
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;

public:

  materialSimpleBendingEnergyGradientMat( const MeshTopologySaver &topology,
                                          const VectorType &undefShell,
                                          const VectorType &defShell )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ) {

  }

  // energy evaluation
  void apply( const VectorType &faceWeights, VectorType &Dest ) const {

    if ( faceWeights.size() != _topology.getNumFaces()) {
      throw std::length_error( "materialSimpleBendingEnergyGradientMat::apply(): weight vector size dont match!" );
    }

    if ( Dest.size() != _topology.getNumFaces()) {
      Dest.resize( _topology.getNumFaces());
    }

    Dest.setZero();

    VectorType edgeWeightsFirstDerivative( _topology.getNumEdges());
    setEdgeWeightsFirstDerivative( faceWeights, edgeWeightsFirstDerivative );

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // set up vertices and edges
      VecType Pi, Pj, Pk, Pl, temp;

      // get deformed geometry
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );

      // compute deformed dihedral angle
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );

      // get undeformed geometry
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );

      // compute volume, length of edge and theta difference
      // A_e = h_e * l_e = (|T_1| + |T_2|)/3 if T_1 and T_2 share edge e
      // Furthermore, |T| = 0.5 * |e_1 x e_2|, if e_1 and e_2 are edges of T
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );

      //compute gradient wrt to material weights, each edge weight is the mean of the two adjacent vertex weights
      //as we derive wrt. to vertex weights, we have outer derivative which we computed in function setDerivedEdgeWeights
      //the inner derivate is 0.5*faceWeight
      Dest[fi] += 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * delTheta * elengthSqr / vol;
      Dest[fj] += 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * delTheta * elengthSqr / vol;

#ifdef DEBUGMODE
                                                                                                                              if( hasNanEntries( Dest ) ){
                                std::cerr << "NaN in simple bending gradient deformed in edge " << edgeIdx << "! " << std::endl;
                                if( hasNanEntries(defShell) ){
                                        std::cerr << "Argument has NaN entries! " << std::endl;
                                }
                                else{
                                        std::cerr << "delTheta = " << delTheta << std::endl;
                                        std::cerr << "elengthSqr = " << elengthSqr << std::endl;
                                        std::cerr << "vol = " << vol << std::endl;
                                }
                                throw BasicException("SimpleBendingGradientMat::apply(): NaN Error!");
                        }
#endif
    }
  }

  //bending energy scales with delta^3, so derivative scales with delta^2, here we compute the outer derivative with respect to edge indices
  void setEdgeWeightsFirstDerivative( const VectorType &faceWeights, VectorType &edgeWeightsFirstDerivative ) const {
    edgeWeightsFirstDerivative.resize( _topology.getNumEdges());
    edgeWeightsFirstDerivative.setZero();
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeightsFirstDerivative[edgeIdx] = 3 * tmp * tmp;
    }

  }

};

//! \todo Revise, derivative test not correct
//! \brief Second derivative of simple bending energy w.r.t. the material configuration (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingEnergyHessianMat
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  mutable int _rowOffset, _colOffset;

public:
  materialSimpleBendingEnergyHessianMat( const MeshTopologySaver &topology,
                                         const VectorType &undefShell,
                                         const VectorType &defShell,
                                         int rowOffset = 0,
                                         int colOffset = 0 ) :
          _topology( topology ),
          _undefShell( undefShell ),
          _defShell( defShell ),
          _rowOffset( rowOffset ),
          _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &faceWeights, MatrixType &Dest ) const {
    assembleHessian( faceWeights, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &faceWeights, MatrixType &Hessian ) const {
    int dofs =  _topology.getNumVertices();
    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != dofs ))
      Hessian.resize( dofs, dofs );
    Hessian.setZero();

    // set up triplet list
    TripletListType tripletList;
    // per edge we have 2 vertices which because of reflexity enter two times in our hessian, i.e. 4 combinations per edge
    tripletList.reserve( 4 * _topology.getNumEdges());
    // fill matrix from triplets
    pushTriplets( faceWeights, tripletList );
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  // fill triplets
  void pushTriplets( const VectorType &faceWeights, TripletListType &tripletList ) const {

    if (faceWeights.size() != _topology.getNumFaces()) {
      throw std::length_error( "materialSimpleBendingEnergyHessianMat::apply(): wrong size of weight vector!" );
    }

    VectorType edgeWeightsFirstDerivative( _topology.getNumEdges());
    setEdgeWeightsFirstDerivative( faceWeights, edgeWeightsFirstDerivative );
    VectorType edgeWeightsSecondDerivative( _topology.getNumEdges());
    setEdgeWeightsSecondDerivative( faceWeights, edgeWeightsSecondDerivative );

    // run over all edges and fill triplets
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // set up vertices and edges
      VecType Pi, Pj, Pk, Pl, temp;

      // get deformed geometry
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );

      // compute deformed dihedral angle
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );

      // get undeformed geometry
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );

      // compute volume, length of edge and theta difference
      // A_e = h_e * l_e = (|T_1| + |T_2|)/3 if T_1 and T_2 share edge e
      // Furthermore, |T| = 0.5 * |e_1 x e_2|, if e_1 and e_2 are edges of T
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      // CAUTION We omitted a factor 3 here!

      //compute gradient wrt to material weights, each edge weight is the mean of the two adjacent vertex weights
      //as we derive wrt. to vertex weights, we have outer derivative which we computed in function setDerivedEdgeWeights
      //the inner derivate is 0.5*vertexWeight

      RealType tmp = 0;

      //each edge counts 2 times for each index, see documentation
      tmp = edgeWeightsSecondDerivative[edgeIdx] * 0.5 * 0.5 * delTheta * delTheta * elengthSqr / vol;
      tripletList.emplace_back(  _rowOffset + fi, _colOffset + fi, tmp );
      tripletList.emplace_back(  _rowOffset + fj, _colOffset + fj, tmp );
      tripletList.emplace_back(  _rowOffset + fi, _colOffset + fj, tmp );
      tripletList.emplace_back(  _rowOffset + fj, _colOffset + fi, tmp );

    }
  }

  //bending energy scales with delta^3, so derivative scales with delta^2, here we compute the outer derivative with respect to edge indices
  void setEdgeWeightsFirstDerivative( const VectorType &faceWeights, VectorType &edgeWeightsFirstDerivative ) const {
    edgeWeightsFirstDerivative.resize( _topology.getNumEdges());
    edgeWeightsFirstDerivative.setZero();
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if (fi == -1 || fj == -1)
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeightsFirstDerivative[edgeIdx] = 3 * tmp * tmp;
    }

  }

  //bending energy scales with delta^3, so second derivative scales with delta, here we compute the outer derivative with respect to edge indices
  void setEdgeWeightsSecondDerivative( const VectorType faceWeights, VectorType &edgeWeightsSecondDerivative ) const {
    edgeWeightsSecondDerivative.resize( _topology.getNumEdges());
    edgeWeightsSecondDerivative.setZero();
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if (fi == -1 || fj == -1)
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeightsSecondDerivative[edgeIdx] = 6 * tmp;
    }

  }


};


//==========================================================================================================
//! \brief First derivative of simple bending energy w.r.t. the deformed configuration (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingGradientDef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;

public:
  materialSimpleBendingGradientDef( const MeshTopologySaver &topology,
                                    const VectorType &undefShell,
                                    const VectorType &faceWeights ) :
          _topology( topology ),
          _undefShell( undefShell ),
          _faceWeights( faceWeights ),
          _edgeWeights( setEdgeWeights()) {

  }

  void apply( const VectorType &defShell, VectorType &Dest ) const {

    if ( _undefShell.size() != defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << defShell.size() << std::endl;
      throw BasicException( "materialSimpleBendingGradientDef::apply(): sizes dont match!" );
    }

    if ( Dest.size() != defShell.size())
      Dest.resize( defShell.size());

    Dest.setZero();

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      //! first get undefomed quantities
      VecType Pi, Pj, Pk, Pl, temp;
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );


      // compute Ak + Al
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));

      //! now get the deformed values
      getXYZCoord<VectorType, VecType>( defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( defShell, Pl, pl );

      // compute weighted differnce of dihedral angles
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      delTheta *= -2. * _edgeWeights[edgeIdx] * elengthSqr / vol;

      // compute first derivatives of dihedral angle
      VecType thetak, thetal, thetai, thetaj;
      getThetaGradK( Pi, Pj, Pk, thetak );
      getThetaGradK( Pj, Pi, Pl, thetal );
      getThetaGradI( Pi, Pj, Pk, Pl, thetai );
      getThetaGradJ( Pi, Pj, Pk, Pl, thetaj );

      // assemble in global vector
      for ( int i = 0; i < 3; i++ ) {
        Dest[i * _topology.getNumVertices() + pi] += delTheta * thetai[i];
        Dest[i * _topology.getNumVertices() + pj] += delTheta * thetaj[i];
        Dest[i * _topology.getNumVertices() + pk] += delTheta * thetak[i];
        Dest[i * _topology.getNumVertices() + pl] += delTheta * thetal[i];
      }


#ifdef DEBUGMODE
                                                                                                                              if( hasNanEntries( Dest ) ){
				std::cerr << "NaN in simple bending gradient deformed in edge " << edgeIdx << "! " << std::endl;
				if( hasNanEntries(defShell) ){
					std::cerr << "Argument has NaN entries! " << std::endl;
				}
				else{
					std::cerr << "delTheta = " << delTheta << std::endl;
					std::cerr << "elengthSqr = " << elengthSqr << std::endl;
					std::cerr << "vol = " << vol << std::endl;
				}
				throw BasicException("SimpleBendingGradientDef::apply(): NaN Error!");
			}
#endif

    }
  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights = VectorType::Zero( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }
};

//==========================================================================================================
//! \brief First derivative of simple bending energy w.r.t. the deformed configuration (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class testMaterialSimpleBendingGradientDef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;

public:
  testMaterialSimpleBendingGradientDef( const MeshTopologySaver &topology,
                                        const VectorType &undefShell,
                                        const VectorType &defShell ) :
          _topology( topology ),
          _undefShell( undefShell ),
          _defShell( defShell ) {

  }

  void apply( const VectorType &faceWeights, VectorType &Dest ) const {

    if ( _undefShell.size() != _defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << _defShell.size() << std::endl;
      throw std::length_error( "testMaterialSimpleBendingGradientDef::apply(): sizes dont match!" );
    }

    if ( faceWeights.size() != _topology.getNumFaces())
      throw std::length_error( "testMaterialSimpleBendingGradientDef::apply(): wrong size of weight vector!" );

    if ( Dest.size() != _defShell.size())
      Dest.resize( _defShell.size());

    VectorType edgeWeights( _topology.getNumEdges());
    setEdgeWeights( faceWeights, edgeWeights );

    Dest.setZero();

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      //! first get undefomed quantities
      VecType Pi, Pj, Pk, Pl, temp;
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );


      // compute Ak + Al
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));

      //! now get the deformed values
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );

      // compute weighted differnce of dihedral angles
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      delTheta *= -2. * edgeWeights[edgeIdx] * elengthSqr / vol;

      // compute first derivatives of dihedral angle
      VecType thetak, thetal, thetai, thetaj;
      getThetaGradK( Pi, Pj, Pk, thetak );
      getThetaGradK( Pj, Pi, Pl, thetal );
      getThetaGradI( Pi, Pj, Pk, Pl, thetai );
      getThetaGradJ( Pi, Pj, Pk, Pl, thetaj );

      // assemble in global vector
      for ( int i = 0; i < 3; i++ ) {
        Dest[i * _topology.getNumVertices() + pi] += delTheta * thetai[i];
        Dest[i * _topology.getNumVertices() + pj] += delTheta * thetaj[i];
        Dest[i * _topology.getNumVertices() + pk] += delTheta * thetak[i];
        Dest[i * _topology.getNumVertices() + pl] += delTheta * thetal[i];
      }


#ifdef DEBUGMODE
                                                                                                                              if( hasNanEntries( Dest ) ){
				std::cerr << "NaN in simple bending gradient deformed in edge " << edgeIdx << "! " << std::endl;
				if( hasNanEntries(_defShell) ){
					std::cerr << "Argument has NaN entries! " << std::endl;
				}
				else{
					std::cerr << "delTheta = " << delTheta << std::endl;
					std::cerr << "elengthSqr = " << elengthSqr << std::endl;
					std::cerr << "vol = " << vol << std::endl;
				}
				throw BasicException("SimpleBendingGradientDef::apply(): NaN Error!");
			}

			Dest = Dest.transpose();
#endif

    }

  }

  //bending energy scales with delta^3
  void setEdgeWeights( const VectorType &faceWeights, VectorType &edgeWeights ) const {
    edgeWeights.resize(  _topology.getNumEdges());
    edgeWeights.setZero();
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }
  }
};


//! \brief Second derivative of simple bending energy w.r.t. the deformed configuration (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingHessianDef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;
  mutable int _rowOffset, _colOffset;

public:
  materialSimpleBendingHessianDef( const MeshTopologySaver &topology,
                                   const VectorType &undefShell,
                                   const VectorType &faceWeights,
                                   int rowOffset = 0,
                                   int colOffset = 0 ) :
          _topology( topology ),
          _undefShell( undefShell ),
          _faceWeights( faceWeights ),
          _edgeWeights( setEdgeWeights()),
          _rowOffset( rowOffset ),
          _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &defShell, MatrixType &Dest ) const {
    assembleHessian( defShell, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &defShell, MatrixType &Hessian ) const {
    int dofs = 3 * _topology.getNumVertices();
    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != dofs ))
      Hessian.resize( dofs, dofs );
    Hessian.setZero();

    // set up triplet list
    TripletListType tripletList;
    // per edge we have 4 active vertices, i.e. 16 combinations each producing a 3x3-matrix
    tripletList.reserve( 16 * 9 * _topology.getNumEdges());
    // fill matrix from triplets
    pushTriplets( defShell, tripletList );
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  // fill triplets
  void pushTriplets( const VectorType &defShell, TripletListType &tripletList, RealType factor = 1. ) const {

    if ( _undefShell.size() != defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << defShell.size() << std::endl;
      throw BasicException( "materialSimpleBendingHessianDef::pushTriplets(): sizes dont match!" );
    }


    // run over all edges and fill triplets
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      //! first get undefomed quantities
      VecType Pi, Pj, Pk, Pl, temp;
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );
      RealType delThetaDouble = getDihedralAngle( Pi, Pj, Pk, Pl );

      // compute Ak + Al
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr = VecType( Pj - Pi ).normSqr();

      // get deformed vertex positions
      getXYZCoord<VectorType, VecType>( defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( defShell, Pl, pl );

      // compute difference in dihedral angles
      delThetaDouble -= getDihedralAngle( Pi, Pj, Pk, Pl );
      delThetaDouble *= -2. * elengthSqr / vol;
      RealType localFactor = 2. * elengthSqr / vol;

      // compute first derivatives of dihedral angle
      VecType thetak, thetal, thetai, thetaj;
      getThetaGradK( Pi, Pj, Pk, thetak );
      getThetaGradK( Pj, Pi, Pl, thetal );
      getThetaGradI( Pi, Pj, Pk, Pl, thetai );
      getThetaGradJ( Pi, Pj, Pk, Pl, thetaj );

      // now compute second derivatives of dihedral angle
      MatType tensorProduct, H, aux;

      //kk
      getHessThetaKK( Pi, Pj, Pk, aux );
      tensorProduct.makeTensorProduct( thetak, thetak );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pk, pk, H, factor * _edgeWeights[edgeIdx] );

      //ik & ki (Hki = Hik)
      getHessThetaIK( Pi, Pj, Pk, aux );
      tensorProduct.makeTensorProduct( thetai, thetak );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pi, pk, H, factor * _edgeWeights[edgeIdx] );

      //jk & kj (Hkj = Hjk)
      getHessThetaJK( Pi, Pj, Pk, aux );
      tensorProduct.makeTensorProduct( thetaj, thetak );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pj, pk, H, factor * _edgeWeights[edgeIdx] );

      //ll
      getHessThetaKK( Pj, Pi, Pl, aux );
      tensorProduct.makeTensorProduct( thetal, thetal );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pl, pl, H, factor * _edgeWeights[edgeIdx] );

      //il & li (Hli = Hil)
      getHessThetaJK( Pj, Pi, Pl, aux );
      tensorProduct.makeTensorProduct( thetai, thetal );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pi, pl, H, factor * _edgeWeights[edgeIdx] );

      //jl & lj (Hlj = Hjl)
      getHessThetaIK( Pj, Pi, Pl, aux );
      tensorProduct.makeTensorProduct( thetaj, thetal );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pj, pl, H, factor * _edgeWeights[edgeIdx] );

      //kl/lk: Hkl = 0 and Hlk = 0
      tensorProduct.makeTensorProduct( thetak, thetal );
      tensorProduct *= localFactor;
      localToGlobal( tripletList, pk, pl, tensorProduct, factor * _edgeWeights[edgeIdx] );

      //ii
      getHessThetaII( Pi, Pj, Pk, Pl, aux );
      tensorProduct.makeTensorProduct( thetai, thetai );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pi, pi, H, factor * _edgeWeights[edgeIdx] );

      //jj
      getHessThetaII( Pj, Pi, Pl, Pk, aux );
      tensorProduct.makeTensorProduct( thetaj, thetaj );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pj, pj, H, factor * _edgeWeights[edgeIdx] );

      //ij & ji (Hij = Hji)
      getHessThetaJI( Pi, Pj, Pk, Pl, H );
      H *= delThetaDouble;
      tensorProduct.makeTensorProduct( thetai, thetaj );
      H.addMultiple( tensorProduct, localFactor );
      localToGlobal( tripletList, pi, pj, H, factor * _edgeWeights[edgeIdx] );

    }
  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights = VectorType::Zero( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }

protected:
  void
  localToGlobal( TripletListType &tripletList, int k, int l, const MatType &localMatrix, const RealType weight ) const {
    int numV = _topology.getNumVertices();
    for ( int i = 0; i < 3; i++ )
      for ( int j = 0; j < 3; j++ )
        tripletList.emplace_back( _rowOffset + i * numV + k, _colOffset + j * numV + l, weight * localMatrix( i, j ));

    if ( k != l ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.emplace_back( _rowOffset + i * numV + l, _colOffset + j * numV + k, weight * localMatrix( j, i ));
    }
  }


};

//==========================================================================================================
//! \brief First derivative of simple bending energy w.r.t. the undeformed configuration (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingGradientUndef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_defShell;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;

public:
  materialSimpleBendingGradientUndef( const MeshTopologySaver &topology,
                                      const VectorType &defShell,
                                      const VectorType &faceWeights ) :
          _topology( topology ),
          _defShell( defShell ),
          _faceWeights( faceWeights ),
          _edgeWeights( setEdgeWeights()) {

  }

  void apply( const VectorType &undefShell, VectorType &Dest ) const {

    if ( undefShell.size() != _defShell.size()) {
      std::cerr << "size of undef = " << undefShell.size() << " vs. size of def = " << _defShell.size() << std::endl;
      throw std::length_error( "materialSimpleBendingGradientUndef::apply(): sizes dont match!" );
    }

    if ( Dest.size() != undefShell.size())
      Dest.resize( undefShell.size());

    Dest.setZero();

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // first get defomed quantities
      VecType Pi, Pj, Pk, Pl, temp;
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );

      //!now get the undeformed values
      getXYZCoord<VectorType, VecType>( undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( undefShell, Pl, pl );

      // compute Ak + Al, |e|^2 and diff. o dihedral angles
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));
      // note signe here!
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );

      // derivatives
      VecType gradk, gradl, gradi, gradj, gradTheta, gradArea;
      RealType factorGradTheta = -2. * delTheta * elengthSqr / vol;
      RealType factorGradArea = -1. * delTheta * delTheta * elengthSqr / ( vol * vol );
      RealType factorGradEdgeLengthSqr = 2. * delTheta * delTheta / vol;

      // d_k
      getThetaGradK( Pi, Pj, Pk, gradTheta );
      getAreaGradK( Pi, Pj, Pk, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, gradk );

      // d_l
      getThetaGradK( Pj, Pi, Pl, gradTheta );
      getAreaGradK( Pj, Pi, Pl, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, gradl );

      // d_i
      getThetaGradI( Pi, Pj, Pk, Pl, gradTheta );
      getAreaGradK( Pj, Pk, Pi, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, gradi );
      getAreaGradK( Pl, Pj, Pi, gradArea );
      gradi.addMultiple( gradArea, factorGradArea );
      gradi.addMultiple( Pi - Pj, factorGradEdgeLengthSqr );

      // d_j
      getThetaGradJ( Pi, Pj, Pk, Pl, gradTheta );
      getAreaGradK( Pk, Pi, Pj, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, gradj );
      getAreaGradK( Pi, Pl, Pj, gradArea );
      gradj.addMultiple( gradArea, factorGradArea );
      gradj.addMultiple( Pj - Pi, factorGradEdgeLengthSqr );

      // assemble in global vector
      for ( int i = 0; i < 3; i++ ) {
        Dest[i * _topology.getNumVertices() + pi] += _edgeWeights[edgeIdx] * gradi[i];
        Dest[i * _topology.getNumVertices() + pj] += _edgeWeights[edgeIdx] * gradj[i];
        Dest[i * _topology.getNumVertices() + pk] += _edgeWeights[edgeIdx] * gradk[i];
        Dest[i * _topology.getNumVertices() + pl] += _edgeWeights[edgeIdx] * gradl[i];
      }

    }

  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights = VectorType::Zero( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }


};

//! \brief Second derivative of simple bending energy w.r.t. the undeformed configuration (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingHessianUndef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_defShell;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;
  mutable int _rowOffset, _colOffset;

public:
  materialSimpleBendingHessianUndef( const MeshTopologySaver &topology,
                                     const VectorType &defShell,
                                     const VectorType &faceWeights,
                                     int rowOffset = 0,
                                     int colOffset = 0 ) :
          _topology( topology ),
          _defShell( defShell ),
          _faceWeights( faceWeights ),
          _edgeWeights( setEdgeWeights()),
          _rowOffset( rowOffset ),
          _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &undefShell, MatrixType &Dest ) const {

    int dofs = 3 * _topology.getNumVertices();
    if (( Dest.rows() != dofs ) || ( Dest.cols() != dofs ))
      Dest.resize( dofs, dofs );
    Dest.setZero();
    assembleHessian( undefShell, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &undefShell, MatrixType &Hessian ) const {

    // set up triplet list
    TripletListType tripletList;
    // per edge we have 4 active vertices, i.e. 16 combinations each producing a 3x3-matrix
    tripletList.reserve( 16 * 9 * _topology.getNumEdges());

    // fill matrix from triplets
    pushTriplets( undefShell, tripletList );
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &undefShell, TripletListType &tripletList, RealType factor = 1. ) const {

    if ( undefShell.size() != _defShell.size()) {
      std::cerr << "size of undef = " << undefShell.size() << " vs. size of def = " << _defShell.size() << std::endl;
      throw BasicException( "materialSimpleBendingHessianUndef::pushTriplets(): sizes dont match!" );
    }

    // fill triplets
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( this->_topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // set up vertices and edges
      VecType Pi, Pj, Pk, Pl, temp;

      // first get defomed quantities
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );


      // get undeformed vertex positions
      getXYZCoord<VectorType, VecType>( undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( undefShell, Pl, pl );

      // compute Ak + Al, |e|^2 and dihedral angles
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr = VecType( Pj - Pi ).normSqr();
      // note the sign!
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      delTheta *= -1.;

      // compute first derivatives of dihedral angle
      VecType thetak, thetal, thetai, thetaj;
      getThetaGradK( Pi, Pj, Pk, thetak );
      getThetaGradK( Pj, Pi, Pl, thetal );
      getThetaGradI( Pi, Pj, Pk, Pl, thetai );
      getThetaGradJ( Pi, Pj, Pk, Pl, thetaj );

      // compute first derivatives of area
      VecType areak, areal, areai, areaj;
      getAreaGradK( Pi, Pj, Pk, areak );
      getAreaGradK( Pj, Pi, Pl, areal );
      getAreaGradK( Pj, Pk, Pi, areai );
      getAreaGradK( Pl, Pj, Pi, temp );
      areai += temp;
      getAreaGradK( Pk, Pi, Pj, areaj );
      getAreaGradK( Pi, Pl, Pj, temp );
      areaj += temp;

      // now compute second derivatives
      MatType H, auxMat;
      VecType auxVec, e( Pj - Pi );

      //*k
      getWeightedVectorSum<RealType>( elengthSqr, thetak, -1. * delTheta * elengthSqr / vol, areak, auxVec );
      getWeightedVectorSum<RealType>( 2., thetak, -1. * delTheta / vol, areak, temp );

      //kk
      H.makeTensorProduct( thetak, auxVec );
      getHessThetaKK( Pi, Pj, Pk, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxMat.makeTensorProduct( areak, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaKK( Pi, Pj, Pk, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      H *= 2. / vol;
      localToGlobal( tripletList, pk, pk, H, factor * _edgeWeights[edgeIdx] );

      //lk
      H.makeTensorProduct( thetal, auxVec );
      auxMat.makeTensorProduct( areal, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      H *= 2. / vol;
      localToGlobal( tripletList, pl, pk, H, factor * _edgeWeights[edgeIdx] );

      //ik
      H.makeTensorProduct( thetai, auxVec );
      getHessThetaIK( Pi, Pj, Pk, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxMat.makeTensorProduct( areai, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaIK( Pi, Pj, Pk, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, -1. * delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pi, pk, H, factor * _edgeWeights[edgeIdx] );

      //jk
      H.makeTensorProduct( thetaj, auxVec );
      getHessThetaJK( Pi, Pj, Pk, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxMat.makeTensorProduct( areaj, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaIK( Pj, Pi, Pk, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pj, pk, H, factor * _edgeWeights[edgeIdx] );

      //*l
      getWeightedVectorSum<RealType>( elengthSqr, thetal, -1. * delTheta * elengthSqr / vol, areal, auxVec );
      getWeightedVectorSum<RealType>( 2., thetal, -1. * delTheta / vol, areal, temp );

      //ll
      H.makeTensorProduct( thetal, auxVec );
      getHessThetaKK( Pj, Pi, Pl, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxMat.makeTensorProduct( areal, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaKK( Pj, Pi, Pl, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      H *= 2. / vol;
      localToGlobal( tripletList, pl, pl, H, factor * _edgeWeights[edgeIdx] );

      //il
      H.makeTensorProduct( thetai, auxVec );
      getHessThetaJK( Pj, Pi, Pl, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxMat.makeTensorProduct( areai, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaIK( Pi, Pj, Pl, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, -1. * delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pi, pl, H, factor * _edgeWeights[edgeIdx] );

      //jl
      H.makeTensorProduct( thetaj, auxVec );
      getHessThetaIK( Pj, Pi, Pl, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxMat.makeTensorProduct( areaj, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaIK( Pj, Pi, Pl, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pj, pl, H, factor * _edgeWeights[edgeIdx] );

      //*j
      getWeightedVectorSum<RealType>( elengthSqr, thetaj, -1. * delTheta * elengthSqr / vol, areaj, auxVec );
      auxVec.addMultiple( e, 2. * delTheta );// caution with factor 2!!!!!!!
      getWeightedVectorSum<RealType>( 2., thetaj, -1. * delTheta / vol, areaj, temp );

      //jj
      H.makeTensorProduct( thetaj, auxVec );
      getHessThetaII( Pj, Pi, Pl, Pk, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxVec.addMultiple( e, -1. * delTheta );
      auxMat.makeTensorProduct( areaj, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaKK( Pk, Pi, Pj, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      getHessAreaKK( Pi, Pl, Pj, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, delTheta );
      H.addToDiagonal( delTheta * delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pj, pj, H, factor * _edgeWeights[edgeIdx] );

      //ij
      auxVec.addMultiple( e, delTheta );
      H.makeTensorProduct( thetai, auxVec );
      getHessThetaJI( Pi, Pj, Pk, Pl, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxVec.addMultiple( e, -1. * delTheta );
      auxMat.makeTensorProduct( areai, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaIK( Pi, Pk, Pj, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      getHessAreaIK( Pi, Pl, Pj, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, -1. * delTheta );
      H.addToDiagonal( -1. * delTheta * delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pi, pj, H, factor * _edgeWeights[edgeIdx] );

      //*i
      getWeightedVectorSum<RealType>( elengthSqr, thetai, -1. * delTheta * elengthSqr / vol, areai, auxVec );
      auxVec.addMultiple( e, -2. * delTheta ); // caution with factor 2!!!!!!!
      getWeightedVectorSum<RealType>( 2., thetai, -1. * delTheta / vol, areai, temp );

      //ii
      H.makeTensorProduct( thetai, auxVec );
      getHessThetaII( Pi, Pj, Pk, Pl, auxMat );
      H.addMultiple( auxMat, delTheta * elengthSqr );
      auxVec.addMultiple( e, 1. * delTheta );
      auxMat.makeTensorProduct( areai, auxVec );
      H.addMultiple( auxMat, -1. * delTheta / vol );
      getHessAreaKK( Pl, Pj, Pi, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      getHessAreaKK( Pj, Pk, Pi, auxMat );
      H.addMultiple( auxMat, -0.5 * delTheta * delTheta * elengthSqr / vol );
      auxMat.makeTensorProduct( e, temp );
      H.addMultiple( auxMat, -1. * delTheta );
      H.addToDiagonal( delTheta * delTheta );
      H *= 2. / vol;
      localToGlobal( tripletList, pi, pi, H, factor * _edgeWeights[edgeIdx] );
    }

  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights = VectorType::Zero( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }

protected:
  void
  localToGlobal( TripletListType &tripletList, int k, int l, const MatType &localMatrix, const RealType weight ) const {
    int numV = _topology.getNumVertices();
    for ( int i = 0; i < 3; i++ )
      for ( int j = 0; j < 3; j++ )
        tripletList.push_back(
                TripletType( _rowOffset + i * numV + k, _colOffset + j * numV + l, weight * localMatrix( i, j )));

    if ( k != l ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.push_back(
                  TripletType( _rowOffset + i * numV + l, _colOffset + j * numV + k, weight * localMatrix( j, i )));
    }
  }

};


//! \brief Mixed second derivative of simple bending energy (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingHessianMixed
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_inactiveGeometry;
  const bool _activeShellIsDeformed, _firstDerivWRTDef;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;
  mutable int _rowOffset, _colOffset;

public:
  materialSimpleBendingHessianMixed( const MeshTopologySaver &topology,
                                     const VectorType &InactiveGeometry,
                                     const bool ActiveShellIsDeformed,
                                     const bool FirstDerivWRTDef,
                                     const VectorType &faceWeights,
                                     int rowOffset = 0,
                                     int colOffset = 0 ) :
          _topology( topology ),
          _inactiveGeometry( InactiveGeometry ),
          _activeShellIsDeformed( ActiveShellIsDeformed ),
          _firstDerivWRTDef( FirstDerivWRTDef ),
          _faceWeights( faceWeights ),
          _edgeWeights( setEdgeWeights()),
          _rowOffset( rowOffset ),
          _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &ActiveGeometry, MatrixType &Dest ) const {
    int dofs = 3 * _topology.getNumVertices();
    if (( Dest.rows() != dofs ) || ( Dest.cols() != dofs ))
      Dest.resize( dofs, dofs );
    Dest.setZero();
    assembleHessian( ActiveGeometry, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &ActiveGeometry, MatrixType &Hessian ) const {

    // set up triplet list
    TripletListType tripletList;
    // per edge we have 4 active vertices, i.e. 16 combinations each producing a 3x3-matrix
    tripletList.reserve( 16 * 9 * _topology.getNumEdges());

    pushTriplets( ActiveGeometry, tripletList );

    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &ActiveGeometry, TripletListType &tripletList, RealType factor = 1. ) const {

    const VectorType *undefShellP = _activeShellIsDeformed ? &_inactiveGeometry : &ActiveGeometry;
    const VectorType *defShellP = _activeShellIsDeformed ? &ActiveGeometry : &_inactiveGeometry;

    // fill triplets
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( this->_topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      std::vector<int> idx;
      idx.push_back( pi );
      idx.push_back( pj );
      idx.push_back( pk );
      idx.push_back( pl );

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      // set up vertices and edges
      VecType Pi, Pj, Pk, Pl, gradArea, gradTheta, temp;
      std::vector<VecType> undefGrads( 4 ), defGrads( 4 );

      // first get deformed quantities
      getXYZCoord<VectorType, VecType>( *defShellP, Pi, pi );
      getXYZCoord<VectorType, VecType>( *defShellP, Pj, pj );
      getXYZCoord<VectorType, VecType>( *defShellP, Pk, pk );
      getXYZCoord<VectorType, VecType>( *defShellP, Pl, pl );
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );

      getThetaGradI( Pi, Pj, Pk, Pl, defGrads[0] );
      getThetaGradJ( Pi, Pj, Pk, Pl, defGrads[1] );
      getThetaGradK( Pi, Pj, Pk, defGrads[2] );
      getThetaGradK( Pj, Pi, Pl, defGrads[3] );


      // get undeformed vertex positions
      getXYZCoord<VectorType, VecType>( *undefShellP, Pi, pi );
      getXYZCoord<VectorType, VecType>( *undefShellP, Pj, pj );
      getXYZCoord<VectorType, VecType>( *undefShellP, Pk, pk );
      getXYZCoord<VectorType, VecType>( *undefShellP, Pl, pl );

      // compute Ak + Al, |e|^2 and diff. o dihedral angles
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));
      // note signe here!
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );

      // factors
      RealType factorGradTheta = -2. * elengthSqr / vol;
      RealType factorGradArea = -2. * delTheta * elengthSqr / ( vol * vol );
      RealType factorGradEdgeLengthSqr = 4. * delTheta / vol;

      // d_i
      getThetaGradI( Pi, Pj, Pk, Pl, gradTheta );
      getAreaGradK( Pj, Pk, Pi, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, undefGrads[0] );
      getAreaGradK( Pl, Pj, Pi, gradArea );
      undefGrads[0].addMultiple( gradArea, factorGradArea );
      undefGrads[0].addMultiple( Pi - Pj, factorGradEdgeLengthSqr );

      // d_j
      getThetaGradJ( Pi, Pj, Pk, Pl, gradTheta );
      getAreaGradK( Pk, Pi, Pj, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, undefGrads[1] );
      getAreaGradK( Pi, Pl, Pj, gradArea );
      undefGrads[1].addMultiple( gradArea, factorGradArea );
      undefGrads[1].addMultiple( Pj - Pi, factorGradEdgeLengthSqr );

      // d_k
      getThetaGradK( Pi, Pj, Pk, gradTheta );
      getAreaGradK( Pi, Pj, Pk, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, undefGrads[2] );

      // d_l
      getThetaGradK( Pj, Pi, Pl, gradTheta );
      getAreaGradK( Pj, Pi, Pl, gradArea );
      getWeightedVectorSum<RealType>( factorGradTheta, gradTheta, factorGradArea, gradArea, undefGrads[3] );

      // local to global
      for ( int m = 0; m < 4; m++ ) {
        for ( int n = 0; n < 4; n++ ) {
          MatType matrix;
          matrix.makeTensorProduct( defGrads[m], undefGrads[n] );
          localToGlobal( tripletList, idx[m], idx[n], matrix, factor * _edgeWeights[edgeIdx] );
        }
      }


    }

  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights = VectorType::Zero( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }

protected:
  void
  localToGlobal( TripletListType &tripletList, int k, int l, const MatType &localMatrix, const RealType weight ) const {
    int numV = _topology.getNumVertices();
    if ( _firstDerivWRTDef ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.push_back(
                  TripletType( _rowOffset + i * numV + k, _colOffset + j * numV + l, weight * localMatrix( i, j )));
    }
    else {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.push_back(
                  TripletType( _rowOffset + i * numV + l, _colOffset + j * numV + k, weight * localMatrix( j, i )));
    }
  }

};

//! \brief Mixed second derivative of simple bending energy wrt. to deformed geometry and weight vector (cf. class materialSimpleBendingEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialSimpleBendingHessianMixedDefMat
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  mutable int _rowOffset, _colOffset;

public:
  materialSimpleBendingHessianMixedDefMat( const MeshTopologySaver &topology,
                                           const VectorType &undefShell,
                                           const VectorType &defShell,
                                           int rowOffset = 0,
                                           int colOffset = 0 ) :
          _topology( topology ),
          _undefShell( undefShell ),
          _defShell( defShell ),
          _rowOffset( rowOffset ),
          _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &faceWeights, MatrixType &Dest ) const {

    if ( _undefShell.size() != _defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << _defShell.size() << std::endl;
      throw BasicException( "materialSimpleBendingHessianMixedDefMat::apply(): sizes dont match!" );
    }

    if ( faceWeights.size() != _topology.getNumFaces()) {
      throw BasicException( "materialSimpleBendingHessianMixedDefMat::apply(): wrong size of weight vector!" );
    }

    if (( Dest.rows() != 3 * _topology.getNumVertices()) || ( Dest.cols() != _topology.getNumFaces()))
      Dest.resize( 3 * _topology.getNumVertices(), _topology.getNumFaces());
    Dest.setZero();
    assembleHessian( faceWeights, Dest );

  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &faceWeights, MatrixType &Hessian ) const {

    // set up triplet list
    TripletListType tripletList;
    // per edge we have 4 active vertices, i.e. 16 combinations each producing a 3x3-matrix
    tripletList.reserve( 16 * 9 * _topology.getNumEdges());

    pushTriplets( faceWeights, tripletList );

    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &faceWeights, TripletListType &tripletList, RealType factor = 1. ) const {

    VectorType edgeWeightsFirstDerivative( _topology.getNumEdges());
    setEdgeWeightsFirstDerivative( faceWeights, edgeWeightsFirstDerivative );

    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      //! first get undefomed quantities
      VecType Pi, Pj, Pk, Pl, temp;
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );
      RealType delTheta = getDihedralAngle( Pi, Pj, Pk, Pl );


      // compute Ak + Al
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr( dotProduct( Pj - Pi, Pj - Pi ));

      //! now get the deformed values
      getXYZCoord<VectorType, VecType>( _defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _defShell, Pl, pl );

      // compute weighted differnce of dihedral angles
      delTheta -= getDihedralAngle( Pi, Pj, Pk, Pl );
      delTheta *= -2. * elengthSqr / vol;

      // compute first derivatives of dihedral angle
      VecType thetak, thetal, thetai, thetaj;
      getThetaGradK( Pi, Pj, Pk, thetak );
      getThetaGradK( Pj, Pi, Pl, thetal );
      getThetaGradI( Pi, Pj, Pk, Pl, thetai );
      getThetaGradJ( Pi, Pj, Pk, Pl, thetaj );

      // assemble in global vector
      for ( int i = 0; i < 3; i++ ) {

        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pi, _colOffset + fi,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetai[i] );
        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pi, _colOffset + fj,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetai[i] );

        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pj, _colOffset + fi,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetaj[i] );
        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pj, _colOffset + fj,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetaj[i] );

        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pk, _colOffset + fi,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetak[i] );
        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pk, _colOffset + fj,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetak[i] );

        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pl, _colOffset + fi,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetal[i] );
        tripletList.emplace_back( _rowOffset + i * _topology.getNumVertices() + pl, _colOffset + fj,
                                  factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] * delTheta * thetal[i] );

      }


#ifdef DEBUGMODE
      if( hasNanEntries( Dest ) ){
        std::cerr << "NaN in simple bending gradient deformed in edge " << edgeIdx << "! " << std::endl;
        if( hasNanEntries(_defShell) ){
          std::cerr << "Argument has NaN entries! " << std::endl;
        }
        else{
          std::cerr << "delTheta = " << delTheta << std::endl;
          std::cerr << "elengthSqr = " << elengthSqr << std::endl;
          std::cerr << "vol = " << vol << std::endl;
        }
        throw BasicException("SimpleBendingGradientDef::apply(): NaN Error!");
      }
#endif

    }


  }

  //bending energy scales with delta^3, so derivative scales with delta^2, here we compute the outer derivative with respect to edge indices
  void setEdgeWeightsFirstDerivative( const VectorType &faceWeights, VectorType &edgeWeightsFirstDerivative ) const {
    edgeWeightsFirstDerivative.resize( _topology.getNumEdges());
    edgeWeightsFirstDerivative.setZero();
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeightsFirstDerivative[edgeIdx] = 3 * tmp * tmp;
    }

  }


};


template<typename ConfiguratorType>
class materialSimpleBendingThirdDerivDefMat  { // : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType>

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_faceWeights;
  const VectorType _edgeWeights;
  mutable int _rowOffset, _colOffset;

public:
  materialSimpleBendingThirdDerivDefMat( const MeshTopologySaver &topology,
                                   const VectorType &undefShell,
                                   const VectorType &faceWeights,
                                   int rowOffset = 0,
                                   int colOffset = 0 ) :
          _topology( topology ),
          _undefShell( undefShell ),
          _faceWeights( faceWeights ),
          _edgeWeights( setEdgeWeights()),
          _rowOffset( rowOffset ),
          _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
//  void apply( const VectorType &defShell, MatrixType &Dest ) const {
//    assembleHessian( defShell, Dest );
//  }
//
//  // assmeble Hessian matrix via triplet list
//  void assembleHessian( const VectorType &defShell, MatrixType &Hessian ) const {
//    int dofs = 3 * _topology.getNumVertices();
//    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != dofs ))
//      Hessian.resize( dofs, dofs );
//    Hessian.setZero();
//
//    // set up triplet list
//    TripletListType tripletList;
//    // per edge we have 4 active vertices, i.e. 16 combinations each producing a 3x3-matrix
//    tripletList.reserve( 16 * 9 * _topology.getNumEdges());
//    // fill matrix from triplets
//    pushTriplets( defShell, tripletList );
//    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
//  }

  // fill triplets
  void pushTriplets( const VectorType &defShell, std::vector<TripletListType> &tripletList, RealType factor = 1. ) const {

    if ( _undefShell.size() != defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << defShell.size() << std::endl;
      throw std::length_error( "materialSimpleBendingHessianDef::pushTriplets(): sizes dont match!" );
    }

    if ( tripletList.size() != _rowOffset + 3 * _topology.getNumVertices())
      tripletList.resize( _rowOffset + 3 * _topology.getNumVertices());


    VectorType edgeWeightsFirstDerivative( _topology.getNumEdges());
    setEdgeWeightsFirstDerivative( _faceWeights, edgeWeightsFirstDerivative );


    // run over all edges and fill triplets
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {

      if ( !( _topology.isEdgeValid( edgeIdx )))
        continue;

      int pi( _topology.getAdjacentNodeOfEdge( edgeIdx, 0 )),
              pj( _topology.getAdjacentNodeOfEdge( edgeIdx, 1 )),
              pk( _topology.getOppositeNodeOfEdge( edgeIdx, 0 )),
              pl( _topology.getOppositeNodeOfEdge( edgeIdx, 1 ));

      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      // no bending at boundary edges
      if ( std::min( pl, pk ) < 0 )
        continue;

      //! first get undefomed quantities
      VecType Pi, Pj, Pk, Pl, temp;
      getXYZCoord<VectorType, VecType>( _undefShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( _undefShell, Pl, pl );
      RealType delThetaDouble = getDihedralAngle( Pi, Pj, Pk, Pl );

      // compute Ak + Al
      temp.makeCrossProduct( Pk - Pj, Pi - Pk );
      RealType vol = temp.norm() / 2.;
      temp.makeCrossProduct( Pl - Pi, Pj - Pl );
      vol += temp.norm() / 2.;
      RealType elengthSqr = VecType( Pj - Pi ).normSqr();

      // get deformed vertex positions
      getXYZCoord<VectorType, VecType>( defShell, Pi, pi );
      getXYZCoord<VectorType, VecType>( defShell, Pj, pj );
      getXYZCoord<VectorType, VecType>( defShell, Pk, pk );
      getXYZCoord<VectorType, VecType>( defShell, Pl, pl );

      // compute difference in dihedral angles
      delThetaDouble -= getDihedralAngle( Pi, Pj, Pk, Pl );
      delThetaDouble *= -2. * elengthSqr / vol;
      RealType localFactor = 2. * elengthSqr / vol;

      // compute first derivatives of dihedral angle
      VecType thetak, thetal, thetai, thetaj;
      getThetaGradK( Pi, Pj, Pk, thetak );
      getThetaGradK( Pj, Pi, Pl, thetal );
      getThetaGradI( Pi, Pj, Pk, Pl, thetai );
      getThetaGradJ( Pi, Pj, Pk, Pl, thetaj );

      // now compute second derivatives of dihedral angle
      MatType tensorProduct, H, aux;

      //kk
      getHessThetaKK( Pi, Pj, Pk, aux );
      tensorProduct.makeTensorProduct( thetak, thetak );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pk, pk, fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pk, pk, fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //ik & ki (Hki = Hik)
      getHessThetaIK( Pi, Pj, Pk, aux );
      tensorProduct.makeTensorProduct( thetai, thetak );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pi, pk,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pi, pk,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //jk & kj (Hkj = Hjk)
      getHessThetaJK( Pi, Pj, Pk, aux );
      tensorProduct.makeTensorProduct( thetaj, thetak );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pj, pk,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pj, pk,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //ll
      getHessThetaKK( Pj, Pi, Pl, aux );
      tensorProduct.makeTensorProduct( thetal, thetal );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pl, pl,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pl, pl,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //il & li (Hli = Hil)
      getHessThetaJK( Pj, Pi, Pl, aux );
      tensorProduct.makeTensorProduct( thetai, thetal );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pi, pl,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pi, pl,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //jl & lj (Hlj = Hjl)
      getHessThetaIK( Pj, Pi, Pl, aux );
      tensorProduct.makeTensorProduct( thetaj, thetal );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pj, pl,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pj, pl,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //kl/lk: Hkl = 0 and Hlk = 0
      tensorProduct.makeTensorProduct( thetak, thetal );
      tensorProduct *= localFactor;
      localToGlobal( tripletList, pk, pl,  fi, tensorProduct, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pk, pl,  fj, tensorProduct, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //ii
      getHessThetaII( Pi, Pj, Pk, Pl, aux );
      tensorProduct.makeTensorProduct( thetai, thetai );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pi, pi,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pi, pi,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //jj
      getHessThetaII( Pj, Pi, Pl, Pk, aux );
      tensorProduct.makeTensorProduct( thetaj, thetaj );
      getWeightedMatrixSum( localFactor, tensorProduct, delThetaDouble, aux, H );
      localToGlobal( tripletList, pj, pj,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pj, pj,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

      //ij & ji (Hij = Hji)
      getHessThetaJI( Pi, Pj, Pk, Pl, H );
      H *= delThetaDouble;
      tensorProduct.makeTensorProduct( thetai, thetaj );
      H.addMultiple( tensorProduct, localFactor );
      localToGlobal( tripletList, pi, pj,  fi, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );
      localToGlobal( tripletList, pi, pj,  fj, H, factor * 0.5 * edgeWeightsFirstDerivative[edgeIdx] );

    }
  }

  //bending energy scales with delta^3
  VectorType setEdgeWeights() {
    VectorType edgeWeights = VectorType::Zero( _topology.getNumEdges());
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( _faceWeights[fi] + _faceWeights[fj] );
      edgeWeights[edgeIdx] = tmp * tmp * tmp;
    }

    return edgeWeights;
  }

  //bending energy scales with delta^3, so derivative scales with delta^2, here we compute the outer derivative with respect to edge indices
  void setEdgeWeightsFirstDerivative( const VectorType &faceWeights, VectorType &edgeWeightsFirstDerivative ) const {
    edgeWeightsFirstDerivative.resize( _topology.getNumEdges());
    edgeWeightsFirstDerivative.setZero();
    RealType tmp = 0;
    for ( int edgeIdx = 0; edgeIdx < _topology.getNumEdges(); ++edgeIdx ) {
      int fi = _topology.getAdjacentTriangleOfEdge(edgeIdx, 0);
      int fj = _topology.getAdjacentTriangleOfEdge(edgeIdx, 1);;

      if ( fi == -1 || fj == -1 )
        continue;

      tmp = 0.5 * ( faceWeights[fi] + faceWeights[fj] );
      edgeWeightsFirstDerivative[edgeIdx] = 3 * tmp * tmp;
    }

  }

protected:
  void localToGlobal( std::vector<TripletListType> &tripletList, int k, int l, int h, const MatType &localMatrix,
                      const RealType weight ) const {
    int numV = _topology.getNumVertices();
    for ( int i = 0; i < 3; i++ )
      for ( int j = 0; j < 3; j++ )
        tripletList[_rowOffset + i * numV + k].emplace_back( _colOffset + j * numV + l, h, weight * localMatrix( i, j ));

    if ( k != l ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList[_rowOffset + i * numV + l].emplace_back( _colOffset + j * numV + k, h, weight * localMatrix( j, i ));
    }
  }


};

//=============================================================================
#endif // MATERIALSHELLDEFORMATIONS_HH defined
//=============================================================================
