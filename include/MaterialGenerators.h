/**
 * \file
 * \brief Generators for random material distributions
 */

#ifndef BILEVELSHAPEOPT_MATERIALGENERATORS_H
#define BILEVELSHAPEOPT_MATERIALGENERATORS_H

#include <random>

#include <goast/Core.h>

/**
 * \brief Generate material distribution by splitting it along two perpendicular planes both perpendicular to the x-y-plane
 * \author Sassen
 *
 * This generator splits vertices into ones with thick and ones with thin material along two perpendicular planes both
 * perpendicular to the x-y-plane. The two opposite V-shaped segments share the same thickness.
 * The angle between the first of these planes and the x-axis is the uniformly distribute between given min and max
 * value.
 */
class SplitGenerator {
public:
  using VectorType = typename DefaultConfigurator::VectorType;
  using RealType = typename DefaultConfigurator::RealType;
  using SampleType = VectorType;


  /**
   * \param thickness Base thickness of material
   * \param min minimal angle between first plane and x-axis
   * \param max maximal angle between first plane and x-axis
   * \param refGeometry nodal positions used for splitting up the vertices
   * \param splitOffset determines width of a buffer zone around the splitting lines in which vertices keep base thickness
   * \param materialFactor relative change of material thickness from base to thin/thick
   */
  SplitGenerator( RealType thickness, RealType min, RealType max, const VectorType &refGeometry, RealType splitOffset,
                  RealType materialFactor )
          : m_randomEngine{ std::random_device{}() }, m_distribution{ min, max }, m_splitOffset{ splitOffset },
            m_thickness{ thickness }, m_refGeometry{ refGeometry }, m_materialFactor{ materialFactor } {
  }

  VectorType operator()() const {
    RealType splitAngle = m_distribution( m_randomEngine );

//    std::cout << " .... splitAngle = " << splitAngle << std::endl;

    VectorType vertexWeights = VectorType::Constant( m_refGeometry.size() / 3, m_thickness );

    VectorType front_n1( 3 );
    front_n1 << std::cos( splitAngle ), std::sin( splitAngle ), 0;
    VectorType front_n2( 3 );
    front_n2 << std::sin( splitAngle ), -std::cos( splitAngle ), 0;

    for ( int vertexIdx = 0; vertexIdx < m_refGeometry.size() / 3; vertexIdx++ ) {
      VectorType p( 3 );
      getXYZCoord( m_refGeometry, p, vertexIdx );

      if ( front_n1.dot( p ) > m_splitOffset && front_n2.dot( p ) > m_splitOffset )
        vertexWeights[vertexIdx] += m_materialFactor * m_thickness;
      if ( front_n1.dot( p ) < -m_splitOffset && front_n2.dot( p ) < -m_splitOffset )
        vertexWeights[vertexIdx] += m_materialFactor * m_thickness;
      if ( front_n1.dot( p ) > m_splitOffset && front_n2.dot( p ) < -m_splitOffset )
        vertexWeights[vertexIdx] -= m_materialFactor * m_thickness;
      if ( front_n1.dot( p ) < -m_splitOffset && front_n2.dot( p ) > m_splitOffset )
        vertexWeights[vertexIdx] -= m_materialFactor * m_thickness;
    }

    return vertexWeights;
  }

private:
  const RealType m_thickness;
  const RealType m_splitOffset;
  const RealType m_materialFactor;
  const VectorType &m_refGeometry;
  mutable std::mt19937 m_randomEngine;
  mutable std::uniform_real_distribution<RealType> m_distribution;
};

class BeamGenerator {
public:
  using VectorType = typename DefaultConfigurator::VectorType;
  using RealType = typename DefaultConfigurator::RealType;
  using SampleType = VectorType;



  BeamGenerator( RealType thickness, RealType min, RealType max, const VectorType &refGeometry, RealType beamWidth,
                 RealType materialFactor )
          : m_randomEngine{ std::random_device{}() }, m_distribution{ min, max }, m_beamWidth{ beamWidth },
            m_thickness{ thickness }, m_refGeometry{ refGeometry }, m_materialFactor{ materialFactor } {
  }

  VectorType operator()() const {
    RealType splitAngle = m_distribution( m_randomEngine );

//    std::cout << " .... splitAngle = " << splitAngle << std::endl;

    VectorType vertexWeights = VectorType::Constant( m_refGeometry.size() / 3, m_thickness );

    VectorType front_n1( 3 );
    front_n1 << std::cos( splitAngle ), std::sin( splitAngle ), 0;
    VectorType front_n2( 3 );
    front_n2 << std::sin( splitAngle ), -std::cos( splitAngle ), 0;

    for ( int vertexIdx = 0; vertexIdx < m_refGeometry.size() / 3; vertexIdx++ ) {
      VectorType p( 3 );
      getXYZCoord( m_refGeometry, p, vertexIdx );

      RealType dist = std::min( std::abs( front_n1.dot( p )), std::abs( front_n2.dot( p )));
      RealType gaussFactor = m_materialFactor * ( dist <= m_beamWidth ? (dist >= m_beamWidth/3. ? P(dist-m_beamWidth/3., 2./3.*m_beamWidth) : 1.) : 0. );
      // std::exp( -dist * dist / ( 2 * m_beamWidth * m_beamWidth ))

      vertexWeights[vertexIdx] += gaussFactor * m_thickness;
    }

    return vertexWeights;
  }

protected:
  static RealType P( RealType x, RealType w ) {
    return ( w * w - x * x ) * ( w * w - x * x ) / ( w * w * w * w );
  };

private:
  const RealType m_thickness;
  const RealType m_beamWidth;
  const RealType m_materialFactor;
  const VectorType &m_refGeometry;
  mutable std::mt19937 m_randomEngine;
  mutable std::uniform_real_distribution<RealType> m_distribution;
};

class SingleBeamGenerator  {
public:
  using VectorType = typename DefaultConfigurator::VectorType;
  using RealType = typename DefaultConfigurator::RealType;
  using SampleType = VectorType;



  SingleBeamGenerator( RealType thickness, RealType min, RealType max, const VectorType &refGeometry, RealType beamWidth,
                 RealType materialFactor )
          : m_randomEngine{ std::random_device{}() }, m_distribution{ min, max }, m_beamWidth{ beamWidth },
            m_thickness{ thickness }, m_refGeometry{ refGeometry }, m_materialFactor{ materialFactor } {
  }

  VectorType operator()() const {
    RealType splitAngle = m_distribution( m_randomEngine );

    VectorType vertexWeights = VectorType::Constant( m_refGeometry.size() / 3, m_thickness );

    VectorType front_n1( 3 );
    front_n1 << std::cos( splitAngle ), std::sin( splitAngle ), 0;

    for ( int vertexIdx = 0; vertexIdx < m_refGeometry.size() / 3; vertexIdx++ ) {
      VectorType p( 3 );
      getXYZCoord( m_refGeometry, p, vertexIdx );

      RealType dist = std::abs( front_n1.dot( p ));
      RealType gaussFactor = m_materialFactor * ( dist <= m_beamWidth ? (dist >= m_beamWidth/3. ? P(dist-m_beamWidth/3., 2./3.*m_beamWidth) : 1.) : 0. );
      // std::exp( -dist * dist / ( 2 * m_beamWidth * m_beamWidth ))

      vertexWeights[vertexIdx] += gaussFactor * m_thickness;
    }

    return vertexWeights;
  }

protected:
  static RealType P( RealType x, RealType w ) {
    return ( w * w - x * x ) * ( w * w - x * x ) / ( w * w * w * w );
  };

private:
  const RealType m_thickness;
  const RealType m_beamWidth;
  const RealType m_materialFactor;
  const VectorType &m_refGeometry;
  mutable std::mt19937 m_randomEngine;
  mutable std::uniform_real_distribution<RealType> m_distribution;
};

class ConstantGenerator  {
public:
  using VectorType = typename DefaultConfigurator::VectorType;
  using RealType = typename DefaultConfigurator::RealType;
  using SampleType = VectorType;


  ConstantGenerator( const VectorType &VertexWeights ) : m_vertexWeights( VertexWeights ) {}

  VectorType operator()() const {
    return m_vertexWeights;
  }

private:
  const VectorType &m_vertexWeights;

};

#endif //BILEVELSHAPEOPT_MATERIALGENERATORS_H
