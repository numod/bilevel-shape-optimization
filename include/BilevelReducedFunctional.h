//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_BILEVELREDUCEDFUNCTIONAL_H
#define BILEVELSHAPEOPT_BILEVELREDUCEDFUNCTIONAL_H

#include <goast/Optimization/Objectives.h>

#include "ShapeOptimization/StateEquationInterface.h"
#include "Optimization/StochasticOperators.h"
#include "OptimalForceSolver.h"

template<typename ConfiguratorType, class DeformationType>
class BilevelReducedFunctional
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::FullMatrixType FullMatrixType;

  const BaseOp<VectorType, RealType> &m_J;
  StateEquationSolverBase<ConfiguratorType> &m_ElasticitySolver;
  OptimalForceSolver<ConfiguratorType, DeformationType> &m_ForceSolver;
  const FullMatrixType &m_forceBasis;

public:
  BilevelReducedFunctional( const BaseOp<VectorType, RealType> &J,
                            StateEquationSolverBase<ConfiguratorType> &ElasticitySolver,
                            OptimalForceSolver<ConfiguratorType, DeformationType> &ForceSolver,
                            const FullMatrixType &forceBasis )
          : m_J( J ), m_ElasticitySolver( ElasticitySolver ), m_ForceSolver( ForceSolver ), m_forceBasis( forceBasis ) {

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    VectorType ldForce = m_ForceSolver.solve( Arg );

    // TODO: This only works for Material as design variable!!
    VectorType DesignAndForce( m_forceBasis.rows() + Arg.size());
    DesignAndForce.head( m_forceBasis.rows()) = m_forceBasis * ldForce;
    DesignAndForce.tail( Arg.size()) = Arg;

    VectorType deformedGeometry = m_ElasticitySolver.solve( DesignAndForce );

    VectorType combinedVector( DesignAndForce.size() + deformedGeometry.size());
    combinedVector.head( DesignAndForce.size()) = DesignAndForce;
    combinedVector.tail( deformedGeometry.size()) = deformedGeometry;

    if ( m_ElasticitySolver.lastSolveSuccessful())
      Dest = m_J( combinedVector );
    else
      Dest = std::numeric_limits<RealType>::infinity();

  }

  int getTargetDimension() const {
    return 1;
  }
};

template<typename ConfiguratorType, class DeformationType>
class BilevelReducedGradient
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::FullMatrixType FullMatrixType;

  const BaseOp<VectorType, RealType> &m_J;
  const BaseOp<VectorType, VectorType> &m_DJ;
  StateEquationSolverBase<ConfiguratorType> &m_ElasticitySolver;
  OptimalForceSolver<ConfiguratorType, DeformationType> &m_ForceSolver;
  const FullMatrixType &m_forceBasis;
  const std::vector<int> &m_dirichletBoundary;
public:
  BilevelReducedGradient( const BaseOp<VectorType, RealType> &J,
                          const BaseOp<VectorType, VectorType> &DJ,
                          StateEquationSolverBase<ConfiguratorType> &ElasticitySolver,
                          OptimalForceSolver<ConfiguratorType, DeformationType> &ForceSolver,
                          const FullMatrixType &forceBasis,
                          const std::vector<int> &dirichletBoundary ) : m_J( J ),
                                                                        m_DJ( DJ ),
                                                                        m_ElasticitySolver( ElasticitySolver ),
                                                                        m_ForceSolver( ForceSolver ),
                                                                        m_dirichletBoundary( dirichletBoundary ),
                                                                        m_forceBasis( forceBasis ) {

  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();

    VectorType ldForce = m_ForceSolver.solve( Arg );

    // TODO: This only works for Material as design variable!!
    VectorType DesignAndForce( m_forceBasis.rows() + Arg.size());
    DesignAndForce.head( m_forceBasis.rows()) = m_forceBasis * ldForce;
    DesignAndForce.tail( Arg.size()) = Arg;

    VectorType deformedGeometry = m_ElasticitySolver.solve( DesignAndForce );

    VectorType combinedVector( DesignAndForce.size() + deformedGeometry.size());
    combinedVector.head( DesignAndForce.size()) = DesignAndForce;
    combinedVector.tail( deformedGeometry.size()) = deformedGeometry;

    if ( !m_ElasticitySolver.lastSolveSuccessful()) {
      Dest.setConstant( std::numeric_limits<RealType>::infinity());
    }
    else {
      // Gradient of J
      VectorType costGradient = m_DJ( combinedVector );

//      std::cout << std::scientific << std::endl;
//      std::cout << " .. pure cost derivative : " << costGradient.segment(m_forceBasis.rows(), Arg.size()).transpose() << std::endl;

      // Derivative of J w.r.t. \delta
      Dest = costGradient.segment(m_forceBasis.rows(), Arg.size());

      // Derivative of J w.r.t. deformed configuration \tilde y
      VectorType solGradient = costGradient.tail( deformedGeometry.size());
      VectorType adjointElastSolution = m_ElasticitySolver.solveAdjoint( DesignAndForce, solGradient );

      MatrixType mixedElastDerivative = m_ElasticitySolver.mixedDerivative( DesignAndForce );
      VectorType elastDerivative = mixedElastDerivative.transpose() * adjointElastSolution;

//      std::cout << " .. elastDerivative : " << elastDerivative.transpose() << std::endl;
//      std::cout << " .. elastDerivative : " << elastDerivative.tail( Arg.size()).transpose() << std::endl;

      Dest -= elastDerivative.tail( Arg.size());

      // Derivative of J w.r.t. force
      VectorType partialForceDerivative = m_forceBasis.transpose() * ( elastDerivative.head( m_forceBasis.rows()) -
                                                                       costGradient.head( m_forceBasis.rows()));

//      std::cout << " .. partialForceDerivative : " << partialForceDerivative.transpose() << std::endl;


      VectorType adjointForceSolution = m_ForceSolver.solveAdjoint( Arg, partialForceDerivative );
      MatrixType mixedForceDerivative = m_ForceSolver.mixedDerivative( Arg );

      std::cout << " .. adjointForceSolution.norm = " << adjointForceSolution.norm() << std::endl;
      std::cout << " .. mixedForceDerivative.norm = " << mixedForceDerivative.norm() << std::endl;

//      std::cout << " .. ForceDerivative : " << (mixedForceDerivative.transpose() * adjointForceSolution).transpose() << std::endl;

      Dest += mixedForceDerivative.transpose() * adjointForceSolution;
    }
  }

  int getTargetDimension() const {
    return 1;
  }
};




template<typename ConfiguratorType, class DeformationType>
class LinearBilevelReducedFunctional
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::FullMatrixType FullMatrixType;

  const BaseOp<VectorType, RealType> &m_J;
  StateEquationSolverBase<ConfiguratorType> &m_ElasticitySolver;
  LinearizedOptimalForceSolver<ConfiguratorType, DeformationType> &m_ForceSolver;
  const FullMatrixType &m_forceBasis;

public:
  LinearBilevelReducedFunctional( const BaseOp<VectorType, RealType> &J,
                            StateEquationSolverBase<ConfiguratorType> &ElasticitySolver,
                                  LinearizedOptimalForceSolver<ConfiguratorType, DeformationType> &ForceSolver,
                            const FullMatrixType &forceBasis )
          : m_J( J ), m_ElasticitySolver( ElasticitySolver ), m_ForceSolver( ForceSolver ), m_forceBasis( forceBasis ) {

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    VectorType ldForce = m_ForceSolver.solve( Arg );

    // TODO: This only works for Material as design variable!!
    VectorType DesignAndForce( m_forceBasis.rows() + Arg.size());
    DesignAndForce.head( m_forceBasis.rows()) = m_forceBasis * ldForce;
    DesignAndForce.tail( Arg.size()) = Arg;

    VectorType deformedGeometry = m_ElasticitySolver.solve( DesignAndForce );

    VectorType combinedVector( DesignAndForce.size() + deformedGeometry.size());
    combinedVector.head( DesignAndForce.size()) = DesignAndForce;
    combinedVector.tail( deformedGeometry.size()) = deformedGeometry;

    if ( m_ElasticitySolver.lastSolveSuccessful())
      Dest = m_J( combinedVector );
    else
      Dest = std::numeric_limits<RealType>::infinity();

  }

  int getTargetDimension() const {
    return 1;
  }
};

template<typename ConfiguratorType, class DeformationType>
class LinearBilevelReducedGradient
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::FullMatrixType FullMatrixType;

  const BaseOp<VectorType, RealType> &m_J;
  const BaseOp<VectorType, VectorType> &m_DJ;
  StateEquationSolverBase<ConfiguratorType> &m_ElasticitySolver;
  LinearizedOptimalForceSolver<ConfiguratorType, DeformationType> &m_ForceSolver;
  const FullMatrixType &m_forceBasis;
  const std::vector<int> &m_dirichletBoundary;
public:
  LinearBilevelReducedGradient( const BaseOp<VectorType, RealType> &J,
                          const BaseOp<VectorType, VectorType> &DJ,
                          StateEquationSolverBase<ConfiguratorType> &ElasticitySolver,
                                LinearizedOptimalForceSolver<ConfiguratorType, DeformationType> &ForceSolver,
                          const FullMatrixType &forceBasis,
                          const std::vector<int> &dirichletBoundary ) : m_J( J ),
                                                                        m_DJ( DJ ),
                                                                        m_ElasticitySolver( ElasticitySolver ),
                                                                        m_ForceSolver( ForceSolver ),
                                                                        m_dirichletBoundary( dirichletBoundary ),
                                                                        m_forceBasis( forceBasis ) {

  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();

    VectorType ldForce = m_ForceSolver.solve( Arg );

    // TODO: This only works for Material as design variable!!
    VectorType DesignAndForce( m_forceBasis.rows() + Arg.size());
    DesignAndForce.head( m_forceBasis.rows()) = m_forceBasis * ldForce;
    DesignAndForce.tail( Arg.size()) = Arg;

    VectorType deformedGeometry = m_ElasticitySolver.solve( DesignAndForce );

    VectorType combinedVector( DesignAndForce.size() + deformedGeometry.size());
    combinedVector.head( DesignAndForce.size()) = DesignAndForce;
    combinedVector.tail( deformedGeometry.size()) = deformedGeometry;

    if ( !m_ElasticitySolver.lastSolveSuccessful()) {
      Dest.setConstant( std::numeric_limits<RealType>::infinity());
    }
    else {
      // Gradient of J
      VectorType costGradient = m_DJ( combinedVector );

//      std::cout << std::scientific << std::endl;
//      std::cout << " .. pure cost derivative : " << costGradient.segment(m_forceBasis.rows(), Arg.size()).transpose() << std::endl;

      // Derivative of J w.r.t. \delta
      Dest = costGradient.segment(m_forceBasis.rows(), Arg.size());

      // Derivative of J w.r.t. deformed configuration \tilde y
      VectorType solGradient = costGradient.tail( deformedGeometry.size());
      VectorType adjointElastSolution = m_ElasticitySolver.solveAdjoint( DesignAndForce, solGradient );

      MatrixType mixedElastDerivative = m_ElasticitySolver.mixedDerivative( DesignAndForce );
      VectorType elastDerivative = mixedElastDerivative.transpose() * adjointElastSolution;

//      std::cout << " .. elastDerivative : " << elastDerivative.transpose() << std::endl;
//      std::cout << " .. elastDerivative : " << elastDerivative.tail( Arg.size()).transpose() << std::endl;

      Dest -= elastDerivative.tail( Arg.size());

      // Derivative of J w.r.t. force
      VectorType partialForceDerivative = m_forceBasis.transpose() * ( elastDerivative.head( m_forceBasis.rows()) -
                                                                       costGradient.head( m_forceBasis.rows()));

//      std::cout << " .. partialForceDerivative : " << partialForceDerivative.transpose() << std::endl;


      VectorType adjointForceSolution = m_ForceSolver.solveAdjoint( Arg, partialForceDerivative );
      MatrixType mixedForceDerivative = m_ForceSolver.mixedDerivative( Arg );

//      std::cout << " .. adjointForceSolution.norm = " << adjointForceSolution.norm() << std::endl;
//      std::cout << " .. mixedForceDerivative.norm = " << mixedForceDerivative.norm() << std::endl;

//      std::cout << " .. ForceDerivative : " << (mixedForceDerivative.transpose() * adjointForceSolution).transpose() << std::endl;

      VectorType bilevelDerivative =        mixedForceDerivative.transpose() * adjointForceSolution;

      Dest +=bilevelDerivative;

//      for ( int i = 0; i < Dest.size(); i++ ) {
//        std::cout << " ## i = " << i << ": "  << std::scientific << Dest[i] << " = "
//                  << costGradient.segment( m_forceBasis.rows(), Arg.size())[i] << " - "
//                  << elastDerivative.tail( Arg.size())[i] << " + "
//                  << bilevelDerivative[i] << std::endl;
//      }  trackingWeight: 1.


    }
  }

  void detailedApply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();

    VectorType ldForce = m_ForceSolver.solve( Arg );

    // TODO: This only works for Material as design variable!!
    VectorType DesignAndForce( m_forceBasis.rows() + Arg.size());
    DesignAndForce.head( m_forceBasis.rows()) = m_forceBasis * ldForce;
    DesignAndForce.tail( Arg.size()) = Arg;

    VectorType deformedGeometry = m_ElasticitySolver.solve( DesignAndForce );

    VectorType combinedVector( DesignAndForce.size() + deformedGeometry.size());
    combinedVector.head( DesignAndForce.size()) = DesignAndForce;
    combinedVector.tail( deformedGeometry.size()) = deformedGeometry;

    if ( !m_ElasticitySolver.lastSolveSuccessful()) {
      Dest.setConstant( std::numeric_limits<RealType>::infinity());
    }
    else {
      // Gradient of J
      VectorType costGradient = m_DJ( combinedVector );

//      std::cout << std::scientific << std::endl;
//      std::cout << " .. pure cost derivative : " << costGradient.segment(m_forceBasis.rows(), Arg.size()).transpose() << std::endl;

      // Derivative of J w.r.t. \delta
      Dest = costGradient.segment(m_forceBasis.rows(), Arg.size());

      // Derivative of J w.r.t. deformed configuration \tilde y
      VectorType solGradient = costGradient.tail( deformedGeometry.size());
      VectorType adjointElastSolution = m_ElasticitySolver.solveAdjoint( DesignAndForce, solGradient );

      MatrixType mixedElastDerivative = m_ElasticitySolver.mixedDerivative( DesignAndForce );
      VectorType elastDerivative = mixedElastDerivative.transpose() * adjointElastSolution;

//      std::cout << " .. elastDerivative : " << elastDerivative.transpose() << std::endl;
//      std::cout << " .. elastDerivative : " << elastDerivative.tail( Arg.size()).transpose() << std::endl;

      Dest -= elastDerivative.tail( Arg.size());

      // Derivative of J w.r.t. force
      VectorType partialForceDerivativeA = m_forceBasis.transpose() *  elastDerivative.head( m_forceBasis.rows());
      VectorType partialForceDerivativeB = -m_forceBasis.transpose() * costGradient.head( m_forceBasis.rows());

//      std::cout << " .. partialForceDerivative : " << partialForceDerivative.transpose() << std::endl;


      VectorType adjointForceSolutionA = m_ForceSolver.solveAdjoint( Arg, partialForceDerivativeA );
      VectorType adjointForceSolutionB = m_ForceSolver.solveAdjoint( Arg, partialForceDerivativeB );
      MatrixType mixedForceDerivative = m_ForceSolver.mixedDerivative( Arg );

//      std::cout << " .. adjointForceSolution.norm = " << adjointForceSolution.norm() << std::endl;
//      std::cout << " .. mixedForceDerivative.norm = " << mixedForceDerivative.norm() << std::endl;

//      std::cout << " .. ForceDerivative : " << (mixedForceDerivative.transpose() * adjointForceSolution).transpose() << std::endl;

      VectorType bilevelDerivativeA =        mixedForceDerivative.transpose() * adjointForceSolutionA;
      VectorType bilevelDerivativeB =        mixedForceDerivative.transpose() * adjointForceSolutionB;

      Dest +=bilevelDerivativeA + bilevelDerivativeB;

      for ( int i = 0; i < Dest.size(); i++ ) {
        std::cout << " ## i = " << i << ": "  << std::scientific << Dest[i] << " = "
                  << costGradient.segment( m_forceBasis.rows(), Arg.size())[i] << " - "
                  << elastDerivative.tail( Arg.size())[i] << " + "
                  << bilevelDerivativeA[i] << " + "
                  << bilevelDerivativeB[i] << std::endl;
      }


    }
  }

  int getTargetDimension() const {
    return 1;
  }
};

#endif //BILEVELSHAPEOPT_BILEVELREDUCEDFUNCTIONAL_H
