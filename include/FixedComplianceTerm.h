//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_FIXEDCOMPLIANCETERM_H
#define BILEVELSHAPEOPT_FIXEDCOMPLIANCETERM_H

#include <goast/Core/Configurators.h>
#include <goast/Core/Topology.h>
//#include "materialShellDeformations.h"
#include "Optimization/StochasticOperators.h"

template<typename ConfiguratorType, class DeformationType>
class FixedComplianceTerm
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const VectorType &m_refGeometry;
  DeformationType &m_W;
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_vertexMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:

  FixedComplianceTerm( const MeshTopologySaver &Topology,
                       const VectorType &refGeometry,
                       DeformationType &W,
                       const bool UndefIsControl = true,
                       const bool ForceIsControl = false,
                       const bool MaterialIsControl = false,
                       const bool UndefIsParameter = false,
                       const bool ForceIsParameter = false,
                       const bool MaterialIsParameter = false ) :
          m_W( W ), m_refGeometry( refGeometry ), m_numVertices( Topology.getNumVertices()),
          m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTerm: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTerm: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTerm: Material distribution can be either control or parameter, not both!" );

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ComplianceTerm: wrong dimension of argument! " + std::to_string( Arg.size()) + " vs. " +
                               std::to_string( m_numControls + 3 * m_numVertices ));

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );
    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;

    m_W.applyEnergy( undefGeometry, defGeometry, Dest );
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType, class DeformationType>
class FixedComplianceTermGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const VectorType &m_refGeometry;
  DeformationType &m_W;
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_vertexMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  FixedComplianceTermGradient( const MeshTopologySaver &topology,
                               const VectorType &refGeometry,
                               DeformationType &W,
                               const bool UndefIsControl = true,
                               const bool ForceIsControl = false,
                               const bool MaterialIsControl = false,
                               const bool UndefIsParameter = false,
                               const bool ForceIsParameter = false,
                               const bool MaterialIsParameter = false ) :
          m_W( W ), m_refGeometry( refGeometry ), m_numVertices( topology.getNumVertices()),
          m_numFaces( topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTermGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTermGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTermGradient: Material distribution can be either control or parameter, not both!" );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error(
              "ComplianceTermGradient: wrong dimension of argument! " + std::to_string( Arg.size()) + " vs. " +
              std::to_string( m_numControls + 3 * m_numVertices ));

    Dest.resize( Arg.size());
    Dest.setZero();

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );

    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;


    // Calculate undeformed Gradient, if necessary
    if ( m_UndefIsControl ) {
      VectorType energyGradient_undef( 3 * m_numVertices );
      m_W.applyUndefGradient( undefGeometry, defGeometry, energyGradient_undef );

      Dest.segment( m_beginUndefControl, 3 * m_numVertices ) = energyGradient_undef;
    }

    // Calculate deformed Gradient
    VectorType energyGradient_def( 3 * m_numVertices );
    m_W.applyDefGradient( undefGeometry, defGeometry, energyGradient_def );
    Dest.tail( 3 * m_numVertices ) = energyGradient_def;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};

#endif //BILEVELSHAPEOPT_FIXEDCOMPLIANCETERM_H
