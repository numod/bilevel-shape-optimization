//
// Created by josua on 27.10.20.
//

#ifndef BILEVELSHAPEOPT_PROXIMALMAPPINGS_H
#define BILEVELSHAPEOPT_PROXIMALMAPPINGS_H


template<typename RealType, typename VectorType>
VectorType projectionOnBall( RealType radius, RealType t, const VectorType &p ) {
  VectorType nP = p;

  if ( nP.norm() > radius )
    nP = ( radius / nP.norm()) * nP;

  return nP;
}

template<typename RealType, typename VectorType>
VectorType projectionOnBox( const VectorType &lowerBounds, const VectorType &upperBounds, RealType t,
                            const VectorType &p ) {
  VectorType nP = p;

  nP.array() = nP.array().max( lowerBounds.array());
  nP.array() = nP.array().min( upperBounds.array());

  return nP;
}

template<typename RealType, typename VectorType>
VectorType projectionOnBoxBall( RealType radius, const VectorType &lowerBounds, const VectorType &upperBounds,
                                RealType t, const VectorType &p ) {
  VectorType nP = projectionOnBox<RealType, VectorType>( lowerBounds, upperBounds, t, p );

  if ( nP.norm() > radius ) {
    RealType lambda_max = nP.norm() / ( 2. * radius );
    RealType lambda_min = 0;
    RealType lambda = ( lambda_max + lambda_min ) / 2.;

    while ( lambda_max - lambda_min > std::numeric_limits<RealType>::epsilon()) {
      nP = projectionOnBox<RealType, VectorType>( lowerBounds, upperBounds, t, p / ( 2 * lambda + 1 ));

      RealType phi = nP.squaredNorm() - radius * radius;
      if ( phi == 0. ) {
        break;
      }
      else if ( phi > 0. ) {
        lambda_min = lambda;
      }
      else if ( phi < 0. ) {
        lambda_max = lambda;
      }
      lambda = ( lambda_max + lambda_min ) / 2.;
    }
  }

  return nP;
}

#endif //BILEVELSHAPEOPT_PROXIMALMAPPINGS_H
