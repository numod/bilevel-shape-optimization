//
// Created by josua on 15.07.20.
//

#ifndef BILEVELSHAPEOPT_STOCHASTICGRADIENTDESCENT_H
#define BILEVELSHAPEOPT_STOCHASTICGRADIENTDESCENT_H

#include <goast/Optimization/optInterface.h>

#include "StochasticOperators.h"
#include "Utils.h"


template<typename ConfiguratorType>
class StochasticArmijoLineSearch { //  : public LineSearchBase<ConfiguratorType>
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  // Parameters
  mutable RealType m_startTau = 1.;
  RealType m_tauMin = 1e-12, m_tauMax = 4.;
  RealType m_beta = 0.5;
  RealType m_sigma = 0.1;

  // Functional
  const BaseOp<VectorType, RealType> &m_F;
  const BaseOp<VectorType, VectorType> &m_DF;

  // Fixed vars and bounds
  const std::vector<int> *_fixedVariables;

  // output
  bool m_quiet = true;


public:
  StochasticArmijoLineSearch( const BaseOp<VectorType, RealType> &F,
                                     const BaseOp<VectorType, VectorType> &DF,
                                     bool quiet = true )
          : m_F( F ), m_DF( DF ), _fixedVariables( nullptr ), m_quiet( quiet ) {}

  RealType getStepsize( const VectorType &currentPosition,
                        const VectorType &currentGrad,
                        const VectorType &descentDir,
                        RealType previousTau = -1.,
                        RealType currentEnergy = std::numeric_limits<RealType>::quiet_NaN()) const {
    RealType tau = previousTau > 0 ? std::min( std::max( previousTau, m_tauMin ), m_tauMax ) : m_startTau;
    if ( !m_quiet )
      std::cout << " .... currentEnergy = " << std::scientific << std::setprecision(4) << currentEnergy << std::endl;

    const RealType f = std::isnan( currentEnergy ) ? m_F( currentPosition ) : currentEnergy;
    if ( !m_quiet )
      std::cout << " .... f = " << f << std::endl;
    VectorType newPosition = currentPosition + tau * descentDir;
    VectorType offset = currentPosition - newPosition;
    RealType fNew = m_F( newPosition );

    if ( std::isnan( fNew ))
      fNew = std::numeric_limits<RealType>::infinity();

    RealType funcDecrease = fNew - f;
    RealType predDecrease = currentGrad.dot( offset );
    RealType offsetNorm = offset.squaredNorm() / ( 2 * tau );

//    RealType G = (fNew - f) / (tau * Df);

    //check
    int i = 0;
    if ( funcDecrease <= -m_sigma * predDecrease + offsetNorm && fNew <= f ) {
      if ( !m_quiet )
        std::cout << " .. Increasing stepsize. Start: " << std::scientific << std::setprecision(4) << tau << std::endl;
      if ( !m_quiet )
        std::cout << " .. Start F: " << std::scientific << std::setprecision(4) << f << std::endl;
      //time step too small
      RealType prev_tau = tau;
      while ( funcDecrease <= -m_sigma * predDecrease + offsetNorm && fNew <= f && tau < m_tauMax ) {
        i++;
        prev_tau = tau;
        tau = std::min( 2. * tau, m_tauMax );

        if ( !m_quiet )
          std::cout << " .... tau = " << std::scientific << std::setprecision(4) << tau << std::endl;
        if ( !m_quiet )
          std::cout << " .. New F: " << std::scientific << std::setprecision(4) << fNew << std::endl;

        newPosition = currentPosition + tau * descentDir;
        offset = currentPosition - newPosition;

        fNew = m_F( newPosition );

        if ( std::isnan( fNew ))
          fNew = std::numeric_limits<RealType>::infinity();

        funcDecrease = fNew - f;
        predDecrease = currentGrad.dot( offset );
        offsetNorm = offset.squaredNorm() / ( 2 * tau );
      }
      if ( funcDecrease > -m_sigma * predDecrease + offsetNorm || fNew > f )
        tau = prev_tau;
    }
//    else {
    if ( !m_quiet )
      std::cout << " .. Decreasing stepsize. Start: " << std::scientific << tau << std::endl;
    if ( !m_quiet )
      std::cout << " .. Start F: " << std::scientific << std::setprecision( 4 ) << f << std::endl;
    // time step too large
    while (( funcDecrease > -m_sigma * predDecrease + offsetNorm || fNew > f ) && tau > m_tauMin ) {
      i++;
      if ( tau > m_tauMin )
        tau *= 0.5;

      if ( !m_quiet )
        std::cout << " .... tau = " << std::scientific << std::setprecision( 4 ) << tau << std::endl;
      if ( !m_quiet )
        std::cout << " .. New F: " << std::scientific << std::setprecision( 4 ) << fNew << std::endl;

      newPosition = currentPosition + tau * descentDir;
      offset = currentPosition - newPosition;

      fNew = m_F( newPosition );

      if ( std::isnan( fNew ))
        fNew = std::numeric_limits<RealType>::infinity();

      funcDecrease = fNew - f;
      predDecrease = currentGrad.dot( offset );
      offsetNorm = offset.squaredNorm() / ( 2 * tau );
//        std::cout << " .... fNew = " << std::setprecision(6) << std::scientific << fNew << std::endl;
//        std::cout << " .... funcDecrease = " << std::scientific << funcDecrease << std::endl;
//        std::cout << " .... predDecrease = " << std::scientific << predDecrease << std::endl;
//        std::cout << " .... offsetNorm = " << std::scientific << offsetNorm << std::endl;
//        std::cout << " .... proxEffect = " << std::scientific << (newPosition -  (currentPosition + tau * descentDir)).norm() << std::endl;
//        std::cout << " .... norm = " << std::scientific << (currentPosition + tau * descentDir).norm() << std::endl;
//        std::cout << " .... norm = " << std::scientific <<  m_prox( tau, currentPosition + tau * descentDir).norm() << std::endl;
//        std::cout << " .... norm = " << std::scientific << newPosition.norm() << std::endl;
    }
//    }

    return tau > m_tauMin ? tau : 0.;
  }

  void setParameter( const std::string &name, RealType value ) {
    if ( name == "minimal_stepsize" )
      m_tauMin = value;
    else if ( name == "maximal_stepsize" )
      m_tauMax = value;
    else if ( name == "initial_stepsize" )
      m_startTau = value;
    else if ( name == "reduction_factor" )
      m_beta = value;
    else if ( name == "decrease_factor" )
      m_sigma = value;
    else
      throw std::runtime_error( "ProximalArmijoLineSearch::setParameter(): Unknown real parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, int value ) {
    if ( name == "quiet" )
      m_quiet = value;
    else
      throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown integer parameter '" + name + "'." );
  }
//
//  void setParameter( const std::string &name, std::string value ) {
//    throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown string parameter '" + name + "'." );
//  }
};

/**
 * \brief Stochastic gradient descent method for sampled functions
 * \tparam ConfiguratorType Container with data types
 * \author Sassen
 */
template<typename ConfiguratorType>
class StochasticGradientDescent : public OptimizationBase<ConfiguratorType> {

protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  SampledBaseOp<VectorType, RealType, VectorType> &m_E;
  SampledBaseOp<VectorType, VectorType, VectorType> &m_DE;

  // Stepsize
  RealType m_tau;
  // - ADAM
  RealType m_beta1, m_beta2, m_smooth;
  // Controller
  TIMESTEP_CONTROLLER m_stepsizeMethod;

  // Stopping criteria
  int m_maxIterations;
  RealType m_stopEpsilon;
  RealType m_maxStepsize = 1.;
  bool m_useGradientBasedStopping;

  // Fixed variables and bounds
  const std::vector<int> *m_fixedVariables;


  // Output
  QUIET_MODE m_quietMode;

public:
  StochasticGradientDescent( SampledBaseOp<VectorType, RealType, VectorType> &E,
                             SampledBaseOp<VectorType, VectorType, VectorType> &DE,
                             int MaxIterations = 1000,
                             RealType StopEpsilon = 1e-8,
                             QUIET_MODE quietMode = SUPERQUIET,
                             const RealType tau = 1e-2,
                             TIMESTEP_CONTROLLER stepsizeMethod = CONST_TIMESTEP_CONTROL,
                             const RealType beta1 = 0.9,
                             const RealType beta2 = 0.999,
                             const RealType smooth = 1e-8 )
          : m_E( E ), m_DE( DE ),
            m_maxIterations( MaxIterations ), m_stopEpsilon( StopEpsilon ),
            m_useGradientBasedStopping( true ), m_quietMode( quietMode ), m_fixedVariables( nullptr ), m_tau( tau ),
            m_beta1( beta1 ), m_beta2( beta2 ), m_smooth( smooth ), m_stepsizeMethod(stepsizeMethod) {}

  void setBoundaryMask( const std::vector<int> &Mask ) {
    setFixedVariables( Mask );
  }

  void setFixedVariables( const std::vector<int> &fixedVariables ) override {
    m_fixedVariables = &fixedVariables;
  }


  void solve( const VectorType &x_0, VectorType &x_k ) const {

    if ( m_maxIterations == 0 )
      return;

    x_k = x_0;

    // Sample functionals
    m_E.computeSamples();
    m_DE.setSamples( m_E.getSamples());

    RealType energy =  m_E( x_k );

    if ( m_quietMode == SHOW_ALL ) {
      std::cout << "========================================================================================="
                << std::endl;
      std::cout << "Start gradient descent with " << m_maxIterations << " iterations and eps = " << m_stopEpsilon << "."
                << std::endl;
      std::cout << "Initial energy: " << energy << std::endl;
      std::cout << "========================================================================================="
                << std::endl;
    }

    int iterations = 0;
    bool stoppingCriterion = true; // stoppingCriterion == true means the iteration will be continued
    RealType error;

    // initial gradient
    VectorType energyGradient = m_DE( x_k );
    if ( m_fixedVariables )
      applyMaskToVector( *m_fixedVariables, energyGradient );


    // 'memory' for momentum-based methods
    VectorType gradientMemory = energyGradient;
    VectorType scaledGradientMemory = gradientMemory / ( 1 - m_beta1 );
    VectorType squaredGradientMemory = energyGradient.array().square();
    VectorType scaledSquaredGradientMemory = squaredGradientMemory / ( 1 - m_beta2 );

    // values of previous iterations
    VectorType x_old = x_k;

//    StepsizeControl<ConfiguratorType> stepsizeControl( m_E, m_DE, m_stepsizeMethod, 0.1, 0.9, 1.0, 1e-12, 4. );
    StochasticArmijoLineSearch<ConfiguratorType> stepsizeControl( m_E, m_DE, !(m_quietMode == SHOW_ALL) );
    stepsizeControl.setParameter("maximal_stepsize", m_maxStepsize);

    RealType tau = m_tau;

    // iteration loop
    while ( stoppingCriterion ) {
      iterations++;

      x_old = x_k;




      if (m_stepsizeMethod != CONST_TIMESTEP_CONTROL) {
        VectorType descentDirection = energyGradient;
        descentDirection *= -1;
        tau = stepsizeControl.getStepsize( x_k, energyGradient, descentDirection, tau, energy );

        x_k += tau * descentDirection;
      }
      else {
        // --- ADAM ---
        // linear combination of old gradients and new gradient
        gradientMemory = m_beta1 * gradientMemory + ( 1. - m_beta1 ) * energyGradient;
        // linear combination of old gradients squared plus new gradient squared
        squaredGradientMemory =
                m_beta2 * squaredGradientMemory + ( 1. - m_beta2 ) * energyGradient.array().square().matrix();

        // Scaled memory
        scaledGradientMemory = gradientMemory / ( 1 - m_beta1 );
        scaledSquaredGradientMemory = squaredGradientMemory / ( 1 - m_beta2 );

        // Actual step + projection on admissible set
        x_k -= m_tau *
               ( scaledGradientMemory.array() / ( scaledSquaredGradientMemory.array().sqrt() + m_smooth )).matrix();
      }

      // Sample functionals
      m_E.computeSamples();
      m_DE.setSamples( m_E.getSamples());

      energy = m_E( x_k );

      // compute energy gradient
      m_DE.apply( x_k, energyGradient );

      // apply boundary conditions
      if ( m_fixedVariables )
        applyMaskToVector( *m_fixedVariables, energyGradient );

      // compute error
      //! \todo other stopping criteria?
      error = energyGradient.norm();

      if ( m_quietMode == SHOW_ALL ) {
        std::cout << "step = " << iterations << " , stepsize = " << std::scientific  << std::setprecision(6) << tau
                  << ", energy = " << energy << ", error = " << error << std::endl;
      }

      stoppingCriterion = ( error > m_stopEpsilon ) && ( iterations < m_maxIterations ) && (tau > 1.e-10);
    } // end iteration loop

    if ( m_quietMode != SUPERQUIET ) {
      std::cout << "========================================================================================="
                << std::endl;
      std::cout << "Finished gradient descent after " << iterations << " steps (max. steps = " << m_maxIterations
                << ", tol = " << m_stopEpsilon << ")." << std::endl;
//      std::cout << "Final stepsize = " << std::scientific << tau << ", energy = " << energyNew << ", error = " << error
//                << std::endl;
      std::cout << "========================================================================================="
                << std::endl;
    }

  }

  void setParameter( const std::string &name, RealType value ) override {
    if (name == "maximal_stepsize")
      m_maxStepsize = value;
  }

  void setParameter( const std::string &name, int value ) override {
    throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown integer parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, std::string value ) override {
    throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown string parameter '" + name + "'." );
  }


};

#endif //BILEVELSHAPEOPT_STOCHASTICGRADIENTDESCENT_H
