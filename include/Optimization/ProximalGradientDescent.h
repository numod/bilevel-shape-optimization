//
// Created by josua on 15.07.20.
//

#ifndef BILEVELSHAPEOPT_STOCHASTICPROXIMALGRADIENTDESCENT_H
#define BILEVELSHAPEOPT_STOCHASTICPROXIMALGRADIENTDESCENT_H

#include <chrono>


#include <goast/Optimization/optInterface.h>
#include <goast/Optimization/optParameters.h>

#include "StochasticOperators.h"
#include "Utils.h"

template<typename ConfiguratorType>
class ProximalArmijoLineSearch { //  : public LineSearchBase<ConfiguratorType>
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  // Parameters
  mutable RealType m_startTau = 1.;
  RealType m_tauMin = 1e-12, m_tauMax = 4.;
  RealType m_beta = 0.5;
  RealType m_sigma = 0.1;

  // Functional
  const BaseOp<VectorType, RealType> &m_F;
  const BaseOp<VectorType, VectorType> &m_DF;
  const std::function<VectorType( RealType, const VectorType & )> m_prox; // Proximal map of h

  // Fixed vars and bounds
  const std::vector<int> *_fixedVariables;

  // output
  bool m_quiet = true;


public:
  explicit ProximalArmijoLineSearch( const BaseOp<VectorType, RealType> &F,
                                     const BaseOp<VectorType, VectorType> &DF,
                                     const std::function<VectorType( RealType, const VectorType & )> &prox,
                                     bool quiet = true )
          : m_F( F ), m_DF( DF ), _fixedVariables( nullptr ), m_prox( prox ), m_quiet( quiet ) {}

  RealType getStepsize( const VectorType &currentPosition,
                        const VectorType &currentGrad,
                        const VectorType &descentDir,
                        RealType previousTau = -1.,
                        RealType currentEnergy = std::numeric_limits<RealType>::quiet_NaN()) const {
    RealType tau = previousTau > 0 ? std::min( std::max( previousTau, m_tauMin ), m_tauMax ) : m_startTau;
    if ( !m_quiet )
      std::cout << " .... currentEnergy = " << std::scientific << std::setprecision(4) << currentEnergy << std::endl;

      const RealType f = std::isnan( currentEnergy ) ? m_F( currentPosition ) : currentEnergy;
    if ( !m_quiet )
      std::cout << " .... f = " << f << std::endl;
    VectorType newPosition = m_prox( tau, currentPosition + tau * descentDir );
    VectorType offset = currentPosition - newPosition;
    RealType fNew = m_F( newPosition );

    if ( std::isnan( fNew ))
      fNew = std::numeric_limits<RealType>::infinity();

    RealType funcDecrease = fNew - f;
    RealType predDecrease = currentGrad.dot( offset );
    RealType offsetNorm = offset.squaredNorm() / ( 2 * tau );

//    RealType G = (fNew - f) / (tau * Df);

    //check
    int i = 0;
    if ( funcDecrease <= -m_sigma * predDecrease + offsetNorm && fNew <= f ) {
      if ( !m_quiet )
        std::cout << " .. Increasing stepsize. Start: " << std::scientific << std::setprecision(4) << tau << std::endl;
      if ( !m_quiet )
        std::cout << " .. Start F: " << std::scientific << std::setprecision(4) << f << std::endl;
      //time step too small
      RealType prev_tau = tau;
      while ( funcDecrease <= -m_sigma * predDecrease + offsetNorm && fNew <= f && tau < m_tauMax ) {
        i++;
        prev_tau = tau;
        tau = std::min( 2. * tau, m_tauMax );

        if ( !m_quiet )
          std::cout << " .... tau = " << std::scientific << std::setprecision(4) << tau << std::endl;
        if ( !m_quiet )
          std::cout << " .. New F: " << std::scientific << std::setprecision(4) << fNew << std::endl;

        newPosition = m_prox( tau, currentPosition + tau * descentDir );
        offset = currentPosition - newPosition;

        fNew = m_F( newPosition );

        if ( std::isnan( fNew ))
          fNew = std::numeric_limits<RealType>::infinity();

        funcDecrease = fNew - f;
        predDecrease = currentGrad.dot( offset );
        offsetNorm = offset.squaredNorm() / ( 2 * tau );
      }
      if ( funcDecrease > -m_sigma * predDecrease + offsetNorm || fNew > f )
        tau = prev_tau;
    }
//    else {
      if ( !m_quiet )
        std::cout << " .. Decreasing stepsize. Start: " << std::scientific << tau << std::endl;
      if ( !m_quiet )
        std::cout << " .. Start F: " << std::scientific << std::setprecision( 4 ) << f << std::endl;
      // time step too large
      while (( funcDecrease > -m_sigma * predDecrease + offsetNorm || fNew > f ) && tau > m_tauMin ) {
        i++;
        if ( tau > m_tauMin )
          tau *= 0.5;

        if ( !m_quiet )
          std::cout << " .... tau = " << std::scientific << std::setprecision( 4 ) << tau << std::endl;
        if ( !m_quiet )
          std::cout << " .. New F: " << std::scientific << std::setprecision( 4 ) << fNew << std::endl;

        newPosition = m_prox( tau, currentPosition + tau * descentDir );
        offset = currentPosition - newPosition;

        fNew = m_F( newPosition );

        if ( std::isnan( fNew ))
          fNew = std::numeric_limits<RealType>::infinity();

        funcDecrease = fNew - f;
        predDecrease = currentGrad.dot( offset );
        offsetNorm = offset.squaredNorm() / ( 2 * tau );
//        std::cout << " .... fNew = " << std::setprecision(6) << std::scientific << fNew << std::endl;
//        std::cout << " .... funcDecrease = " << std::scientific << funcDecrease << std::endl;
//        std::cout << " .... predDecrease = " << std::scientific << predDecrease << std::endl;
//        std::cout << " .... offsetNorm = " << std::scientific << offsetNorm << std::endl;
//        std::cout << " .... proxEffect = " << std::scientific << (newPosition -  (currentPosition + tau * descentDir)).norm() << std::endl;
//        std::cout << " .... norm = " << std::scientific << (currentPosition + tau * descentDir).norm() << std::endl;
//        std::cout << " .... norm = " << std::scientific <<  m_prox( tau, currentPosition + tau * descentDir).norm() << std::endl;
//        std::cout << " .... norm = " << std::scientific << newPosition.norm() << std::endl;
      }
//    }

    return tau > m_tauMin ? tau : 0.;
  }

  void setParameter( const std::string &name, RealType value ) {
    if ( name == "minimal_stepsize" )
      m_tauMin = value;
    else if ( name == "maximal_stepsize" )
      m_tauMax = value;
    else if ( name == "initial_stepsize" )
      m_startTau = value;
    else if ( name == "reduction_factor" )
      m_beta = value;
    else if ( name == "decrease_factor" )
      m_sigma = value;
    else
      throw std::runtime_error( "ProximalArmijoLineSearch::setParameter(): Unknown real parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, int value ) {
    if ( name == "quiet" )
      m_quiet = value;
    else
      throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown integer parameter '" + name + "'." );
  }
//
//  void setParameter( const std::string &name, std::string value ) {
//    throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown string parameter '" + name + "'." );
//  }
};
template<typename ConfiguratorType>
class ProximalSimpleLineSearch { //  : public LineSearchBase<ConfiguratorType>
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  // Parameters
  mutable RealType m_startTau = 1.;
  RealType m_tauMin = 1e-12, m_tauMax = 4.;
  RealType m_beta = 0.5;
  RealType m_sigma = 0.1;

  // Functional
  const BaseOp<VectorType, RealType> &m_F;
  const std::function<VectorType( RealType, const VectorType & )> m_prox; // Proximal map of h

  // Fixed vars and bounds

  // output
  bool m_quiet = true;


public:
  explicit ProximalSimpleLineSearch( const BaseOp<VectorType, RealType> &F,
                                     const std::function<VectorType( RealType, const VectorType & )> &prox,
                                     bool quiet = true )
          : m_F( F ), m_prox( prox ), m_quiet( quiet ) {}

  RealType getStepsize( const VectorType &currentPosition,
                        const VectorType &currentGrad,
                        const VectorType &descentDir,
                        RealType previousTau = -1.,
                        RealType currentEnergy = std::numeric_limits<RealType>::quiet_NaN()) const {
    RealType tau = previousTau > 0 ? std::min( std::max( previousTau, m_tauMin ), m_tauMax ) : m_startTau;

    const RealType f = std::isnan( currentEnergy ) ? m_F( currentPosition ) : currentEnergy;
    VectorType newPosition = m_prox( tau, currentPosition + tau * descentDir );
    VectorType offset = currentPosition - newPosition;
    RealType fNew = m_F( newPosition );

    if ( std::isnan( fNew ))
      fNew = std::numeric_limits<RealType>::infinity();

    if (std::isnan(fNew))
      fNew = std::numeric_limits<RealType>::infinity();

    if (fNew < f && tau < m_tauMax) {
      if ( !m_quiet )
        std::cout << " .. Increasing stepsize. Start: " << std::scientific << tau << std::endl;
      if ( !m_quiet )
        std::cout << " .. Start F: " << std::scientific << f << std::endl;
      while (tau < m_tauMax) {
        RealType newTau = tau * m_beta;

        if ( !m_quiet )
          std::cout << " .... tau = " << std::scientific << tau << std::endl;

        newPosition = m_prox( newTau, currentPosition + newTau * descentDir );
        RealType sfNew = m_F( newPosition );

        if ( std::isnan( fNew ))
          sfNew = std::numeric_limits<RealType>::infinity();

        if (sfNew < fNew) {
          tau = newTau;
          fNew = sfNew;
          if ( !m_quiet )
            std::cout << " .... tau = " << std::scientific << tau << std::endl;
          if ( !m_quiet )
            std::cout << " .. New F: " << std::scientific << fNew << std::endl;
        }
        else
          break;
      }
    }
    else {
      if ( !m_quiet )
        std::cout << " .. Decreasing stepsize. Start: " << std::scientific << tau << std::endl;
      if ( !m_quiet )
        std::cout << " .. Start F: " << std::scientific << f << std::endl;
      while (( fNew >= f ) && ( tau >= m_tauMin )) {
        tau = tau * m_beta;

        if ( !m_quiet )
          std::cout << " .... tau = " << std::scientific << tau << std::endl;


        newPosition = m_prox( tau, currentPosition + tau * descentDir );
        fNew = m_F( newPosition );

        if ( !m_quiet )
          std::cout << " .. New F: " << std::scientific << fNew << std::endl;

        if ( std::isnan( fNew ))
          fNew = std::numeric_limits<RealType>::infinity();
      }

    }
    return tau > m_tauMin ? tau : 0.;
  }

  void setParameter( const std::string &name, RealType value ) {
    if ( name == "minimal_stepsize" )
      m_tauMin = value;
    else if ( name == "maximal_stepsize" )
      m_tauMax = value;
    else if ( name == "initial_stepsize" )
      m_startTau = value;
    else if ( name == "reduction_factor" )
      m_beta = value;
    else
      throw std::runtime_error( "ProximalSimpleLineSearch::setParameter(): Unknown real parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, int value ) {
    if ( name == "quiet" )
      m_quiet = value;
    else
      throw std::runtime_error( "ProximalSimpleLineSearch::setParameter(): Unknown integer parameter '" + name + "'." );
  }
//
//  void setParameter( const std::string &name, std::string value ) {
//    throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown string parameter '" + name + "'." );
//  }
};

template<typename ConfiguratorType>
class ProximalGradientDescent : public OptimizationBase<ConfiguratorType> {

protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  // Optimization problem
  const BaseOp<VectorType, RealType> &m_E;
  const BaseOp<VectorType, VectorType> &m_DE;
  const std::function<VectorType( RealType, const VectorType & )> m_prox; // Proximal map of h

  // Stepsize
  RealType m_startTau = 1.;
  RealType m_tauMin = 1e-12, m_tauMax = 4.;
  TIMESTEP_CONTROLLER m_linesearchMethod = CONST_TIMESTEP_CONTROL;
  bool m_acceleration = false;

  // Stopping criteria
  int m_maxIterations;
  RealType m_stopEpsilon;
  bool m_useGradientBasedStopping;

  // Fixed variables and bounds
  const std::vector<int> *m_fixedVariables;

  // Output
  QUIET_MODE m_quietMode;

public:
  ProximalGradientDescent( const BaseOp<VectorType, RealType> &E,
                           const BaseOp<VectorType, VectorType> &DE,
                           const std::function<VectorType( RealType, const VectorType & )> &prox,
                           int MaxIterations = 1000,
                           RealType StopEpsilon = 1e-8,
                           QUIET_MODE quietMode = SUPERQUIET )
          : m_E( E ), m_DE( DE ), m_prox( prox ),
            m_maxIterations( MaxIterations ), m_stopEpsilon( StopEpsilon ),
            m_useGradientBasedStopping( false ), m_quietMode( quietMode ),
            m_fixedVariables( nullptr ) {}

  void setBoundaryMask( const std::vector<int> &Mask ) {
    setFixedVariables( Mask );
  }

  void setFixedVariables( const std::vector<int> &fixedVariables ) override {
    m_fixedVariables = &fixedVariables;
  }

  void solve( const VectorType &x_0, VectorType &x_k ) const {
    if ( m_maxIterations == 0 )
      return;

    x_k = x_0;




    int iterations = 0;
    bool stoppingCriterion = true; // stoppingCriterion == true means the iteration will be continued
    RealType error;

    // initial energy
    RealType energy = m_E( x_k );

    // initial gradient
    VectorType energyGradient = m_DE( x_k );
    if ( m_fixedVariables )
      applyMaskToVector( *m_fixedVariables, energyGradient );

    if ( m_quietMode == SHOW_ALL ) {
      std::cout << "========================================================================================="
                << std::endl;
      std::cout << "Start proximal gradient descent with " << m_maxIterations << " iterations and eps = "
                << m_stopEpsilon << "."
                << std::endl;
      std::cout << "Initial energy: " << energy << std::endl;
      std::cout << "========================================================================================="
                << std::endl;
    }

    // Stepsize control
    ProximalArmijoLineSearch<ConfiguratorType> stepsizeControl( m_E, m_DE, m_prox, m_quietMode != SHOW_ALL );
    stepsizeControl.setParameter( "minimal_stepsize", m_tauMin );
    stepsizeControl.setParameter( "maximal_stepsize", m_tauMax );
    stepsizeControl.setParameter( "initial_stepsize", m_startTau );
    ProximalSimpleLineSearch<ConfiguratorType> simpleStepsizeControl( m_E, m_prox, m_quietMode != SHOW_ALL );
    simpleStepsizeControl.setParameter( "minimal_stepsize", m_tauMin );
    simpleStepsizeControl.setParameter( "maximal_stepsize", m_tauMax );
    simpleStepsizeControl.setParameter( "initial_stepsize", m_startTau );
    RealType tau = m_startTau;

    // values of previous iterations
    VectorType x_old = x_k;
    VectorType x_old2 = x_k;

    // iteration loop
    while ( stoppingCriterion ) {
      iterations++;

      auto t_start = std::chrono::high_resolution_clock::now();

      x_old2 = x_old;
      x_old = x_k;

      // Acceleration
      if ( m_acceleration ) {
        x_k += ( iterations - 1. ) / ( iterations + 2. ) * ( x_old - x_old2 );
      }
      if (iterations > 1 ) {
        // compute energy and gradient
        m_E.apply( x_k, energy );
        if (( std::isnan( energy ) || std::isinf( energy )) && m_acceleration ) {
          x_k -= ( iterations - 1. ) / ( iterations + 2. ) * ( x_old - x_old2 );
          m_E.apply( x_k, energy );
        }

        m_DE.apply( x_k, energyGradient );
      }
      // apply boundary conditions
      if ( m_fixedVariables )
        applyMaskToVector( *m_fixedVariables, energyGradient );

      // Stepsize
      VectorType descentDirection = -energyGradient;
      // Actual step + projection on admissible set (= prox map)
      if ( m_linesearchMethod == ARMIJO )
        tau = stepsizeControl.getStepsize( x_k, energyGradient, descentDirection, tau, energy );
      else if ( m_linesearchMethod == SIMPLE_TIMESTEP_CONTROL )
        tau = simpleStepsizeControl.getStepsize( x_k, energyGradient, descentDirection, tau, energy );
      else
        tau = m_startTau;

      x_k = m_prox( tau, x_k + tau * descentDirection );

      // compute error
      //! \todo other stopping criteria?
      error = ( x_k - x_old ).norm();

      auto t_end = std::chrono::high_resolution_clock::now();

      if ( m_quietMode == SHOW_ALL ) {
        // Final energy
        m_E.apply( x_k, energy );
        t_end = std::chrono::high_resolution_clock::now();

        std::cout << " -- PGD -- Iter " << std::setw( 3 ) << iterations << ": " << std::scientific << std::setprecision( 6 )
                  << energy
                  << " || " << energyGradient.norm()
                  << " || " << error
                  << " ||===|| " << std::setw( 13 ) << tau
                  << std::setprecision( 2 ) << std::fixed
                  << " ||===|| " << std::setw( 6 )
                  << std::chrono::duration<RealType, std::milli>( t_end - t_start ).count()
                  << "ms"
                  << std::endl;
      }

      stoppingCriterion = ( error > m_stopEpsilon ) && ( iterations < m_maxIterations );

//      std::cout << " .. Iter " << iterations << ": " << x_k.transpose() << std::endl;
//      std::cout << " .. Iter " << iterations << ": " << x_k.norm() << std::endl;
    } // end iteration loop

    if ( m_quietMode == SHOW_ALL || m_quietMode == SHOW_TERMINATION_INFO ||
         ( m_quietMode == SHOW_ONLY_IF_FAILED && (error > m_stopEpsilon || tau == 0.) )) {
      std::cout << " -- PGD -- Final (" << std::setw( 2 ) << iterations << "): " << std::scientific
                << std::setprecision( 6 )
                << energy
                << " || " << energyGradient.norm()
                << " || " << error
                << " ||===|| " << std::setw( 13 ) << tau
                << std::endl;
    }

  }

  void setParameter( const std::string &name, RealType value ) override {
    if ( name == "minimal_stepsize" )
      m_tauMin = value;
    else if ( name == "maximal_stepsize" )
      m_tauMax = value;
    else if ( name == "initial_stepsize" )
      m_startTau = value;
    else
      throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown real parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, int value ) override {
    if ( name == "acceleration" )
      m_acceleration = static_cast<bool>(value);
    else if ( name == "stepsize_control" )
      m_linesearchMethod = static_cast<TIMESTEP_CONTROLLER> (value);
    else
      throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown integer parameter '" +
                                name + "'." );
  }

  void setParameter( const std::string &name, std::string value ) override {
    throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown string parameter '" + name + "'." );
  }


};

#endif //BILEVELSHAPEOPT_STOCHASTICGRADIENTDESCENT_H
