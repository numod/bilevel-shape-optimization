#pragma clang diagnostic push
#pragma ide diagnostic ignored "bugprone-reserved-identifier"
/**
 * \file
 * \brief Operators for stochastic optimization problems
 * \author Sassen
 */
#ifndef BILEVELSHAPEOPT_STOCHASTICOPERATORS_H
#define BILEVELSHAPEOPT_STOCHASTICOPERATORS_H

#include <goast/Core/BaseOpInterface.h>


/**
 * \brief A parametrized function \f$f_\mu \colon U \to V\f$ where \f$\mu\f$ is the parameter
 * \author Sassen
 * \tparam _DomainType Type of domain elements
 * \tparam _RangeType Type of range elements
 * \tparam _ParameterType Type of a parameter
 */
template<typename _DomainType, typename _RangeType = _DomainType, typename _ParameterType = _DomainType>
class ParametrizedBaseOp : public BaseOp<_DomainType, _RangeType> {
public:
  using DomainType = _DomainType;
  using RangeType = _RangeType;
  using ParameterType = _ParameterType;

  virtual void setParameters( const ParameterType &parameters ) = 0;

  virtual const ParameterType &getParameters() = 0;
};

template<typename ConfiguratorType>
class ParametrizedAdditionOp
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;


  using ObjectiveOpType = ParametrizedBaseOp<VectorType, RealType, VectorType>;

//  const ObjectiveOp<ConfiguratorType> &_constraintOp;
  std::vector<ObjectiveOpType *> _Ops;
  const VectorType &_Weights;

  const int _numOps;
//  const int _numShapes;

public:
  template<class... Items>
  explicit ParametrizedAdditionOp( const VectorType &Weights, Items &... constraintOps ) : _Weights( Weights ),
                                                                                           _numOps(
                                                                                                   sizeof...( constraintOps )) {
    append_to_vector( _Ops, constraintOps... );

  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    Dest = 0.;

    for ( int i = 0; i < _numOps; i++ )
      Dest += _Weights[i] * ( *_Ops[i] )( Arg );
  }

  int getTargetDimension() const override {
    return 1;
  }

  void setParameters( const VectorType &parameters ) override {
    for ( auto &Op : _Ops )
      Op->setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return _Ops[0]->getParameters();
  }

private:
  void append_to_vector( std::vector<ObjectiveOpType *> &outputvector,
                         ObjectiveOpType &elem ) {
    outputvector.push_back( &elem );
  };

  template<typename ...T1toN>
  void append_to_vector( std::vector<ObjectiveOpType *> &outputvector,
                         ObjectiveOpType &elem, T1toN &... elems ) {
    outputvector.push_back( &elem );
    append_to_vector( outputvector, elems... );
  };

};

template<typename ConfiguratorType>
class ParametrizedAdditionGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
protected:
  typedef typename ConfiguratorType::RealType RealType;

  typedef typename ConfiguratorType::VectorType VectorType;


//  const ObjectiveOp<ConfiguratorType> &_constraintOp;
  using ObjectiveGradType = ParametrizedBaseOp<VectorType, VectorType, VectorType>;
  std::vector<ObjectiveGradType *> _Ops;
  const VectorType &_Weights;

  const int _numOps;
//  const int _numShapes;

public:
  template<class... Items>
  explicit ParametrizedAdditionGradient( const VectorType &Weights, Items &... constraintOps ) : _Weights( Weights ),
                                                                                                 _numOps(
                                                                                                         sizeof...( constraintOps )) {
    append_to_vector( _Ops, constraintOps... );

  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();

    for ( int i = 0; i < _numOps; i++ )
      Dest += _Weights[i] * ( *_Ops[i] )( Arg );
  }

  int getTargetDimension() const override {
    return 1;
  }

  void setParameters( const VectorType &parameters ) override {
    for ( auto &Op : _Ops )
      Op->setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return _Ops[0]->getParameters();
  }

private:
  void append_to_vector( std::vector<ObjectiveGradType *> &outputvector,
                         ObjectiveGradType &elem ) {
    outputvector.push_back( &elem );
  };

  template<typename ...T1toN>
  void append_to_vector( std::vector<ObjectiveGradType *> &outputvector,
                         ObjectiveGradType &elem, T1toN &... elems ) {
    outputvector.push_back( &elem );
    append_to_vector( outputvector, elems... );
  };

};

/**
 * \brief A function \f$f \colon U \to V\f$ whose values depend on sampled parameters \f$\mu_1,\ldots,\mu_n\f$
 * \author Sassen
 * \tparam _DomainType Type of domain elements
 * \tparam _RangeType Type of range elements
 * \tparam _SampleType Type of samples
 *
 * The most typical example of this will be Monte Carlo approximation of integrals, e.g. expected values
 */
template<typename _DomainType, typename _RangeType = _DomainType, typename _SampleType = _DomainType>
class SampledBaseOp : public BaseOp<_DomainType, _RangeType> {
public:
  using DomainType = _DomainType;
  using RangeType = _RangeType;
  using SampleType = _SampleType;

  /**
   * \brief Compute new samples
   */
  virtual void computeSamples() = 0;

  virtual void setSamples( const std::vector<SampleType> &samples ) = 0;

  virtual const std::vector<SampleType> &getSamples() = 0;
};

template<typename _DomainType, typename _RangeType = _DomainType, typename _SampleType = _DomainType>
class NegativeOp : public SampledBaseOp<_DomainType, _RangeType, _SampleType> {
public:
  using DomainType = _DomainType;
  using RangeType = _RangeType;
  using SampleType = _SampleType;

protected:
  SampledBaseOp<_DomainType, _RangeType, _SampleType> &m_Op;

public:
  NegativeOp( SampledBaseOp<_DomainType, _RangeType, _SampleType> &Op ) : m_Op( Op ) {}

  void computeSamples() override {
    m_Op.computeSamples();
  }

  void setSamples( const std::vector<SampleType> &samples ) override {
    m_Op.setSamples( samples );
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Op.getSamples();
  }

  void apply( const DomainType &Arg, RangeType &Dest ) const override {
    m_Op.apply( Arg, Dest );
    Dest *= -1.;
  }

  int getTargetDimension() const override {
    return m_Op.getTargetDimension();
  }
};

#endif //BILEVELSHAPEOPT_PARAMETRIZEDBASEOP_H

#pragma clang diagnostic pop