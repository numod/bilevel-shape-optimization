//
// Created by josua on 15.07.20.
//

#ifndef BILEVELSHAPEOPT_STOCHASTICPROXIMALGRADIENTDESCENT_H
#define BILEVELSHAPEOPT_STOCHASTICPROXIMALGRADIENTDESCENT_H

#include <chrono>

#include <goast/Optimization/optInterface.h>
#include <goast/Optimization/optParameters.h>

#include "StochasticOperators.h"
#include "Utils.h"


/**
 * \brief Stochastic gradient descent method for sampled functions
 * \tparam ConfiguratorType Container with data types
 * \author Sassen
 */
template<typename ConfiguratorType>
class StochasticProximalGradientDescent : public OptimizationBase<ConfiguratorType> {

protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  SampledBaseOp<VectorType, RealType, VectorType> &m_E;
  SampledBaseOp<VectorType, VectorType, VectorType> &m_DE;

  // Stepsize
  RealType m_tau;
  // Control
  TIMESTEP_CONTROLLER m_timestepController = SIMPLE_TIMESTEP_CONTROL;


  // Stopping criteria
  int m_maxIterations;
  RealType m_stopEpsilon;
  bool m_useGradientBasedStopping;

  // Fixed variables and bounds
  const std::vector<int> *m_fixedVariables;
  const VectorType *m_lowerBounds;
  const VectorType *m_upperBounds;
  RealType m_maxNorm = std::numeric_limits<RealType>::infinity();


  // Output
  QUIET_MODE m_quietMode;

public:
  StochasticProximalGradientDescent( SampledBaseOp<VectorType, RealType, VectorType> &E,
                                     SampledBaseOp<VectorType, VectorType, VectorType> &DE,
                                     int MaxIterations = 1000,
                                     RealType StopEpsilon = 1e-8,
                                     QUIET_MODE quietMode = SUPERQUIET,
                                     const RealType tau = 1e-2 )
          : m_E( E ), m_DE( DE ),
            m_maxIterations( MaxIterations ), m_stopEpsilon( StopEpsilon ),
            m_useGradientBasedStopping( true ), m_quietMode( quietMode ), m_fixedVariables( nullptr ), m_tau( tau ),
            m_lowerBounds( nullptr ), m_upperBounds( nullptr ) {}

  void setBoundaryMask( const std::vector<int> &Mask ) {
    setFixedVariables( Mask );
  }

  void setFixedVariables( const std::vector<int> &fixedVariables ) override {
    m_fixedVariables = &fixedVariables;
  }

  void setVariableBounds( const VectorType &lowerBounds, const VectorType &upperBounds ) override {
    throw std::logic_error( "ProximalGradientDescent: Variable bounds are currently not implemented!" );
    m_lowerBounds = &lowerBounds;
    m_upperBounds = &upperBounds;
  }

  void setMaximalNorm( const RealType maxNorm ) {
    m_maxNorm = maxNorm;
  }


  void solve( const VectorType &x_0, VectorType &x_k ) const {

    if ( m_maxIterations == 0 )
      return;

    x_k = x_0;

    // Sample functionals
    m_E.computeSamples();
    m_DE.setSamples( m_E.getSamples());

    if ( m_quietMode == SHOW_ALL ) {
      std::cout << "========================================================================================="
                << std::endl;
      std::cout << "Start proximal gradient descent with " << m_maxIterations << " iterations and eps = "
                << m_stopEpsilon << "."
                << std::endl;
      std::cout << "Initial energy: " << m_E( x_k ) << std::endl;
      std::cout << "========================================================================================="
                << std::endl;
    }

    int iterations = 0;
    bool stoppingCriterion = true; // stoppingCriterion == true means the iteration will be continued
    RealType error;

    // initial energy
    RealType energy = m_E( x_k );

    // initial gradient
    VectorType energyGradient = m_DE( x_k );
    if ( m_fixedVariables )
      applyMaskToVector( *m_fixedVariables, energyGradient );
    if ( m_lowerBounds && m_upperBounds )
      projectGradientOnBox( x_k, energyGradient, *m_lowerBounds, *m_upperBounds );

    // Stepsize control
    StepsizeControl<ConfiguratorType> stepsizeControl( m_E, m_DE, m_timestepController, 0.1, 0.9, 1e-2, 1e-12, 1e-1 );
    RealType tau = m_tau;

    // values of previous iterations
    VectorType x_old = x_k;
    VectorType x_old2 = x_k;

    // iteration loop
    while ( stoppingCriterion ) {
      iterations++;

      auto t_start = std::chrono::high_resolution_clock::now();

      x_old2 = x_old;
      x_old = x_k;

      // Acceleration
      x_k += ( iterations - 1. ) / ( iterations + 2. ) * ( x_old - x_old2 );

      // Sample functionals
      m_E.computeSamples();
      m_DE.setSamples( m_E.getSamples());

      // compute energy and gradient
      m_E.apply( x_k, energy );
      m_DE.apply( x_k, energyGradient );

      // apply boundary conditions
      if ( m_fixedVariables )
        applyMaskToVector( *m_fixedVariables, energyGradient );

      // Project gradient on admissible set
      if ( m_lowerBounds && m_upperBounds )
        projectGradientOnBox( energyGradient, x_k, *m_lowerBounds, *m_upperBounds );

      // Stepsize
      VectorType descentDirection = -energyGradient;
      // Actual step + projection on admissible set (= prox map)
      tau = m_tau;// stepsizeControl.getStepsize( x_k, energyGradient, descentDirection, tau, energy );
      x_k += tau * descentDirection;

      if ( m_lowerBounds && m_upperBounds )
        projectPointOnBox( x_k, *m_lowerBounds, *m_upperBounds );

      projectPointOnBall( x_k, m_maxNorm );

      // compute error
      //! \todo other stopping criteria?
      error = ( x_k - x_old ).norm();

      auto t_end = std::chrono::high_resolution_clock::now();

      if ( m_quietMode == SHOW_ALL ) {
        std::cout << "step = " << iterations << " , stepsize = " << std::scientific << std::setprecision( 6 )
                  << ( x_k - x_old ).norm()
                  << ", energy = " << m_E( x_k ) << ", error = " << error << ", t = " << std::fixed
                  << std::chrono::duration<double>( t_end - t_start ).count() << "s"
                  << std::endl;
      }

      stoppingCriterion = ( error > m_stopEpsilon ) && ( iterations < m_maxIterations );
    } // end iteration loop

    if ( m_quietMode != SUPERQUIET ) {
      std::cout << "========================================================================================="
                << std::endl;
      std::cout << "Finished gradient descent after " << iterations << " steps (max. steps = " << m_maxIterations
                << ", tol = " << m_stopEpsilon << ")." << std::endl;
//      std::cout << "Final stepsize = " << std::scientific << tau << ", energy = " << energyNew << ", error = " << error
//                << std::endl;
      std::cout << "========================================================================================="
                << std::endl;
    }

  }

  void setParameter( const std::string &name, RealType value ) override {
    throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown real parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, int value ) override {
    throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown integer parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, std::string value ) override {
    throw std::runtime_error( "StochasticGradientDescent::setParameter(): Unknown string parameter '" + name + "'." );
  }


};


#endif //BILEVELSHAPEOPT_STOCHASTICGRADIENTDESCENT_H
