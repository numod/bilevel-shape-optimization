/**
 * \brief Interfaces for line search methods, plus some basic methods
 * \author Sassen
 *
 * \warning This is work-in-progress!
 */

#ifndef OPTIMIZATION_LINESEARCH_H
#define OPTIMIZATION_LINESEARCH_H

#include <string>
#include <vector>
#include <stdexcept>
#include <limits>

#include "goast/Optimization/Objectives.h"

enum LINE_SEARCH_METHOD {
  C = -1,
  SIMPLE_LINE_SEARCH = 0,
  A = 1,
  Q = 2,
  N = 3,
  QUAD = 4
};

/**
 * \brief Basic interface for all line search methods
 * \tparam ConfiguratorType Container with data types
 *
 * \todo Move to CRTP
 */
template<typename ConfiguratorType>
class LineSearchBase {
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::ScalarType ScalarType;
  typedef typename ConfiguratorType::VectorType VectorType;

public:
  LineSearchBase() = default;

  virtual ~LineSearchBase() = default;

  /**** Essential methods of the line serach ****/

  /**
   * \brief Obtain stepsize for the given starting point, gradient and direction
   * \param currentPosition current position where the line search will start
   * \param currentGrad gradient st the starting point
   * \param descentDir direction of the line search
   * \param tau_before previous stepsize
   * \param currentEnergy (optional) energy value at the current position, if not given will be evaluated
   * \return computed stepsize
   */
  virtual RealType getStepsize( const VectorType &currentPosition,
                                const VectorType &currentGrad,
                                const VectorType &descentDir,
                                RealType previousTau,
                                RealType currentEnergy ) const = 0;

  /**** Mandatory methods for setting parameters ****/

  /**
   * \brief Set a real-valued parameter
   * \param[in] name Name of the parameter
   * \param[in] value Value of the parameter
   */
  virtual void setParameter( const std::string &name, RealType value ) = 0;

  /**
   * \brief Set a integer-valued parameter
   * \param[in] name Name of the parameter
   * \param[in] value Value of the parameter
   */
  virtual void setParameter( const std::string &name, int value ) = 0;

  /**
   * \brief Set a string-valued parameter
   * \param[in] name Name of the parameter
   * \param[in] value Value of the parameter
   */
  virtual void setParameter( const std::string &name, std::string value ) = 0;

  /**** Optional methods for defining the problem ****/

  /**
   * \brief Set which variables should remain fixed during the optimization
   * \param[in] fixedVariables Vector containing indices of variables to fix
   * \note This is an optional method, not every method has to implement it.
   */
  virtual void setFixedVariables( const std::vector<int> &fixedVariables ) {
    throw std::logic_error( "StepsizeControlInterface::setFixedVariables(): Unimplemented function! The chosen method "
                            "does not seem to provide the possibility to fix variables." );
  }

  /**
   * \brief Set box constraints i.e. lower and upper bounds on the individual variables
   * \param[in] lowerBounds Vector containing lower bounds on the variables
   * \param[in] upperBounds Vector containing upper bounds on the variables
   */
  virtual void setVariableBounds( const VectorType &lowerBounds, const VectorType &upperBounds ) {
    throw std::logic_error( "StepsizeControlInterface::setVariableBounds(): Unimplemented function! The chosen method "
                            "does not seem to allow box constraints." );
  }
};

template<typename ConfiguratorType>
class SimpleLineSearch : public LineSearchBase<ConfiguratorType> {
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::ScalarType ScalarType;
  typedef typename ConfiguratorType::VectorType VectorType;

  // Parameters
  mutable RealType _startTau = 1.;
  RealType _tauMin = 1e-12, _tauMax = 4.;

  // Functional
  const BaseOp<VectorType, ScalarType> &_E;

  // Fixed vars and bounds
  const std::vector<int> *_fixedVariables;

  const VectorType *_lowerBounds;
  const VectorType *_upperBounds;


public:
  explicit SimpleLineSearch( const BaseOp<VectorType, ScalarType> &E ) : _E( E ), _fixedVariables( nullptr ),
                                                                         _lowerBounds( nullptr ),
                                                                         _upperBounds( nullptr ) {}

  RealType getStepsize( const VectorType &currentPosition,
                        const VectorType &currentGrad,
                        const VectorType &descentDir,
                        RealType previousTau = -1.,
                        RealType currentEnergy = std::numeric_limits<RealType>::quiet_NaN()) const override {
    RealType tau = previousTau > 0 ? std::min( std::max( 2 * previousTau, _tauMin ), _tauMax ) : _startTau;

    RealType f = std::isnan( currentEnergy ) ? _E( currentPosition )[0] : currentEnergy;

    VectorType newPosition( currentPosition + tau * descentDir );
    RealType fNew = _E( newPosition )[0];

    if ( std::isnan( fNew ))
      fNew = std::numeric_limits<RealType>::infinity();

    while (( fNew >= f ) && ( tau >= _tauMin )) {
      tau = tau * 0.5;

      newPosition = currentPosition + tau * descentDir;
      fNew = _E( newPosition )[0];

      if ( std::isnan( fNew ))
        fNew = std::numeric_limits<RealType>::infinity();
    }

    return tau > _tauMin ? tau : 0.;
  }

  void setParameter( const std::string &name, RealType value ) override {
    if ( name == "minimal_stepsize" )
      _tauMin = value;
    else if ( name == "maximal_stepsize" )
      _tauMax = value;
    else if ( name == "initial_stepsize" )
      _startTau = value;
    else
      throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown real parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, int value ) override {
    throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown integer parameter '" + name + "'." );
  }

  void setParameter( const std::string &name, std::string value ) override {
    throw std::runtime_error( "SimpleLineSearch::setParameter(): Unknown string parameter '" + name + "'." );
  }
};


//! \todo Better name!
template<typename ConfiguratorType>
class DynamicLineSearch : public LineSearchBase<ConfiguratorType> {
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::ScalarType ScalarType;
  typedef typename ConfiguratorType::VectorType VectorType;

  std::unique_ptr<LineSearchBase<ConfiguratorType>> _lineSearchMethod;

public:
  template<typename ... Types>
  explicit DynamicLineSearch( const LINE_SEARCH_METHOD method, Types &&... args ) {
    switch ( method ) {
      case SIMPLE_LINE_SEARCH:
        _lineSearchMethod.reset( new SimpleLineSearch<ConfiguratorType>( std::forward<Types>( args )... ));
        break;
      default:
        throw std::runtime_error( "DynamicLineSearch: unknown line search method!" );
    }
  }

  inline RealType getStepsize( const VectorType &currentPosition,
                               const VectorType &currentGrad,
                               const VectorType &descentDir,
                               RealType previousTau,
                               RealType currentEnergy ) const override {
    return _lineSearchMethod->getStepsize( currentPosition, currentGrad, descentDir, previousTau, currentEnergy );
  }

  inline void setParameter( const std::string &name, RealType value ) override {
    _lineSearchMethod->setParameter( name, value );
  }

  inline void setParameter( const std::string &name, int value ) override {
    _lineSearchMethod->setParameter( name, value );
  }

  inline void setParameter( const std::string &name, std::string value ) override {
    _lineSearchMethod->setParameter( name, value );
  }

  inline void setFixedVariables( const std::vector<int> &fixedVariables ) override {
    _lineSearchMethod->setFixedVariables( fixedVariables );
  }

  inline void setVariableBounds( const VectorType &lowerBounds, const VectorType &upperBounds ) override {
    _lineSearchMethod->setVariableBounds( lowerBounds, upperBounds );
  }
};

#endif //OPTIMIZATION_LINESEARCH_H
