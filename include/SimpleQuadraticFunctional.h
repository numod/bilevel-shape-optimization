//
// Created by josua on 07.02.21.
//

#ifndef BILEVELSHAPEOPT_SIMPLEQUADRATICFUNCTIONAL_H
#define BILEVELSHAPEOPT_SIMPLEQUADRATICFUNCTIONAL_H

#include <goast/Core.h>


template<typename ConfiguratorType>
class SimpleQuadraticFunctional
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

public:

  void apply( const VectorType &Arg, RealType &Dest ) const {
    Dest = 0.5 * Arg.squaredNorm();
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class SimpleQuadraticGradient
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

public:
  void apply( const VectorType &Arg, VectorType &Dest ) const {
    Dest = Arg;
  }
};

#endif //BILEVELSHAPEOPT_SIMPLEQUADRATICFUNCTIONAL_H
