//
// Created by josua on 03.06.20.
//

#ifndef BILEVELSHAPEOPT_NONLINEARELASTICITYSOLVER_H
#define BILEVELSHAPEOPT_NONLINEARELASTICITYSOLVER_H

#include <goast/external/ipoptBoxConstraintSolver.h>
#include <goast/Core/FEM.h>
#include <goast/Optimization/LineSearchNewton.h>
#include <goast/DiscreteShells.h>

#include <utility>

#include "ShapeOptimization/StateEquationInterface.h"
#include "NodalAreas.h"
#include "FreeEnergy.h"


template<typename ConfiguratorType, class DeformationType>
class NonlinearElasticitySolver : public StateEquationSolverBase<ConfiguratorType> {

public:

  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

//  using ShellDeformationType =  ShellDeformation<ConfiguratorType, NonlinearMembraneDeformation<ConfiguratorType>, SimpleBendingDeformation<ConfiguratorType> >;

public:
  const RealType updateTolerance = 1e-10;

  // Underlying mesh and deformation model etc.
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;

  mutable DeformationType m_W;
  const std::vector<int> &m_dirichletBoundary;
  mutable MatrixType m_Hessian;
  mutable LinearSolver<ConfiguratorType> m_HessianSolver;

  // additional helpers
  const int m_numVertices;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_vertexForces;
  mutable VectorType m_vertexMaterial;

  // (cached) output
  mutable VectorType m_defDisplacement;
  mutable bool m_solvedAccurately;
  mutable MatrixType m_mixedDerivative;

  //
  mutable bool m_updateDeformation = true;
  mutable bool m_updateHessian = true;
  mutable bool m_updateMixedDerviative = true;


  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginForceControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = true;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginForceParameter, m_beginMaterialParameter;
public:
  NonlinearElasticitySolver( const MeshTopologySaver &Topology,
                             const VectorType &refGeometry,
                             const std::vector<int> &dirichletBoundary,
                             const VectorType undefDisplacement,
                             const VectorType vertexForces,
                             const VectorType vertexMaterial,
                             const bool UndefIsControl = true,
                             const bool ForceIsControl = false,
                             const bool MaterialIsControl = false,
                             const bool UndefIsParameter = false,
                             const bool ForceIsParameter = false,
                             const bool MaterialIsParameter = false )
          : m_Topology( Topology ), m_refGeometry( refGeometry ), m_dirichletBoundary( dirichletBoundary ),
            m_undefDisplacement( std::move( undefDisplacement )), m_vertexForces( std::move( vertexForces )),
            m_vertexMaterial( std::move( vertexMaterial )), m_W( Topology, vertexMaterial ),
            m_defDisplacement( undefDisplacement ), m_solvedAccurately( false ),
            m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
            m_MaterialIsControl( MaterialIsControl ),
            m_UndefIsParameter( UndefIsParameter ), m_ForceIsParameter( ForceIsParameter ),
            m_MaterialIsParameter( MaterialIsParameter ),
            m_numVertices( Topology.getNumVertices()),
            m_HessianSolver(  ) { // CHOLMOD_LLT

    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numVertices;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginForceParameter = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginForceParameter += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numVertices;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "NonlinearElasticitySolver: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "NonlinearElasticitySolver: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "NonlinearElasticitySolver: Material distribution can be either control or parameter, not both!" );

    m_mixedDerivative.resize( 3 * m_numVertices, m_numControls );

    // Initial solution
    computeDeformation();
    buildHessian();
  }

  bool updateControl( const VectorType &Control ) {
    assert( Control.size() == m_numControls &&
            "NonlinearElasticitySolver::updateControls(): Wrong number of controls." );

    bool updated = false;
    if ( m_UndefIsControl )
      updated = updateUndeformed( Control.segment( m_beginUndefControl, 3 * m_numVertices )) || updated;
    if ( m_ForceIsControl )
      updated = updateForces( Control.segment( m_beginForceControl, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsControl ) {
      bool materialUpdated = updateMaterial( Control.segment( m_beginMaterialControl, m_numVertices ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation || !m_solvedAccurately;

    return updated;
  }

  bool updateParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "NonlinearElasticitySolver::updateParameters(): Wrong number of parameters." );

    bool updated = false;
    if ( m_UndefIsParameter )
      updated = updateUndeformed( Parameters.segment( m_beginUndefParameter, 3 * m_numVertices )) || updated;
    if ( m_ForceIsParameter )
      updated = updateForces( Parameters.segment( m_beginForceParameter, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsParameter ) {
      bool materialUpdated = updateMaterial( Parameters.segment( m_beginMaterialParameter, m_numVertices ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation || !m_solvedAccurately;

    return updated;
  }


  VectorType solve( const VectorType &Arg ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeDeformation();

    return m_defDisplacement;
  }


  VectorType solveAdjoint( const VectorType &Arg, const VectorType &rhs ) override {
    updateControl( Arg );

    // If necessary compute new deformation
    if ( m_updateDeformation )
      computeDeformation();

    // Build hessian of adjoint problem
    if ( m_updateHessian )
      buildHessian();

    // Solve adjoint equation
    VectorType maskedRhs = rhs;
    applyMaskToVector( m_dirichletBoundary, maskedRhs );

    VectorType adjointSolution( 3 * m_numVertices );
    m_HessianSolver.backSubstitute( maskedRhs, adjointSolution );

    return adjointSolution;
  }


  MatrixType mixedDerivative( const VectorType &Arg ) override {
    updateControl( Arg );

    // If necessary compute new deformation
    if ( m_updateDeformation )
      computeDeformation();

    // Compute mixed derivative
    if ( m_updateMixedDerviative ) {
      TripletListType tripletList;

      // dUndef dDef
      if ( m_UndefIsControl ) {
        VectorType undefGeometry = m_refGeometry + m_undefDisplacement;
        VectorType defGeometry = m_refGeometry + m_defDisplacement;
        m_W.pushTripletsMixedHessian( undefGeometry, defGeometry, tripletList, 0, m_beginUndefControl, true, 1. );
        computeNodalAreaForceGradientMatrix_Negative_Transposed<ConfiguratorType>( m_Topology, m_vertexForces ).pushTriplets(
                undefGeometry, tripletList, 0, m_beginUndefControl );
      }

      // dF dDef
      if ( m_ForceIsControl ) {
        VectorType nodalAreas( m_numVertices );
        computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

        for ( int i = 0; i < m_numVertices; i++ )
          for ( const int j : { 0, 1, 2 } )
            tripletList.emplace_back( i + j * m_numVertices, i + j * m_numVertices + m_beginForceControl,
                                      -nodalAreas[i] );
      }

      // dMaterial dDef
      if ( m_MaterialIsControl ) {
        m_W.pushTripletsMixedHessianDefParam( m_refGeometry + m_undefDisplacement, m_refGeometry + m_defDisplacement, tripletList, 0, m_beginMaterialControl, 1. );
      }

      // Assembly
      m_mixedDerivative.setZero();
      m_mixedDerivative.setFromTriplets( tripletList.begin(), tripletList.end());

      m_updateMixedDerviative = false;
    }

    return m_mixedDerivative;
  }

  bool lastSolveSuccessful() const override {
    return m_solvedAccurately;
  }


protected:
  void computeDeformation() const {
    // Build functionals
    FreeEnergy<ConfiguratorType> FE( m_Topology, m_refGeometry, m_undefDisplacement, m_W, m_vertexForces );
    FreeEnergyGradient<ConfiguratorType> DFE( m_Topology, m_refGeometry, m_undefDisplacement, m_W, m_vertexForces );
    FreeEnergyHessian<ConfiguratorType> D2FE( m_Topology, m_refGeometry, m_undefDisplacement, m_W );

    // Check initialization
    VectorType test1 = DFE( m_undefDisplacement );
    applyMaskToVector( m_dirichletBoundary, test1 );
    VectorType test2 = DFE( m_defDisplacement );
    applyMaskToVector( m_dirichletBoundary, test2 );
//    if ( test1.norm() < test2.norm() ) {
    if ( FE( m_undefDisplacement ) < FE( m_defDisplacement ) ) { // | !m_solvedAccurately
//#ifndef NDEBUG
      std::cerr << "Reset defDisplacement to undefDisplacement" << std::endl;
//#endif
      m_defDisplacement = m_undefDisplacement;
    }

    // Solve nonlinear optimization problem
    LineSearchNewton<ConfiguratorType> Solver( FE, DFE, D2FE, 1e-7, 1000, SHOW_ONLY_IF_FAILED );
    Solver.setBoundaryMask( m_dirichletBoundary );
    Solver.setParameter("tau_increase", 5.);
    Solver.solve( m_defDisplacement, m_defDisplacement );

    // Check if solved accurately
    VectorType gradient = DFE( m_defDisplacement );
    applyMaskToVector( m_dirichletBoundary, gradient );

    if ( gradient.norm() > 1e-5 ) {
      std::cout << " .. Final gradient too big." << std::endl;
      m_solvedAccurately = false;
    }
    else {
      m_solvedAccurately = true;
    }

    m_updateDeformation = false;
  }

  /**
   * \brief Compute linearization of the state equation
   */
  void buildHessian() {
    m_W.applyDefHessian( m_refGeometry + m_undefDisplacement, m_refGeometry + m_defDisplacement, m_Hessian );
    applyMaskToSymmetricMatrix( m_dirichletBoundary, m_Hessian );
    m_HessianSolver.prepareSolver( m_Hessian );

    m_updateHessian = false;
  }

  bool updateUndeformed( const VectorType &undefDisplacement ) const {
    if (( m_undefDisplacement - undefDisplacement ).norm() > updateTolerance ) {
      m_undefDisplacement = undefDisplacement;
      return true;
    }
    else {
      return false;
    }

  }

  bool updateForces( const VectorType &vertexForces ) const {
    if (( m_vertexForces - vertexForces ).norm() > updateTolerance ) {
      m_vertexForces = vertexForces;
      return true;
    }
    else {
      return false;
    }

  }

  bool updateMaterial( const VectorType &vertexMaterial ) const {
    if (( m_vertexMaterial - vertexMaterial ).norm() > updateTolerance ) {
      m_vertexMaterial = vertexMaterial;
      m_W.setParameters( m_vertexMaterial );
      return true;
    }
    else {
      return false;
    }

  }

};


#endif //BILEVELSHAPEOPT_NONLINEARELASTICITYSOLVER_H
