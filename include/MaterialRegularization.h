//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_MATERIALREGULARIZATION_H
#define BILEVELSHAPEOPT_MATERIALREGULARIZATION_H

#include <goast/Core/Configurators.h>
#include <goast/Core/Topology.h>
#include <goast/Core/FEM.h>
//#include "materialShellDeformations.h"
#include "Optimization/StochasticOperators.h"
//
//template<typename ConfiguratorType>
//class MaterialDirichletRegularization
//        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
//public:
//  using RealType = typename ConfiguratorType::RealType;
//  using VectorType = typename ConfiguratorType::VectorType;
//  using MatrixType = typename ConfiguratorType::SparseMatrixType;
//protected:
//  const int m_numVertices;
//
//  mutable MatrixType m_stiffnessMatrix;
//
//  mutable VectorType m_Parameters;
//
//  // Controls
//  const bool m_UndefIsControl = true;
//  const bool m_ForceIsControl = false;
//  const bool m_MaterialIsControl = false;
//
//  int m_numControls, m_beginUndefControl, m_beginMaterialControl;
//
//  // Parameters
//  const bool m_UndefIsParameter = false;
//  const bool m_ForceIsParameter = false;
//  const bool m_MaterialIsParameter = false;
//
//  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;
//
//public:
//
//  MaterialDirichletRegularization( const MeshTopologySaver &Topology,
//                                   const VectorType &refGeometry,
//                                   const bool UndefIsControl = true,
//                                   const bool ForceIsControl = false,
//                                   const bool MaterialIsControl = false,
//                                   const bool UndefIsParameter = false,
//                                   const bool ForceIsParameter = false,
//                                   const bool MaterialIsParameter = false ) :
//          m_numVertices( Topology.getNumVertices()),
//          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
//          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
//          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
//
//    // Determine structure of control variable
//    m_numControls = 0;
//    m_beginMaterialControl = 0;
//    m_beginUndefControl = 0;
//    if ( m_UndefIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_ForceIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsControl ) {
//      m_numControls += m_numVertices;
//    }
//
//
//    // Determine structure of parameter variable
//    m_numParameters = 0;
//    m_beginMaterialParameter = 0;
//    m_beginUndefParameter = 0;
//    if ( m_UndefIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_ForceIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsParameter ) {
//      m_numParameters += m_numVertices;
//    }
//
//    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
//            "ComplianceTerm: Undeformed geometry can be either control or parameter, not both!" );
//    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
//            "ComplianceTerm: Acting force can be either control or parameter, not both!" );
//    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
//            "ComplianceTerm: Material distribution can be either control or parameter, not both!" );
//
//    computeStiffnessMatrix<ConfiguratorType>( Topology, refGeometry, m_stiffnessMatrix, false );
//
//  }
//
//
//  void apply( const VectorType &Arg, RealType &Dest ) const {
//    if ( Arg.size() < m_numControls + 3 * m_numVertices )
//      throw std::length_error( "ComplianceTerm: wrong dimension of argument!" );
//
//    if ( m_MaterialIsControl )
//      Dest = 0.5 * Arg.segment( m_beginMaterialControl, m_numVertices ).transpose() * m_stiffnessMatrix *
//             Arg.segment( m_beginMaterialControl, m_numVertices );
//    else
//      Dest = 0.;
//  }
//
//  void setParameters( const VectorType &Parameters ) override {
//    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );
//    m_Parameters = Parameters;
//  }
//
//  const VectorType &getParameters() override {
//    return m_Parameters;
//  }
//
//  int getTargetDimension() const {
//    return 1;
//  }
//};
//
//
//template<typename ConfiguratorType>
//class MaterialDirichletRegularizationGradient
//        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
//public:
//  using RealType = typename ConfiguratorType::RealType;
//  using VectorType = typename ConfiguratorType::VectorType;
//  using MatrixType = typename ConfiguratorType::SparseMatrixType;
//
//protected:
//  const int m_numVertices;
//  mutable MatrixType m_stiffnessMatrix;
//
//  // (cached) properties
//  mutable VectorType m_Parameters;
//
//  // Controls
//  const bool m_UndefIsControl = true;
//  const bool m_ForceIsControl = false;
//  const bool m_MaterialIsControl = false;
//
//  int m_numControls, m_beginUndefControl, m_beginMaterialControl;
//
//  // Parameters
//  const bool m_UndefIsParameter = false;
//  const bool m_ForceIsParameter = false;
//  const bool m_MaterialIsParameter = false;
//
//  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;
//
//public:
//  MaterialDirichletRegularizationGradient( const MeshTopologySaver &Topology,
//                                           const VectorType &refGeometry,
//                                           const bool UndefIsControl = true,
//                                           const bool ForceIsControl = false,
//                                           const bool MaterialIsControl = false,
//                                           const bool UndefIsParameter = false,
//                                           const bool ForceIsParameter = false,
//                                           const bool MaterialIsParameter = false ) :
//          m_numVertices( Topology.getNumVertices()),
//          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
//          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
//          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
//    // Determine structure of control variable
//    m_numControls = 0;
//    m_beginMaterialControl = 0;
//    m_beginUndefControl = 0;
//    if ( m_UndefIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_ForceIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsControl ) {
//      m_numControls += m_numVertices;
//    }
//
//    // Determine structure of parameter variable
//    m_numParameters = 0;
//    m_beginMaterialParameter = 0;
//    m_beginUndefParameter = 0;
//    if ( m_UndefIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_ForceIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsParameter ) {
//      m_numParameters += m_numVertices;
//    }
//
//    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
//            "MaterialDirichletRegularizationGradient: Undeformed geometry can be either control or parameter, not both!" );
//    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
//            "MaterialDirichletRegularizationGradient: Acting force can be either control or parameter, not both!" );
//    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
//            "MaterialDirichletRegularizationGradient: Material distribution can be either control or parameter, not both!" );
//
//    computeStiffnessMatrix<ConfiguratorType>( Topology, refGeometry, m_stiffnessMatrix, false );
//  }
//
//
//  void apply( const VectorType &Arg, VectorType &Dest ) const {
//    if ( Arg.size() != m_numControls + 3 * m_numVertices )
//      throw std::length_error( "ComplianceTermGradient: wrong dimension of argument!" );
//
//    Dest.resize( Arg.size());
//    Dest.setZero();
//
//    if ( m_MaterialIsControl )
//      Dest.segment( m_beginMaterialControl, m_numVertices ) = m_stiffnessMatrix * Arg.segment( m_beginMaterialControl, m_numVertices );
//  }
//
//  void setParameters( const VectorType &Parameters ) override {
//    assert( Parameters.size() == m_numParameters &&
//            "MaterialDirichletRegularizationGradient::setParameters: Invalid number of parameters!" );
//
//    m_Parameters = Parameters;
//  }
//
//  const VectorType &getParameters() override {
//    return m_Parameters;
//  }
//
//  int getTargetDimension() const {
//    return 3 * m_numVertices;
//  }
//
//
//};
//
//
//
//template<typename ConfiguratorType>
//class MaterialL2Regularization
//        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
//public:
//  using RealType = typename ConfiguratorType::RealType;
//  using VectorType = typename ConfiguratorType::VectorType;
//  using MatrixType = typename ConfiguratorType::SparseMatrixType;
//protected:
//  const int m_numVertices;
//
//  mutable MatrixType m_massMatrix;
//
//  mutable VectorType m_Parameters;
//
//  // Controls
//  const bool m_UndefIsControl = true;
//  const bool m_ForceIsControl = false;
//  const bool m_MaterialIsControl = false;
//
//  int m_numControls, m_beginUndefControl, m_beginMaterialControl;
//
//  // Parameters
//  const bool m_UndefIsParameter = false;
//  const bool m_ForceIsParameter = false;
//  const bool m_MaterialIsParameter = false;
//
//  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;
//
//public:
//
//  MaterialL2Regularization( const MeshTopologySaver &Topology,
//                                   const VectorType &refGeometry,
//                                   const bool UndefIsControl = true,
//                                   const bool ForceIsControl = false,
//                                   const bool MaterialIsControl = false,
//                                   const bool UndefIsParameter = false,
//                                   const bool ForceIsParameter = false,
//                                   const bool MaterialIsParameter = false ) :
//          m_numVertices( Topology.getNumVertices()),
//          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
//          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
//          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
//
//    // Determine structure of control variable
//    m_numControls = 0;
//    m_beginMaterialControl = 0;
//    m_beginUndefControl = 0;
//    if ( m_UndefIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_ForceIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsControl ) {
//      m_numControls += m_numVertices;
//    }
//
//
//    // Determine structure of parameter variable
//    m_numParameters = 0;
//    m_beginMaterialParameter = 0;
//    m_beginUndefParameter = 0;
//    if ( m_UndefIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_ForceIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsParameter ) {
//      m_numParameters += m_numVertices;
//    }
//
//    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
//            "ComplianceTerm: Undeformed geometry can be either control or parameter, not both!" );
//    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
//            "ComplianceTerm: Acting force can be either control or parameter, not both!" );
//    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
//            "ComplianceTerm: Material distribution can be either control or parameter, not both!" );
//
//    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, false );
//
//  }
//
//
//  void apply( const VectorType &Arg, RealType &Dest ) const {
//    if ( Arg.size() < m_numControls + 3 * m_numVertices )
//      throw std::length_error( "ComplianceTerm: wrong dimension of argument!" );
//
//    if ( m_MaterialIsControl )
//      Dest = 0.5 * Arg.segment( m_beginMaterialControl, m_numVertices ).transpose() * m_massMatrix *
//             Arg.segment( m_beginMaterialControl, m_numVertices );
//    else
//      Dest = 0.;
//  }
//
//  void setParameters( const VectorType &Parameters ) override {
//    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );
//    m_Parameters = Parameters;
//  }
//
//  const VectorType &getParameters() override {
//    return m_Parameters;
//  }
//
//  int getTargetDimension() const {
//    return 1;
//  }
//};
//
//
//template<typename ConfiguratorType>
//class MaterialL2RegularizationGradient
//        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
//public:
//  using RealType = typename ConfiguratorType::RealType;
//  using VectorType = typename ConfiguratorType::VectorType;
//  using MatrixType = typename ConfiguratorType::SparseMatrixType;
//
//protected:
//  const int m_numVertices;
//  mutable MatrixType m_massMatrix;
//
//  // (cached) properties
//  mutable VectorType m_Parameters;
//
//  // Controls
//  const bool m_UndefIsControl = true;
//  const bool m_ForceIsControl = false;
//  const bool m_MaterialIsControl = false;
//
//  int m_numControls, m_beginUndefControl, m_beginMaterialControl;
//
//  // Parameters
//  const bool m_UndefIsParameter = false;
//  const bool m_ForceIsParameter = false;
//  const bool m_MaterialIsParameter = false;
//
//  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;
//
//public:
//  MaterialL2RegularizationGradient( const MeshTopologySaver &Topology,
//                                           const VectorType &refGeometry,
//                                           const bool UndefIsControl = true,
//                                           const bool ForceIsControl = false,
//                                           const bool MaterialIsControl = false,
//                                           const bool UndefIsParameter = false,
//                                           const bool ForceIsParameter = false,
//                                           const bool MaterialIsParameter = false ) :
//          m_numVertices( Topology.getNumVertices()),
//          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
//          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
//          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
//    // Determine structure of control variable
//    m_numControls = 0;
//    m_beginMaterialControl = 0;
//    m_beginUndefControl = 0;
//    if ( m_UndefIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_ForceIsControl ) {
//      m_numControls += 3 * m_numVertices;
//      m_beginMaterialControl += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsControl ) {
//      m_numControls += m_numVertices;
//    }
//
//    // Determine structure of parameter variable
//    m_numParameters = 0;
//    m_beginMaterialParameter = 0;
//    m_beginUndefParameter = 0;
//    if ( m_UndefIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_ForceIsParameter ) {
//      m_numParameters += 3 * m_numVertices;
//      m_beginMaterialParameter += 3 * m_numVertices;
//    }
//    if ( m_MaterialIsParameter ) {
//      m_numParameters += m_numVertices;
//    }
//
//    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
//            "MaterialDirichletRegularizationGradient: Undeformed geometry can be either control or parameter, not both!" );
//    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
//            "MaterialDirichletRegularizationGradient: Acting force can be either control or parameter, not both!" );
//    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
//            "MaterialDirichletRegularizationGradient: Material distribution can be either control or parameter, not both!" );
//
//    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, false );
//  }
//
//
//  void apply( const VectorType &Arg, VectorType &Dest ) const {
//    if ( Arg.size() != m_numControls + 3 * m_numVertices )
//      throw std::length_error( "ComplianceTermGradient: wrong dimension of argument!" );
//
//    Dest.resize( Arg.size());
//    Dest.setZero();
//
//    if ( m_MaterialIsControl )
//      Dest.segment( m_beginMaterialControl, m_numVertices ) = m_massMatrix * Arg.segment( m_beginMaterialControl, m_numVertices );
//  }
//
//  void setParameters( const VectorType &Parameters ) override {
//    assert( Parameters.size() == m_numParameters &&
//            "MaterialDirichletRegularizationGradient::setParameters: Invalid number of parameters!" );
//
//    m_Parameters = Parameters;
//  }
//
//  const VectorType &getParameters() override {
//    return m_Parameters;
//  }
//
//  int getTargetDimension() const {
//    return 3 * m_numVertices;
//  }
//
//
//};


template<typename ConfiguratorType>
class MaterialMassLogBarrier
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const int m_numVertices;
  const int m_numFaces;
  VectorType m_faceAreas;
  const RealType m_barrierValue;

  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:

  MaterialMassLogBarrier( const MeshTopologySaver &Topology,
                        const VectorType &refGeometry,
                        const RealType barrierValue,
                        const bool UndefIsControl = true,
                        const bool ForceIsControl = false,
                        const bool MaterialIsControl = false,
                        const bool UndefIsParameter = false,
                        const bool ForceIsParameter = false,
                        const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrierValue( barrierValue ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "MaterialMassLogBarrier: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "MaterialMassLogBarrier: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "MaterialMassLogBarrier: Material distribution can be either control or parameter, not both!" );

    getFaceAreas<ConfiguratorType>( Topology, refGeometry, m_faceAreas );
  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < m_numControls + 3 * m_numVertices )
      throw std::length_error( "MaterialMassLogBarrier: wrong dimension of argument!" );

    if ( m_MaterialIsControl )
      Dest = -std::log(m_barrierValue - m_faceAreas.dot( Arg.segment( m_beginMaterialControl, m_numFaces )));
    else
      Dest = 0.;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "MaterialMassLogBarrier::setParameters: Invalid number of parameters!" );
    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class MaterialMassLogBarrierGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const int m_numVertices;
  const int m_numFaces;
  VectorType m_faceAreas;
  const RealType m_barrierValue;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  MaterialMassLogBarrierGradient( const MeshTopologySaver &Topology,
                                    const VectorType &refGeometry,
                                  const RealType barrierValue,
                                    const bool UndefIsControl = true,
                                    const bool ForceIsControl = false,
                                    const bool MaterialIsControl = false,
                                    const bool UndefIsParameter = false,
                                    const bool ForceIsParameter = false,
                                    const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrierValue(barrierValue) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "MaterialMassLogBarrierGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "MaterialMassLogBarrierGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "MaterialMassLogBarrierGradient: Material distribution can be either control or parameter, not both!" );

    getFaceAreas<ConfiguratorType>( Topology, refGeometry, m_faceAreas );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "MaterialMassLogBarrierGradient::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    if ( m_MaterialIsControl )
      Dest.segment( m_beginMaterialControl, m_numFaces ) = m_faceAreas / ( m_barrierValue - m_faceAreas.dot(
              Arg.segment( m_beginMaterialControl, m_numFaces )));
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "MaterialMassLogBarrierGradient::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};



template<typename ConfiguratorType>
class MaterialPointLogBarrier
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const int m_numVertices;
  const int m_numFaces;
  VectorType m_faceAreas;
  RealType m_totalArea;
  const RealType m_barrier;

  const std::vector<int> &m_dirichletFaces;

  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:

  MaterialPointLogBarrier( const MeshTopologySaver &Topology,
                           const VectorType &refGeometry,
                           RealType barrier,
                           const std::vector<int> &dirichletFaces,
                           const bool UndefIsControl = true,
                           const bool ForceIsControl = false,
                           const bool MaterialIsControl = false,
                           const bool UndefIsParameter = false,
                           const bool ForceIsParameter = false,
                           const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrier( barrier ), m_dirichletFaces(dirichletFaces) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "MaterialPointLogBarrier: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "MaterialPointLogBarrier: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "MaterialPointLogBarrier: Material distribution can be either control or parameter, not both!" );

    getFaceAreas<ConfiguratorType>( Topology, refGeometry, m_faceAreas );
    applyMaskToVector(dirichletFaces, m_faceAreas);
    m_faceAreas /= m_faceAreas.sum();
  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < m_numControls + 3 * m_numVertices )
      throw std::length_error( "MaterialMassLogBarrier: wrong dimension of argument!" );

    if ( m_MaterialIsControl ) {
      VectorType LogMaterial = ( Arg.segment( m_beginMaterialControl, m_numFaces ).array() - m_barrier ).log();
      applyMaskToVector(m_dirichletFaces, LogMaterial);
      Dest = -1. * LogMaterial.dot( m_faceAreas );

      if (std::isnan(Dest))
        Dest = std::numeric_limits<RealType>::infinity();
    }
    else
      Dest = 0.;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "MaterialMassLogBarrier::setParameters: Invalid number of parameters!" );
    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class MaterialPointLogBarrierGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const int m_numVertices;
  const int m_numFaces;
  VectorType m_faceAreas;
  RealType m_totalArea;
  const RealType m_barrier;

  const std::vector<int> &m_dirichletFaces;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  MaterialPointLogBarrierGradient( const MeshTopologySaver &Topology,
                                  const VectorType &refGeometry,
                                  RealType barrier,
                                   const std::vector<int> &dirichletFaces,
                                  const bool UndefIsControl = true,
                                  const bool ForceIsControl = false,
                                  const bool MaterialIsControl = false,
                                  const bool UndefIsParameter = false,
                                  const bool ForceIsParameter = false,
                                  const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrier(barrier), m_dirichletFaces(dirichletFaces)  {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "MaterialPointLogBarrierGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "MaterialPointLogBarrierGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "MaterialPointLogBarrierGradient: Material distribution can be either control or parameter, not both!" );

    getFaceAreas<ConfiguratorType>( Topology, refGeometry, m_faceAreas );
    applyMaskToVector(dirichletFaces, m_faceAreas);
    m_faceAreas /= m_faceAreas.sum();
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "MaterialPointLogBarrierGradient::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    if ( m_MaterialIsControl )
      Dest.segment( m_beginMaterialControl, m_numFaces ) = -m_faceAreas.array() / (Arg.segment( m_beginMaterialControl, m_numFaces ).array() - m_barrier);
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "MaterialPointLogBarrierGradient::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};


template<typename ConfiguratorType>
class MaterialUpperLogBarrier
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const int m_numVertices;
  const int m_numFaces;
  VectorType m_faceAreas;
  RealType m_totalArea;
  RealType m_barrier;

  const std::vector<int> &m_dirichletFaces;

  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:

  MaterialUpperLogBarrier( const MeshTopologySaver &Topology,
                           const VectorType &refGeometry,
                           RealType barrier,
                           const std::vector<int> &dirichletFaces,
                           const bool UndefIsControl = true,
                           const bool ForceIsControl = false,
                           const bool MaterialIsControl = false,
                           const bool UndefIsParameter = false,
                           const bool ForceIsParameter = false,
                           const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrier(barrier), m_dirichletFaces(dirichletFaces)  {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "MaterialPointLogBarrier: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "MaterialPointLogBarrier: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "MaterialPointLogBarrier: Material distribution can be either control or parameter, not both!" );

    getFaceAreas<ConfiguratorType>( Topology, refGeometry, m_faceAreas );
    applyMaskToVector(dirichletFaces, m_faceAreas);
    m_faceAreas /= m_faceAreas.sum();
  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < m_numControls + 3 * m_numVertices )
      throw std::length_error( "MaterialMassLogBarrier: wrong dimension of argument!" );

    if ( m_MaterialIsControl ) {
      VectorType LogMaterial = ( m_barrier - Arg.segment( m_beginMaterialControl, m_numFaces ).array()).log();
      applyMaskToVector( m_dirichletFaces, LogMaterial );
      Dest = -1. * LogMaterial.dot( m_faceAreas );

      if ( std::isnan( Dest ))
        Dest = std::numeric_limits<RealType>::infinity();
    }
    else {
      Dest = 0.;
    }
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "MaterialMassLogBarrier::setParameters: Invalid number of parameters!" );
    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType>
class MaterialUpperLogBarrierGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const int m_numVertices;
  const int m_numFaces;
  VectorType m_faceAreas;
  RealType m_totalArea;
  const RealType m_barrier;

  const std::vector<int> &m_dirichletFaces;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  MaterialUpperLogBarrierGradient( const MeshTopologySaver &Topology,
                                   const VectorType &refGeometry,
                                   RealType barrier,
                                   const std::vector<int> &dirichletFaces,
                                   const bool UndefIsControl = true,
                                   const bool ForceIsControl = false,
                                   const bool MaterialIsControl = false,
                                   const bool UndefIsParameter = false,
                                   const bool ForceIsParameter = false,
                                   const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrier(barrier), m_dirichletFaces(dirichletFaces) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "MaterialPointLogBarrierGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "MaterialPointLogBarrierGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "MaterialPointLogBarrierGradient: Material distribution can be either control or parameter, not both!" );

    getFaceAreas<ConfiguratorType>( Topology, refGeometry, m_faceAreas );
    applyMaskToVector(dirichletFaces, m_faceAreas);
    m_faceAreas /= m_faceAreas.sum();
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "MaterialPointLogBarrierGradient::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    if ( m_MaterialIsControl ) {
      Dest.segment( m_beginMaterialControl, m_numFaces ) =
              m_faceAreas.array() / ( m_barrier - Arg.segment( m_beginMaterialControl, m_numFaces ).array());
    }
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "MaterialPointLogBarrierGradient::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};

#endif //BILEVELSHAPEOPT_MATERIALREGULARIZATION_H
