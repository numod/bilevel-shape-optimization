//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_COMPLIANCETERM_H
#define BILEVELSHAPEOPT_COMPLIANCETERM_H

#include <goast/Core/Configurators.h>
//#include "materialShellDeformations.h"
#include "Optimization/StochasticOperators.h"


template<typename ConfiguratorType, class DeformationType>
class ComplianceTerm
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
protected:
  const VectorType &m_refGeometry;
  DeformationType &m_W;
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:

  ComplianceTerm( const MeshTopologySaver &Topology,
                  const VectorType &refGeometry,
                  DeformationType &W,
                  const bool UndefIsControl = true,
                  const bool ForceIsControl = false,
                  const bool MaterialIsControl = false,
                  const bool UndefIsParameter = false,
                  const bool ForceIsParameter = false,
                  const bool MaterialIsParameter = false ) :
          m_W( W ), m_refGeometry( refGeometry ), m_numVertices( Topology.getNumVertices()),
          m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTerm: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTerm: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTerm: Material distribution can be either control or parameter, not both!" );

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ComplianceTerm: wrong dimension of argument! " + std::to_string(Arg.size()) + " vs. " + std::to_string(m_numControls + 3 * m_numVertices) );

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );
    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;

    if ( m_MaterialIsControl ) {
      m_faceMaterial = Arg.segment( m_beginMaterialControl, m_numFaces );
      m_W.setParameters( m_faceMaterial );
    }

    m_W.applyEnergy( undefGeometry, defGeometry, Dest );
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numFaces );
      m_W.setParameters( m_faceMaterial );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType, class DeformationType>
class ComplianceTermGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  const VectorType &m_refGeometry;
  DeformationType &m_W;
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  ComplianceTermGradient( const MeshTopologySaver &topology,
                          const VectorType &refGeometry,
                          DeformationType &W,
                          const bool UndefIsControl = true,
                          const bool ForceIsControl = false,
                          const bool MaterialIsControl = false,
                          const bool UndefIsParameter = false,
                          const bool ForceIsParameter = false,
                          const bool MaterialIsParameter = false ) :
          m_W( W ), m_refGeometry( refGeometry ), m_numVertices( topology.getNumVertices()),
          m_numFaces( topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTermGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTermGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTermGradient: Material distribution can be either control or parameter, not both!" );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ComplianceTermGradient: wrong dimension of argument! " + std::to_string(Arg.size()) + " vs. " + std::to_string(m_numControls + 3 * m_numVertices) );

    Dest.resize( Arg.size());
    Dest.setZero();

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );

    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;


    // Calculate undeformed Gradient, if necessary
    if ( m_UndefIsControl ) {
      VectorType energyGradient_undef( 3 * m_numVertices );
      m_W.applyUndefGradient( undefGeometry, defGeometry, energyGradient_undef );

      Dest.segment( m_beginUndefControl, 3 * m_numVertices ) = energyGradient_undef;
    }

    // Calculate material Gradient, if necessary
    if ( m_MaterialIsControl ) {
      VectorType energyGradient_material( m_numFaces );
      m_W.applyParameterGradient( undefGeometry, defGeometry, energyGradient_material );

      Dest.segment( m_beginMaterialControl, m_numFaces ) = energyGradient_material;
    }

    // Calculate deformed Gradient
    VectorType energyGradient_def( 3 * m_numVertices );
    m_W.applyDefGradient( undefGeometry, defGeometry, energyGradient_def );
    Dest.tail( 3 * m_numVertices ) = energyGradient_def;
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numFaces );
      m_W.setParameters( m_faceMaterial );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};


template<typename ConfiguratorType, class DeformationType>
class ComplianceTermHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const VectorType &m_refGeometry;
  DeformationType &m_W;
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  ComplianceTermHessian( const MeshTopologySaver &topology,
                          const VectorType &refGeometry,
                          DeformationType &W,
                          const bool UndefIsControl = true,
                          const bool ForceIsControl = false,
                          const bool MaterialIsControl = false,
                          const bool UndefIsParameter = false,
                          const bool ForceIsParameter = false,
                          const bool MaterialIsParameter = false ) :
          m_W( W ), m_refGeometry( refGeometry ), m_numVertices( topology.getNumVertices()),
          m_numFaces( topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    if ( m_MaterialIsControl )
      throw std::runtime_error( "ComplianceTermHessian: Material as DOF is currently not supported!" );

    if ( m_UndefIsControl )
      throw std::runtime_error( "ComplianceTermHessian: Undeformed as DOF is currently not supported!" );

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTermHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTermHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTermHessian: Material distribution can be either control or parameter, not both!" );
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ComplianceTermHessian: wrong dimension of argument! " + std::to_string(Arg.size()) + " vs. " + std::to_string(m_numControls + 3 * m_numVertices) );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );

    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;

    TripletListType triplets;

//    if ( m_UndefIsControl ) {
//      VectorType energyGradient_undef( 3 * m_numVertices );
//      m_W.applyUndefGradient( undefGeometry, defGeometry, energyGradient_undef );
//
//      Dest.segment( m_beginUndefControl, 3 * m_numVertices ) = energyGradient_undef;
//    }
//
//    // Calculate material Gradient, if necessary
//    if ( m_MaterialIsControl ) {
//      VectorType energyGradient_material( m_numFaces );
//      m_W.applyParameterGradient( undefGeometry, defGeometry, energyGradient_material );
//
//
//      Dest.segment( m_beginMaterialControl, m_numFaces ) = energyGradient_material;
//    }

    // Calculate deformed hessian
    m_W.pushTripletsDefHessian( undefGeometry, defGeometry, triplets, m_numControls, m_numControls );

    Dest.setFromTriplets(triplets.begin(), triplets.end());
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTermHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numFaces );
      m_W.setParameters( m_faceMaterial );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};


template<typename ConfiguratorType, class DeformationType>
class LinearizedComplianceTerm
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TensorType = typename ConfiguratorType::TensorType;
protected:
  const VectorType &m_refGeometry;
  mutable DeformationType m_W;
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_Hessian;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:

  LinearizedComplianceTerm( const MeshTopologySaver &Topology,
                            const VectorType &refGeometry,
                            const VectorType undefDisplacement,
                            const VectorType vertexMaterial,
                            const bool UndefIsControl = true,
                            const bool ForceIsControl = false,
                            const bool MaterialIsControl = false,
                            const bool UndefIsParameter = false,
                            const bool ForceIsParameter = false,
                            const bool MaterialIsParameter = false ) :
          m_W( Topology, vertexMaterial ), m_refGeometry( refGeometry ), m_numVertices( Topology.getNumVertices()),
          m_numFaces( Topology.getNumFaces()),
          m_undefDisplacement( std::move( undefDisplacement )),
          m_faceMaterial( std::move( vertexMaterial )),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }


    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTerm: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTerm: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTerm: Material distribution can be either control or parameter, not both!" );

    m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );

  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    if ( Arg.size() < 3 * m_numVertices )
      throw std::length_error( "ComplianceTerm: wrong dimension of argument!" );

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );
    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;

    if ( m_MaterialIsControl ) {
      m_faceMaterial = Arg.segment( m_beginMaterialControl, m_numFaces );
      m_W.setParameters( m_faceMaterial );
      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }

    Dest = 0.5 *  ( undefGeometry - defGeometry ).transpose() * m_Hessian * ( undefGeometry - defGeometry );
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numFaces );
      m_W.setParameters( m_faceMaterial );
      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 1;
  }
};


template<typename ConfiguratorType, class DeformationType>
class LinearizedComplianceTermGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TensorType = typename ConfiguratorType::TensorType;

protected:
  const VectorType &m_refGeometry;
  mutable DeformationType m_W;
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_Hessian;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  LinearizedComplianceTermGradient( const MeshTopologySaver &Topology,
                                    const VectorType &refGeometry,
                                    const VectorType undefDisplacement,
                                    const VectorType vertexMaterial,
                                    const bool UndefIsControl = true,
                                    const bool ForceIsControl = false,
                                    const bool MaterialIsControl = false,
                                    const bool UndefIsParameter = false,
                                    const bool ForceIsParameter = false,
                                    const bool MaterialIsParameter = false ) :
          m_W( Topology, vertexMaterial ), m_refGeometry( refGeometry ), m_numVertices( Topology.getNumVertices()),
          m_numFaces( Topology.getNumFaces()),
          m_undefDisplacement( std::move( undefDisplacement )),
          m_faceMaterial( std::move( vertexMaterial )),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ComplianceTermGradient: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ComplianceTermGradient: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ComplianceTermGradient: Material distribution can be either control or parameter, not both!" );

    m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "ComplianceTermGradient: wrong dimension of argument!" );

    Dest.resize( Arg.size());
    Dest.setZero();

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );

    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;

    if ( m_MaterialIsControl ) {
      m_faceMaterial = Arg.segment( m_beginMaterialControl, m_numFaces );
      m_W.setParameters( m_faceMaterial );
      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );

      TensorType matDerivHessian;
      m_W.applyThirdDerivativeDefParam( m_refGeometry, m_refGeometry, matDerivHessian );

      MatrixType offTimesDeriv(3 * m_numVertices, m_numFaces);
      offTimesDeriv.setZero();
      matDerivHessian.applyVector( undefGeometry - defGeometry, offTimesDeriv );

      Dest.segment( m_beginMaterialControl, m_numFaces ) = 0.5 * ( undefGeometry - defGeometry ).transpose() * offTimesDeriv;
    }


    // Calculate undeformed Gradient, if necessary
    if ( m_UndefIsControl ) {
      Dest.segment( m_beginUndefControl, 3 * m_numVertices ) = m_Hessian * ( undefGeometry - defGeometry );
    }

    // Calculate deformed Gradient
    Dest.tail( 3 * m_numVertices ) = -1. * m_Hessian * ( undefGeometry - defGeometry );
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "ComplianceTerm::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numFaces );
      m_W.setParameters( m_faceMaterial );
      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};

template<typename ConfiguratorType, class DeformationType>
class LinearizedComplianceTermHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const VectorType &m_refGeometry;
  mutable DeformationType m_W;
  const int m_numVertices;
  const int m_numFaces;
  mutable MatrixType m_Hessian;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  LinearizedComplianceTermHessian( const MeshTopologySaver &Topology,
                                    const VectorType &refGeometry,
                                    const VectorType undefDisplacement,
                                    const VectorType vertexMaterial,
                                    const bool UndefIsControl = true,
                                    const bool ForceIsControl = false,
                                    const bool MaterialIsControl = false,
                                    const bool UndefIsParameter = false,
                                    const bool ForceIsParameter = false,
                                    const bool MaterialIsParameter = false ) :
          m_W( Topology, vertexMaterial ), m_refGeometry( refGeometry ), m_numVertices( Topology.getNumVertices()),
          m_numFaces( Topology.getNumFaces()),
          m_undefDisplacement( std::move( undefDisplacement )),
          m_faceMaterial( std::move( vertexMaterial )),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

//    if ( m_MaterialIsControl )
//      throw std::runtime_error( "LinearizedComplianceTermHessian: Material as DOF is currently not supported!" );

    if ( m_UndefIsControl )
      throw std::runtime_error( "LinearizedComplianceTermHessian: Undeformed as DOF is currently not supported!" );

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "LinearizedComplianceTermHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "LinearizedComplianceTermHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "LinearizedComplianceTermHessian: Material distribution can be either control or parameter, not both!" );

    m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_numControls + 3 * m_numVertices )
      throw std::length_error( "LinearizedComplianceTermHessian: wrong dimension of argument! " + std::to_string(Arg.size()) + " vs. " + std::to_string(m_numControls + 3 * m_numVertices) );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    VectorType defGeometry = m_refGeometry + Arg.tail( 3 * m_numVertices );
    VectorType undefGeometry = m_refGeometry;
    if ( m_UndefIsControl )
      undefGeometry += Arg.segment( m_beginUndefControl, 3 * m_numVertices );
    else if ( m_UndefIsParameter )
      undefGeometry += m_undefDisplacement;

    TripletListType triplets;

    if ( m_MaterialIsControl ) {
      m_faceMaterial = Arg.segment( m_beginMaterialControl, m_numFaces );
      m_W.setParameters( m_faceMaterial );
      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }

    // Deformed Deformed
    m_W.pushTripletsDefHessian( m_refGeometry, m_refGeometry, triplets, m_numControls, m_numControls, 1. );

    // Material Deformed
    if ( m_MaterialIsControl ) {
      VectorType Offset = undefGeometry - defGeometry ;

      std::vector<TripletListType> mdTriplets;
      m_W.pushTripletsThirdDerivativeDefParam(m_refGeometry, m_refGeometry, mdTriplets, 1.);

      for ( int i = 0; i < 3 * m_numVertices; i++ ) {
        for ( auto triplet : mdTriplets[i]) {
          triplets.emplace_back( triplet.row() + m_numControls, triplet.col() + m_beginMaterialControl, -1. * Offset[i] * triplet.value());
          triplets.emplace_back( triplet.col() + m_beginMaterialControl, triplet.row() + m_numControls, -1. * Offset[i] * triplet.value());
        }
      }

    }

    Dest.setFromTriplets(triplets.begin(), triplets.end());
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "LinearizedComplianceTermHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numFaces );
      m_W.setParameters( m_faceMaterial );
      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};

#endif //BILEVELSHAPEOPT_COMPLIANCETERM_H
