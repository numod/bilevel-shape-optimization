//
// Created by josua on 03.06.20.
//

#ifndef BILEVELSHAPEOPT_LINEAROPTIMALFORCESOLVER_H
#define BILEVELSHAPEOPT_LINEAROPTIMALFORCESOLVER_H

#include<Eigen/SparseQR>

#include <goast/external/ipoptBoxConstraintSolver.h>
#include <goast/Core/FEM.h>
#include <goast/Optimization/LineSearchNewton.h>
#include <goast/DiscreteShells.h>

#include <utility>

#include "ShapeOptimization/StateEquationInterface.h"
#include "NodalAreas.h"
#include "FreeEnergy.h"
#include "ReducedForces.h"
#include "ForceRegularization.h"
#include <yaml-cpp/yaml.h>
#include "OptimalForceSolver.h"
#include "ComplianceTerm.h"
#include "LinearElasticitySolver.h"


template<typename ConfiguratorType>
class ReducedForceL2LogBarrierHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const int m_numVertices;
  mutable MatrixType m_massMatrix;
  const RealType m_barrierValue;

  // Force space
  const FullMatrixType &m_forceBasis;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ReducedForceL2LogBarrierHessian( const MeshTopologySaver &Topology,
                            const VectorType &refGeometry,
                            const RealType barrierValue,
                            const FullMatrixType &forceBasis,
                            const bool UndefIsControl = true,
                            const bool ForceIsControl = false,
                            const bool MaterialIsControl = false,
                            const bool UndefIsParameter = false,
                            const bool ForceIsParameter = false,
                            const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_barrierValue( barrierValue * barrierValue ), m_forceBasis( forceBasis ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += Topology.getNumFaces();
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += Topology.getNumFaces();
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ReducedForceL2LogBarrierHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ReducedForceL2LogBarrierHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ReducedForceL2LogBarrierHessian: Material distribution can be either control or parameter, not both!" );

    computeLumpedMassMatrix<ConfiguratorType>( Topology, refGeometry, m_massMatrix, true );
    m_massMatrix = (m_forceBasis.transpose() * m_massMatrix * m_forceBasis).sparseView();
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_forceBasis.cols() )
      throw std::length_error( "ReducedForceL2LogBarrierHessian::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    TripletListType triplets;

    RealType L2 = Arg.transpose() * m_massMatrix * Arg;
    RealType logTerm = -std::log( m_barrierValue - L2 );

    VectorType L2grad = 2 * m_massMatrix * Arg;

    Dest = ( 2 * m_massMatrix ) / ( m_barrierValue - L2 ) +
           ( L2grad * L2grad.transpose()) / (( m_barrierValue - L2 ) * ( m_barrierValue - L2 ));


  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ReducedForceL2LogBarrierHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_forceBasis.cols();
  }


};
template<typename ConfiguratorType>
class ReducedForceCylinderLogBarrierHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

protected:
  const int m_numVertices;
  const int m_numFaces;
  mutable VectorType m_nodalAreas;
  mutable MatrixType m_massMatrixH;
  mutable MatrixType m_massMatrixR;

  const RealType m_radius;
  const RealType m_height;
  const int m_cylinderDim;
  std::array<int, 2> m_radialDim{ -1, -1 };

  // Force space
  const FullMatrixType &m_forceBasis;

  // (cached) properties
  mutable VectorType m_Parameters;

  // Controls
  const bool m_UndefIsControl = true;
  const bool m_ForceIsControl = false;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginForceControl;

  // Parameters
  const bool m_UndefIsParameter = false;
  const bool m_ForceIsParameter = false;
  const bool m_MaterialIsParameter = false;

  int m_numParameters;

public:
  ReducedForceCylinderLogBarrierHessian( const MeshTopologySaver &Topology,
                                         const VectorType &refGeometry,
                                         int cylinderDim,
                                         const RealType radius,
                                         const RealType height,
                                         const FullMatrixType &forceBasis,
                                         const bool UndefIsControl = true,
                                         const bool ForceIsControl = false,
                                         const bool MaterialIsControl = false,
                                         const bool UndefIsParameter = false,
                                         const bool ForceIsParameter = false,
                                         const bool MaterialIsParameter = false ) :
          m_numVertices( Topology.getNumVertices()), m_numFaces( Topology.getNumFaces()),
          m_UndefIsControl( UndefIsControl ), m_ForceIsControl( ForceIsControl ),
          m_MaterialIsControl( MaterialIsControl ), m_UndefIsParameter( UndefIsParameter ),
          m_ForceIsParameter( ForceIsParameter ), m_MaterialIsParameter( MaterialIsParameter ),
          m_forceBasis( forceBasis ),
          m_radius( radius * radius ), m_height( height * height ), m_cylinderDim( cylinderDim ), m_radialDim{ -1, -1 } {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginForceControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginForceControl += 3 * m_numVertices;
    }
    if ( m_ForceIsControl ) {
      m_numControls += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += Topology.getNumFaces();
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_ForceIsParameter ) {
      m_numParameters += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += Topology.getNumFaces();
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "ReducedForceCylinderLogBarrierHessian: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_ForceIsParameter && m_ForceIsControl ) &&
            "ReducedForceCylinderLogBarrierHessian: Acting force can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "ReducedForceCylinderLogBarrierHessian: Material distribution can be either control or parameter, not both!" );


    if ( cylinderDim == 0 )
      m_radialDim = { 1, 2 };
    else if ( cylinderDim == 1 )
      m_radialDim = { 0, 2 };
    else if ( cylinderDim == 2 )
      m_radialDim = { 0, 1 };
    else
      throw std::runtime_error( "ReducedForceCylinderLogBarrierHessian: Invalid cylinder dim." );

    computeNodalAreas<ConfiguratorType>( Topology, refGeometry, m_nodalAreas );

    m_massMatrixH.resize( 3 * m_numVertices, 3 * m_numVertices );
    m_massMatrixR.resize( 3 * m_numVertices, 3 * m_numVertices );
    TripletListType tripletsH, tripletsR;
    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ ) {
      tripletsH.emplace_back( m_cylinderDim * m_numVertices + vertexIdx, m_cylinderDim * m_numVertices + vertexIdx,
                              m_nodalAreas[vertexIdx] );
      tripletsR.emplace_back( m_radialDim[0] * m_numVertices + vertexIdx, m_radialDim[0] * m_numVertices + vertexIdx,
                              m_nodalAreas[vertexIdx] );
      tripletsR.emplace_back( m_radialDim[1] * m_numVertices + vertexIdx, m_radialDim[1] * m_numVertices + vertexIdx,
                              m_nodalAreas[vertexIdx] );
    }
    m_massMatrixH.setFromTriplets( tripletsH.begin(), tripletsH.end());
    m_massMatrixR.setFromTriplets( tripletsR.begin(), tripletsR.end());

    m_massMatrixH = (m_forceBasis.transpose() * m_massMatrixH * m_forceBasis).sparseView();
    m_massMatrixR = (m_forceBasis.transpose() * m_massMatrixR * m_forceBasis).sparseView();
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_forceBasis.cols() )
      throw std::length_error( "ReducedForceCylinderLogBarrierHessian::apply: wrong dimension of argument!" );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    TripletListType triplets;

    RealType L2H = Arg.transpose() * m_massMatrixH * Arg;
    RealType L2R = Arg.transpose() * m_massMatrixR * Arg;

    VectorType L2gradH = 2 * m_massMatrixH * Arg;
    VectorType L2gradR = 2 * m_massMatrixR * Arg;

    Dest = ( 2 * m_massMatrixH ) / ( m_height - L2H ) +
           ( L2gradH * L2gradH.transpose()) / (( m_height - L2H ) * ( m_height - L2H ));
    Dest += ( 2 * m_massMatrixR ) / ( m_radius - L2R ) +
            ( L2gradR * L2gradR.transpose()) / (( m_radius - L2R ) * ( m_radius - L2R ));


  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters &&
            "ReducedForceCylinderLogBarrierHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return m_forceBasis.cols();
  }


};

template<typename ConfiguratorType, class DeformationType>
class LinearForceOptHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

  struct {
    int maxNumIterations = 1;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 10.;
    TIMESTEP_CONTROLLER stepsizeController = ARMIJO;
    bool acceleration = false;
    RealType regularizationWeight = 0.;
    RealType objectiveWeight = 1.;
    RealType barrierValue = 1000.;
    bool useL2Constraint = true;
    bool useCylinderBarrier = false;
    RealType cylinderHeightFactor = 1.;
    int cylinderHeightDimension = 2;
  } Configuration;

  const RealType updateTolerance = 1e-10;

  // Underlying mesh and deformation model etc.
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;

  mutable DeformationType m_W;
  const std::vector<int> &m_dirichletBoundary;

  // Force space
  const FullMatrixType &m_forceBasis;

  // Solvers and functionals

  // additional helpers
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;
  mutable VectorType m_Parameters;

  // (cached) output
  mutable FullMatrixType m_redMatrix;


  // Controls
  const bool m_UndefIsControl = true;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = true;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  LinearForceOptHessian( const MeshTopologySaver &Topology,
                                const VectorType &refGeometry,
                                const std::vector<int> &dirichletBoundary,
                                const VectorType undefDisplacement,
                                const VectorType faceMaterial,
                                const FullMatrixType &forceBasis,
                                const VectorType initialForces,
                                const bool UndefIsControl = true,
                                const bool MaterialIsControl = false,
                                const bool UndefIsParameter = false,
                                const bool MaterialIsParameter = false )
          : m_Topology( Topology ), m_refGeometry( refGeometry ), m_dirichletBoundary( dirichletBoundary ),
            m_undefDisplacement( std::move( undefDisplacement )),
            m_faceMaterial( std::move( faceMaterial )), m_W( Topology, faceMaterial ),
            m_UndefIsControl( UndefIsControl ),
            m_MaterialIsControl( MaterialIsControl ),
            m_UndefIsParameter( UndefIsParameter ),
            m_MaterialIsParameter( MaterialIsParameter ),
            m_numVertices( Topology.getNumVertices()),
            m_numFaces( Topology.getNumFaces()),
            m_forceBasis( forceBasis ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "NonlinearElasticitySolver: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "NonlinearElasticitySolver: Material distribution can be either control or parameter, not both!" );


    MatrixType M( 3 * m_numVertices, 3 * m_numVertices );

    VectorType nodalAreas( m_numVertices );
    computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

    for ( int i = 0; i < m_numVertices; i++ )
      for ( const int j : { 0, 1, 2 } )
        M.coeffRef( i + j * m_numVertices, i + j * m_numVertices) = -nodalAreas[i];


    MatrixType elasticHessian;
    m_W.applyDefHessian( m_refGeometry, m_refGeometry, elasticHessian );
    MatrixType complianceHessian = elasticHessian;

    applyMaskToSymmetricMatrix( m_dirichletBoundary, elasticHessian );



    LinearSolver<ConfiguratorType> elasticInverse;
    elasticInverse.prepareSolver(elasticHessian);


    FullMatrixType rhM = M * m_forceBasis;

    FullMatrixType invMat1( 3 * m_numVertices, m_forceBasis.cols());
    for ( int j = 0; j < m_forceBasis.cols(); j++ ) {
      VectorType lsSolution;
      VectorType tmp_rhs = rhM.col( j );
      elasticInverse.backSubstitute( tmp_rhs, lsSolution );
      invMat1.col( j ) = lsSolution;
    }

    FullMatrixType redMatrix = m_forceBasis.transpose() * M * invMat1;

////    rhM = complianceHessian * invMat1;
////
////    FullMatrixType invMat2(3 * m_numVertices, m_forceBasis.cols());
////    for ( int j = 0; j < m_forceBasis.cols(); j++) {
////      VectorType lsSolution;
////      VectorType tmp_rhs = rhM.col( j );
////      elasticInverse.backSubstitute(tmp_rhs, lsSolution);
////      invMat2.col(j) = lsSolution;
////
////    }
////
////    FullMatrixType fullMatrix = m_forceBasis.transpose() * M * invMat2;
//
//    std::cout << " .. redMatrix - full Matrix = " << std::scientific << std::setprecision(6) << (redMatrix - fullMatrix).norm() << std::endl;
//    std::cout << " .. redMatrix - full Matrix = " << std::scientific << std::setprecision(6) << (redMatrix - fullMatrix).norm() / fullMatrix.norm()<< std::endl;

    m_redMatrix = redMatrix;
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_forceBasis.cols() )
      throw std::length_error( "LinearizedComplianceTermHessian: wrong dimension of argument! " + std::to_string(Arg.size()) + " vs. " + std::to_string( m_forceBasis.cols()) );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    VectorType vertexForces = m_forceBasis * Arg;

    // inverse elastic hessian
    VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
    DesignAndForce.head(3 * m_numVertices) = vertexForces;

    if ( m_UndefIsControl )
      throw std::runtime_error("Undeformed control currently not possible.");
    if ( m_MaterialIsControl )
      DesignAndForce.tail(m_numFaces) = m_faceMaterial;

    FullMatrixType projR;
    if ( Configuration.useCylinderBarrier ) {
      ReducedForceCylinderLogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry,
                                                                   Configuration.cylinderHeightDimension,
                                                                   Configuration.barrierValue,
                                                                   Configuration.cylinderHeightFactor *
                                                                   Configuration.barrierValue,
                                                                   m_forceBasis,
                                                                   false, true, true,
                                                                   m_UndefIsParameter, false,
                                                                   m_MaterialIsParameter );
      projR = D2R( Arg );
    }
    else {
      ReducedForceL2LogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                             m_forceBasis,
                                                             false, true, true,
                                                             m_UndefIsParameter, false,
                                                             m_MaterialIsParameter );
      projR = D2R( Arg );
    }

//    ReducedForceL2LogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry, Configuration.barrierValue,
//                                                           m_forceBasis,
//                                                    false, true, true,
//                                                    m_UndefIsParameter, false,
//                                                    m_MaterialIsParameter );
//    ForceL2LogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry, Configuration.barrierValue,
//                                                    false, true, true,
//                                                    m_UndefIsParameter, false,
//                                                    m_MaterialIsParameter );


//    FullMatrixType projR = m_forceBasis.transpose() * D2R(combinedVector).block( 0, 0, 3 * m_numVertices, 3 * m_numVertices ) * m_forceBasis;

    FullMatrixType adjMat = Configuration.objectiveWeight * m_redMatrix - Configuration.regularizationWeight * projR;

    Dest = -adjMat.sparseView();
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "LinearizedComplianceTermHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
      m_faceMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numVertices );
      m_W.setParameters( m_faceMaterial );
//      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};



template<typename ConfiguratorType, class DeformationType>
class FullLinearForceOptHessian
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using TensorType = typename ConfiguratorType::TensorType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

  struct {
    int maxNumIterations = 1;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 10.;
    TIMESTEP_CONTROLLER stepsizeController = ARMIJO;
    bool acceleration = false;
    RealType regularizationWeight = 0.;
    RealType objectiveWeight = 1.;
    RealType barrierValue = 1000.;
    bool useL2Constraint = true;
    bool useCylinderBarrier = false;
    RealType cylinderHeightFactor = 1.;
    int cylinderHeightDimension = 2;
  } Configuration;

  const RealType updateTolerance = 1e-10;

  // Underlying mesh and deformation model etc.
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;

  mutable DeformationType m_W;
  const std::vector<int> &m_dirichletBoundary;

  // Force space
  const FullMatrixType &m_forceBasis;

  // Solvers and functionals
  mutable LinearElasticitySolver<ConfiguratorType, DeformationType> m_SES;

  // additional helpers
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_Parameters;

  // (cached) output
  mutable FullMatrixType m_redMatrix;


  // Controls
  const bool m_UndefIsControl = true;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = true;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;

public:
  FullLinearForceOptHessian( const MeshTopologySaver &Topology,
                         const VectorType &refGeometry,
                         const std::vector<int> &dirichletBoundary,
                         const VectorType undefDisplacement,
                         const VectorType faceMaterial,
                         const FullMatrixType &forceBasis,
                         const VectorType initialForces,
                         const bool UndefIsControl = true,
                         const bool MaterialIsControl = false,
                         const bool UndefIsParameter = false,
                         const bool MaterialIsParameter = false )
          : m_Topology( Topology ), m_refGeometry( refGeometry ), m_dirichletBoundary( dirichletBoundary ),
            m_undefDisplacement( std::move( undefDisplacement )), m_W( Topology, faceMaterial ),
            m_UndefIsControl( UndefIsControl ),
            m_MaterialIsControl( MaterialIsControl ),
            m_UndefIsParameter( UndefIsParameter ),
            m_MaterialIsParameter( MaterialIsParameter ),
            m_numVertices( Topology.getNumVertices()),
            m_numFaces( Topology.getNumFaces()),
            m_forceBasis( forceBasis ),
            m_SES( Topology, refGeometry, dirichletBoundary, undefDisplacement, forceBasis * initialForces,
                   faceMaterial, false, true, false, UndefIsParameter | UndefIsControl, false, false ){
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "NonlinearElasticitySolver: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "NonlinearElasticitySolver: Material distribution can be either control or parameter, not both!" );


    MatrixType M( 3 * m_numVertices, 3 * m_numVertices );

    VectorType nodalAreas( m_numVertices );
    computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

    for ( int i = 0; i < m_numVertices; i++ )
      for ( const int j : { 0, 1, 2 } )
        M.coeffRef( i + j * m_numVertices, i + j * m_numVertices) = -nodalAreas[i];


    MatrixType elasticHessian;
    m_W.applyDefHessian( m_refGeometry, m_refGeometry, elasticHessian );
    MatrixType complianceHessian = elasticHessian;

    applyMaskToSymmetricMatrix( m_dirichletBoundary, elasticHessian );



    LinearSolver<ConfiguratorType> elasticInverse(CHOLMOD_LLT);
    elasticInverse.prepareSolver(elasticHessian);


    FullMatrixType rhM = M * m_forceBasis;

    FullMatrixType invMat1( 3 * m_numVertices, m_forceBasis.cols());
    for ( int j = 0; j < m_forceBasis.cols(); j++ ) {
      VectorType lsSolution;
      VectorType tmp_rhs = rhM.col( j );
      elasticInverse.backSubstitute( tmp_rhs, lsSolution );
      invMat1.col( j ) = lsSolution;
    }

    FullMatrixType redMatrix = m_forceBasis.transpose() * M * invMat1;


    m_redMatrix = redMatrix;
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ( Arg.size() != m_forceBasis.cols() + m_numFaces)
      throw std::length_error( "LinearizedComplianceTermHessian: wrong dimension of argument! " + std::to_string(Arg.size()) + " vs. " + std::to_string(  m_forceBasis.cols() + m_numFaces) );

    Dest.resize( Arg.size(), Arg.size());
    Dest.setZero();

    TripletListType tripletList;

    pushForceTriplets(Arg, tripletList);
    pushMixedTriplets(Arg, tripletList);

    Dest.setFromTriplets(tripletList.begin(), tripletList.end());
  }

  void pushForceTriplets( const VectorType &Arg, TripletListType &Dest ) const {
    FullMatrixType projR;
    if ( Configuration.useCylinderBarrier ) {
      ReducedForceCylinderLogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry,
                                                                   Configuration.cylinderHeightDimension,
                                                                   Configuration.barrierValue,
                                                                   Configuration.cylinderHeightFactor *
                                                                   Configuration.barrierValue,
                                                                   m_forceBasis,
                                                                   false, true, false,
                                                                   m_UndefIsParameter, false,
                                                                   m_MaterialIsParameter );
      projR = D2R( Arg.head( m_forceBasis.cols()));
    }
    else {
      ReducedForceL2LogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                             m_forceBasis,
                                                             false, true, false,
                                                             m_UndefIsParameter, false,
                                                             m_MaterialIsParameter );
      projR = D2R( Arg.head( m_forceBasis.cols()));
    }

    FullMatrixType adjMat = Configuration.objectiveWeight * m_redMatrix - Configuration.regularizationWeight * projR;

    for (int i : {0,1,2})
      for (int j : {0,1,2})
        Dest.emplace_back(i,j, -adjMat(i,j));

  }

  void pushMixedTriplets( const VectorType &Arg, TripletListType &Dest ) const {
    VectorType vertexForces =  m_forceBasis * Arg.head(m_forceBasis.cols());
    std::cout << " vertexForces.nrm = " << vertexForces.norm() << std::endl;
    VectorType faceMaterial = Arg.tail( m_numFaces );


    // mass matrix = mixed derivative of free energy
    MatrixType M( 3 * m_numVertices, 3 * m_numVertices );

    VectorType nodalAreas( m_numVertices );
    computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

    for ( int i = 0; i < m_numVertices; i++ )
      for ( const int j : { 0, 1, 2 } )
        M.coeffRef( i + j * m_numVertices, i + j * m_numVertices) = -nodalAreas[i];

    // inverse elastic hessian
    VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
    DesignAndForce.head(3 * m_numVertices) = vertexForces;

    if ( m_UndefIsControl )
      throw std::runtime_error("Undeformed control currently not possible.");
    if ( m_MaterialIsControl )
      DesignAndForce.tail(m_numFaces) = faceMaterial;

    VectorType defDisplacement = m_SES.solve( vertexForces );

    VectorType MaterialAndDef( 3 * m_numVertices + m_numFaces );
    MaterialAndDef.head( m_numFaces ) = faceMaterial;
    MaterialAndDef.tail( 3 * m_numVertices ) = defDisplacement;

    // Compliance gradient
    LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, faceMaterial,
                                                                            false, false, false,
                                                                            m_UndefIsParameter, false,
                                                                            m_MaterialIsParameter );

    VectorType complianceGradient  = DC(defDisplacement);

    // Compliance hessian
    LinearizedComplianceTermHessian<ConfiguratorType, DeformationType> D2C( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, faceMaterial,
                                                                            false, false, true,
                                                                            m_UndefIsParameter, false,
                                                                            false );

    MatrixType fullComplianceHessian = D2C( MaterialAndDef );
    MatrixType defComplianceHessian = fullComplianceHessian.block( m_numFaces, m_numFaces,
                                                                   3 * m_numVertices, 3 * m_numVertices );
    MatrixType mixedComplianceHessian = fullComplianceHessian.block( 0, m_numFaces, m_numFaces,
                                                                     3 * m_numVertices ).transpose();


    // Elastic hessian
    MatrixType elasticHessian;
    m_W.applyDefHessian( m_refGeometry, m_refGeometry, elasticHessian );

    std::cout << " .... elastic vs compliance hessian = " << std::scientific << std::setprecision(6) << (defComplianceHessian - elasticHessian).norm() / elasticHessian.norm() << std::endl;

    applyMaskToSymmetricMatrix( m_dirichletBoundary, elasticHessian );
    LinearSolver<ConfiguratorType> elasticInverse;
    elasticInverse.prepareSolver( elasticHessian );

    // Material derivative of Hessian
    VectorType defDisplacementMinusDisplacement = defDisplacement - m_undefDisplacement;

    std::vector<TripletListType> mdTriplets;
    m_W.pushTripletsThirdDerivativeDefParam( m_refGeometry, m_refGeometry, mdTriplets, 1. );

    TripletListType tripletList;
    for ( int i = 0; i < 3 * m_numVertices; i++ ) {
      for ( auto triplet : mdTriplets[i] ) {
        tripletList.emplace_back( triplet.row(), triplet.col(), defDisplacementMinusDisplacement[i] * triplet.value());
      }
    }
    MatrixType mixedDerivElast( 3 * m_numVertices, m_numFaces ); // Also second derivative of compliance
    mixedDerivElast.setFromTriplets( tripletList.begin(), tripletList.end());

    TensorType DmatH( 3 * m_numVertices, 3 * m_numVertices, m_numFaces );
    DmatH.setFromTriplets( mdTriplets );

    for ( auto idx : m_dirichletBoundary ) {
      DmatH[idx].setZero();
    }

    for ( int i = 0; i < 3 * m_numVertices; i++ )
      applyMaskToRow( m_dirichletBoundary, DmatH[i], false );

    // Mixed compliance hessian
//    LinearizedComplianceTermHessian<ConfiguratorType, DeformationType> D2C( m_Topology, m_refGeometry,
//                                                                            m_undefDisplacement, m_vertexMaterial,
//                                                                            false, false, true,
//                                                                            m_UndefIsParameter, false,
//                                                                            m_MaterialIsParameter );
//    VectorType MaterialDeformation( m_numVertices +  3 * m_numVertices );
//    MaterialDeformation.head( m_numVertices) = m_vertexMaterial;
//    MaterialDeformation.tail( 3 * m_numVertices ) = defDisplacement;
//
//    MatrixType linCoHess = D2C(MaterialDeformation).block(m_numVertices, 0, 3*m_numVertices, m_numVertices);
//
//    std::cout << " .... (linCoHess - mixedDerivElast).norm = " << (linCoHess - mixedDerivElast).norm() << std::endl;


    VectorType rightVector;
    elasticInverse.backSubstitute(complianceGradient, rightVector);

    std::cout << " .. complianceGradient.norm = " << complianceGradient.norm() << std::endl;
    std::cout << " .. rightVector.norm = " << (rightVector).norm() << std::endl;
    std::cout << " .. offset.norm  = " << (defDisplacementMinusDisplacement).norm() << std::endl;
    std::cout << " .. offset - rightVector = " << (defDisplacementMinusDisplacement - rightVector).norm() << std::endl;
    std::cout << " .. offset - rightVector = " << (defDisplacementMinusDisplacement - rightVector).norm() / defDisplacementMinusDisplacement.norm() << std::endl;

//    rightVector = defDisplacementMinusDisplacement;

    FullMatrixType leftMatrix = - m_forceBasis.transpose() * M;
    for (int i = 0; i < leftMatrix.rows(); i++) {
      VectorType lsSolution;
      elasticInverse.backSubstitute(leftMatrix.row(i), lsSolution);
      leftMatrix.row(i) = lsSolution;
    }

    MatrixType rightMatrix(3 * m_numVertices, m_numFaces);
    rightMatrix.setZero();
    DmatH.applyVector( rightVector, rightMatrix );

//    rightMatrix += mixedComplianceHessian;

    FullMatrixType mixedDerivative = Configuration.objectiveWeight * leftMatrix * rightMatrix;

    std::cout << " .. mixedDerivative = " << mixedDerivative.rows() << " x " << mixedDerivative.cols() << std::endl;

    for (int i = 0; i < mixedDerivative.rows(); i++) {
      for ( int j = 0; j < mixedDerivative.cols(); j++ ) {
        Dest.emplace_back( i, j + m_forceBasis.cols(), mixedDerivative( i, j ));
        Dest.emplace_back( j + m_forceBasis.cols(), i, mixedDerivative( i, j ));
      }
    }
  }

  void setParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numParameters && "LinearizedComplianceTermHessian::setParameters: Invalid number of parameters!" );

    m_Parameters = Parameters;

    if ( m_UndefIsParameter )
      m_undefDisplacement = m_Parameters.segment( m_beginUndefParameter, 3 * m_numVertices );
    if ( m_MaterialIsParameter ) {
//      m_vertexMaterial = m_Parameters.segment( m_beginMaterialParameter, m_numVertices );
//      m_W.setParameters( m_vertexMaterial );
//      m_W.applyDefHessian( m_refGeometry, m_refGeometry, m_Hessian );
    }
  }

  const VectorType &getParameters() override {
    return m_Parameters;
  }

  int getTargetDimension() const {
    return 3 * m_numVertices;
  }


};

template<typename ConfiguratorType, class DeformationType>
class LinearizedOptimalForceSolver : public StateEquationSolverBase<ConfiguratorType> {

public:

  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using TensorType = typename ConfiguratorType::TensorType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

//  using ShellDeformationType =  ShellDeformation<ConfiguratorType, NonlinearMembraneDeformation<ConfiguratorType>, SimpleBendingDeformation<ConfiguratorType> >;

public:
  struct {
    int maxNumIterations = 0;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 10.;
    TIMESTEP_CONTROLLER stepsizeController = ARMIJO;
    bool acceleration = false;
    RealType regularizationWeight = 0.;
    RealType objectiveWeight = 1.;
    RealType barrierValue = 1000.;
    bool useL2Constraint = true;
    bool useCylinderBarrier = false;
    RealType cylinderHeightFactor = 1.;
    int cylinderHeightDimension = 2;
  } Configuration;

  const RealType updateTolerance = 1e-10;

  // Underlying mesh and deformation model etc.
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;

  mutable DeformationType m_W;
  const std::vector<int> &m_dirichletBoundary;

  // Force space
  const FullMatrixType &m_forceBasis;
  mutable VectorType m_forceCoordinates;
  const VectorType m_initialCoordinates;
  mutable VectorType m_vertexForces;

  // Solvers and functionals
  mutable LinearElasticitySolver<ConfiguratorType, DeformationType> m_SES;

  // Adjoint
  mutable FullMatrixType m_inverseHessian;

  // additional helpers
  const int m_numVertices;
  const int m_numFaces;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_faceMaterial;

  // (cached) output
  mutable VectorType m_defDisplacement;
  mutable bool m_solvedAccurately;

  //
  mutable bool m_updateDeformation = true;
  mutable bool m_updateHessian = true;
  mutable bool m_updateMixedDerviative = true;
  mutable bool m_updateBFGS = true;


  // Controls
  const bool m_UndefIsControl = true;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = true;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;
public:
  LinearizedOptimalForceSolver( const MeshTopologySaver &Topology,
                      const VectorType &refGeometry,
                      const std::vector<int> &dirichletBoundary,
                      const VectorType undefDisplacement,
                      const VectorType faceMaterial,
                      const FullMatrixType &forceBasis,
                      const VectorType initialForces,
                      const bool UndefIsControl = true,
                      const bool MaterialIsControl = false,
                      const bool UndefIsParameter = false,
                      const bool MaterialIsParameter = false )
          : m_Topology( Topology ), m_refGeometry( refGeometry ), m_dirichletBoundary( dirichletBoundary ),
            m_undefDisplacement( std::move( undefDisplacement )), m_initialCoordinates( initialForces ),
            m_forceCoordinates( std::move( initialForces )),
            m_faceMaterial( std::move( faceMaterial )),
            m_W( Topology, faceMaterial ),
            m_defDisplacement( undefDisplacement ), m_solvedAccurately( false ),
            m_UndefIsControl( UndefIsControl ),
            m_MaterialIsControl( MaterialIsControl ),
            m_UndefIsParameter( UndefIsParameter ),
            m_MaterialIsParameter( MaterialIsParameter ),
            m_numVertices( Topology.getNumVertices()),
            m_numFaces( Topology.getNumFaces()),
            m_forceBasis( forceBasis ), m_vertexForces( forceBasis * m_forceCoordinates ),
            m_SES( Topology, refGeometry, dirichletBoundary, undefDisplacement, forceBasis * m_forceCoordinates,
                   faceMaterial, false, true, false, UndefIsParameter | UndefIsControl, false,
                   MaterialIsParameter | MaterialIsControl ) {
    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numFaces;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numFaces;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "LinearizedOptimalForceSolver: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "LinearizedOptimalForceSolver: Material distribution can be either control or parameter, not both!" );

    if ( m_UndefIsControl )
      throw std::runtime_error("LinearizedOptimalForceSolver: Undeformed control currently not possible.");

    // Initial solution
    computeForce();
//    buildHessian();
  }

  bool updateControl( const VectorType &Control ) {
    assert( Control.size() == m_numControls &&
            "NonlinearElasticitySolver::updateControls(): Wrong number of controls." );

    bool updated = false;
    if ( m_UndefIsControl )
      updated = updateUndeformed( Control.segment( m_beginUndefControl, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsControl ) {
      bool materialUpdated = updateMaterial( Control.segment( m_beginMaterialControl, m_numFaces ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation || !m_solvedAccurately;
    m_updateBFGS = updated || m_updateBFGS || !m_solvedAccurately;


    return updated;
  }

  bool updateParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numControls &&
            "NonlinearElasticitySolver::updateParameters(): Wrong number of parameters." );

    bool updated = false;
    if ( m_UndefIsParameter )
      updated = updateUndeformed( Parameters.segment( m_beginUndefParameter, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsParameter ) {
      bool materialUpdated = updateMaterial( Parameters.segment( m_beginMaterialParameter, m_numFaces ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation || !m_solvedAccurately;
    m_updateBFGS = updated || m_updateBFGS || !m_solvedAccurately;

    return updated;
  }


  VectorType solve( const VectorType &Arg ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

    return m_forceCoordinates;
  }

  VectorType forceSolve( const VectorType &Arg ) {
    updateControl( Arg );

    // Compute deformation
    computeForce();

    return m_forceCoordinates;
  }


  VectorType solveAdjoint( const VectorType &Arg, const VectorType &rhs ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

    return m_inverseHessian * rhs;
  }


  MatrixType mixedDerivative( const VectorType &Arg ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

    // Exact calculation

    // mass matrix = mixed derivative of free energy
    MatrixType M( 3 * m_numVertices, 3 * m_numVertices );

    VectorType nodalAreas( m_numVertices );
    computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );

    for ( int i = 0; i < m_numVertices; i++ )
      for ( const int j : { 0, 1, 2 } )
        M.coeffRef( i + j * m_numVertices, i + j * m_numVertices) = -nodalAreas[i];

    // inverse elastic hessian
    VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
    DesignAndForce.head(3 * m_numVertices) = m_vertexForces;

    if ( m_UndefIsControl )
      throw std::runtime_error("Undeformed control currently not possible.");
    if ( m_MaterialIsControl )
      DesignAndForce.tail(m_numFaces) = m_faceMaterial;

    VectorType defDisplacement = m_SES.solve( m_vertexForces );

    VectorType MaterialAndDef( 3 * m_numVertices + m_numFaces );
    MaterialAndDef.head( m_numFaces ) = m_faceMaterial;
    MaterialAndDef.tail( 3 * m_numVertices ) = defDisplacement;

    // Compliance gradient
    LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, m_faceMaterial,
                                                                            false, false, false,
                                                                            m_UndefIsParameter, false,
                                                                            m_MaterialIsParameter );

    VectorType complianceGradient  = DC(defDisplacement);

    // Compliance hessian
    LinearizedComplianceTermHessian<ConfiguratorType, DeformationType> D2C( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, m_faceMaterial,
                                                                            false, false, true,
                                                                            m_UndefIsParameter, false,
                                                                            false );

    MatrixType fullComplianceHessian = D2C( MaterialAndDef );
    MatrixType defComplianceHessian = fullComplianceHessian.block( m_numFaces, m_numFaces,
                                                                   3 * m_numVertices, 3 * m_numVertices );
    MatrixType mixedComplianceHessian = fullComplianceHessian.block( 0, m_numFaces, m_numFaces,
                                                                     3 * m_numVertices ).transpose();


    // Elastic hessian
    MatrixType elasticHessian;
    m_W.applyDefHessian( m_refGeometry, m_refGeometry, elasticHessian );

    applyMaskToSymmetricMatrix( m_dirichletBoundary, elasticHessian );
    LinearSolver<ConfiguratorType> elasticInverse;
    elasticInverse.prepareSolver( elasticHessian );

    // Material derivative of Hessian
    VectorType defDisplacementMinusDisplacement = defDisplacement - m_undefDisplacement;

    std::vector<TripletListType> mdTriplets;
    m_W.pushTripletsThirdDerivativeDefParam( m_refGeometry, m_refGeometry, mdTriplets, 1. );

    TripletListType tripletList;
    for ( int i = 0; i < 3 * m_numVertices; i++ ) {
      for ( auto triplet : mdTriplets[i] ) {
        tripletList.emplace_back( triplet.row(), triplet.col(), defDisplacementMinusDisplacement[i] * triplet.value());
      }
    }
    MatrixType mixedDerivElast( 3 * m_numVertices, m_numFaces ); // Also second derivative of compliance
    mixedDerivElast.setFromTriplets( tripletList.begin(), tripletList.end());

    TensorType DmatH( 3 * m_numVertices, 3 * m_numVertices, m_numFaces );
    DmatH.setFromTriplets( mdTriplets );

    for ( auto idx : m_dirichletBoundary ) {
      DmatH[idx].setZero();
    }

    for ( int i = 0; i < 3 * m_numVertices; i++ )
      applyMaskToRow( m_dirichletBoundary, DmatH[i], false );

    // Mixed compliance hessian
//    LinearizedComplianceTermHessian<ConfiguratorType, DeformationType> D2C( m_Topology, m_refGeometry,
//                                                                            m_undefDisplacement, m_vertexMaterial,
//                                                                            false, false, true,
//                                                                            m_UndefIsParameter, false,
//                                                                            m_MaterialIsParameter );
//    VectorType MaterialDeformation( m_numVertices +  3 * m_numVertices );
//    MaterialDeformation.head( m_numVertices) = m_vertexMaterial;
//    MaterialDeformation.tail( 3 * m_numVertices ) = defDisplacement;
//
//    MatrixType linCoHess = D2C(MaterialDeformation).block(m_numVertices, 0, 3*m_numVertices, m_numVertices);
//
//    std::cout << " .... (linCoHess - mixedDerivElast).norm = " << (linCoHess - mixedDerivElast).norm() << std::endl;


    VectorType rightVector;
    elasticInverse.backSubstitute(complianceGradient, rightVector);

//    rightVector = defDisplacementMinusDisplacement;

    FullMatrixType leftMatrix = - m_forceBasis.transpose() * M;
    for (int i = 0; i < leftMatrix.rows(); i++) {
      VectorType lsSolution;
      elasticInverse.backSubstitute(leftMatrix.row(i), lsSolution);
      leftMatrix.row(i) = lsSolution;
    }

    MatrixType rightMatrix(3 * m_numVertices, m_numFaces);
    rightMatrix.setZero();
    DmatH.applyVector( rightVector, rightMatrix );

//    rightMatrix += mixedComplianceHessian;

    FullMatrixType mixedDerivative = Configuration.objectiveWeight * leftMatrix * rightMatrix;

    return mixedDerivative.sparseView();
  }

  bool lastSolveSuccessful() const override {
    return m_solvedAccurately;
  }

  RealType evaluateWithForce(const VectorType &inForce) const {
//    std::cout << " .. computingForce" << std::endl;
      // Build functionals
      LinearizedComplianceTerm<ConfiguratorType, DeformationType> C( m_Topology, m_refGeometry,
                                                                     m_undefDisplacement, m_faceMaterial,
                                                                     false, true, false,
                                                                     m_UndefIsParameter | m_UndefIsControl, false,
                                                                     m_MaterialIsParameter | m_MaterialIsControl );

      ForceL2LogBarrier<ConfiguratorType> R( m_Topology, m_refGeometry, Configuration.barrierValue,
                                             false, true, false,
                                             m_UndefIsParameter | m_UndefIsControl, false,
                                             m_MaterialIsParameter | m_MaterialIsControl );

      VectorType weights( 2 );
      weights << Configuration.objectiveWeight, -Configuration.regularizationWeight;

      ParametrizedAdditionOp<DefaultConfigurator> J( weights, C, R );

      ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( J, m_SES );

      VectorType emptyVector;

      ReducedForceFunctional<ConfiguratorType> Jld( m_Topology, Jred, m_forceBasis, m_dirichletBoundary,
                                                    emptyVector, emptyVector );



      SimpleNegativeOp<VectorType, RealType> nJ( Jld );

      {
        VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
        DesignAndForce.head( 3 * m_numVertices ) = m_forceBasis * inForce;

        if ( m_UndefIsControl )
          throw std::runtime_error( "Undeformed control currently not possible." );
        if ( m_MaterialIsControl )
          DesignAndForce.tail( m_numFaces ) = m_faceMaterial;

        ReducedPDECParametrizedFunctional<ConfiguratorType> Cred( C, m_SES );
        ReducedPDECParametrizedFunctional<ConfiguratorType> Rred( R, m_SES );
        std::cout << " .... C = " << Cred(m_forceBasis * inForce) << " || R = " << Rred(m_forceBasis * inForce) << std::endl;
      }

      return nJ(inForce);
  }

  void testCylinderDerivatives() const {
    ForceCylinderLogBarrier<ConfiguratorType> R( m_Topology, m_refGeometry, 1, Configuration.barrierValue,
                                                 2 * Configuration.barrierValue,
                                                 false, true, false,
                                                 m_UndefIsParameter | m_UndefIsControl, false,
                                                 m_MaterialIsParameter | m_MaterialIsControl );
    ForceCylinderLogBarrierGradient<ConfiguratorType> DR( m_Topology, m_refGeometry, 1, Configuration.barrierValue,
                                                          2 * Configuration.barrierValue,
                                                          false, true, false,
                                                          m_UndefIsParameter | m_UndefIsControl, false,
                                                          m_MaterialIsParameter | m_MaterialIsControl );
    ForceCylinderLogBarrierHessian<ConfiguratorType> D2R( m_Topology, m_refGeometry, 1, Configuration.barrierValue,
                                                          2 * Configuration.barrierValue,
                                                          false, true, false,
                                                          m_UndefIsParameter | m_UndefIsControl, false,
                                                          m_MaterialIsParameter | m_MaterialIsControl );




    VectorType ForceAndDef( 6 * m_numVertices);
    ForceAndDef.head( 3 * m_numVertices ) = m_vertexForces;
    ForceAndDef.tail( 3 * m_numVertices ) = m_SES.solve( m_vertexForces );

    std::cout << " .. R = " << std::scientific << std::setprecision(6) << R(ForceAndDef) << std::endl;

//    ScalarValuedDerivativeTester<ConfiguratorType>( R, DR, 1e-5 ).plotAllDirections( ForceAndDef, "testDR" );
    ScalarValuedDerivativeTester<ConfiguratorType>( R, DR, 1e-5 ).plotRandomDirections( ForceAndDef, 100, "testDR" );
    ScalarValuedDerivativeTester<ConfiguratorType>( R, DR, 1e-5 ).testRandomDirections( ForceAndDef, 10, true );
    VectorValuedDerivativeTester<ConfiguratorType>( DR, D2R, 1e-5 ).plotRandomDirections( ForceAndDef, 100, "testD2R" );

    ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( R, m_SES );
    ReducedPDECParametrizedGradient<ConfiguratorType> DJred( R, DR, m_SES, m_dirichletBoundary );

    VectorType emptyVector;

    ReducedForceFunctional<ConfiguratorType> Jld( m_Topology, Jred, m_forceBasis, m_dirichletBoundary,
                                                  emptyVector, emptyVector );
    ReducedForceGradient<ConfiguratorType> DJld( m_Topology, DJred, m_forceBasis, m_dirichletBoundary,
                                                 emptyVector, emptyVector );


    ReducedForceCylinderLogBarrierHessian<ConfiguratorType> D2Jld( m_Topology, m_refGeometry, 1, Configuration.barrierValue,
                                                          2 * Configuration.barrierValue, m_forceBasis,
                                                          false, true, false,
                                                          m_UndefIsParameter | m_UndefIsControl, false,
                                                          m_MaterialIsParameter | m_MaterialIsControl );

    ScalarValuedDerivativeTester<ConfiguratorType>( Jld, DJld, 1e-5 ).plotAllDirections( m_forceCoordinates, "testDJld" );
    VectorValuedDerivativeTester<ConfiguratorType>( DJld, D2Jld, 1e-5 ).plotAllDirections( m_forceCoordinates, "testD2Jld" );
  }

  void testDerivatives() const {
//    std::cout << " .. computingForce" << std::endl;
    // Build functionals
    LinearizedComplianceTerm<ConfiguratorType, DeformationType> C( m_Topology, m_refGeometry,
                                                                   m_undefDisplacement, m_faceMaterial,
                                                                   false, true, false,
                                                                   m_UndefIsParameter | m_UndefIsControl, false, m_MaterialIsParameter | m_MaterialIsControl );
    LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, m_faceMaterial,
                                                                            false, true, false,
                                                                            m_UndefIsParameter | m_UndefIsControl,
                                                                            false, m_MaterialIsParameter |
                                                                                   m_MaterialIsControl );

    std::unique_ptr<ParametrizedBaseOp<VectorType, RealType, VectorType>> R;
    std::unique_ptr<ParametrizedBaseOp<VectorType, VectorType, VectorType>> DR;
    if ( Configuration.useCylinderBarrier ) {
      R.reset( new ForceCylinderLogBarrier<ConfiguratorType>( m_Topology, m_refGeometry,
                                                              Configuration.cylinderHeightDimension,
                                                              Configuration.barrierValue,
                                                              Configuration.cylinderHeightFactor *
                                                              Configuration.barrierValue,
                                                              false, true, false,
                                                              m_UndefIsParameter | m_UndefIsControl, false,
                                                              m_MaterialIsParameter | m_MaterialIsControl ));
      DR.reset( new ForceCylinderLogBarrierGradient<ConfiguratorType>( m_Topology, m_refGeometry,
                                                                       Configuration.cylinderHeightDimension,
                                                                       Configuration.barrierValue,
                                                                       Configuration.cylinderHeightFactor *
                                                                       Configuration.barrierValue,
                                                                       false, true, false,
                                                                       m_UndefIsParameter | m_UndefIsControl, false,
                                                                       m_MaterialIsParameter | m_MaterialIsControl ));
    }
    else {
      R.reset( new ForceL2LogBarrier<ConfiguratorType>( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                        false, true, false,
                                                        m_UndefIsParameter | m_UndefIsControl, false,
                                                        m_MaterialIsParameter | m_MaterialIsControl ));
      DR.reset( new ForceL2LogBarrierGradient<ConfiguratorType>( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                                 false, true, false,
                                                                 m_UndefIsParameter | m_UndefIsControl, false,
                                                                 m_MaterialIsParameter | m_MaterialIsControl ));
    }

    VectorType weights( 2 );
    weights << Configuration.objectiveWeight, -Configuration.regularizationWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, C, *R );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DC, *DR );

    ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( J, m_SES );
    ReducedPDECParametrizedGradient<ConfiguratorType> DJred( J, DJ, m_SES, m_dirichletBoundary );

    VectorType emptyVector;

    ReducedForceFunctional<ConfiguratorType> Jld( m_Topology, Jred, m_forceBasis, m_dirichletBoundary,
                                                  emptyVector, emptyVector );
    ReducedForceGradient<ConfiguratorType> DJld( m_Topology, DJred, m_forceBasis, m_dirichletBoundary,
                                                 emptyVector, emptyVector );


    SimpleNegativeOp<VectorType, RealType> nJ( Jld );
    SimpleNegativeOp<VectorType, VectorType> nDJ( DJld );

    LinearForceOptHessian<ConfiguratorType, DeformationType> nD2J( m_Topology, m_refGeometry, m_dirichletBoundary,
                                                                   m_undefDisplacement, m_faceMaterial, m_forceBasis,
                                                                   m_forceCoordinates, m_UndefIsControl,
                                                                   m_MaterialIsControl, m_UndefIsParameter,
                                                                   m_MaterialIsParameter );

    nD2J.Configuration.maxNumIterations = Configuration.maxNumIterations;
    nD2J.Configuration.optimalityTolerance = Configuration.optimalityTolerance;
    nD2J.Configuration.stepsize = Configuration.stepsize;
    nD2J.Configuration.maximalStepsize = Configuration.maximalStepsize;
    nD2J.Configuration.stepsizeController = Configuration.stepsizeController;
    nD2J.Configuration.acceleration = Configuration.acceleration;
    nD2J.Configuration.useL2Constraint = Configuration.useL2Constraint;
    nD2J.Configuration.barrierValue = Configuration.barrierValue;
    nD2J.Configuration.regularizationWeight = Configuration.regularizationWeight;
    nD2J.Configuration.objectiveWeight = Configuration.objectiveWeight;
    nD2J.Configuration.useCylinderBarrier = Configuration.useCylinderBarrier;
    nD2J.Configuration.cylinderHeightFactor = Configuration.cylinderHeightFactor;
    nD2J.Configuration.cylinderHeightDimension = Configuration.cylinderHeightDimension;
    std::cout << " .... nD2J = " << std::scientific << std::setprecision(6) << std::endl << nD2J(m_forceCoordinates) << std::endl;

    ScalarValuedDerivativeTester<ConfiguratorType>(nJ, nDJ, 1e-5).plotAllDirections(m_forceCoordinates, "testOFS_nDJ");
    VectorValuedDerivativeTester<ConfiguratorType>(nDJ, nD2J, 1e-5).plotAllDirections(m_forceCoordinates, "testOFS_nD2J");
    VectorValuedDerivativeTester<ConfiguratorType>(nDJ, nD2J, 1e-5).testAllDirections(m_forceCoordinates, true);

//    {
//      LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
//                                                                              m_undefDisplacement, m_vertexMaterial,
//                                                                              false, false, true,
//                                                                              m_UndefIsParameter | m_UndefIsControl,
//                                                                              false, false );
//      LinearizedComplianceTermHessian<ConfiguratorType, DeformationType> D2C( m_Topology, m_refGeometry,
//                                                                              m_undefDisplacement, m_vertexMaterial,
//                                                                              false, false, true,
//                                                                              m_UndefIsParameter | m_UndefIsControl,
//                                                                              false, false );
//
//
//      VectorType MaterialAndDef( 4 * m_numVertices);
//      MaterialAndDef.head( m_numVertices ) = m_vertexMaterial;
//      MaterialAndDef.tail( 3 * m_numVertices ) = m_SES.solve( m_vertexForces );
//
//
//      VectorValuedDerivativeTester<ConfiguratorType>(DC, D2C, 1e-5).plotRandomDirections(MaterialAndDef, 100, "testOFS_nD2C");
//      VectorValuedDerivativeTester<ConfiguratorType>(DC, D2C, 1e-5).testRandomDirections(MaterialAndDef, 100);
//
//    }
  }

  void testMixedDerivatives() const {
//    std::cout << " .. computingForce" << std::endl;

    LinearElasticitySolver<ConfiguratorType, DeformationType> SES( m_Topology, m_refGeometry, m_dirichletBoundary,
                                                                   m_undefDisplacement,
                                                                   m_forceBasis * m_forceCoordinates,
                                                                   m_faceMaterial, false, true, true,
                                                                   m_UndefIsParameter | m_UndefIsControl,
                                                                   false, false );
    // Build functionals
    LinearizedComplianceTerm<ConfiguratorType, DeformationType> C( m_Topology, m_refGeometry,
                                                                   m_undefDisplacement, m_faceMaterial,
                                                                   false, true, true,
                                                                   m_UndefIsParameter | m_UndefIsControl, false, false );
    LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, m_faceMaterial,
                                                                            false, true, true,
                                                                            m_UndefIsParameter | m_UndefIsControl,
                                                                            false,false);

    std::unique_ptr<ParametrizedBaseOp<VectorType, RealType, VectorType>> R;
    std::unique_ptr<ParametrizedBaseOp<VectorType, VectorType, VectorType>> DR;
    if ( Configuration.useCylinderBarrier ) {
      R.reset( new ForceCylinderLogBarrier<ConfiguratorType>( m_Topology, m_refGeometry,
                                                              Configuration.cylinderHeightDimension,
                                                              Configuration.barrierValue,
                                                              Configuration.cylinderHeightFactor *
                                                              Configuration.barrierValue,
                                                              false, true, true,
                                                              m_UndefIsParameter | m_UndefIsControl, false,false));
      DR.reset( new ForceCylinderLogBarrierGradient<ConfiguratorType>( m_Topology, m_refGeometry,
                                                                       Configuration.cylinderHeightDimension,
                                                                       Configuration.barrierValue,
                                                                       Configuration.cylinderHeightFactor *
                                                                       Configuration.barrierValue,
                                                                       false, true, true,
                                                                       m_UndefIsParameter | m_UndefIsControl, false, false ));
    }
    else {
      R.reset( new ForceL2LogBarrier<ConfiguratorType>( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                        false, true, true,
                                                        m_UndefIsParameter | m_UndefIsControl, false,false ));
      DR.reset( new ForceL2LogBarrierGradient<ConfiguratorType>( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                                 false, true, true,
                                                                 m_UndefIsParameter | m_UndefIsControl, false,false ));
    }

    VectorType weights( 2 );
    weights << Configuration.objectiveWeight, -Configuration.regularizationWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, C, *R );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DC, *DR );

    ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( J, SES );
    ReducedPDECParametrizedGradient<ConfiguratorType> DJred( J, DJ, SES, m_dirichletBoundary );

    VectorType emptyVector;

    PartialReducedForceFunctional<ConfiguratorType> Jld( m_Topology, Jred, m_forceBasis, m_dirichletBoundary,
                                                         0, m_numFaces );
    PartialReducedForceGradient<ConfiguratorType> DJld( m_Topology, DJred, m_forceBasis, m_dirichletBoundary,
                                                        0, m_numFaces );


    SimpleNegativeOp<VectorType, RealType> nJ( Jld );
    SimpleNegativeOp<VectorType, VectorType> nDJ( DJld );

    FullLinearForceOptHessian<ConfiguratorType, DeformationType> nD2J( m_Topology, m_refGeometry, m_dirichletBoundary,
                                                                   m_undefDisplacement, m_faceMaterial, m_forceBasis,
                                                                   m_forceCoordinates, m_UndefIsControl,
                                                                   m_MaterialIsControl, m_UndefIsParameter,
                                                                   m_MaterialIsParameter );

    nD2J.Configuration.maxNumIterations = Configuration.maxNumIterations;
    nD2J.Configuration.optimalityTolerance = Configuration.optimalityTolerance;
    nD2J.Configuration.stepsize = Configuration.stepsize;
    nD2J.Configuration.maximalStepsize = Configuration.maximalStepsize;
    nD2J.Configuration.stepsizeController = Configuration.stepsizeController;
    nD2J.Configuration.acceleration = Configuration.acceleration;
    nD2J.Configuration.useL2Constraint = Configuration.useL2Constraint;
    nD2J.Configuration.barrierValue = Configuration.barrierValue;
    nD2J.Configuration.regularizationWeight = Configuration.regularizationWeight;
    nD2J.Configuration.objectiveWeight = Configuration.objectiveWeight;

    VectorType ForceAndMaterial(m_forceCoordinates.size() + m_numFaces);
    ForceAndMaterial.head(m_forceCoordinates.size()) = m_forceCoordinates;
    ForceAndMaterial.tail(m_numFaces) = m_faceMaterial;

    MatrixType fullHessian  = nD2J(ForceAndMaterial);
    MatrixType fHT  = fullHessian.transpose();

    std::cout << " ... symmetric = " << std::scientific << std::setprecision(6) << (fullHessian - fHT).norm() << std::endl;


    std::cout << " .... Jld = " << std::scientific << std::setprecision(6) << std::endl << Jld(ForceAndMaterial) << std::endl;

//    ScalarValuedDerivativeTester<ConfiguratorType>(nJ, nDJ, 1e-5).plotRandomDirections(ForceAndMaterial, 100, "testOFS_full_nDJ");
    VectorValuedDerivativeTester<ConfiguratorType>(nDJ, nD2J, 1e-5).plotRandomDirections(ForceAndMaterial, 30, "testOFS_full_nD2J");
    VectorValuedDerivativeTester<ConfiguratorType>(nDJ, nD2J, 1e-4).testRandomDirections(ForceAndMaterial, 30, true);
  }

protected:
  void computeForce() const {
//    std::cout << " .. computingForce" << std::endl;
    // Build functionals
    LinearizedComplianceTerm<ConfiguratorType, DeformationType> C( m_Topology, m_refGeometry,
                                                                   m_undefDisplacement, m_faceMaterial,
                                                                   false, true, false,
                                                                   m_UndefIsParameter | m_UndefIsControl, false, m_MaterialIsParameter | m_MaterialIsControl );
    LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, m_faceMaterial,
                                                                            false, true, false,
                                                                            m_UndefIsParameter | m_UndefIsControl,
                                                                            false, m_MaterialIsParameter |
                                                                                   m_MaterialIsControl );

    std::unique_ptr<ParametrizedBaseOp<VectorType, RealType, VectorType>> R;
    std::unique_ptr<ParametrizedBaseOp<VectorType, VectorType, VectorType>> DR;
    if ( Configuration.useCylinderBarrier ) {
      R.reset( new ForceCylinderLogBarrier<ConfiguratorType>( m_Topology, m_refGeometry,
                                                              Configuration.cylinderHeightDimension,
                                                              Configuration.barrierValue,
                                                              Configuration.cylinderHeightFactor *
                                                              Configuration.barrierValue,
                                                              false, true, false,
                                                              m_UndefIsParameter | m_UndefIsControl, false,
                                                              m_MaterialIsParameter | m_MaterialIsControl ));
      DR.reset( new ForceCylinderLogBarrierGradient<ConfiguratorType>( m_Topology, m_refGeometry,
                                                                       Configuration.cylinderHeightDimension,
                                                                       Configuration.barrierValue,
                                                                       Configuration.cylinderHeightFactor *
                                                                       Configuration.barrierValue,
                                                                       false, true, false,
                                                                       m_UndefIsParameter | m_UndefIsControl, false,
                                                                       m_MaterialIsParameter | m_MaterialIsControl ));
    }
    else {
      R.reset( new ForceL2LogBarrier<ConfiguratorType>( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                        false, true, false,
                                                        m_UndefIsParameter | m_UndefIsControl, false,
                                                        m_MaterialIsParameter | m_MaterialIsControl ));
      DR.reset( new ForceL2LogBarrierGradient<ConfiguratorType>( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                                 false, true, false,
                                                                 m_UndefIsParameter | m_UndefIsControl, false,
                                                                 m_MaterialIsParameter | m_MaterialIsControl ));
    }

    VectorType weights( 2 );
    weights << Configuration.objectiveWeight, -Configuration.regularizationWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, C, *R );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DC, *DR );

    ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( J, m_SES );
    ReducedPDECParametrizedGradient<ConfiguratorType> DJred( J, DJ, m_SES, m_dirichletBoundary );

    VectorType emptyVector;

    ReducedForceFunctional<ConfiguratorType> Jld( m_Topology, Jred, m_forceBasis, m_dirichletBoundary,
                                                  emptyVector, emptyVector );
    ReducedForceGradient<ConfiguratorType> DJld( m_Topology, DJred, m_forceBasis, m_dirichletBoundary,
                                                 emptyVector, emptyVector );


    SimpleNegativeOp<VectorType, RealType> nJ( Jld );
    SimpleNegativeOp<VectorType, VectorType> nDJ( DJld );

    LinearForceOptHessian<ConfiguratorType, DeformationType> nD2J( m_Topology, m_refGeometry, m_dirichletBoundary,
                                                                   m_undefDisplacement, m_faceMaterial, m_forceBasis,
                                                                   m_forceCoordinates, m_UndefIsControl,
                                                                   m_MaterialIsControl, m_UndefIsParameter,
                                                                   m_MaterialIsParameter );

    nD2J.Configuration.maxNumIterations = Configuration.maxNumIterations;
    nD2J.Configuration.optimalityTolerance = Configuration.optimalityTolerance;
    nD2J.Configuration.stepsize = Configuration.stepsize;
    nD2J.Configuration.maximalStepsize = Configuration.maximalStepsize;
    nD2J.Configuration.stepsizeController = Configuration.stepsizeController;
    nD2J.Configuration.acceleration = Configuration.acceleration;
    nD2J.Configuration.useL2Constraint = Configuration.useL2Constraint;
    nD2J.Configuration.barrierValue = Configuration.barrierValue;
    nD2J.Configuration.regularizationWeight = Configuration.regularizationWeight;
    nD2J.Configuration.objectiveWeight = Configuration.objectiveWeight;
    nD2J.Configuration.useCylinderBarrier = Configuration.useCylinderBarrier;
    nD2J.Configuration.cylinderHeightFactor = Configuration.cylinderHeightFactor;
    nD2J.Configuration.cylinderHeightDimension = Configuration.cylinderHeightDimension;


//    VectorValuedDerivativeTester<ConfiguratorType>(nDJ, nD2J, 1e-6).plotAllDirections(m_forceCoordinates, "testnD2J_");

//    {
//      ReducedPDECParametrizedFunctional<ConfiguratorType> Cred( C, m_SES );
//      ReducedPDECParametrizedFunctional<ConfiguratorType> Rred( *R, m_SES );
//      if(Cred(m_vertexForces) <= 1.e-8) {
//        std::cout << " .. Resetting force due too small init value." << std::endl;
//        m_forceCoordinates.setConstant( 1e-5);
//
//        m_vertexForces = m_forceBasis * m_forceCoordinates;
//
////        std::cout << " .... C = " << Cred(m_vertexForces) << " || R = " << Rred(m_vertexForces) << std::endl;
//      }
//    }


    std::vector<RealType> x_l(m_forceCoordinates.size(), -std::numeric_limits<RealType>::infinity());
    std::vector<RealType> x_u(m_forceCoordinates.size(), std::numeric_limits<RealType>::infinity());
//      GradientDescent<DefaultConfigurator> Solver( nJ, nDJ, Configuration.maxNumIterations,
//                                                   Configuration.optimalityTolerance, Configuration.stepsizeController,
//                                                   SHOW_ALL, 0.1, 1.e-6, Configuration.maximalStepsize );
//      QuasiNewtonBFGS<DefaultConfigurator> Solver( nJ, nDJ, Configuration.maxNumIterations,
//                                                   Configuration.optimalityTolerance, Configuration.stepsizeController, 50,
//                                                   SHOW_ALL, 0.1, 1.e-6, Configuration.maximalStepsize );
//    NewtonOptimizationMethod<ConfiguratorType> Solver( nJ, nDJ, nD2J, Configuration.maxNumIterations,
//                                                       Configuration.optimalityTolerance, NEWTON_OPTIMAL, SHOW_ALL, 0.1,
//                                                       1e-10, Configuration.maximalStepsize );
    LineSearchNewton<ConfiguratorType> Solver( nJ, nDJ, nD2J, Configuration.optimalityTolerance,
                                               Configuration.maxNumIterations, SHOW_TERMINATION_INFO );
    Solver.setParameter( "maximal_stepsize", Configuration.maximalStepsize );
    Solver.setParameter( "direction_beta", 1.e-3 );
    Solver.setParameter( "tau_increase", 5. );
//
//      IpoptBoxConstraintFirstOrderSolver<DefaultConfigurator> Solver(nJ, nDJ, Configuration.maxNumIterations,
//                                                                     Configuration.optimalityTolerance,x_l, x_u );
    auto t_start = std::chrono::high_resolution_clock::now();
    Solver.solve( m_forceCoordinates, m_forceCoordinates );
    auto t_end = std::chrono::high_resolution_clock::now();

    m_inverseHessian = FullMatrixType(nD2J(m_forceCoordinates)).inverse();


    m_vertexForces = m_forceBasis * m_forceCoordinates;

    {
      ReducedPDECParametrizedFunctional<ConfiguratorType> Cred( C, m_SES );
      ReducedPDECParametrizedFunctional<ConfiguratorType> Rred( *R, m_SES );
//      std::cout << std::scientific << std::setprecision(6) << " .... C = " << Cred(m_vertexForces) << " || R = " << Rred(m_vertexForces) << std::endl;
    }

//    applyMaskToVector(m_dirichletBoundary, m_vertexForces);

    m_solvedAccurately = true;
    m_updateDeformation = false;
  }



  /**
   * \brief Compute linearization of the state equation
   */
//  void buildHessian() {
//    m_W.applyDefHessian( m_refGeometry + m_undefDisplacement, m_refGeometry + m_defDisplacement, m_Hessian );
//    applyMaskToSymmetricMatrix( m_dirichletBoundary, m_Hessian );
//    m_HessianSolver.prepareSolver( m_Hessian );
//
//    m_updateHessian = false;
//  }

  bool updateUndeformed( const VectorType &undefDisplacement ) const {
    if (( m_undefDisplacement - undefDisplacement ).norm() > updateTolerance ) {
      m_undefDisplacement = undefDisplacement;
      return true;
    }
    else {
      return false;
    }

  }


  bool updateMaterial( const VectorType &vertexMaterial ) const {
    if (( m_faceMaterial - vertexMaterial ).norm() > updateTolerance ) {
      m_faceMaterial = vertexMaterial;
      m_SES.updateParameters( m_faceMaterial );
      m_W.setParameters( m_faceMaterial );
      return true;
    }
    else {
      return false;
    }

  }

};


#endif //BILEVELSHAPEOPT_LINEAROPTIMALFORCESOLVER_H
