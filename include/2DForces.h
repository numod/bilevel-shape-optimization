//
// Created by josua on 16.07.20.
//

#ifndef BILEVELSHAPEOPT_2DFORCES_H
#define BILEVELSHAPEOPT_2DFORCES_H

#include "Optimization/StochasticOperators.h"
#include <goast/Core/Topology.h>

template<typename ConfiguratorType>
class TwoDForceFunctional
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;

  using ObjectiveOpType = ParametrizedBaseOp<VectorType, RealType, VectorType>;

  const int m_numVertices;
  ObjectiveOpType &m_F;
  const std::vector<int> &m_boundaryMask;
  const RealType m_force_z;

  mutable VectorType m_forces;

public:
  TwoDForceFunctional( const MeshTopologySaver &Topology, ObjectiveOpType &F, const RealType force_z,
                       const std::vector<int> &BoundaryMask )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_force_z( force_z ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()) {
    m_forces.tail( m_numVertices ).array() = m_force_z;
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    assert( Arg.size() == 2 && "TwoDForceFunctional::apply: Wrong size of input!" );

    m_forces.segment( 0, m_numVertices ).array() = Arg[0];
    m_forces.segment( m_numVertices, m_numVertices ).array() = Arg[1];

    m_F.apply( m_forces, Dest );
  }

  void setParameters( const VectorType &parameters ) override {
    m_F.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_F.getParameters();
  }

};

template<typename ConfiguratorType>
class TwoDForceGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;

  using ObjectiveGradType = ParametrizedBaseOp<VectorType, VectorType, VectorType>;

  const int m_numVertices;
  ObjectiveGradType &m_DF;
  const std::vector<int> &m_boundaryMask;
  const RealType m_force_z;

  mutable VectorType m_forces;
  mutable VectorType m_fullGradient;

  MatrixType reductionMatrix;
public:
  TwoDForceGradient( const MeshTopologySaver &Topology, ObjectiveGradType &DF, const RealType force_z,
                     const std::vector<int> &BoundaryMask )
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_force_z( force_z ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()) {
    m_forces.tail( m_numVertices ).array() = m_force_z;

    reductionMatrix.resize( 2, 3 * m_numVertices );
    for ( int i = 0; i < m_numVertices; i++ ) {
      if ( std::find( BoundaryMask.begin(), BoundaryMask.end(), i ) != BoundaryMask.end())
        continue;
      reductionMatrix.coeffRef( 0, i ) = 1.;
      reductionMatrix.coeffRef( 1, m_numVertices + i ) = 1.;
    }
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    assert( Arg.size() == 2 && "TwoDForceFunctional::apply: Wrong size of input!" );

    m_forces.segment( 0, m_numVertices ).array() = Arg[0];
    m_forces.segment( m_numVertices, m_numVertices ).array() = Arg[1];

    m_DF.apply( m_forces, m_fullGradient );

    Dest.resize(2);
    Dest = reductionMatrix * m_fullGradient;
  }

  void setParameters( const VectorType &parameters ) override {
    m_DF.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_DF.getParameters();
  }

};

#endif //BILEVELSHAPEOPT_2DFORCES_H
