#ifndef BILEVELSHAPEOPT_FREEENERGY_HH
#define BILEVELSHAPEOPT_FREEENERGY_HH


#include <goast/Core.h>
#include <goast/Core/FEM.h>


/**
 * \brief Total free energy
 * \author Echelmeyer, Sassen
 */
template<typename ConfiguratorType>
class FreeEnergy : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  // Problem
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;
  const VectorType &m_undefDisplacement;
  const DeformationBase<ConfiguratorType> &m_W;
  const VectorType &m_vertexForces;
  const int m_numVertices;

  // Cache
  MatrixType m_massMatrix;

public:

  FreeEnergy( const MeshTopologySaver &topology,
              const VectorType &refGeometry,
              const VectorType &displacement,
              const DeformationBase<ConfiguratorType> &W,
              const VectorType &vertexForces ) : m_Topology( topology ), m_refGeometry( refGeometry ),
                                                 m_undefDisplacement( displacement ), m_W( W ),
                                                 m_vertexForces( vertexForces ),
                                                 m_numVertices( m_Topology.getNumVertices()) {
    computeLumpedMassMatrix<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, m_massMatrix );
  }


  void apply( const VectorType &Arg, RealType &Dest ) const {
    assert( Arg.size() == 3 * m_numVertices && "FreeEnergy: wrong dimension of argument!" );

    VectorType defGeometry = m_refGeometry + Arg;

    m_W.applyEnergy( m_refGeometry + m_undefDisplacement, defGeometry, Dest );

    Dest -= defGeometry.transpose() * m_massMatrix * m_vertexForces;
  }
};

template<typename ConfiguratorType>
class FreeEnergyGradient : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  // Problem
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;
  const VectorType &m_undefDisplacement;
  const DeformationBase<ConfiguratorType> &m_W;
  const VectorType &m_vertexForces;

  const int m_numVertices;

  // Cache
  MatrixType m_massMatrix;

public:

  FreeEnergyGradient( const MeshTopologySaver &topology,
                      const VectorType &refGeometry,
                      const VectorType &displacement,
                      const DeformationBase<ConfiguratorType> &W,
                      const VectorType &vertexForces ) : m_Topology( topology ),
                                                         m_refGeometry( refGeometry ),
                                                         m_undefDisplacement( displacement ),
                                                         m_W( W ), m_vertexForces( vertexForces ),
                                                         m_numVertices( m_Topology.getNumVertices()) {
    computeLumpedMassMatrix<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, m_massMatrix );
  }


  void apply( const VectorType &Arg, VectorType &Dest ) const {
    assert( Arg.size() == 3 * m_numVertices && "FreeEnergyGradient: wrong dimension of argument!" );

    VectorType defGeometry = m_refGeometry + Arg;
    VectorType undefGeometry = m_refGeometry + m_undefDisplacement;

    Dest.resize( 3 * m_numVertices );

    m_W.applyDefGradient( undefGeometry, defGeometry, Dest );

    Dest -= m_massMatrix * m_vertexForces;
  }
};

/**
 * \brief Hessian of total free energy
 * \sa FreeEnergy
 */
template<typename ConfiguratorType>
class FreeEnergyHessian
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {
public:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

protected:
  // Problem
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;
  const VectorType &m_undefDisplacement;
  const DeformationBase<ConfiguratorType> &m_W;

  const int m_numVertices;


public:

  FreeEnergyHessian( const MeshTopologySaver &topology,
                     const VectorType &refGeometry,
                     const VectorType &displacement,
                     const DeformationBase<ConfiguratorType> &W ) : m_Topology( topology ),
                                                                    m_refGeometry( refGeometry ),
                                                                    m_undefDisplacement( displacement ),
                                                                    m_W( W ),
                                                                    m_numVertices( m_Topology.getNumVertices()) {
  }


  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    assert( Arg.size() == 3 * m_numVertices && "FreeEnergyHessian: wrong dimension of argument!" );

    VectorType defGeometry = m_refGeometry + Arg;
    m_W.applyDefHessian( m_refGeometry + m_undefDisplacement, defGeometry, Dest );
  }
};

//=============================================================================
#endif // BILEVELSHAPEOPT_FREEENERGY_HH ended
//=============================================================================