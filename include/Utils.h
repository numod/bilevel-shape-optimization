//
// Created by josua on 16.07.20.
//

#ifndef BILEVELSHAPEOPT_UTILS_H
#define BILEVELSHAPEOPT_UTILS_H


//====================================================================================================================================
//=================================================== Projections on Box and Ball ====================================================
//====================================================================================================================================

/**
 * \author Sassen
 * \brief Get coordinates of point which are on boundary of box
 * \param p points
 * \param lowerBounds lower bounds of the box
 * \param upperBounds upper bounds of the box
 * \param eps (optional) tolerance for check
 * \return list of bounds active for p
 * \note If a coordinate is outside the given bounds it is _not_ counted as active!
 */
template<typename VectorType>
std::vector<int> activeBounds( const VectorType &p, const VectorType &lowerBounds, const VectorType &upperBounds,
                               const typename VectorType::Scalar eps =
                               std::numeric_limits<typename VectorType::Scalar>::epsilon() * 10 ) {
  assert( p.size() == lowerBounds.size());
  assert( p.size() == upperBounds.size());

  std::vector<int> activeSet;

  for ( int i = 0; i < p.size(); i++ ) {
    if ( std::abs( p[i] - lowerBounds[i] ) < eps || std::abs( p[i] - upperBounds[i] ) < eps )
      activeSet.push_back( i );
  }

  return activeSet;
}

/**
 * \author Sassen
 * \brief Project point onto coordinate box
 * \param p point, will be replaced by projection
 * \param lowerBounds lower bounds of the box
 * \param upperBounds upper bounds of the box
 */
template<typename VectorType>
void projectPointOnBox( VectorType &p, const VectorType &lowerBounds, const VectorType &upperBounds ) {
  p.array() = p.array().max( lowerBounds.array());
  p.array() = p.array().min( upperBounds.array());
}

/**
 * \author Sassen
 * \brief Project direction onto coordinate box, i.e. remove components of the direction pointing outwards the box for which the point is on the boundary
 * \param d direction, will be modified
 * \param p point
 * \param lowerBounds lower bounds of the box
 * \param upperBounds upper bounds of the box
 */
template<typename VectorType>
void projectDirectionOnBox( VectorType &d, const VectorType &p,
                            const VectorType &lowerBounds, const VectorType &upperBounds,
                            const typename VectorType::Scalar eps =
                            std::numeric_limits<typename VectorType::Scalar>::epsilon() * 10 ) {
  for ( int i = 0; i < p.size(); i++ ) {
    if (( std::abs( p[i] - lowerBounds[i] ) < eps && d[i] < 0 ) ||
        ( std::abs( p[i] - upperBounds[i] ) < eps && d[i] > 0 ))
      d[i] = 0;
  }
}

/**
 * \author Sassen
 * \brief Project gradient onto coordinate box, i.e. remove components of the direction pointing inwards the box for which the point is on the boundary
 * \param d direction, will be modified
 * \param p point
 * \param lowerBounds lower bounds of the box
 * \param upperBounds upper bounds of the box
 */
template<typename VectorType>
void projectGradientOnBox( VectorType &d, const VectorType &p,
                           const VectorType &lowerBounds, const VectorType &upperBounds,
                           const typename VectorType::Scalar eps =
                           std::numeric_limits<typename VectorType::Scalar>::epsilon() * 10 ) {
  for ( int i = 0; i < p.size(); i++ ) {
    if (( std::abs( p[i] - lowerBounds[i] ) < eps && d[i] > 0. ) ||
        ( std::abs( p[i] - upperBounds[i] ) < eps && d[i] < 0. ))
      d[i] = 0.;
  }
}

/**
 * \author Echelmeyer
 * \brief Project point onto ball with given radius
 * \param p point, will be replaced by projection
 * \param radius of the ball
 */
template<typename VectorType>
void projectPointOnBall( VectorType &p, const typename VectorType::Scalar radius) {
  using RealType = typename VectorType::Scalar;
//  if (fixedVariables) {
//    RealType fullSquaredNorm = p.squaredNorm();
//    RealType subSquaredNorm = 0;
//    for ( int idx : *fixedVariables )
//      subSquaredNorm += p[idx] * p[idx];
//    RealType scalingFactor =
//  }

  if ( p.norm() > radius ) {
    p = ( radius / p.norm()) * p;
  }
}

/**
 * \author Echelmeyer
 * \brief Project gradient onto the ball, i.e. remove components pointing outwards the box
 * \param d direction, will be modified
 * \param p point
 * \param radius of the ball
 */
template<typename VectorType>
void projectDirectionOnBall( VectorType &d, const VectorType &p, const typename VectorType::Scalar radius,
                             const typename VectorType::Scalar eps =
                             std::numeric_limits<typename VectorType::Scalar>::epsilon() * 10 ) {
  if ( (p.norm() - radius) > -eps ) {
    d = d - (d.dot( p ) / p.norm()) * p;
  }
}


#endif //BILEVELSHAPEOPT_UTILS_H
