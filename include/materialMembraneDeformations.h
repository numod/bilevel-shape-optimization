//=============================================================================
//
//  CLASS MaterialShellDeformations
//
//=============================================================================


#ifndef MATERIALMEMBRANEDEFORMATIONS_HH
#define MATERIALMEMBRANEDEFORMATIONS_HH


//== INCLUDES =================================================================
#include <goast/Core/Auxiliary.h>
#include <goast/Core/LocalMeshGeometry.h>
#include <goast/Core/Topology.h>
#include <goast/Core/BaseOpInterface.h>

//#define DEBUGMODE
//!==========================================================================================================
//! NONLINEAR HYPERELASTIC MEMBRANE ENERGY
//!==========================================================================================================


//==========================================================================================================
//! \brief Geometric membrane energy.
//! \author Heeren, Echelmeyer
//! The active shell is considered the argument whereas the inactive shell is given in the constructor.
template<typename ConfiguratorType>
class materialNonlinearMembraneEnergy
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_inactiveGeometry;
  const bool _activeShellIsDeformed;
  const VectorType &_faceWeights;
  RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter;

public:

  materialNonlinearMembraneEnergy( const MeshTopologySaver &topology,
                                   const VectorType &InactiveGeometry,
                                   const bool ActiveShellIsDeformed,
                                   const VectorType &faceWeights,
                                   RealType Mu = 1.,
                                   RealType Lambda = 1. )
          : _topology( topology ),
            _inactiveGeometry( InactiveGeometry ),
            _activeShellIsDeformed( ActiveShellIsDeformed ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ) {

  }


  // energy evaluation
  void apply( const VectorType &ActiveGeometry, RealType &Dest ) const {

    if ( ActiveGeometry.size() != _inactiveGeometry.size()) {
      std::cerr << "size of active = " << ActiveGeometry.size() << " vs. size of inactive = "
                << _inactiveGeometry.size() << std::endl;
      throw BasicException( "materialNonlinearMembraneEnergy::apply(): sizes dont match!" );
    }

    const VectorType *defShellP = _activeShellIsDeformed ? &ActiveGeometry : &_inactiveGeometry;
    const VectorType *undefShellP = _activeShellIsDeformed ? &_inactiveGeometry : &ActiveGeometry;

    Dest = 0.;

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));

      // set up deformed vertices and edges
      VecType Ei, Ej, Ek, temp;
      getXYZCoord<VectorType, VecType>( *defShellP, temp, pi );
      getXYZCoord<VectorType, VecType>( *defShellP, Ej, pj );
      getXYZCoord<VectorType, VecType>( *defShellP, Ek, pk );
      Ei = Ek - Ej;
      Ej = temp - Ek;
      Ek = Ei + Ej;

      // compute edge lengths
      RealType liSqr = Ei.normSqr();
      RealType ljSqr = Ej.normSqr();
      RealType lkSqr = Ek.normSqr();
      // compute volume
      temp.makeCrossProduct( Ei, Ej );
      RealType volDefSqr = temp.normSqr() / 4.;

      // check whether area is finite
      if ( std::sqrt( volDefSqr ) < 1e-15 ) {
        std::cerr << "WARNING thrown in materialNonlinearMembraneEnergy! Probably face " << faceIdx
                  << " in deformed mesh is degenerated! " << std::endl;
        Dest = std::numeric_limits<RealType>::infinity();
        return;
      }

      // set up undeformed vertices and edges
      getXYZCoord<VectorType, VecType>( *undefShellP, temp, pi );
      getXYZCoord<VectorType, VecType>( *undefShellP, Ej, pj );
      getXYZCoord<VectorType, VecType>( *undefShellP, Ek, pk );
      Ei = Ek - Ej;
      Ej = temp - Ek;
      Ek = Ei + Ej;

      // compute volume
      temp.makeCrossProduct( Ei, Ej );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      // check whether area is finite
      if ( volUndef < 1e-15 ) {
        std::cerr << "WARNING thrown in materialNonlinearMembraneEnergy! Probably face " << faceIdx
                  << " in undeformed mesh is degenerated! " << std::endl;
        Dest = std::numeric_limits<RealType>::infinity();
        return;
      }
      //CAUTION mind the signs! (Ek is actually -Ek here!)
      RealType traceTerm = ( dotProduct( Ej, Ek ) * liSqr + dotProduct( Ek, Ei ) * ljSqr -
                             dotProduct( Ei, Ej ) * lkSqr );

      // volume of triangle * evaluation of energy density
      Dest += _faceWeights[faceIdx] * (( _mu / 8. * traceTerm + _lambdaQuarter * volDefSqr ) / volUndef -
                                       ( _muHalfPlusLambdaQuarter * std::log( volDefSqr / volUndefSqr ) + _mu +
                                         _lambdaQuarter ) * volUndef );

#ifdef DEBUGMODE
                                                                                                                              if( std::isnan( Dest[0] ) ){
          std::cerr << "NaN in membrane energy in face " << faceIdx << "! " << std::endl;
          if( hasNanEntries(ActiveGeometry) )
            std::cerr << "Argument has NaN entries! " << std::endl;
          else{
            std::cerr << "traceTerm = " << traceTerm << std::endl;
            std::cerr << "volUndefSqr = " << volUndefSqr << std::endl;
            std::cerr << "volDefSqr = " << volDefSqr << std::endl;
          }
          throw BasicException("materialNonlinearMembraneEnergy::apply(): NaN Error!");
      }
#endif
    }
  }

  void setWeight( double Mu ) {
    _mu = Mu;
    _lambdaQuarter = Mu / 4.;
    _muHalfPlusLambdaQuarter = ( _mu / 2. + _lambdaQuarter );
  }
};

//==========================================================================================================
//! \brief Geometric membrane energy with material input
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialInputNonlinearMembraneEnergy
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter;

public:

  materialInputNonlinearMembraneEnergy( const MeshTopologySaver &topology,
                                        const VectorType &undefShell,
                                        const VectorType &defShell,
                                        RealType Mu = 1.,
                                        RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ) {

  }


  // energy evaluation
  void apply( const VectorType &faceWeights, RealType &Dest ) const {

    if ( faceWeights.size() != _topology.getNumFaces()) {
      throw BasicException( "materialInputNonlinearMembraneEnergy::apply(): wrong size of weight vector!" );
    }

    Dest = 0.;

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));

      // set up deformed vertices and edges
      VecType Ei, Ej, Ek, temp;
      getXYZCoord<VectorType, VecType>( _defShell, temp, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Ej, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Ek, pk );
      Ei = Ek - Ej;
      Ej = temp - Ek;
      Ek = Ei + Ej;

      // compute edge lengths
      RealType liSqr = Ei.normSqr();
      RealType ljSqr = Ej.normSqr();
      RealType lkSqr = Ek.normSqr();
      // compute volume
      temp.makeCrossProduct( Ei, Ej );
      RealType volDefSqr = temp.normSqr() / 4.;

      // check whether area is finite
      if ( std::sqrt( volDefSqr ) < 1e-15 ) {
        std::cerr << "WARNING thrown in materialNonlinearMembraneEnergy! Probably face " << faceIdx
                  << " in deformed mesh is degenerated! " << std::endl;
        Dest = std::numeric_limits<RealType>::infinity();
        return;
      }

      // set up undeformed vertices and edges
      getXYZCoord<VectorType, VecType>( _undefShell, temp, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Ej, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Ek, pk );
      Ei = Ek - Ej;
      Ej = temp - Ek;
      Ek = Ei + Ej;

      // compute volume
      temp.makeCrossProduct( Ei, Ej );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      // check whether area is finite
      if ( volUndef < 1e-15 ) {
        std::cerr << "WARNING thrown in materialNonlinearMembraneEnergy! Probably face " << faceIdx
                  << " in undeformed mesh is degenerated! " << std::endl;
        Dest = std::numeric_limits<RealType>::infinity();
        return;
      }
      //CAUTION mind the signs! (Ek is actually -Ek here!)
      RealType traceTerm = ( dotProduct( Ej, Ek ) * liSqr + dotProduct( Ek, Ei ) * ljSqr -
                             dotProduct( Ei, Ej ) * lkSqr );

      // volume of triangle * evaluation of energy density
      Dest += faceWeights[faceIdx] * (( _mu / 8. * traceTerm + _lambdaQuarter * volDefSqr ) / volUndef -
                                         ( _muHalfPlusLambdaQuarter * std::log( volDefSqr / volUndefSqr ) + _mu +
                                           _lambdaQuarter ) * volUndef );

#ifdef DEBUGMODE
                                                                                                                              if( std::isnan( Dest[0] ) ){
          std::cerr << "NaN in membrane energy in face " << faceIdx << "! " << std::endl;
          if( hasNanEntries(ActiveGeometry) )
            std::cerr << "Argument has NaN entries! " << std::endl;
          else{
            std::cerr << "traceTerm = " << traceTerm << std::endl;
            std::cerr << "volUndefSqr = " << volUndefSqr << std::endl;
            std::cerr << "volDefSqr = " << volDefSqr << std::endl;
          }
          throw BasicException("materialNonlinearMembraneEnergy::apply(): NaN Error!");
      }
#endif
    }
  }

  void setWeight( double Mu ) {
    _mu = Mu;
    _lambdaQuarter = Mu / 4.;
    _muHalfPlusLambdaQuarter = ( _mu / 2. + _lambdaQuarter );
  }


};

template<typename ConfiguratorType>
class materialNonlinearMembraneEnergyGradientMat
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter;

public:

  materialNonlinearMembraneEnergyGradientMat( const MeshTopologySaver &topology,
                                              const VectorType &undefShell,
                                              const VectorType &defShell,
                                              RealType Mu = 1.,
                                              RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ) {}


  // energy evaluation
  void apply( const VectorType &faceWeights, VectorType &Dest ) const {

    if ( faceWeights.size() != _topology.getNumFaces()) {
      throw BasicException( "materialNonlinearMembraneEnergyGradientMat::apply(): wrong size of weight vector!" );
    }

    Dest.resize(_topology.getNumFaces());
    Dest.setZero();

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));

      // set up deformed vertices and edges
      VecType Ei, Ej, Ek, temp;
      getXYZCoord<VectorType, VecType>( _defShell, temp, pi );
      getXYZCoord<VectorType, VecType>( _defShell, Ej, pj );
      getXYZCoord<VectorType, VecType>( _defShell, Ek, pk );
      Ei = Ek - Ej;
      Ej = temp - Ek;
      Ek = Ei + Ej;

      // compute edge lengths
      RealType liSqr = Ei.normSqr();
      RealType ljSqr = Ej.normSqr();
      RealType lkSqr = Ek.normSqr();
      // compute volume
      temp.makeCrossProduct( Ei, Ej );
      RealType volDefSqr = temp.normSqr() / 4.;

      // check whether area is finite
      if ( std::sqrt( volDefSqr ) < 1e-15 ) {
        std::cerr << "WARNING thrown in materialNonlinearMembraneEnergy! Probably face " << faceIdx
                  << " in deformed mesh is degenerated! " << std::endl;
        Dest[0] = std::numeric_limits<RealType>::infinity();
        return;
      }

      // set up undeformed vertices and edges
      getXYZCoord<VectorType, VecType>( _undefShell, temp, pi );
      getXYZCoord<VectorType, VecType>( _undefShell, Ej, pj );
      getXYZCoord<VectorType, VecType>( _undefShell, Ek, pk );
      Ei = Ek - Ej;
      Ej = temp - Ek;
      Ek = Ei + Ej;

      // compute volume
      temp.makeCrossProduct( Ei, Ej );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      // check whether area is finite
      if ( volUndef < 1e-15 ) {
        std::cerr << "WARNING thrown in materialNonlinearMembraneEnergy! Probably face " << faceIdx
                  << " in undeformed mesh is degenerated! " << std::endl;
        Dest[0] = std::numeric_limits<RealType>::infinity();
        return;
      }
      //CAUTION mind the signs! (Ek is actually -Ek here!)
      RealType traceTerm = ( dotProduct( Ej, Ek ) * liSqr + dotProduct( Ek, Ei ) * ljSqr -
                             dotProduct( Ei, Ej ) * lkSqr );

      // Material gradient is 1/3 of normal NonlinearMembraneEnergy, see documentation
      Dest[faceIdx] += (( _mu / 8. * traceTerm + _lambdaQuarter * volDefSqr ) / volUndef -
                   ( _muHalfPlusLambdaQuarter * std::log( volDefSqr / volUndefSqr ) + _mu + _lambdaQuarter ) *
                   volUndef );

#ifdef DEBUGMODE
                                                                                                                              if( std::isnan( Dest[0] ) ){
          std::cerr << "NaN in membrane energy in face " << faceIdx << "! " << std::endl;
          if( hasNanEntries(ActiveGeometry) )
            std::cerr << "Argument has NaN entries! " << std::endl;
          else{
            std::cerr << "traceTerm = " << traceTerm << std::endl;
            std::cerr << "volUndefSqr = " << volUndefSqr << std::endl;
            std::cerr << "volDefSqr = " << volDefSqr << std::endl;
          }
          throw BasicException("materialNonlinearMembraneEnergy::apply(): NaN Error!");
      }
#endif
    }
  }

  void setWeight( double Mu ) {
    _mu = Mu;
    _lambdaQuarter = Mu / 4.;
    _muHalfPlusLambdaQuarter = ( _mu / 2. + _lambdaQuarter );
  }

};

//! \brief Second derivative of membrane energy w.r.t. the deformed configuration.
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneHessianMat
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  const RealType _mu, _lambda, _muHalfPlusLambdaQuarter;
  mutable int _rowOffset, _colOffset;

public:
  materialNonlinearMembraneHessianMat( const MeshTopologySaver &topology,
                                       const VectorType &undefShell,
                                       const VectorType &defShell,
                                       int rowOffset = 0,
                                       int colOffset = 0,
                                       RealType Mu = 1.,
                                       RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ),
            _mu( Mu ),
            _lambda( Lambda ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambda / 4. ),
            _rowOffset( rowOffset ),
            _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  void apply( const VectorType &faceWeights, MatrixType &Dest ) const {
    assembleHessian( faceWeights, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &faceWeights, MatrixType &Hessian ) const {
    int dofs = _topology.getNumFaces();
    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != dofs ))
      Hessian.resize( dofs, dofs );
    Hessian.setZero();
  }


};


//! \brief First derivative of membrane energy w.r.t. the deformed configuration.
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneGradientDef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_faceWeights;
  RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter;

public:
  materialNonlinearMembraneGradientDef( const MeshTopologySaver &topology,
                                        const VectorType &undefShell,
                                        const VectorType &faceWeights,
                                        RealType Mu = 1.,
                                        RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ) {

  }

  //
  void apply( const VectorType &defShell, VectorType &Dest ) const {

    if ( _undefShell.size() != defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << defShell.size() << std::endl;
      throw BasicException( "materialNonlinearMembraneGradientDef::apply(): sizes dont match!" );
    }

    if ( Dest.size() != defShell.size())
      Dest.resize( defShell.size());

    Dest.setZero();

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), defEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( nodes[2] - nodes[1], nodes[0] - nodes[2] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        defEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( nodes[2] - nodes[1], nodes[0] - nodes[2] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      //! trace part of gradient, e_tr = volUndef * _mu/2. *  traceDistTensor
      VecType factors;
      for ( int i = 0; i < 3; i++ )
        factors[i] = -0.25 * _mu * dotProduct( undefEdges[( i + 2 ) % 3], undefEdges[( i + 1 ) % 3] ) / volUndef;
      RealType factor = 2. * ( _lambdaQuarter * volDef / volUndef - _muHalfPlusLambdaQuarter * volUndef / volDef );

      for ( int i = 0; i < 3; i++ ) {
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], temp );
        for ( int j = 0; j < 3; j++ )
          Dest[j * _topology.getNumVertices() + nodesIdx[i]] += _faceWeights[faceIdx] * ( factor * temp[j] +
                                                                                          factors[( i + 1 ) % 3] *
                                                                                          defEdges[( i + 1 ) % 3][j] -
                                                                                          factors[( i + 2 ) % 3] *
                                                                                          defEdges[( i + 2 ) % 3][j] );
      }

    }
  }

  void setWeight( double Mu ) {
    _mu = Mu;
    _lambdaQuarter = Mu / 4.;
    _muHalfPlusLambdaQuarter = ( _mu / 2. + _lambdaQuarter );
  }

};


//! \brief First derivative of membrane energy w.r.t. the deformed configuration.
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class testMaterialNonlinearMembraneGradientDef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter;

public:
  testMaterialNonlinearMembraneGradientDef( const MeshTopologySaver &topology,
                                            const VectorType &undefShell,
                                            const VectorType &defShell,
                                            RealType Mu = 1.,
                                            RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ) {

  }

  //
  void apply( const VectorType &faceWeights, VectorType &Dest ) const {

    if ( _undefShell.size() != _defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << _defShell.size() << std::endl;
      throw std::length_error( "materialNonlinearMembraneGradientDef::apply(): sizes dont match!" );
    }
    if ( faceWeights.size() != _topology.getNumFaces())
      throw std::length_error( "testMaterialNonlinearMembraneGradientDef::apply(): wrong size of weight vector!" );

    if ( Dest.size() != _defShell.size())
      Dest.resize( _defShell.size());

    Dest.setZero();

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), defEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( nodes[2] - nodes[1], nodes[0] - nodes[2] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        defEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( nodes[2] - nodes[1], nodes[0] - nodes[2] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      //! trace part of gradient, e_tr = volUndef * _mu/2. *  traceDistTensor
      VecType factors;
      for ( int i = 0; i < 3; i++ )
        factors[i] = -0.25 * _mu * dotProduct( undefEdges[( i + 2 ) % 3], undefEdges[( i + 1 ) % 3] ) / volUndef;
      RealType factor = 2. * ( _lambdaQuarter * volDef / volUndef - _muHalfPlusLambdaQuarter * volUndef / volDef );

      for ( int i = 0; i < 3; i++ ) {
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], temp );
        for ( int j = 0; j < 3; j++ )
          Dest[j * _topology.getNumVertices() + nodesIdx[i]] += faceWeights[faceIdx] * ( factor * temp[j] +
                                                                                         factors[( i + 1 ) % 3] *
                                                                                         defEdges[( i + 1 ) % 3][j] -
                                                                                         factors[( i + 2 ) % 3] *
                                                                                         defEdges[( i + 2 ) % 3][j] );
      }

    }
  }

  void setWeight( double Mu ) {
    _mu = Mu;
    _lambdaQuarter = Mu / 4.;
    _muHalfPlusLambdaQuarter = ( _mu / 2. + _lambdaQuarter );
  }
};


//! \brief Second derivative of membrane energy w.r.t. the deformed configuration.
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneHessianDef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_faceWeights;
  const RealType _mu, _lambda, _muHalfPlusLambdaQuarter;
  mutable int _rowOffset, _colOffset;

public:
  materialNonlinearMembraneHessianDef( const MeshTopologySaver &topology,
                                       const VectorType &undefShell,
                                       const VectorType &faceWeights,
                                       int rowOffset = 0,
                                       int colOffset = 0,
                                       RealType Mu = 1.,
                                       RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambda( Lambda ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambda / 4. ),
            _rowOffset( rowOffset ),
            _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &defShell, MatrixType &Dest ) const {
    assembleHessian( defShell, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &defShell, MatrixType &Hessian ) const {
    int dofs = 3 * _topology.getNumVertices();
    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != dofs ))
      Hessian.resize( dofs, dofs );
    Hessian.setZero();

    // set up triplet list
    TripletListType tripletList;
    // per face we have 3 active vertices, i.e. 9 combinations each producing a 3x3-matrix
    tripletList.reserve( 9 * 9 * _topology.getNumFaces());

    pushTriplets( defShell, tripletList );

    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &defShell, TripletListType &tripletList, RealType factor = 1. ) const {

    if ( _undefShell.size() != defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << defShell.size() << std::endl;
      throw std::length_error( "materialNonlinearMembraneHessianDef::pushTriplets(): sizes dont match!" );
    }

    // run over all faces
    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), defEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( undefEdges[0], undefEdges[1] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        defEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( defEdges[0], defEdges[1] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      VecType traceFactors;
      for ( int i = 0; i < 3; i++ )
        traceFactors[i] = -0.25 * _mu * dotProduct( undefEdges[( i + 2 ) % 3], undefEdges[( i + 1 ) % 3] ) / volUndef;

      RealType mixedFactor = 0.5 * _lambda / volUndef + 2. * _muHalfPlusLambdaQuarter * volUndef / volDefSqr;
      RealType areaFactor = 0.5 * _lambda * volDef / volUndef - 2. * _muHalfPlusLambdaQuarter * volUndef / volDef;

      // precompute area gradients
      std::vector<VecType> gradArea( 3 );
      for ( int i = 0; i < 3; i++ )
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], gradArea[i] );

      // compute local matrices
      MatType tensorProduct, H, auxMat;

      // i==j
      for ( int i = 0; i < 3; i++ ) {
        getHessAreaKK( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], auxMat );
        tensorProduct.makeTensorProduct( gradArea[i], gradArea[i] );
        getWeightedMatrixSum( areaFactor, auxMat, mixedFactor, tensorProduct, H );
        H.addToDiagonal( traceFactors[( i + 1 ) % 3] + traceFactors[( i + 2 ) % 3] );
        localToGlobal( tripletList, nodesIdx[i], nodesIdx[i], H, factor * _faceWeights[faceIdx] );
      }

      // i!=j
      for ( int i = 0; i < 3; i++ ) {
        getHessAreaIK( nodes[i], nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], auxMat );
        tensorProduct.makeTensorProduct( gradArea[i], gradArea[( i + 2 ) % 3] );
        getWeightedMatrixSum( areaFactor, auxMat, mixedFactor, tensorProduct, H );
        H.addToDiagonal( -traceFactors[( i + 1 ) % 3] );
        localToGlobal( tripletList, nodesIdx[i], nodesIdx[( i + 2 ) % 3], H, factor * _faceWeights[faceIdx] );
      }

    }
  }

protected:
  void
  localToGlobal( TripletListType &tripletList, int k, int l, const MatType &localMatrix, const RealType weight ) const {
    int numV = _topology.getNumVertices();
    for ( int i = 0; i < 3; i++ )
      for ( int j = 0; j < 3; j++ )
        tripletList.emplace_back( _rowOffset + i * numV + k, _colOffset + j * numV + l, weight * localMatrix( i, j ));

    if ( k != l ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.emplace_back( _rowOffset + i * numV + l, _colOffset + j * numV + k, weight * localMatrix( j, i ));
    }
  }

};


//! \brief First derivative of geometric membrane energy w.r.t. the undeformed configuration (cf. class NonlinearMembraneEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneGradientUndef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;

  const MeshTopologySaver &_topology;
  const VectorType &_defShell;
  const VectorType &_faceWeights;
  const RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter, _const;

public:
  materialNonlinearMembraneGradientUndef( const MeshTopologySaver &topology,
                                          const VectorType &defShell,
                                          const VectorType &faceWeights,
                                          RealType Mu = 1.,
                                          RealType Lambda = 1. )
          : _topology( topology ),
            _defShell( defShell ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ),
            _const( _mu + _lambdaQuarter ) {

  }

//
  void apply( const VectorType &undefShell, VectorType &Dest ) const {

    if ( Dest.size() != undefShell.size())
      Dest.resize( undefShell.size());

    Dest.setZero();

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), fixedEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get fixed edgess
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        fixedEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];

      // compute volume
      temp.makeCrossProduct( nodes[1] - nodes[0], nodes[2] - nodes[1] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      VecType defLengthSqr;
      for ( int i = 0; i < 3; i++ )
        defLengthSqr[i] = fixedEdges[i].normSqr();

      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];

      // compute volume
      temp.makeCrossProduct( nodes[1] - nodes[0], nodes[2] - nodes[1] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      RealType traceTerm = 0.;
      for ( int i = 0; i < 3; i++ )
        traceTerm -= dotProduct( undefEdges[( i + 1 ) % 3], undefEdges[( i + 2 ) % 3] ) * defLengthSqr[i];

      RealType factor1 = ( 0.125 * _mu * traceTerm + _lambdaQuarter * volDefSqr ) / volUndefSqr;
      RealType factor2 =
              _muHalfPlusLambdaQuarter * std::log( volDefSqr / volUndefSqr ) + _const - 2 * _muHalfPlusLambdaQuarter;
      RealType factorAreaGrad = factor1 + factor2;
      RealType factorTraceGrad = 0.125 * _mu / volUndef;

      std::vector<VecType> gradTrace( 3 );
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          gradTrace[i][j] = defLengthSqr[i] * ( undefEdges[( i + 1 ) % 3][j] - undefEdges[( i + 2 ) % 3][j] ) +
                            undefEdges[i][j] * ( defLengthSqr[( i + 1 ) % 3] - defLengthSqr[( i + 2 ) % 3] );

      // E = (0.125 * _mu * traceTerm + _lambdaQuarter * volDefSqr ) / volUndef;
      // grad E[j] = 0.125 * _mu * grad traceTerm[j] / volUndef  - (0.125 * _mu * traceTerm + _lambdaQuarter * volDefSqr )  grad volUndef[j] / volUndef^2
      for ( int i = 0; i < 3; i++ ) {
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], temp );
        for ( int j = 0; j < 3; j++ )
          Dest[j * _topology.getNumVertices() + nodesIdx[i]] +=
                  _faceWeights[faceIdx] * ( factorTraceGrad * gradTrace[i][j] - factorAreaGrad * temp[j] );
      }

    }
  }
};


//! \brief Second derivative of geometric membrane energy w.r.t. the undeformed configuration (cf. class NonlinearMembraneEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneHessianUndef
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_defShell;
  const VectorType &_faceWeights;
  const RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter, _const;
  mutable int _rowOffset, _colOffset;

public:
  materialNonlinearMembraneHessianUndef( const MeshTopologySaver &topology,
                                         const VectorType &defShell,
                                         const VectorType &faceWeights,
                                         int rowOffset = 0,
                                         int colOffset = 0,
                                         RealType Mu = 1.,
                                         RealType Lambda = 1. )
          : _topology( topology ),
            _defShell( defShell ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ),
            _const( _mu + _lambdaQuarter ),
            _rowOffset( rowOffset ),
            _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &undefShell, MatrixType &Dest ) const {
    int dofs = 3 * _topology.getNumVertices();
    if (( Dest.rows() != dofs ) || ( Dest.cols() != dofs ))
      Dest.resize( dofs, dofs );
    Dest.setZero();
    assembleHessian( undefShell, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &undefShell, MatrixType &Hessian ) const {

    // set up triplet list
    TripletListType tripletList;
    // per face we have 3 active vertices, i.e. 9 combinations each producing a 3x3-matrix
    tripletList.reserve( 9 * 9 * _topology.getNumFaces());

    pushTriplets( undefShell, tripletList );
    // fill matrix from triplets
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &undefShell, TripletListType &tripletList, RealType factor = 1. ) const {
    // run over all faces
    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), fixedEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get fixed edgess
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        fixedEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];

      // compute volume
      temp.makeCrossProduct( nodes[1] - nodes[0], nodes[2] - nodes[1] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      VecType defLengthSqr;
      for ( int i = 0; i < 3; i++ )
        defLengthSqr[i] = fixedEdges[i].normSqr();

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( undefEdges[0], undefEdges[1] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      RealType traceTerm = 0.;
      for ( int i = 0; i < 3; i++ )
        traceTerm -= dotProduct( undefEdges[( i + 1 ) % 3], undefEdges[( i + 2 ) % 3] ) * defLengthSqr[i];

      std::vector<VecType> gradTrace( 3 );
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          gradTrace[i][j] = defLengthSqr[i] * ( undefEdges[( i + 1 ) % 3][j] - undefEdges[( i + 2 ) % 3][j] ) +
                            undefEdges[i][j] * ( defLengthSqr[( i + 1 ) % 3] - defLengthSqr[( i + 2 ) % 3] );

      // precompute area gradients
      std::vector<VecType> gradArea( 3 );
      for ( int i = 0; i < 3; i++ )
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], gradArea[i] );

      RealType areaFactor = 0.125 * _mu * traceTerm + _lambdaQuarter * volDefSqr;
      RealType negHessAreaFactor = _muHalfPlusLambdaQuarter * ( std::log( volDefSqr / volUndefSqr ) - 2. ) + _const +
                                   areaFactor / volUndefSqr;
      RealType mixedAreaFactor = 2 * ( areaFactor / volUndefSqr + _muHalfPlusLambdaQuarter ) / volUndef;
      RealType mixedFactor = -0.125 * _mu / volUndefSqr;
      RealType hessTraceFactor = 0.125 * _mu / volUndef;


      // compute local matrices
      MatType tensorProduct, H, auxMat;

      // i==j
      for ( int i = 0; i < 3; i++ ) {
        getHessAreaKK( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], H );
        H *= -1. * negHessAreaFactor;

        tensorProduct.makeTensorProduct( gradArea[i], gradTrace[i] );
        H.addMultiple( tensorProduct, mixedFactor );
        tensorProduct.makeTensorProduct( gradTrace[i], gradArea[i] );
        H.addMultiple( tensorProduct, mixedFactor );

        tensorProduct.makeTensorProduct( gradArea[i], gradArea[i] );
        H.addMultiple( tensorProduct, mixedAreaFactor );

        H.addToDiagonal( hessTraceFactor * 2 * defLengthSqr[i] );
        localToGlobal( tripletList, nodesIdx[i], nodesIdx[i], H, factor * _faceWeights[faceIdx] );
      }

      // i!=j
      for ( int i = 0; i < 3; i++ ) {
        getHessAreaIK( nodes[i], nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], H );
        H *= -1. * negHessAreaFactor;

        tensorProduct.makeTensorProduct( gradArea[i], gradTrace[( i + 2 ) % 3] );
        H.addMultiple( tensorProduct, mixedFactor );
        tensorProduct.makeTensorProduct( gradTrace[i], gradArea[( i + 2 ) % 3] );
        H.addMultiple( tensorProduct, mixedFactor );

        tensorProduct.makeTensorProduct( gradArea[i], gradArea[( i + 2 ) % 3] );
        H.addMultiple( tensorProduct, mixedAreaFactor );

        H.addToDiagonal(
                hessTraceFactor * ( defLengthSqr[( i + 1 ) % 3] - defLengthSqr[i] - defLengthSqr[( i + 2 ) % 3] ));
        localToGlobal( tripletList, nodesIdx[i], nodesIdx[( i + 2 ) % 3], H, factor * _faceWeights[faceIdx] );
      }

    }
  }

  //membrane energy scales with delta
  const VectorType setFaceWeights() {
    VectorType faceWeights( _topology.getNumFaces());
    RealType tmp = 0;
    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {
      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));

      faceWeights[faceIdx] = ( _faceWeights[pi] + _faceWeights[pj] + _faceWeights[pk] ) / 3;
    }

    return faceWeights;
  }

protected:
  void
  localToGlobal( TripletListType &tripletList, int k, int l, const MatType &localMatrix, const RealType weight ) const {
    int numV = _topology.getNumVertices();
    for ( int i = 0; i < 3; i++ )
      for ( int j = 0; j < 3; j++ )
        tripletList.push_back(
                TripletType( _rowOffset + i * numV + k, _colOffset + j * numV + l, weight * localMatrix( i, j )));

    if ( k != l ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.push_back(
                  TripletType( _rowOffset + i * numV + l, _colOffset + j * numV + k, weight * localMatrix( j, i )));
    }
  }

};


//! \brief Second (mixed) derivative of geometric membrane energy (cf. class NonlinearMembraneEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneHessianMixed
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_inactiveGeometry;
  const VectorType &_faceWeights;
  const bool _activeShellIsDeformed, _firstDerivWRTDef;
  const RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter, _const;
  mutable int _rowOffset, _colOffset;

public:
  materialNonlinearMembraneHessianMixed( const MeshTopologySaver &topology,
                                         const VectorType &InactiveGeometry,
                                         const bool ActiveShellIsDeformed,
                                         const bool FirstDerivWRTDef,
                                         const VectorType &faceWeights,
                                         int rowOffset = 0,
                                         int colOffset = 0,
                                         RealType Mu = 1.,
                                         RealType Lambda = 1. )
          : _topology( topology ),
            _inactiveGeometry( InactiveGeometry ),
            _activeShellIsDeformed( ActiveShellIsDeformed ),
            _firstDerivWRTDef( FirstDerivWRTDef ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ),
            _const( _mu + _lambdaQuarter ),
            _rowOffset( rowOffset ),
            _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &ActiveGeometry, MatrixType &Dest ) const {
    int dofs = 3 * _topology.getNumVertices();
    if ( dofs != ActiveGeometry.size())
      throw std::length_error( "materialNonlinearMembraneHessianMixed::apply: sizes dont match!" );
    if (( Dest.rows() != dofs ) || ( Dest.cols() != dofs ))
      Dest.resize( dofs, dofs );
    assembleHessian( ActiveGeometry, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &ActiveGeometry, MatrixType &Hessian ) const {

    // set up triplet list
    TripletListType tripletList;
    // per face we have 3 active vertices, i.e. 9 combinations each producing a 3x3-matrix
    tripletList.reserve( 9 * 9 * _topology.getNumFaces());

    pushTriplets( ActiveGeometry, tripletList );

    // fill matrix from triplets
    Hessian.setZero();
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &ActiveGeometry, TripletListType &tripletList, RealType factor = 1. ) const {

    const VectorType *undefShellP = _activeShellIsDeformed ? &_inactiveGeometry : &ActiveGeometry;
    const VectorType *defShellP = _activeShellIsDeformed ? &ActiveGeometry : &_inactiveGeometry;

    // run over all faces
    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), defEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );


      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( *undefShellP, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];

      // compute volume
      temp.makeCrossProduct( undefEdges[0], undefEdges[1] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      // precompute undeformed area gradients
      std::vector<VecType> gradUndefArea( 3 );
      for ( int i = 0; i < 3; i++ )
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], gradUndefArea[i] );

      //
      VecType factors;
      for ( int i = 0; i < 3; i++ )
        factors[i] = dotProduct( undefEdges[( i + 1 ) % 3], undefEdges[( i + 2 ) % 3] );

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( *defShellP, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        defEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];

      // compute volume
      temp.makeCrossProduct( nodes[1] - nodes[0], nodes[2] - nodes[1] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      // precomputed deformed trace gradients
      std::vector<VecType> gradDefTrace( 3 );
      for ( int i = 0; i < 3; i++ )
        getWeightedVectorSum<RealType>( -2 * factors[( i + 1 ) % 3], defEdges[( i + 1 ) % 3],
                                        2 * factors[( i + 2 ) % 3], defEdges[( i + 2 ) % 3], gradDefTrace[i] );

      // precompute deformed area gradients
      std::vector<VecType> gradDefArea( 3 );
      for ( int i = 0; i < 3; i++ )
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], gradDefArea[i] );

      // compute local matrices
      MatType tensorProduct, H, auxMat;
      RealType mixedTraceHessFactor = 0.125 * _mu / volUndef;
      RealType MixedAreaFactor =
              -2. * ( _lambdaQuarter * volDef / volUndef + _muHalfPlusLambdaQuarter * volUndef / volDef ) / volUndef;
      RealType MixedFactor = -0.125 * _mu / volUndefSqr;

      // i!=j
      for ( int i = 0; i < 3; i++ ) {
        for ( int j = 0; j < 3; j++ ) {
          // Hess trace term
          if ( i == j ) {
            H.makeTensorProduct( undefEdges[i], defEdges[i] );
          }
          else {
            int k = ( 2 * i + 2 * j ) % 3;
            H.makeTensorProduct( undefEdges[j] - undefEdges[k], defEdges[i] );
            auxMat.makeTensorProduct( undefEdges[i], defEdges[k] );
            H += auxMat;
          }
          H *= -2 * mixedTraceHessFactor;

          // mixed area term
          tensorProduct.makeTensorProduct( gradUndefArea[i], gradDefArea[j] );
          H.addMultiple( tensorProduct, MixedAreaFactor );

          // mixed term
          tensorProduct.makeTensorProduct( gradUndefArea[i], gradDefTrace[j] );
          H.addMultiple( tensorProduct, MixedFactor );

          localToGlobal( tripletList, nodesIdx[i], nodesIdx[j], H, factor * _faceWeights[faceIdx] );
        }
      }

    }
  }

protected:
  void localToGlobal( TripletListType &tripletList, int k, int l, const MatType &localMatrix,
                      const RealType weight ) const {
    int numV = _topology.getNumVertices();
    if ( !_firstDerivWRTDef ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.emplace_back( _rowOffset + i * numV + k, _colOffset + j * numV + l, weight * localMatrix( i, j ));
    }
    else {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList.emplace_back( _rowOffset + i * numV + l, _colOffset + j * numV + k, weight * localMatrix( j, i ));
    }
  }

};

//! \brief Second (mixed) derivative of simple bending energy wrt. to deformed geometry and weight vector (cf. class NonlinearMembraneEnergy above).
//! \author Heeren, Echelmeyer
template<typename ConfiguratorType>
class materialNonlinearMembraneHessianMixedDefMat
        : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {

  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_defShell;
  const RealType _mu, _lambdaQuarter, _muHalfPlusLambdaQuarter, _const;
  mutable int _rowOffset, _colOffset;

public:
  materialNonlinearMembraneHessianMixedDefMat( const MeshTopologySaver &topology,
                                               const VectorType &undefShell,
                                               const VectorType &defShell,
                                               int rowOffset = 0,
                                               int colOffset = 0,
                                               RealType Mu = 1.,
                                               RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _defShell( defShell ),
            _mu( Mu ),
            _lambdaQuarter( Lambda / 4. ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambdaQuarter ),
            _const( _mu + _lambdaQuarter ),
            _rowOffset( rowOffset ),
            _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
  void apply( const VectorType &faceWeights, MatrixType &Dest ) const {
    int numFaces = _topology.getNumFaces();
    int numVertices = _topology.getNumVertices();

    if ( faceWeights.size() != numFaces )
      throw std::length_error( "materialNonlinearMembraneHessianMixedDefMat::apply: wrong size of weight vector!" );
    if (( Dest.rows() != 3 * numVertices ) || ( Dest.cols() != numFaces ))
      Dest.resize( 3 * numVertices, numFaces );
    assembleHessian( faceWeights, Dest );
  }

  // assmeble Hessian matrix via triplet list
  void assembleHessian( const VectorType &faceWeights, MatrixType &Hessian ) const {
    // set up triplet list
    TripletListType tripletList;
    // per face we have 3 active vertices, i.e. 9 combinations each producing a 3x3-matrix
    tripletList.reserve( 9 * 9 * _topology.getNumFaces());

    pushTriplets( faceWeights, tripletList );

    // fill matrix from triplets
    Hessian.setZero();
    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
  }

  //
  void pushTriplets( const VectorType &faceWeights, TripletListType &tripletList, RealType factor = 1. ) const {

    if ( _undefShell.size() != _defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << _defShell.size() << std::endl;
      throw BasicException( "materialNonlinearMembraneGradientHessianDefMat::apply(): sizes dont match!" );
    }

    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      //indices of adjacent nodes of the triangle
      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));


      std::vector<int> nodesIdx( 3 );
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), defEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( nodes[2] - nodes[1], nodes[0] - nodes[2] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        defEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( nodes[2] - nodes[1], nodes[0] - nodes[2] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      //! trace part of gradient, e_tr = volUndef * _mu/2. *  traceDistTensor
      VecType factors;
      for ( int i = 0; i < 3; i++ )
        factors[i] = -0.25 * _mu * dotProduct( undefEdges[( i + 2 ) % 3], undefEdges[( i + 1 ) % 3] ) / volUndef;
      RealType localFactor =
              factor * 2. * ( _lambdaQuarter * volDef / volUndef - _muHalfPlusLambdaQuarter * volUndef / volDef );

      for ( int i = 0; i < 3; i++ ) {
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], temp );
        for ( int j = 0; j < 3; j++ ) {
          tripletList.emplace_back( _rowOffset + j * _topology.getNumVertices() + nodesIdx[i],
                                    _colOffset + faceIdx,
                                    ( localFactor * temp[j] + factors[( i + 1 ) % 3] * defEdges[( i + 1 ) % 3][j] -
                                      factors[( i + 2 ) % 3] * defEdges[( i + 2 ) % 3][j] ) / 3. );
        }
      }

    }


  }

};

template<typename ConfiguratorType>
class materialNonlinearMembraneThirdDerivDefMat { //: public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType>


protected:
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
  typedef typename ConfiguratorType::TripletType TripletType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::MatType MatType;

  typedef std::vector<TripletType> TripletListType;

  const MeshTopologySaver &_topology;
  const VectorType &_undefShell;
  const VectorType &_faceWeights;
  const RealType _mu, _lambda, _muHalfPlusLambdaQuarter;
  mutable int _rowOffset, _colOffset;

public:
  materialNonlinearMembraneThirdDerivDefMat( const MeshTopologySaver &topology,
                                             const VectorType &undefShell,
                                             const VectorType &faceWeights,
                                             int rowOffset = 0,
                                             int colOffset = 0,
                                             RealType Mu = 1.,
                                             RealType Lambda = 1. )
          : _topology( topology ),
            _undefShell( undefShell ),
            _faceWeights( faceWeights ),
            _mu( Mu ),
            _lambda( Lambda ),
            _muHalfPlusLambdaQuarter( _mu / 2. + _lambda / 4. ),
            _rowOffset( rowOffset ),
            _colOffset( colOffset ) {

  }

  void setRowOffset( int rowOffset ) const {
    _rowOffset = rowOffset;
  }

  void setColOffset( int colOffset ) const {
    _colOffset = colOffset;
  }

  //
//  void apply( const VectorType &defShell, MatrixType &Dest ) const {
//    assembleHessian( defShell, Dest );
//  }
//
//  // assmeble Hessian matrix via triplet list
//  void assembleHessian( const VectorType &defShell, MatrixType &Hessian ) const {
//    int dofs = 3 * _topology.getNumVertices();
//    if (( Hessian.rows() != dofs ) || ( Hessian.cols() != dofs ))
//      Hessian.resize( dofs, dofs );
//    Hessian.setZero();
//
//    // set up triplet list
//    TripletListType tripletList;
//    // per face we have 3 active vertices, i.e. 9 combinations each producing a 3x3-matrix
//    tripletList.reserve( 9 * 9 * _topology.getNumFaces());
//
//    pushTriplets( defShell, tripletList );
//
//    // fill matrix from triplets
//    Hessian.setFromTriplets( tripletList.cbegin(), tripletList.cend());
//  }

  //
  void pushTriplets( const VectorType &defShell, std::vector<TripletListType> &tripletList, RealType factor = 1. ) const {

    if ( _undefShell.size() != defShell.size()) {
      std::cerr << "size of undef = " << _undefShell.size() << " vs. size of def = " << defShell.size() << std::endl;
      throw std::length_error( "materialNonlinearMembraneHessianDef::pushTriplets(): sizes dont match!" );
    }

    if ( tripletList.size() != _rowOffset + 3 * _topology.getNumVertices())
      tripletList.resize( _rowOffset + 3 * _topology.getNumVertices());

    // run over all faces
    for ( int faceIdx = 0; faceIdx < _topology.getNumFaces(); ++faceIdx ) {

      std::array<int, 3> nodesIdx{};
      std::vector<VecType> nodes( 3 ), undefEdges( 3 ), defEdges( 3 );
      VecType temp;
      for ( int j = 0; j < 3; j++ )
        nodesIdx[j] = _topology.getNodeOfTriangle( faceIdx, j );

      //! get undeformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( _undefShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        undefEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( undefEdges[0], undefEdges[1] );
      RealType volUndefSqr = temp.normSqr() / 4.;
      RealType volUndef = std::sqrt( volUndefSqr );

      //! get deformed quantities
      for ( int j = 0; j < 3; j++ )
        getXYZCoord<VectorType, VecType>( defShell, nodes[j], nodesIdx[j] );
      for ( int j = 0; j < 3; j++ )
        defEdges[j] = nodes[( j + 2 ) % 3] - nodes[( j + 1 ) % 3];
      // compute volume
      temp.makeCrossProduct( defEdges[0], defEdges[1] );
      RealType volDefSqr = temp.normSqr() / 4.;
      RealType volDef = std::sqrt( volDefSqr );

      VecType traceFactors;
      for ( int i = 0; i < 3; i++ )
        traceFactors[i] = -0.25 * _mu * dotProduct( undefEdges[( i + 2 ) % 3], undefEdges[( i + 1 ) % 3] ) / volUndef;

      RealType mixedFactor = 0.5 * _lambda / volUndef + 2. * _muHalfPlusLambdaQuarter * volUndef / volDefSqr;
      RealType areaFactor = 0.5 * _lambda * volDef / volUndef - 2. * _muHalfPlusLambdaQuarter * volUndef / volDef;

      // precompute area gradients
      std::vector<VecType> gradArea( 3 );
      for ( int i = 0; i < 3; i++ )
        getAreaGradient( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], gradArea[i] );

      // compute local matrices
      MatType tensorProduct, H, auxMat;

      // i==j
      for ( int i = 0; i < 3; i++ ) {
        getHessAreaKK( nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], nodes[i], auxMat );
        tensorProduct.makeTensorProduct( gradArea[i], gradArea[i] );
        getWeightedMatrixSum( areaFactor, auxMat, mixedFactor, tensorProduct, H );
        H.addToDiagonal( traceFactors[( i + 1 ) % 3] + traceFactors[( i + 2 ) % 3] );
        localToGlobal( tripletList, nodesIdx[i], nodesIdx[i], faceIdx, H, factor );
      }

      // i!=j
      for ( int i = 0; i < 3; i++ ) {
        getHessAreaIK( nodes[i], nodes[( i + 1 ) % 3], nodes[( i + 2 ) % 3], auxMat );
        tensorProduct.makeTensorProduct( gradArea[i], gradArea[( i + 2 ) % 3] );
        getWeightedMatrixSum( areaFactor, auxMat, mixedFactor, tensorProduct, H );
        H.addToDiagonal( -traceFactors[( i + 1 ) % 3] );
        localToGlobal( tripletList, nodesIdx[i], nodesIdx[( i + 2 ) % 3], faceIdx, H, factor  );
      }
    }


  }


protected:
  void localToGlobal( std::vector<TripletListType> &tripletList, int k, int l, int h, const MatType &localMatrix,
                      const RealType weight ) const {
    int numV = _topology.getNumVertices();
    for ( int i = 0; i < 3; i++ )
      for ( int j = 0; j < 3; j++ )
        tripletList[_rowOffset + i * numV + k].emplace_back( _colOffset + j * numV + l, h,
                                                             weight * localMatrix( i, j ));

    if ( k != l ) {
      for ( int i = 0; i < 3; i++ )
        for ( int j = 0; j < 3; j++ )
          tripletList[_rowOffset + i * numV + l].emplace_back( _colOffset + j * numV + k, h,
                                                               weight * localMatrix( j, i ));
    }
  }

};


//=============================================================================
#endif // MATERIALSHELLDEFORMATIONS_HH defined
//=============================================================================
