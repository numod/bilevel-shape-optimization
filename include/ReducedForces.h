//
// Created by josua on 16.07.20.
//

#ifndef BILEVELSHAPEOPT_REDUCEDFORCES_H
#define BILEVELSHAPEOPT_REDUCEDFORCES_H

#include "Optimization/StochasticOperators.h"
#include <goast/Core/Topology.h>


template<typename ConfiguratorType>
class ReducedForceFunctional
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;

  using ObjectiveOpType = ParametrizedBaseOp<VectorType, RealType, VectorType>;

  const int m_numVertices;
  ObjectiveOpType &m_F;
  const std::vector<int> &m_boundaryMask;

  mutable VectorType m_forces;

  const FullMatrixType &m_reductionMatrix;

  const VectorType m_beforeVariables;
  const VectorType m_afterVariables;

public:
  ReducedForceFunctional( const MeshTopologySaver &Topology, ObjectiveOpType &F, const FullMatrixType &reductionMatrix,
                          const std::vector<int> &BoundaryMask )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices() ), m_reductionMatrix( reductionMatrix ){
  }

  ReducedForceFunctional( const MeshTopologySaver &Topology, ObjectiveOpType &F, const FullMatrixType &reductionMatrix,
                          const std::vector<int> &BoundaryMask, VectorType beforeVariables, VectorType afterVariables  )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( beforeVariables.size() + 3 * Topology.getNumVertices() + afterVariables.size() ),
            m_reductionMatrix( reductionMatrix ),
            m_beforeVariables( beforeVariables ), m_afterVariables( afterVariables ) {
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    assert( Arg.size() == m_reductionMatrix.cols() &&
            "ReducedForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    if ( m_beforeVariables.size() > 0 )
      m_forces.head( m_beforeVariables.size() ) = m_beforeVariables;

    m_forces.segment( m_beforeVariables.size(), 3 * m_numVertices ) = m_reductionMatrix * Arg;

//    for ( int idx : m_boundaryMask )
//      m_forces[m_beforeVariables.size() + idx] = 0.;

    if ( m_afterVariables.size() > 0 )
      m_forces.tail( m_afterVariables.size() ) = m_afterVariables;

    // Apply functional
    m_F.apply( m_forces, Dest );
  }

  void setParameters( const VectorType &parameters ) override {
    m_F.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_F.getParameters();
  }

};

template<typename ConfiguratorType>
class ReducedForceGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;

  using ObjectiveGradType = ParametrizedBaseOp<VectorType, VectorType, VectorType>;

  const int m_numVertices;
  ObjectiveGradType &m_DF;
  const std::vector<int> &m_boundaryMask;

  mutable VectorType m_forces;
  mutable VectorType m_fullGradient;

  const FullMatrixType &m_reductionMatrix;

  const VectorType m_beforeVariables;
  const VectorType m_afterVariables;
public:
  ReducedForceGradient( const MeshTopologySaver &Topology, ObjectiveGradType &DF, const FullMatrixType &reductionMatrix,
                        const std::vector<int> &BoundaryMask)
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()  ),
            m_reductionMatrix( reductionMatrix ) {
  }
  ReducedForceGradient( const MeshTopologySaver &Topology, ObjectiveGradType &DF, const FullMatrixType &reductionMatrix,
                        const std::vector<int> &BoundaryMask, VectorType beforeVariables, VectorType afterVariables )
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( beforeVariables.size() + 3 * Topology.getNumVertices() + afterVariables.size() ),
            m_reductionMatrix( reductionMatrix ),
            m_beforeVariables( beforeVariables ), m_afterVariables( afterVariables ) {
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    assert( Arg.size() ==  m_reductionMatrix.cols() &&
            "TwoDForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    if ( m_beforeVariables.size() > 0 )
      m_forces.head( m_beforeVariables.size() ) = m_beforeVariables;

    m_forces.segment( m_beforeVariables.size(), 3 * m_numVertices ) = m_reductionMatrix * Arg;

//    for ( int idx : m_boundaryMask )
//      m_forces[m_beforeVariables.size() + idx] = 0.;

    if ( m_afterVariables.size() > 0 )
      m_forces.tail( m_afterVariables.size() ) = m_afterVariables;

    // Compute full gradient
    m_DF.apply( m_forces, m_fullGradient );

    // Compute reduced gradient
    Dest.resize( m_reductionMatrix.cols() );
    Dest = m_reductionMatrix.transpose() * m_fullGradient.segment( m_beforeVariables.size(), 3 * m_numVertices );

  }

  void setParameters( const VectorType &parameters ) override {
    m_DF.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_DF.getParameters();
  }

};


template<typename ConfiguratorType>
class PartialReducedForceFunctional
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;

  using ObjectiveOpType = ParametrizedBaseOp<VectorType, RealType, VectorType>;

  const int m_numVertices;
  ObjectiveOpType &m_F;
  const std::vector<int> &m_boundaryMask;

  mutable VectorType m_forces;

  const FullMatrixType &m_reductionMatrix;

  const int m_beforeVariables;
  const int m_afterVariables;

public:
  PartialReducedForceFunctional( const MeshTopologySaver &Topology, ObjectiveOpType &F, const FullMatrixType &reductionMatrix,
                          const std::vector<int> &BoundaryMask )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices() ), m_reductionMatrix( reductionMatrix ),
            m_beforeVariables( 0 ), m_afterVariables( 0 ){
  }

  PartialReducedForceFunctional( const MeshTopologySaver &Topology, ObjectiveOpType &F, const FullMatrixType &reductionMatrix,
                          const std::vector<int> &BoundaryMask, int beforeVariables, int afterVariables  )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( beforeVariables + 3 * Topology.getNumVertices() + afterVariables ),
            m_reductionMatrix( reductionMatrix ),
            m_beforeVariables( beforeVariables ), m_afterVariables( afterVariables ) {
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    assert( Arg.size() == m_reductionMatrix.cols() + m_beforeVariables + m_afterVariables &&
            "ReducedForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    if ( m_beforeVariables > 0 )
      m_forces.head( m_beforeVariables ) = Arg.head(m_beforeVariables);

    m_forces.segment( m_beforeVariables, 3 * m_numVertices ) = m_reductionMatrix * Arg.segment( m_beforeVariables, m_reductionMatrix.cols() );

    if ( m_afterVariables > 0 )
      m_forces.tail( m_afterVariables ) = Arg.tail( m_afterVariables );

    // Apply functional
    m_F.apply( m_forces, Dest );
  }

  void setParameters( const VectorType &parameters ) override {
    m_F.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_F.getParameters();
  }

};

template<typename ConfiguratorType>
class PartialReducedForceGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;

  using ObjectiveGradType = ParametrizedBaseOp<VectorType, VectorType, VectorType>;

  const int m_numVertices;
  ObjectiveGradType &m_DF;
  const std::vector<int> &m_boundaryMask;

  mutable VectorType m_forces;
  mutable VectorType m_fullGradient;

  const FullMatrixType &m_reductionMatrix;

  const int m_beforeVariables;
  const int m_afterVariables;
public:
  PartialReducedForceGradient( const MeshTopologySaver &Topology, ObjectiveGradType &DF, const FullMatrixType &reductionMatrix,
                        const std::vector<int> &BoundaryMask)
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()  ),
            m_reductionMatrix( reductionMatrix ) ,
            m_beforeVariables( 0 ), m_afterVariables( 0 ) {
  }
  PartialReducedForceGradient( const MeshTopologySaver &Topology, ObjectiveGradType &DF, const FullMatrixType &reductionMatrix,
                        const std::vector<int> &BoundaryMask, int beforeVariables, int afterVariables )
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( beforeVariables + 3 * Topology.getNumVertices() + afterVariables ),
            m_reductionMatrix( reductionMatrix ),
            m_beforeVariables( beforeVariables ), m_afterVariables( afterVariables ) {
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    assert( Arg.size() == m_reductionMatrix.cols() + m_beforeVariables + m_afterVariables &&
            "ReducedForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    if ( m_beforeVariables > 0 )
      m_forces.head( m_beforeVariables ) = Arg.head(m_beforeVariables);

    m_forces.segment( m_beforeVariables, 3 * m_numVertices ) = m_reductionMatrix * Arg.segment( m_beforeVariables, m_reductionMatrix.cols() );

    if ( m_afterVariables > 0 )
      m_forces.tail( m_afterVariables ) = Arg.tail( m_afterVariables );

    // Compute full gradient
    m_DF.apply( m_forces, m_fullGradient );

    // Compute reduced gradient
    Dest.resize( m_reductionMatrix.cols() + m_beforeVariables + m_afterVariables );
    Dest.head(m_beforeVariables) = m_fullGradient.head(m_beforeVariables);
    Dest.segment( m_beforeVariables, m_reductionMatrix.cols() ) = m_reductionMatrix.transpose() * m_fullGradient.segment( m_beforeVariables, 3 * m_numVertices );
    Dest.tail(m_afterVariables) = m_fullGradient.tail(m_afterVariables);

  }

  void setParameters( const VectorType &parameters ) override {
    m_DF.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_DF.getParameters();
  }

};

#endif //BILEVELSHAPEOPT_2DFORCES_H
