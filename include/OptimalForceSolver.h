//
// Created by josua on 03.06.20.
//

#ifndef BILEVELSHAPEOPT_OPTIMALFORCESOLVER_H
#define BILEVELSHAPEOPT_OPTIMALFORCESOLVER_H

#include<Eigen/SparseQR>

#include <goast/external/ipoptBoxConstraintSolver.h>
#include <goast/Core/FEM.h>
#include <goast/Optimization/LineSearchNewton.h>
#include <goast/DiscreteShells.h>

#include <utility>

#include "ShapeOptimization/StateEquationInterface.h"
#include "NodalAreas.h"
#include "FreeEnergy.h"
#include "ReducedForces.h"
#include "ForceRegularization.h"
#include <yaml-cpp/yaml.h>

template<typename t_DomainType, typename t_RangeType = t_DomainType>
class SimpleNegativeOp : public BaseOp<t_DomainType, t_RangeType> {
public:
  using DomainType = t_DomainType;
  using RangeType = t_RangeType;

protected:
  BaseOp<DomainType, RangeType> &m_Op;

public:
  explicit SimpleNegativeOp( BaseOp<DomainType, RangeType> &Op ) : m_Op( Op ) {}

  void apply( const DomainType &Arg, RangeType &Dest ) const override {
    m_Op.apply( Arg, Dest );
    Dest *= -1.;
//    if (std::is_same<RangeType, double>::value) {
//      if (Dest == -std::numeric_limits<double>::infinity())
//        Dest = -std::numeric_limits<double>::infinity();
//    }
  }

  int getTargetDimension() const override {
    return m_Op.getTargetDimension();
  }
};

template<typename t_DomainType>
class SimpleNegativeOp<t_DomainType, double> : public BaseOp<t_DomainType, double> {
public:
  using DomainType = t_DomainType;
  using RangeType = double;

protected:
  BaseOp<DomainType, double> &m_Op;

public:
  explicit SimpleNegativeOp( BaseOp<DomainType, double> &Op ) : m_Op( Op ) {}

  void apply( const DomainType &Arg, double &Dest ) const override {
    m_Op.apply( Arg, Dest );
    if (Dest != std::numeric_limits<double>::infinity())
      Dest *= -1.;
  }

  int getTargetDimension() const override {
    return m_Op.getTargetDimension();
  }
};


template<typename ConfiguratorType, class DeformationType>
class OptimalForceSolver : public StateEquationSolverBase<ConfiguratorType> {

public:

  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using TensorType = typename ConfiguratorType::TensorType;
  using TripletType = typename ConfiguratorType::TripletType;
  using TripletListType = std::vector<TripletType>;

//  using ShellDeformationType =  ShellDeformation<ConfiguratorType, NonlinearMembraneDeformation<ConfiguratorType>, SimpleBendingDeformation<ConfiguratorType> >;

public:
  struct {
    int maxNumIterations = 1;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 10.;
    TIMESTEP_CONTROLLER stepsizeController = ARMIJO;
    bool acceleration = false;
    RealType regularizationWeight = 0.;
    RealType objectiveWeight = 1.;
    RealType barrierValue = 1000.;
    bool useL2Constraint = true;
  } Configuration;

  const RealType updateTolerance = 1e-10;

  // Underlying mesh and deformation model etc.
  const MeshTopologySaver &m_Topology;
  const VectorType &m_refGeometry;

  mutable DeformationType m_W;
  const std::vector<int> &m_dirichletBoundary;

  // Force space
  const FullMatrixType &m_forceBasis;
  mutable VectorType m_forceCoordinates;
  mutable VectorType m_vertexForces;
  const RealType m_forceBound;

  // Solvers and functionals
  mutable NonlinearElasticitySolver<ConfiguratorType, DeformationType> m_SES;

  // BFGS
  mutable FullMatrixType m_inverseHessian;
  mutable FullMatrixType m_fullHessian;
  mutable MatrixType m_mixedHessian;
  mutable VectorType m_lastControl;
  mutable VectorType m_lastForce;
  mutable VectorType m_lastForceGradient;
  mutable VectorType m_lastDaF;
  mutable VectorType m_lastDaFGradient;
  mutable VectorType m_lastControlGradient;
  mutable int m_ApxMode;

  // additional helpers
  const int m_numVertices;

  // (cached) properties
  mutable VectorType m_undefDisplacement;
  mutable VectorType m_vertexMaterial;

  // (cached) output
  mutable VectorType m_defDisplacement;
  mutable bool m_solvedAccurately;

  //
  mutable bool m_updateDeformation = true;
  mutable bool m_updateHessian = true;
  mutable bool m_updateMixedDerviative = true;
  mutable bool m_updateBFGS = true;


  // Controls
  const bool m_UndefIsControl = true;
  const bool m_MaterialIsControl = false;

  int m_numControls, m_beginUndefControl, m_beginMaterialControl;

  // Parameters
  const bool m_UndefIsParameter = true;
  const bool m_MaterialIsParameter = false;

  int m_numParameters, m_beginUndefParameter, m_beginMaterialParameter;
public:
  OptimalForceSolver( const MeshTopologySaver &Topology,
                      const VectorType &refGeometry,
                      const std::vector<int> &dirichletBoundary,
                      const VectorType undefDisplacement,
                      const VectorType vertexMaterial,
                      const FullMatrixType &forceBasis,
                      const RealType forceBound,
                      const VectorType initialForces,
                      const bool UndefIsControl = true,
                      const bool MaterialIsControl = false,
                      const bool UndefIsParameter = false,
                      const bool MaterialIsParameter = false )
          : m_Topology( Topology ), m_refGeometry( refGeometry ), m_dirichletBoundary( dirichletBoundary ),
            m_undefDisplacement( std::move( undefDisplacement )), m_forceCoordinates( std::move( initialForces )),
            m_vertexMaterial( std::move( vertexMaterial )), m_W( Topology, vertexMaterial ),
            m_defDisplacement( undefDisplacement ), m_solvedAccurately( false ),
            m_UndefIsControl( UndefIsControl ),
            m_MaterialIsControl( MaterialIsControl ),
            m_UndefIsParameter( UndefIsParameter ),
            m_MaterialIsParameter( MaterialIsParameter ),
            m_numVertices( Topology.getNumVertices()),
            m_forceBasis( forceBasis ), m_vertexForces( forceBasis * m_forceCoordinates ), m_forceBound( forceBound ),
            m_SES( Topology, refGeometry, dirichletBoundary, undefDisplacement, forceBasis * m_forceCoordinates,
                   vertexMaterial, UndefIsControl, true, MaterialIsControl, UndefIsParameter, false,
                   MaterialIsParameter ), m_ApxMode(0) {

    // Determine structure of control variable
    m_numControls = 0;
    m_beginMaterialControl = 0;
    m_beginUndefControl = 0;
    if ( m_UndefIsControl ) {
      m_numControls += 3 * m_numVertices;
      m_beginMaterialControl += 3 * m_numVertices;
    }
    if ( m_MaterialIsControl ) {
      m_numControls += m_numVertices;
    }

    // Determine structure of parameter variable
    m_numParameters = 0;
    m_beginMaterialParameter = 0;
    m_beginUndefParameter = 0;
    if ( m_UndefIsParameter ) {
      m_numParameters += 3 * m_numVertices;
      m_beginMaterialParameter += 3 * m_numVertices;
    }
    if ( m_MaterialIsParameter ) {
      m_numParameters += m_numVertices;
    }

    assert( !( m_UndefIsParameter && m_UndefIsControl ) &&
            "NonlinearElasticitySolver: Undeformed geometry can be either control or parameter, not both!" );
    assert( !( m_MaterialIsParameter && m_MaterialIsControl ) &&
            "NonlinearElasticitySolver: Material distribution can be either control or parameter, not both!" );


    // Initial solution
    computeForce();
//    buildHessian();
  }

  bool updateControl( const VectorType &Control ) {
    assert( Control.size() == m_numControls &&
            "NonlinearElasticitySolver::updateControls(): Wrong number of controls." );

    bool updated = false;
    if ( m_UndefIsControl )
      updated = updateUndeformed( Control.segment( m_beginUndefControl, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsControl ) {
      bool materialUpdated = updateMaterial( Control.segment( m_beginMaterialControl, m_numVertices ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation || !m_solvedAccurately;
    m_updateBFGS = updated || m_updateBFGS || !m_solvedAccurately;

    return updated;
  }

  bool updateParameters( const VectorType &Parameters ) override {
    assert( Parameters.size() == m_numControls &&
            "NonlinearElasticitySolver::updateParameters(): Wrong number of parameters." );

    bool updated = false;
    if ( m_UndefIsParameter )
      updated = updateUndeformed( Parameters.segment( m_beginUndefParameter, 3 * m_numVertices )) || updated;
    if ( m_MaterialIsParameter ) {
      bool materialUpdated = updateMaterial( Parameters.segment( m_beginMaterialParameter, m_numVertices ));
      updated = materialUpdated || updated;
      m_updateHessian = materialUpdated || m_updateHessian;
    }

    m_updateMixedDerviative = updated || m_updateMixedDerviative;
    m_updateDeformation = updated || m_updateDeformation || !m_solvedAccurately;
    m_updateBFGS = updated || m_updateBFGS || !m_solvedAccurately;

    return updated;
  }


  VectorType solve( const VectorType &Arg ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

    return m_forceCoordinates;
  }

  VectorType forceSolve( const VectorType &Arg ) {
    updateControl( Arg );

    // Compute deformation
    computeForce();

    return m_forceCoordinates;
  }


  VectorType solveAdjoint( const VectorType &Arg, const VectorType &rhs ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

//    std::cout << " .... inverse hessian: " << std::endl << m_inverseHessian << std::endl;
    // SR1
    VectorType SR1Solution = m_inverseHessian * rhs;

//    // Exact calculation
//
//    // inverse mass matrix = mixed derivative of free energy
//    MatrixType Minv( 3 * m_numVertices, 3 * m_numVertices );
//
//    VectorType nodalAreas( m_numVertices );
//    computeNodalAreas<ConfiguratorType>( m_Topology, m_refGeometry + m_undefDisplacement, nodalAreas );
//
//    for ( int i = 0; i < m_numVertices; i++ )
//      for ( const int j : { 0, 1, 2 } )
//        Minv.coeffRef( i + j * m_numVertices, i + j * m_numVertices) = -1. / nodalAreas[i];
//
//    // elastic hessian
//    VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
//    DesignAndForce.head(3 * m_numVertices) = m_vertexForces;
//
//    if ( m_UndefIsControl )
//      throw std::runtime_error("Undeformed control currently not possible.");
//    if ( m_MaterialIsControl )
//      DesignAndForce.tail(m_numVertices) = m_vertexMaterial;
//
//    VectorType defDisplacement = m_SES.solve( DesignAndForce );
//    MatrixType elasticHessian;
//    m_W.applyDefHessian( m_refGeometry + m_undefDisplacement, m_refGeometry + defDisplacement, elasticHessian );
//
//    // inverse objective hessian
//    VectorType combinedVector( DesignAndForce.size() + defDisplacement.size());
//    combinedVector.head( DesignAndForce.size()) = DesignAndForce;
//    combinedVector.tail( defDisplacement.size()) = defDisplacement;
//    ComplianceTermHessian<ConfiguratorType, DeformationType> D2C( m_Topology, m_refGeometry, m_W,
//                                                                  false, false, false,
//                                                                  m_UndefIsParameter, false, m_MaterialIsParameter );
//    MatrixType complianceHessian = D2C(defDisplacement);
//
//    LinearSolver<ConfiguratorType> complianceInverse;
//    complianceInverse.prepareSolver(complianceHessian);
//
//    VectorType lsSolution;
//    complianceInverse.backSubstitute(elasticHessian * Minv * m_forceBasis * rhs, lsSolution);
//
//    VectorType fullSolution  = m_forceBasis.transpose() * Minv * elasticHessian * lsSolution;
//    VectorType redSolution =  m_forceBasis.transpose() * Minv * elasticHessian * Minv * m_forceBasis * rhs;
//
//    std::cout << " .. redSolution - fullSolution = " << (fullSolution - redSolution).norm() << std::endl;
//
//    std::cout << " .. SR1Solution.norm = " << SR1Solution.norm() << std::endl;
//    std::cout << " .. fullSolution.norm = " << fullSolution.norm() << std::endl;
//    std::cout << " .. fullSolution.norm / SR1Solution.norm = " << fullSolution.norm() / SR1Solution.norm() << std::endl;
//
//    std::cout << " .. SR1Solution - fullSolution = " << (fullSolution - SR1Solution).norm() << std::endl;
//    std::cout << " .. (SR1Solution - fullSolution) / SR1Solution = " << (fullSolution - SR1Solution).norm() / SR1Solution.norm() << std::endl;
//    std::cout << " .. (SR1Solution - fullSolution) / fullSolution = " << (fullSolution - SR1Solution).norm() / fullSolution.norm() << std::endl;


    return SR1Solution;


//    return m_fullHessian.block( 0, 0, m_forceBasis.cols(), m_forceBasis.cols()).inverse() * rhs;
  }


  MatrixType mixedDerivative( const VectorType &Arg ) override {
    updateControl( Arg );

    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

//    std::cout << " .... mixed hessian: " << std::endl << m_mixedHessian << std::endl;
//    std::cout << " .... mixed hessian: " << std::endl << m_fullHessian.block( 0, m_forceBasis.cols(), m_forceBasis.cols(), m_numControls ).sparseView() << std::endl;

//    return m_mixedHessian;
    return m_fullHessian.block( 0, m_forceBasis.cols(), m_forceBasis.cols(), m_numControls ).sparseView();
  }

  void updateBFGS(const VectorType &Arg) {
    updateControl( Arg );


    bool updateApx = true;
    // Compute deformation
    if ( m_updateDeformation )
      computeForce();

    if ( !m_updateBFGS )
      return;

    // Build functionals
    LinearizedComplianceTerm<ConfiguratorType, DeformationType> C( m_Topology, m_refGeometry,
                                                                   m_undefDisplacement, m_vertexMaterial,
                                                                   m_UndefIsControl, true, m_MaterialIsControl,
                                                                   m_UndefIsParameter, false, m_MaterialIsParameter );
    LinearizedComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry,
                                                                            m_undefDisplacement, m_vertexMaterial,
                                                                            m_UndefIsControl, true, m_MaterialIsControl,
                                                                            m_UndefIsParameter, false,
                                                                            m_MaterialIsParameter );

    ForceL2LogBarrier<ConfiguratorType> R( m_Topology, m_refGeometry, Configuration.barrierValue,
                                           m_UndefIsControl, true, m_MaterialIsControl,
                                           m_UndefIsParameter, false, m_MaterialIsParameter );
    ForceL2LogBarrierGradient<ConfiguratorType> DR( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                    m_UndefIsControl, true, m_MaterialIsControl,
                                                    m_UndefIsParameter, false,
                                                    m_MaterialIsParameter );

    VectorType weights( 2 );
    weights << Configuration.objectiveWeight, -Configuration.regularizationWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, C, R );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DC, DR );

    ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( J, m_SES );
    ReducedPDECParametrizedGradient<ConfiguratorType> DJred( J, DJ, m_SES, m_dirichletBoundary );

    VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
    DesignAndForce.head(3 * m_numVertices) = m_vertexForces;

    if ( m_UndefIsControl )
      throw std::runtime_error("Undeformed control currently not possible.");
    if ( m_MaterialIsControl )
      DesignAndForce.tail(m_numVertices) = m_vertexMaterial;

    VectorType localGradient = DJred(DesignAndForce);
//    std::cout << " .. localGradient.norm = " << localGradient.norm() << std::endl;

    if ( localGradient.norm() == std::numeric_limits<RealType>::infinity())
      return;

    // Initialize BFGS approximation on first call
    if (m_ApxMode == 0) {
      std::cout << " .. Initialize BFGS  " << std::endl;

      std::cout << " r: " << m_forceBasis.cols() << " --  c: " << m_numControls << std::endl;

//      m_inverseHessian.resize( m_forceBasis.cols(), m_forceBasis.cols());
//      m_inverseHessian.setIdentity();

      m_fullHessian.resize( m_forceBasis.cols() + m_numControls, m_forceBasis.cols() + m_numControls);
      m_fullHessian.setIdentity();

//      m_mixedHessian.resize( m_forceBasis.cols(), m_numControls );
//      m_mixedHessian.setZero();
//      m_mixedHessian = (FullMatrixType::Random(m_forceBasis.cols(), m_numControls) * 0.001).sparseView();
//      for ( int i = 0; i < std::min<long int>(m_forceBasis.cols(), m_numControls); i++)
//        m_mixedHessian.coeffRef(i,i)=1.;


      m_lastForce = m_forceCoordinates;
      m_lastControl = m_vertexMaterial;
      m_lastForceGradient = m_forceBasis.transpose() * localGradient.head(3 * m_numVertices);
      m_lastControlGradient = localGradient.tail(m_numVertices);

      m_lastDaF.resize(m_forceBasis.cols() + m_numControls);
      m_lastDaF.head(m_forceBasis.cols()) = m_forceCoordinates;
      m_lastDaF.tail(m_numControls) = m_vertexMaterial;

      m_lastDaFGradient.resize(m_forceBasis.cols() + m_numControls);
      m_lastDaFGradient.head(m_forceBasis.cols()) = m_lastForceGradient;
      m_lastDaFGradient.tail(m_numControls) = m_lastControlGradient;

      m_ApxMode = 1;
    }
    else {
//      std::cout << " .. Update BFGS" << std::endl;
      // Full Hessian
      VectorType DaF( m_forceBasis.cols() + m_numControls );
      DaF.head(m_forceBasis.cols()) = m_forceCoordinates;
      DaF.tail(m_numControls) = m_vertexMaterial;
      VectorType DaFGradient( m_forceBasis.cols() + m_numControls );
      DaF.head(m_forceBasis.cols()) = m_forceBasis.transpose() * localGradient.head(3 * m_numVertices);
      DaF.tail(m_numControls) = localGradient.tail(m_numVertices);

      VectorType y = DaFGradient -  m_lastDaFGradient;
      VectorType s = DaF - m_lastDaF;

      if (m_ApxMode == 1) {

        if ( std::abs(y.dot(s)) > 1e-12 && y.norm() > 1.e-12 ) {
          std::cout << " .. Scaling full hessian with  " << y.norm() / std::abs(y.dot(s)) << std::endl;
          m_fullHessian *= y.norm() / std::abs(y.dot(s));
        }

      }

//      std::cout << " .... y.norm = " << std::scientific << y.norm() << std::endl;
//      std::cout << " .... s.norm = " << s.norm() << std::endl;
//      std::cout << " .... s*y = " << s.dot( y ) << std::endl;
//      std::cout << " .... s*B*s = " << s.transpose() * m_fullHessian * s << std::endl;
//      std::cout << " .... y*y.T norm = " << (y * y.transpose() ).norm() << std::endl;
//      std::cout << " .... B*s norm = " << (m_fullHessian * s).norm() << std::endl;
//      std::cout << " .... B*s*s.T*B norm = " << (( m_fullHessian * s ) * ( s.transpose() * m_fullHessian)).norm() << std::endl;

//      m_fullHessian += y * y.transpose() / ( y.dot(s) );
//      m_fullHessian -= ( m_fullHessian * s ) * ( s.transpose() * m_fullHessian) /
//                       ( s.transpose() * m_fullHessian * s);

      VectorType yBs = y - m_fullHessian * s;
      if (std::abs(yBs.dot(s)) > 1.e-8 * s.norm()* yBs.norm())
        m_fullHessian += yBs * yBs.transpose() / ( yBs.dot(s) );

//      std::cout << " .... test = " << (m_fullHessian * s - y).norm() << std::endl;


      // Force part
      VectorType y_f = m_forceBasis.transpose() * localGradient.head(3 * m_numVertices) -  m_lastForceGradient;
      VectorType s_f = m_forceCoordinates - m_lastForce;

      if (m_ApxMode == 1) {

//        if ( y_f.norm() > 1.e-12 ) {
//          std::cout << " .. Scaling inverse hessian with  " << std::abs(y_f.dot( s_f )) / y_f.norm() << std::endl;
//          m_inverseHessian *= std::abs(y_f.dot( s_f )) / y_f.norm();
//
//        }

        m_ApxMode = 2;
      }

//      VectorType yBs_f = s_f - m_inverseHessian * y_f;
//      if (std::abs(yBs_f.dot(y_f)) > 1.e-8 * y_f.norm()* yBs_f.norm())
//        m_inverseHessian += yBs_f * yBs_f.transpose() / ( yBs_f.dot(y_f) );

//      if (s_f.norm() > 1e-8 && y_f.norm() > 1e-8)
//          m_inverseHessian +=
//              ( s_f.dot( y_f ) + y_f.transpose() * m_inverseHessian * y_f ) / std::pow( s_f.dot( y_f ), 2 ) * ( s_f * s_f.transpose()) -
//              ((m_inverseHessian*y_f) * s_f.transpose() + s_f * (m_inverseHessian*y_f).transpose()) / s_f.dot( y_f );



      // Mixed part
      VectorType y_c = localGradient.tail(m_numVertices) -  m_lastControlGradient;
      VectorType s_c = m_vertexMaterial -  m_lastControl;

//      std::cout << " .... y_f.norm = " << std::scientific << y_f.norm() << std::endl;
//      std::cout << " .... s_f.norm = " << s_f.norm() << std::endl;
//      std::cout << " .... y_c.norm = " << y_c.norm() << std::endl;
//      std::cout << " .... s_c.norm = " << s_c.norm() << std::endl;
//      std::cout << " .... (y_c*s_c).norm = " << ( y_c.transpose() * s_c ).norm() << std::endl;
//      std::cout << " .... (s_f*B*s_c).norm = " << ( s_f.transpose() * m_mixedHessian * s_c ).norm() << std::endl;
//      std::cout << " .... localGradient.head.norm = " << localGradient.head(3 * m_numVertices).norm() << std::endl;
//      std::cout << " .... localGradient.tail.norm = " << localGradient.tail(m_numVertices).norm() << std::endl;
//      std::cout << " .... forceGradient.norm = " << (m_forceBasis.transpose() * localGradient.head(3 * m_numVertices)).norm() << std::endl;


//      if (y_c.transpose() * s_c > 1.e-10 && s_f.transpose() * m_mixedHessian * s_c > 1.e-10) {
//        FullMatrixType mixedUpdate = y_c * y_f.transpose() / ( y_c.transpose() * s_c );
//        mixedUpdate -= ( m_mixedHessian.transpose() * s_f ) * ( s_c.transpose() * m_mixedHessian.transpose()) /
//                       ( s_f.transpose() * m_mixedHessian * s_c );
//
//        m_mixedHessian += mixedUpdate.transpose().sparseView();
//      }

//      std::cout << " .... mixed vs. part of full: "
//                << ( m_mixedHessian -
//                     m_fullHessian.block( 0, m_forceBasis.cols(), m_forceBasis.cols(), m_numControls )).norm() /
//                   m_fullHessian.block( 0, m_forceBasis.cols(), m_forceBasis.cols(), m_numControls ).norm()
//                << std::endl;

      m_lastForce = m_forceCoordinates;
      m_lastForceGradient = m_forceBasis.transpose() * localGradient.head(3 * m_numVertices);
      m_lastControl = m_vertexMaterial;
      m_lastControlGradient = localGradient.tail(m_numVertices);

      m_lastDaF.head(m_forceBasis.cols()) = m_forceCoordinates;
      m_lastDaF.tail(m_numControls) = m_vertexMaterial;

      m_lastDaFGradient.head(m_forceBasis.cols()) = m_lastForceGradient;
      m_lastDaFGradient.tail(m_numControls) = m_lastControlGradient;
    }

    m_updateBFGS = false;
  }

  bool lastSolveSuccessful() const override {
    return m_solvedAccurately;
  }


protected:
  void computeForce() const {
    // Build functionals
    ComplianceTerm<ConfiguratorType, DeformationType> C( m_Topology, m_refGeometry, m_W,
                                                         m_UndefIsControl, true, m_MaterialIsControl,
                                                         m_UndefIsParameter, false, m_MaterialIsParameter );
    ComplianceTermGradient<ConfiguratorType, DeformationType> DC( m_Topology, m_refGeometry, m_W,
                                                                  m_UndefIsControl, true, m_MaterialIsControl,
                                                                  m_UndefIsParameter, false, m_MaterialIsParameter );

    ForceL2LogBarrier<ConfiguratorType> R( m_Topology, m_refGeometry, Configuration.barrierValue,
                                           m_UndefIsControl, true, m_MaterialIsControl,
                                           m_UndefIsParameter, false, m_MaterialIsParameter );
    ForceL2LogBarrierGradient<ConfiguratorType> DR( m_Topology, m_refGeometry, Configuration.barrierValue,
                                                    m_UndefIsControl, true, m_MaterialIsControl,
                                                    m_UndefIsParameter, false,
                                                    m_MaterialIsParameter );

    VectorType weights( 2 );
    weights << Configuration.objectiveWeight, -Configuration.regularizationWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, C, R );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DC, DR );

    ReducedPDECParametrizedFunctional<ConfiguratorType> Jred( J, m_SES );
    ReducedPDECParametrizedGradient<ConfiguratorType> DJred( J, DJ, m_SES, m_dirichletBoundary );

    VectorType emptyVector;

    ReducedForceFunctional<ConfiguratorType> Jld( m_Topology, Jred, m_forceBasis, m_dirichletBoundary,
                                                  emptyVector, m_vertexMaterial );
    ReducedForceGradient<ConfiguratorType> DJld( m_Topology, DJred, m_forceBasis, m_dirichletBoundary,
                                                 emptyVector, m_vertexMaterial );


    SimpleNegativeOp<VectorType, RealType> nJ( Jld );
    SimpleNegativeOp<VectorType, VectorType> nDJ(  DJld );

    ScalarValuedDerivativeTester<ConfiguratorType>(nJ, nDJ,1e-6).plotAllDirections(m_forceCoordinates, "testDJld");

    if ( Configuration.useL2Constraint ) {
      auto prox = std::bind( projectionOnBall<RealType, VectorType>, m_forceBound, std::placeholders::_1,
                             std::placeholders::_2 );

      ProximalGradientDescent<DefaultConfigurator> Solver( nJ, nDJ, prox, Configuration.maxNumIterations,
                                                           Configuration.optimalityTolerance, SHOW_ONLY_IF_FAILED );
      Solver.setParameter( "initial_stepsize", Configuration.stepsize );
      Solver.setParameter( "maximal_stepsize", Configuration.maximalStepsize );
      Solver.setParameter( "stepsize_control", Configuration.stepsizeController );
      Solver.setParameter( "acceleration", Configuration.acceleration );
      auto t_start = std::chrono::high_resolution_clock::now();
      Solver.solve( m_forceCoordinates, m_forceCoordinates );
      auto t_end = std::chrono::high_resolution_clock::now();
    }
    else {
//      GradientDescent<DefaultConfigurator> Solver( nJ, nDJ, Configuration.maxNumIterations,
//                                                   Configuration.optimalityTolerance, Configuration.stepsizeController,
//                                                   SHOW_ALL, 0.1, 1.e-6, Configuration.maximalStepsize );
      QuasiNewtonBFGS<DefaultConfigurator> Solver( nJ, nDJ, Configuration.maxNumIterations,
                                                   Configuration.optimalityTolerance, Configuration.stepsizeController, 50,
                                                   SHOW_ALL, 0.1, 1.e-6, Configuration.maximalStepsize );
      auto t_start = std::chrono::high_resolution_clock::now();
      Solver.solve( m_forceCoordinates, m_forceCoordinates );
      auto t_end = std::chrono::high_resolution_clock::now();

      m_inverseHessian = Solver.inverseHessianApproximation();
    }

    m_vertexForces = m_forceBasis * m_forceCoordinates;
//    applyMaskToVector(m_dirichletBoundary, m_vertexForces);

    {
      VectorType DesignAndForce( m_numControls + m_forceBasis.rows());
      DesignAndForce.head( 3 * m_numVertices ) = m_vertexForces;

      if ( m_UndefIsControl )
        throw std::runtime_error( "Undeformed control currently not possible." );
      if ( m_MaterialIsControl )
        DesignAndForce.tail( m_numVertices ) = m_vertexMaterial;

      ReducedPDECParametrizedFunctional<ConfiguratorType> Cred( C, m_SES );
      ReducedPDECParametrizedFunctional<ConfiguratorType> Rred( R, m_SES );
      std::cout << " .... C = " << Cred(DesignAndForce) << " || R = " << Rred(DesignAndForce) << std::endl;
    }

    m_solvedAccurately = true;
    m_updateDeformation = false;
  }



  /**
   * \brief Compute linearization of the state equation
   */
//  void buildHessian() {
//    m_W.applyDefHessian( m_refGeometry + m_undefDisplacement, m_refGeometry + m_defDisplacement, m_Hessian );
//    applyMaskToSymmetricMatrix( m_dirichletBoundary, m_Hessian );
//    m_HessianSolver.prepareSolver( m_Hessian );
//
//    m_updateHessian = false;
//  }

  bool updateUndeformed( const VectorType &undefDisplacement ) const {
    if (( m_undefDisplacement - undefDisplacement ).norm() > updateTolerance ) {
      m_undefDisplacement = undefDisplacement;
      return true;
    }
    else {
      return false;
    }

  }


  bool updateMaterial( const VectorType &vertexMaterial ) const {
    if (( m_vertexMaterial - vertexMaterial ).norm() > updateTolerance ) {
      m_vertexMaterial = vertexMaterial;
      m_W.setParameters( m_vertexMaterial );
      return true;
    }
    else {
      return false;
    }

  }

};


#endif //BILEVELSHAPEOPT_OPTIMALFORCESOLVER_H
