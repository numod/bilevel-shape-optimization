//
// Created by josua on 16.07.20.
//

#ifndef BILEVELSHAPEOPT_3DFORCES_H
#define BILEVELSHAPEOPT_3DFORCES_H

#include "Optimization/StochasticOperators.h"
#include <goast/Core/Topology.h>

template<typename ConfiguratorType>
class ThreeDForceFunctional
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  using ObjectiveOpType = ParametrizedBaseOp<VectorType, RealType, VectorType>;

  const int m_numVertices;
  ObjectiveOpType &m_F;
  const std::vector<int> &m_boundaryMask;

  mutable VectorType m_forces;

public:
  ThreeDForceFunctional( const MeshTopologySaver &Topology, ObjectiveOpType &F,
                         const std::vector<int> &BoundaryMask )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()) {
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    assert( Arg.size() == 3 && "TwoDForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    for ( int d : { 0, 1, 2 } )
      m_forces.segment( d * m_numVertices, m_numVertices ).array() = Arg[d];

    applyMaskToVector( m_boundaryMask, m_forces );

    // Apply functional
    m_F.apply( m_forces, Dest );
  }

  void setParameters( const VectorType &parameters ) override {
    m_F.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_F.getParameters();
  }

};

template<typename ConfiguratorType>
class ThreeDForceGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;

  using ObjectiveGradType = ParametrizedBaseOp<VectorType, VectorType, VectorType>;

  const int m_numVertices;
  ObjectiveGradType &m_DF;
  const std::vector<int> &m_boundaryMask;

  mutable VectorType m_forces;
  mutable VectorType m_fullGradient;

  MatrixType reductionMatrix;
public:
  ThreeDForceGradient( const MeshTopologySaver &Topology, ObjectiveGradType &DF,
                       const std::vector<int> &BoundaryMask )
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()) {
    reductionMatrix.resize( 3, 3 * m_numVertices );
    for ( int i = 0; i < m_numVertices; i++ ) {
      if ( std::find( BoundaryMask.begin(), BoundaryMask.end(), i ) != BoundaryMask.end())
        continue;
      for ( int d : { 0, 1, 2 } )
        reductionMatrix.coeffRef( d, d * m_numVertices + i ) = 1.;
    }
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    assert( Arg.size() == 3 && "TwoDForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    for ( int d : { 0, 1, 2 } )
      m_forces.segment( d * m_numVertices, m_numVertices ).array() = Arg[d];

    applyMaskToVector( m_boundaryMask, m_forces );

    // Compute full gradient
    m_DF.apply( m_forces, m_fullGradient );

    // Compute reduced gradient
    Dest.resize( 3 );
    Dest = reductionMatrix * m_fullGradient;
  }

  void setParameters( const VectorType &parameters ) override {
    m_DF.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_DF.getParameters();
  }

};


template<typename ConfiguratorType>
class WeightedThreeDForceFunctional
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;

  using ObjectiveOpType = ParametrizedBaseOp<VectorType, RealType, VectorType>;

  const int m_numVertices;
  ObjectiveOpType &m_F;
  const std::vector<int> &m_boundaryMask;
  const VectorType &m_refGeometry;

  mutable VectorType m_forces;

public:
  WeightedThreeDForceFunctional( const MeshTopologySaver &Topology, const VectorType &refGeometry, ObjectiveOpType &F,
                                 const std::vector<int> &BoundaryMask )
          : m_F( F ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()), m_refGeometry( refGeometry ) {
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    assert( Arg.size() == 3 && "WeightedThreeDForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    VectorType scaleFactors( m_numVertices );
    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ ) {
      VectorType p( 3 );
      getXYZCoord( m_refGeometry, p, vertexIdx );

      scaleFactors[vertexIdx] = p.dot( Arg ) / Arg.squaredNorm();
    }
    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ )
      setXYZCoord( m_forces, ( scaleFactors[vertexIdx] - scaleFactors.maxCoeff()) /
                             ( scaleFactors.minCoeff() - scaleFactors.maxCoeff()) * Arg, vertexIdx );

    applyMaskToVector( m_boundaryMask, m_forces );

    // Apply functional
    m_F.apply( m_forces, Dest );
  }

  void setParameters( const VectorType &parameters ) override {
    m_F.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_F.getParameters();
  }

};

template<typename ConfiguratorType>
class WeightedThreeDForceGradient
        : public ParametrizedBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
protected:
  using RealType = typename ConfiguratorType::RealType;
  using VectorType = typename ConfiguratorType::VectorType;
  using MatrixType = typename ConfiguratorType::SparseMatrixType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;

  using ObjectiveGradType = ParametrizedBaseOp<VectorType, VectorType, VectorType>;

  const int m_numVertices;
  ObjectiveGradType &m_DF;
  const std::vector<int> &m_boundaryMask;
  const VectorType &m_refGeometry;

  mutable VectorType m_forces;
  mutable VectorType m_fullGradient;

  MatrixType reductionMatrix;
public:
  WeightedThreeDForceGradient( const MeshTopologySaver &Topology, const VectorType &refGeometry, ObjectiveGradType &DF,
                               const std::vector<int> &BoundaryMask )
          : m_DF( DF ), m_boundaryMask( BoundaryMask ), m_numVertices( Topology.getNumVertices()),
            m_forces( 3 * Topology.getNumVertices()), m_refGeometry( refGeometry ) {
    reductionMatrix.resize( 3, 3 * m_numVertices );
    for ( int i = 0; i < m_numVertices; i++ ) {
      if ( std::find( BoundaryMask.begin(), BoundaryMask.end(), i ) != BoundaryMask.end())
        continue;
      for ( int d : { 0, 1, 2 } )
        reductionMatrix.coeffRef( d, d * m_numVertices + i ) = 1.;
    }
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    assert( Arg.size() == 3 && "WeightedTwoDForceFunctional::apply: Wrong size of input!" );

    // Construct full force field
    VectorType scaleFactors( m_numVertices );
    std::vector<VectorType> scaleGradients( m_numVertices, VectorType::Zero( 3 ));
    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ ) {
      VectorType p( 3 );
      getXYZCoord( m_refGeometry, p, vertexIdx );

      scaleFactors[vertexIdx] = p.dot( Arg ) / Arg.squaredNorm();
      scaleGradients[vertexIdx] =
              ( p * Arg.squaredNorm() - 2 * Arg * p.dot( Arg )) / ( Arg.squaredNorm() * Arg.squaredNorm());
    }

    int maxIdx, minIdx;
    RealType maxScale = scaleFactors.maxCoeff( &maxIdx );
    RealType minScale = scaleFactors.minCoeff( &minIdx );

    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ )
      setXYZCoord( m_forces, ( scaleFactors[vertexIdx] - maxScale ) / ( minScale - maxScale ) * Arg, vertexIdx );

    applyMaskToVector( m_boundaryMask, m_forces );

    // Compute full gradient
    m_DF.apply( m_forces, m_fullGradient );

    // Compute reduced gradient
    Dest.resize( 3 );
    Dest.setZero();
    for ( int vertexIdx = 0; vertexIdx < m_numVertices; vertexIdx++ ) {
      VectorType grad( 3 );
      getXYZCoord( m_fullGradient, grad, vertexIdx );


      VectorType tmp = ( scaleGradients[vertexIdx] - scaleGradients[maxIdx] ) * ( minScale - maxScale ) -
                       ( scaleFactors[vertexIdx] - maxScale ) * ( scaleGradients[minIdx] - scaleGradients[maxIdx] );

      FullMatrixType localGrad =
              ( scaleFactors[vertexIdx] - maxScale ) / ( minScale - maxScale ) * FullMatrixType::Identity( 3, 3 );
      localGrad += 1. / (( minScale - maxScale ) * ( minScale - maxScale )) * tmp * Arg.transpose();

      Dest += localGrad * grad;
    }
  }

  void setParameters( const VectorType &parameters ) override {
    m_DF.setParameters( parameters );
  }

  const VectorType &getParameters() override {
    return m_DF.getParameters();
  }

};

#endif //BILEVELSHAPEOPT_3DFORCES_H
