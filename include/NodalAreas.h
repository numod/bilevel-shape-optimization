//
// Created by josua on 17.06.20.
//

#ifndef BILEVELSHAPEOPT_NODALAREAS_H
#define BILEVELSHAPEOPT_NODALAREAS_H

#include <goast/Core/Topology.h>
#include <goast/Core/TriangleGeometry.h>

/**
 * \brief Compute negative and transposed gradient-matrix of area-force term, nodal areas derived in direction of S and deformation derived in direction of S
 * \author Echelmeyer
 * for further details check master thesis of Kai Echelmeyer: -DA_F^T
 */
template<typename ConfiguratorType>
class computeNodalAreaForceGradientMatrix_Negative_Transposed { //: public BaseOp<VectorType, TripletListType>

protected:
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::TripletType TripletType;
  using TripletListType = std::vector<TripletType>;

  const MeshTopologySaver &_topology;
  const VectorType &_Forces;
  const int _numVertices;
  const int _numFaces;

public:

  computeNodalAreaForceGradientMatrix_Negative_Transposed( const MeshTopologySaver &Topology, const VectorType &Forces )
          : _topology( Topology ), _Forces( Forces ), _numVertices( _topology.getNumVertices()),
            _numFaces( _topology.getNumFaces()) {

  }

  void pushTriplets( const VectorType &Arg, TripletListType &Dest, int rowOffset, int colOffset ) const {

    VecType local_gradient;
    VecType Pi, Pj, Pk;
    VectorType Coords( 3 ), defCoords( 3 ), Forces( 3 );

    for ( int faceIdx = 0; faceIdx < _numFaces; ++faceIdx ) {

      //indices of resp. vertices
      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));

      //get coords of resp. vertices
      getXYZCoord( Arg, Pi, pi );
      getXYZCoord( Arg, Pj, pj );
      getXYZCoord( Arg, Pk, pk );

      //++++++++++++++++++compute area gradient locally in direction of Pi and compute matrix entries++++++++++++++++++++++++++++++++++
      getAreaGradient( Pj, Pk, Pi, local_gradient );
      local_gradient /= 3;
      //scale with forces at vertex pi
      for ( int j = 0; j < 3; j++ ) {
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + i * _numVertices + rowOffset, pi + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + i * _numVertices + rowOffset, pi + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + i * _numVertices + rowOffset, pi + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
      }

      //++++++++++++++++++compute area gradient locally in direction of Pj and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pk, Pi, Pj, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + i * _numVertices + rowOffset, pj + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + i * _numVertices + rowOffset, pj + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + i * _numVertices + rowOffset, pj + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
      }

      //++++++++++++++++++compute area gradient locally in direction of Pk and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pi, Pj, Pk, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + i * _numVertices + rowOffset, pk + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + i * _numVertices + rowOffset, pk + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + i * _numVertices + rowOffset, pk + j * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
      }


    }

  }

};

template<typename ConfiguratorType>
class computeNodalAreaForceGradientMatrix_Negative { //: public BaseOp<VectorType, TripletListType>

protected:
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::TripletType TripletType;
  using TripletListType = std::vector<TripletType>;

  const MeshTopologySaver &_topology;
  const VectorType &_Forces;
  const int _numVertices;
  const int _numFaces;

public:

  computeNodalAreaForceGradientMatrix_Negative( const MeshTopologySaver &Topology, const VectorType &Forces )
          : _topology( Topology ), _Forces( Forces ), _numVertices( _topology.getNumVertices()),
            _numFaces( _topology.getNumFaces()) {

  }

  void pushTriplets( const VectorType &Arg, TripletListType &Dest, int rowOffset, int colOffset ) const {

    VecType local_gradient;
    VecType Pi, Pj, Pk;
    VectorType Coords( 3 ), defCoords( 3 ), Forces( 3 );

    for ( int faceIdx = 0; faceIdx < _numFaces; ++faceIdx ) {

      //indices of resp. vertices
      int pi = _topology.getNodeOfTriangle( faceIdx, 0 ),
              pj = _topology.getNodeOfTriangle( faceIdx, 1 ),
              pk = _topology.getNodeOfTriangle( faceIdx, 2 );

      //get coords of resp. vertices
      getXYZCoord( Arg, Pi, pi );
      getXYZCoord( Arg, Pj, pj );
      getXYZCoord( Arg, Pk, pk );

      //++++++++++++++++++compute area gradient locally in direction of Pi and compute matrix entries++++++++++++++++++++++++++++++++++
      getAreaGradient( Pj, Pk, Pi, local_gradient );
      local_gradient /= 3;
      //scale with forces at vertex pi
      for ( int j = 0; j < 3; j++ ) {
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + j * _numVertices + rowOffset, pi + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + j * _numVertices + rowOffset, pj + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + j * _numVertices + rowOffset, pk + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
      }
      //++++++++++++++++++compute area gradient locally in direction of Pj and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pk, Pi, Pj, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + j * _numVertices + rowOffset, pi + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + j * _numVertices + rowOffset, pj + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + j * _numVertices + rowOffset, pk + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
      }

      //++++++++++++++++++compute area gradient locally in direction of Pk and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pi, Pj, Pk, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + j * _numVertices + rowOffset, pi + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + j * _numVertices + rowOffset, pj + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + j * _numVertices + rowOffset, pk + i * _numVertices + colOffset,
                             -local_gradient[j] * Forces[i] );
        }
      }


    }

  }

};

template<typename ConfiguratorType>
class computeNodalAreaForceGradientMatrix_Transposed { //: public BaseOp<VectorType, TripletListType>

protected:
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::TripletType TripletType;
  using TripletListType = std::vector<TripletType>;

  const MeshTopologySaver &_topology;
  const VectorType &_Forces;
  const int _numVertices;
  const int _numFaces;

public:

  computeNodalAreaForceGradientMatrix_Transposed( const MeshTopologySaver &Topology, const VectorType &Forces )
          : _topology( Topology ), _Forces( Forces ), _numVertices( _topology.getNumVertices()),
            _numFaces( _topology.getNumFaces()) {

  }

  void pushTriplets( const VectorType &Arg, TripletListType &Dest, int rowOffset, int colOffset ) const {

    VecType local_gradient;
    VecType Pi, Pj, Pk;
    VectorType Coords( 3 ), defCoords( 3 ), Forces( 3 );

    for ( int faceIdx = 0; faceIdx < _numFaces; ++faceIdx ) {

      //indices of resp. vertices
      int pi( _topology.getNodeOfTriangle( faceIdx, 0 )),
              pj( _topology.getNodeOfTriangle( faceIdx, 1 )),
              pk( _topology.getNodeOfTriangle( faceIdx, 2 ));

      //get coords of resp. vertices
      getXYZCoord( Arg, Pi, pi );
      getXYZCoord( Arg, Pj, pj );
      getXYZCoord( Arg, Pk, pk );

      //++++++++++++++++++compute area gradient locally in direction of Pi and compute matrix entries++++++++++++++++++++++++++++++++++
      getAreaGradient( Pj, Pk, Pi, local_gradient );
      local_gradient /= 3;
      //scale with forces at vertex pi
      for ( int j = 0; j < 3; j++ ) {
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + i * _numVertices + rowOffset, pi + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + i * _numVertices + rowOffset, pi + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + i * _numVertices + rowOffset, pi + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
      }

      //++++++++++++++++++compute area gradient locally in direction of Pj and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pk, Pi, Pj, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + i * _numVertices + rowOffset, pj + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + i * _numVertices + rowOffset, pj + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + i * _numVertices + rowOffset, pj + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
      }

      //++++++++++++++++++compute area gradient locally in direction of Pk and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pi, Pj, Pk, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + i * _numVertices + rowOffset, pk + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + i * _numVertices + rowOffset, pk + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + i * _numVertices + rowOffset, pk + j * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
      }

    }

  }

};


template<typename ConfiguratorType>
class computeNodalAreaForceGradientMatrix { //: public BaseOp<VectorType, TripletListType>

protected:
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::VecType VecType;
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::TripletType TripletType;
  using TripletListType = std::vector<TripletType>;

  const MeshTopologySaver &_topology;
  const VectorType &_Forces;
  const int _numVertices;
  const int _numFaces;

public:

  computeNodalAreaForceGradientMatrix( const MeshTopologySaver &Topology, const VectorType &Forces )
          : _topology( Topology ), _Forces( Forces ), _numVertices( _topology.getNumVertices()),
            _numFaces( _topology.getNumFaces()) {

  }

  void pushTriplets( const VectorType &Arg, TripletListType &Dest, int rowOffset, int colOffset ) const {

    VecType local_gradient;
    VecType Pi, Pj, Pk;
    VectorType Coords( 3 ), defCoords( 3 ), Forces( 3 );

    for ( int faceIdx = 0; faceIdx < _numFaces; ++faceIdx ) {

      //indices of resp. vertices
      int pi = _topology.getNodeOfTriangle( faceIdx, 0 ),
              pj = _topology.getNodeOfTriangle( faceIdx, 1 ),
              pk = _topology.getNodeOfTriangle( faceIdx, 2 );

      //get coords of resp. vertices
      getXYZCoord( Arg, Pi, pi );
      getXYZCoord( Arg, Pj, pj );
      getXYZCoord( Arg, Pk, pk );

      //++++++++++++++++++compute area gradient locally in direction of Pi and compute matrix entries++++++++++++++++++++++++++++++++++
      getAreaGradient( Pj, Pk, Pi, local_gradient );
      local_gradient /= 3;
      //scale with forces at vertex pi
      for ( int j = 0; j < 3; j++ ) {
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + j * _numVertices + rowOffset, pi + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + j * _numVertices + rowOffset, pj + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pi + j * _numVertices + rowOffset, pk + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
      }
      //++++++++++++++++++compute area gradient locally in direction of Pj and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pk, Pi, Pj, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + j * _numVertices + rowOffset, pi + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + j * _numVertices + rowOffset, pj + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pj + j * _numVertices + rowOffset, pk + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
      }

      //++++++++++++++++++compute area gradient locally in direction of Pk and compute matrix entries++++++++++++++++++++++++++++++++++
      for ( int j = 0; j < 3; j++ ) {
        getAreaGradient( Pi, Pj, Pk, local_gradient );
        local_gradient /= 3;
        //scale with forces at vertex pi
        getXYZCoord( _Forces, Forces, pi );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + j * _numVertices + rowOffset, pi + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pj
        getXYZCoord( _Forces, Forces, pj );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + j * _numVertices + rowOffset, pj + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
        //scale with forces at vertex pk
        getXYZCoord( _Forces, Forces, pk );
        for ( int i = 0; i < 3; i++ ) {
          Dest.emplace_back( pk + j * _numVertices + rowOffset, pk + i * _numVertices + colOffset,
                             local_gradient[j] * Forces[i] );
        }
      }


    }

  }

};

#endif //BILEVELSHAPEOPT_NODALAREAS_H
