/**
 * \brief Compute optimal force for certain material distribution
 */

//==============Includes================================
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/Core/FEM.h"
#include "goast/external/vtkIO.h"

#include "ComplianceTerm.h"
#include "LinearElasticitySolver.h"
#include "ShapeOptimization/ReducedFunctional.h"
#include "ShapeOptimization/ExpectedValue.h"
#include "Optimization/StochasticGradientDescent.h"
#include "MaterialGenerators.h"
//======================================================

//================================================================================

typedef DefaultConfigurator ConfiguratorType;
typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::VecType VecType;
typedef typename ConfiguratorType::MatType MatType;
typedef std::vector<TripletType> TripletListType;

//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {
  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";


  // Material
  RealType thickness = 0.03;
  RealType materialFactor = 20.;
  RealType splitOffset = 0.25;
  RealType minAngle = 0;
  RealType maxAngle = M_PI_4;

  bool writeExamples = true;


  // Boundary values
  std::vector<int> bndMask;
  bool detectBoundary = true;

  // Forces
  RealType force_x = 0.;
  RealType force_y = 0.;
  RealType force_z = -0.02;

  RealType forceBound = 0.03;

  // Cost functionals
  std::vector<int> trackingIndices;
  int trackingIndicesModulo = 10;

  RealType alpha_reg = 0.01;

  // Shape optimization
  RealType shapeOpt_tolerance = 1e-6;
  int shapeOpt_maxIterations = 10000;
  RealType shapeOpt_stepsize = 5e-2;


  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Additional
  bool testDerivatives = false;

  //</editor-fold>

  //<editor-fold desc="Output folders">
  if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
    outputFolder += "/";

  std::string execName( argv[0] );
  execName = execName.substr( execName.find_last_of( '/' ) + 1 );

  if ( timestampOutput ) {
    std::time_t t = std::time( nullptr );
    std::stringstream ss;
    ss << outputFolder;
    ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
    ss << "_" << execName;
    ss << "/";
    outputFolder = ss.str();
    boost::filesystem::create_directory( outputFolder );
  }

  if ( argc == 2 ) {
    boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                  boost::filesystem::copy_option::overwrite_if_exists );
  }

  std::string outputPrefix = outputFolder + outputFilePrefix;
  //</editor-fold>

  //<editor-fold desc="Output log">
  std::ofstream logFile;
  logFile.open( outputFolder + "/_output.log" );

  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
  typedef boost::iostreams::stream<TeeDevice> TeeStream;
  TeeDevice tee_cout( logFile, output_cout );
  TeeDevice tee_cerr( logFile, output_cerr );

  TeeStream split_cout( tee_cout );
  TeeStream split_cerr( tee_cerr );

  std::cout.rdbuf( split_cout.rdbuf());
  std::cerr.rdbuf( split_cerr.rdbuf());
  //</editor-fold>

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;


    // Material
    VectorType vertexWeights_const = VectorType::Constant( numVertices, thickness );
    VectorType vertexWeights = VectorType::Constant( numVertices, thickness );

    BeamGenerator g( thickness, minAngle, maxAngle, refGeometry, splitOffset, materialFactor );
    vertexWeights = g();

    // Deformation energies
    MaterialShellDeformationType W( Topology, vertexWeights );
    MaterialShellDeformationType W_constant( Topology, vertexWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletBoundary;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletBoundary );

    dirichletBoundary.insert( dirichletBoundary.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;

    // Tracking Type Indices
    if ( trackingIndices.empty()) {
      for ( int i = 0; i < numVertices; i += trackingIndicesModulo ) {
        trackingIndices.push_back( i );
      }
    }
    std::cout << " .. Tracking done" << std::endl;

    // ========================================================================
    // ============================== Functionals =============================
    // ========================================================================


    // Output start values
    saveAsVTP( Topology, refGeometry, outputPrefix + "stochastic_start.vtp", vertexWeights, "weights" );

    VectorType vertexForces( 3 * numVertices );
    vertexForces.segment( 0 * numVertices, numVertices ).array() = force_x;
    vertexForces.segment( 1 * numVertices, numVertices ).array() = force_y;
    vertexForces.segment( 2 * numVertices, numVertices ).array() = force_z;

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Optimization" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // Setup functionals
    LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> LES( Topology, refGeometry,
                                                                                   dirichletBoundary, undefDisplacement,
                                                                                   vertexForces, vertexWeights,
                                                                                   false, true, false,
                                                                                   false, false, true );

    VectorType Solution = LES.solve( vertexForces );

    saveAsVTP<VectorType>( Topology, refGeometry + Solution,
                           outputPrefix + "stochastic_start_def.vtp", vertexWeights, "weights" );

    VectorType combinedVector( vertexForces.size() + Solution.size());
    combinedVector.head( vertexForces.size()) = vertexForces;
    combinedVector.tail( Solution.size()) = Solution;

    ComplianceTerm<DefaultConfigurator, MaterialShellDeformationType> C( Topology, refGeometry, W, false, true, false,
                                                                         false, false, true );
    ComplianceTermGradient<DefaultConfigurator, MaterialShellDeformationType> DC( Topology, refGeometry, W, false, true,
                                                                                  false, false, false, true );

    VectorType functionalWeights = VectorType::Constant( 1, -1. );

    ParametrizedAdditionOp<DefaultConfigurator> J( functionalWeights, C );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( functionalWeights, DC );

    ReducedPDECParametrizedFunctional<DefaultConfigurator> Jred( J, LES );
    ReducedPDECParametrizedGradient<DefaultConfigurator> DJred( J, DJ, LES, dirichletBoundary );

    // Stochastic config
    SampledExpectedValue<ReducedPDECParametrizedFunctional<DefaultConfigurator>, BeamGenerator> EJ( Jred, g, 30 );
    SampledExpectedValue<ReducedPDECParametrizedGradient<DefaultConfigurator>, BeamGenerator> DEJ( DJred, g, 30 );
    DEJ.setSamples( EJ.getSamples());

    if ( writeExamples ) {
      std::map<std::string, VectorType> colorings;
      for ( int i = 0; i < 10; i++ )
        colorings["Material_" + std::to_string( i )] = g();

      saveAsVTP( Topology, refGeometry, outputPrefix + "material_examples.vtp", colorings );
    }

    // Optimization
    std::vector<int> fixedVariables;
//    for ( int i = 0; i < numVertices; i++ )
//      fixedVariables.push_back( 2 * numVertices + i );
    for ( auto idx : dirichletBoundary )
      fixedVariables.push_back( idx );

    VectorType newForce( vertexForces );
    VectorType lowerBounds = VectorType::Constant( 3 * numVertices, -forceBound );
    VectorType upperBounds = VectorType::Constant( 3 * numVertices, forceBound );

    if ( testDerivatives )
      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-6 ).plotAllDirections( vertexForces,
                                                                                                outputPrefix +
                                                                                                "testDJred_" );


    //!  \todo add L2-constraint
    StochasticGradientDescent<DefaultConfigurator> Solver( EJ, DEJ, 500, 1e-6, SHOW_ALL );
    Solver.setFixedVariables( fixedVariables );
//    Solver.setVariableBounds( lowerBounds, upperBounds );
    Solver.setMaximalNorm( std::sqrt( numDOFs ) * forceBound );

    auto t_start = std::chrono::high_resolution_clock::now();
    Solver.solve( vertexForces, newForce );
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout << " - Solution took " << std::fixed
              << std::chrono::duration<double, std::ratio<1> >( t_end - t_start ).count() << "s." << std::endl;

    Solution = LES.solve( newForce );

    defGeometry = refGeometry + Solution;
    std::map<std::string, VectorType> colorings;
    colorings["Force"] = newForce;
    colorings["initForce"] = vertexForces;
    colorings["weights"] = vertexWeights;
    colorings["weights_LES"] = LES.m_vertexMaterial;
    saveAsVTP( Topology, defGeometry, outputPrefix + "stochastic_force_opt.vtp", colorings );

    for ( int i = 0; i < 10; i++ ) {
      vertexWeights = g();
      LES.updateParameters( vertexWeights );
      Solution = LES.solve( newForce );
      defGeometry = refGeometry + Solution;

      colorings["weights"] = vertexWeights;
      colorings["weights_LES"] = LES.m_vertexMaterial;
      saveAsVTP( Topology, defGeometry, outputPrefix + "stochastic_force_opt_" + std::to_string( i ) + ".vtp",
                 colorings );
    }

    std::cout << "===============================================================" << std::endl;

//    std::cout << newForce << std::endl;

    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}