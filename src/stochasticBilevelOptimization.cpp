#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/external/vtkIO.h"
#include "goast/external/ipoptBoxConstraintSolver.h"

#include "Optimization/ProximalGradientDescent.h"
#include "Optimization/ProximalMappings.h"

#include "ShapeOptimization/ReducedFunctional.h"

#include "MaterialDeformationEnergies.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ComplianceTerm.h"
#include "FixedComplianceTerm.h"
#include "TrackingCost.h"
#include "MaterialRegularization.h"
#include "OptimalForceSolver.h"
#include "LinearOptimalForceSolver.h"
#include "BilevelReducedFunctional.h"
#include "Optimization/StochasticGradientDescent.h"

#include "ReducedForces.h"
#include "SimpleQuadraticFunctional.h"
#include "stochasticBilevelOptimization.h"

//================================================================================
class NormalNoiseGenerator {
public:
  using VectorType = typename DefaultConfigurator::VectorType;
  using RealType = typename DefaultConfigurator::RealType;
  using SampleType = VectorType;

  NormalNoiseGenerator( int numFaces, RealType stddev ) : m_randomEngine{ std::random_device{}() },
                                                          m_distribution{ 1, stddev }, m_numFaces{ numFaces },
                                                          m_minValue{ 0.01 }, m_maxValue{ 2 } {
  }

  VectorType operator()() const {
    RealType splitAngle = m_distribution( m_randomEngine );

    VectorType faceWeights( m_numFaces );
    for ( int faceIdx = 0; faceIdx < m_numFaces; faceIdx++ ) {
      faceWeights[faceIdx] = std::max( m_minValue, std::min( m_maxValue, m_distribution( m_randomEngine )));
//      faceWeights[faceIdx] = m_distribution( m_randomEngine );
    }
    return faceWeights;
  }

private:
  const int m_numFaces;
  const RealType m_minValue;
  const RealType m_maxValue;
  mutable std::mt19937 m_randomEngine;
  mutable std::normal_distribution<RealType> m_distribution;
};





//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {
  using RealType = DefaultConfigurator::RealType;
  using VectorType = DefaultConfigurator::VectorType;
  using MatrixType = DefaultConfigurator::SparseMatrixType;
  using TripletType = DefaultConfigurator::TripletType;
  using FullMatrixType = DefaultConfigurator::FullMatrixType;
  using TripletListType = std::vector<TripletType>;
  using VecType = DefaultConfigurator::VecType;

  using MaterialShellDeformationType = MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > ;

  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";

  // Boundary values
  std::vector<int> bndMask{}; // 171
  std::vector<int> trackingIndices{}; // 171
  std::vector<int> dirichletFaces{}; // 171
  bool detectBoundary = true;

  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Material
  RealType initialThickness = 0.03;
  RealType boundaryThickness = 0.03;
  RealType materialBound = 0.02;
  RealType randomPerturbation = 0.;
  bool useL2Constraint = true;


  // Forces
  std::string forceSpace = "twistAndCompress"; // twistAndCompress, Constant
  std::vector<RealType> forceInitialization;


  // shape optimization
  struct {
    bool useNonlinearElasticity = true;
    int maxNumIterations = 100;
    int numSamples = 32;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 1e-3;
    TIMESTEP_CONTROLLER stepsizeController = CONST_TIMESTEP_CONTROL;
    bool acceleration = false;
    RealType trackingWeight = 1.;
    RealType MassWeight = 0.;
    RealType MassBarrier = 1000.;
    RealType LowerPointBarrierWeight = 0.;
    RealType UpperPointBarrierWeight = 0.;
    RealType LowerPointBarrier = 0.;
    RealType UpperPointBarrier = 0.;
    int preIterations = 0;
  } BilevelOptimization;

  // Additional
  bool testDerivatives = false;


  //</editor-fold>


  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );

      meshPath = config["Geometry"]["mesh"].as<std::string>();
      bndMask = config["Geometry"]["boundaryMask"].as<std::vector<int>>();
      trackingIndices = config["Geometry"]["trackingIndices"].as<std::vector<int>>();
      dirichletFaces = config["Geometry"]["dirichletFaces"].as<std::vector<int>>();
      detectBoundary = config["Geometry"]["detectBoundary"].as<bool>();

      outputFilePrefix = config["Output"]["outputFilePrefix"].as<std::string>();
      outputFolder = config["Output"]["outputFolder"].as<std::string>();
      timestampOutput = config["Output"]["timestampOutput"].as<bool>();

      forceSpace = config["Forces"]["space"].as<std::string>();
      forceInitialization = config["Forces"]["initialization"].as<std::vector<RealType>>();

      initialThickness = config["Material"]["initialThickness"].as<RealType>();
      boundaryThickness = config["Material"]["boundaryThickness"].as<RealType>();
      materialBound = config["Material"]["materialBound"].as<RealType>();
      randomPerturbation = config["Material"]["randomPerturbation"].as<RealType>();
      useL2Constraint = config["Material"]["useL2Constraint"].as<bool>();

      BilevelOptimization.useNonlinearElasticity = ( config["BilevelOptimization"]["Elasticity"].as<std::string>() ==
                                                     "Nonlinear" );
      BilevelOptimization.maxNumIterations = config["BilevelOptimization"]["maxNumIterations"].as<int>();
      BilevelOptimization.numSamples = config["BilevelOptimization"]["numSamples"].as<int>();
      BilevelOptimization.optimalityTolerance = config["BilevelOptimization"]["optimalityTolerance"].as<RealType>();
      BilevelOptimization.stepsize = config["BilevelOptimization"]["stepsize"].as<RealType>();
      BilevelOptimization.maximalStepsize = config["BilevelOptimization"]["maximalStepsize"].as<RealType>();
      BilevelOptimization.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["BilevelOptimization"]["stepsizeController"].as<int>());
      BilevelOptimization.acceleration = config["BilevelOptimization"]["acceleration"].as<bool>();
      BilevelOptimization.trackingWeight = config["BilevelOptimization"]["trackingWeight"].as<RealType>();
      BilevelOptimization.MassWeight = config["BilevelOptimization"]["MassWeight"].as<RealType>();
      BilevelOptimization.MassBarrier = config["BilevelOptimization"]["MassBarrier"].as<RealType>();
      BilevelOptimization.LowerPointBarrierWeight = config["BilevelOptimization"]["LowerPointBarrierWeight"].as<RealType>();
      BilevelOptimization.UpperPointBarrierWeight = config["BilevelOptimization"]["UpperPointBarrierWeight"].as<RealType>();
      BilevelOptimization.LowerPointBarrier = config["BilevelOptimization"]["LowerPointBarrier"].as<RealType>();
      BilevelOptimization.UpperPointBarrier = config["BilevelOptimization"]["UpperPointBarrier"].as<RealType>();
      BilevelOptimization.preIterations = config["BilevelOptimization"]["preIterations"].as<int>();

      testDerivatives = config["Additional"]["testDerivatives"].as<bool>();
    }


    //<editor-fold desc="Output folders">
    if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
      outputFolder += "/";

    std::string execName( argv[0] );
    execName = execName.substr( execName.find_last_of( '/' ) + 1 );

    std::string outputFolderName;

    if ( timestampOutput ) {
      std::time_t t = std::time( nullptr );
      std::stringstream ss;
      ss << outputFolder;
      ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss << "_" << execName;
      ss << "/";
      outputFolder = ss.str();

      std::stringstream ss2;
      ss2 << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss2 << "_" << execName;
      outputFolderName = ss2.str();

      boost::filesystem::create_directory( outputFolder );
      std::cout << " ----------- Output folder: " << outputFolder << " ----------- " << std::endl;
    }

    if ( argc == 2 ) {
      boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                    boost::filesystem::copy_option::overwrite_if_exists );
    }

    std::string outputPrefix = outputFolder + outputFilePrefix;
    //</editor-fold>

    //<editor-fold desc="Output log">
    std::ofstream logFile;
    logFile.open( outputFolder + "/_output.log" );


    typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
    typedef boost::iostreams::stream<TeeDevice> TeeStream;
    TeeDevice tee_cout( logFile, output_cout );
    TeeDevice tee_cerr( logFile, output_cerr );

    TeeStream split_cout( tee_cout );
    TeeStream split_cerr( tee_cerr );

    std::cout.rdbuf( split_cout.rdbuf());
    std::cerr.rdbuf( split_cerr.rdbuf());
    //</editor-fold>

    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();
    const int numFaces = Topology.getNumFaces();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    //</editor-fold>

    //<editor-fold desc="Material">
    VectorType faceAreas,nodalAreas;
    getFaceAreas<DefaultConfigurator>( Topology, refGeometry, faceAreas );
    computeNodalAreas<DefaultConfigurator>( Topology, refGeometry, nodalAreas );

//    VectorType vertexWeights_const = VectorType::Constant( numFaces, initialThickness );
    VectorType vertexWeights = VectorType::Constant( numFaces, initialThickness );
//    vertexWeights += VectorType::Random( numFaces ) * initialThickness * randomPerturbation;

    for ( int faceIdx : dirichletFaces ) {
      vertexWeights[faceIdx] = boundaryThickness;
      BilevelOptimization.MassBarrier += faceAreas[faceIdx] * boundaryThickness;
    }
    //</editor-fold>

    // Deformation energies
    MaterialShellDeformationType W( Topology, vertexWeights );
//    MaterialShellDeformationType W_constant( Topology, vertexWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletIndices;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletIndices );

    dirichletIndices.insert( dirichletIndices.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    std::vector<int> dirichletBoundary = dirichletIndices;
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;

    std::cout << " - dirichletBoundary = { ";
    for ( auto v : dirichletBoundary )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    // Forces
    MatrixType massMatrix;
    computeLumpedMassMatrix<DefaultConfigurator>( Topology, refGeometry, massMatrix, true );

    FullMatrixType forceBasis;
    if ( forceSpace == "Constant" ) {
      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.setZero();
      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ).segment( d * numVertices, numVertices ).setConstant( 1. );
      for ( int d : { 0, 1, 2 } )
        std::cout << " .. norm = " << forceBasis.col( d ).norm() << std::endl;

      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ) = forceBasis.col( d ).normalized();
    }
    else if ( forceSpace == "twistAndCompress" ) {
      VectorType horizontalCompressionForce( 3 * numVertices );
      VectorType verticalCompressionForce( 3 * numVertices );
      VectorType twistingForce( 3 * numVertices );

      auto VCFProfile = []( RealType z ) { return z / 4.; };
      auto HCFProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
//      auto HCFProfile = []( RealType z ) { return 1.; };
//      auto TProfile = []( RealType z ) { return ( z - 2. ) / 2.; };
//      auto TProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
      auto TProfile = []( RealType z ) { return z / 4.; };

      for ( int i = 0; i < Topology.getNumVertices(); i++ ) {
        VectorType p( 3 );
        getXYZCoord( refGeometry, p, i );

        VectorType normal = p;
        normal[2] = 0.;
        normal.normalize();

        VectorType hcf = normal * HCFProfile( p[2] );
        setXYZCoord( horizontalCompressionForce, hcf, i );

        VectorType vcf = VectorType::Zero( 3 );
        vcf[2] = VCFProfile( p[2] );
        setXYZCoord( verticalCompressionForce, vcf, i );

        VectorType twist = normal;
        twist[0] = normal[1];
        twist[1] = -normal[0];
        twist *= TProfile( p[2] );
        setXYZCoord( twistingForce, twist, i );
      }


      std::cout << " .. hCF.norm = " << horizontalCompressionForce.norm() << std::endl;
      std::cout << " .. twist.norm = " << twistingForce.norm() << std::endl;
      std::cout << " .. vCF.norm = " << verticalCompressionForce.norm() << std::endl;

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = horizontalCompressionForce.normalized();
      forceBasis.col( 1 ) = twistingForce.normalized();
      forceBasis.col( 2 ) = verticalCompressionForce.normalized();

//      forceBasis.col( 0 ) = horizontalCompressionForce;
//      forceBasis.col( 1 ) = twistingForce;
//      forceBasis.col( 2 ) = verticalCompressionForce;
//      forceBasis.col( 0 ) /= std::sqrt(horizontalCompressionForce.transpose() * massMatrix * horizontalCompressionForce);
//      forceBasis.col( 1 ) /= std::sqrt(twistingForce.transpose() * massMatrix * twistingForce);
//      forceBasis.col( 2 ) /= std::sqrt(verticalCompressionForce.transpose() * massMatrix * verticalCompressionForce);
    }
    else if ( forceSpace == "WindAndSnow"  ) {
      std::vector<VecType> vertexNormals;
      computeVertexNormals<DefaultConfigurator>( Topology, refGeometry, vertexNormals, true );

      VectorType snowForce( 3 * numVertices );
      VectorType windForceA( 3 * numVertices );
      VectorType windForceB( 3 * numVertices );

      for ( int vertexIdx = 0; vertexIdx < Topology.getNumVertices(); vertexIdx++ ) {
        const VecType &n = vertexNormals[vertexIdx];

        VecType d;
        d[0] = std::abs( n[0] );
        setXYZCoord( snowForce, d, vertexIdx );

        d.setZero();
        d[1] = std::abs( n[1] );
        setXYZCoord( windForceA, d, vertexIdx );

        d.setZero();
        d[2] = std::abs( n[2] );
        setXYZCoord( windForceB, d, vertexIdx );
      }

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = snowForce.normalized();
      forceBasis.col( 1 ) = windForceA.normalized();
      forceBasis.col( 2 ) = windForceB.normalized();

//      forceBasis.col( 0 ) = snowForce;
//      forceBasis.col( 1 ) = windForceA;
//      forceBasis.col( 2 ) = windForceB;
//      forceBasis.col( 0 ) /= std::sqrt(snowForce.transpose() * massMatrix * snowForce);
//      forceBasis.col( 1 ) /= std::sqrt(windForceA.transpose() * massMatrix * windForceA);
//      forceBasis.col( 2 ) /= std::sqrt(windForceB.transpose() * massMatrix * windForceB);
    }
    else if ( forceSpace == "WindAndSnow-reg" ) {
      std::vector<VecType> vertexNormals;
      computeVertexNormals<DefaultConfigurator>( Topology, refGeometry, vertexNormals, true );

      VectorType snowForce( 3 * numVertices );
      VectorType windForceA( 3 * numVertices );
      VectorType windForceB( 3 * numVertices );

      for ( int vertexIdx = 0; vertexIdx < Topology.getNumVertices(); vertexIdx++ ) {
        const VecType &n = vertexNormals[vertexIdx];

        VecType d;
        d[0] = std::abs( n[0] );
        d[1] = 1.e-1;
        d[2] = 1.e-1;
        setXYZCoord( snowForce, d, vertexIdx );

        d.setZero();
        d[1] = std::abs( n[1] );
        d[0] = 1.e-1;
        d[2] = 1.e-1;
        setXYZCoord( windForceA, d, vertexIdx );

        d.setZero();
        d[2] = std::abs( n[2] );
        d[0] = 1.e-1;
        d[1] = 1.e-1;
        setXYZCoord( windForceB, d, vertexIdx );
      }

      forceBasis.resize( 3 * numVertices, 3 );
//      forceBasis.col( 0 ) = snowForce.normalized();
//      forceBasis.col( 1 ) = windForceA.normalized();
//      forceBasis.col( 2 ) = windForceB.normalized();
      forceBasis.col( 0 ) = snowForce;
      forceBasis.col( 1 ) = windForceA;
      forceBasis.col( 2 ) = windForceB;
      forceBasis.col( 0 ) /= std::sqrt(snowForce.transpose() * massMatrix * snowForce);
      forceBasis.col( 1 ) /= std::sqrt(windForceA.transpose() * massMatrix * windForceA);
      forceBasis.col( 2 ) /= std::sqrt(windForceB.transpose() * massMatrix * windForceB);
    }


    for ( int idx : dirichletBoundary )
      forceBasis.row( idx ).array() = 0.;

    VectorType ldForce( forceInitialization.size());
    for ( int i = 0; i < forceInitialization.size(); i++ )
      ldForce[i] = forceInitialization[i];

    VectorType vertexForces = forceBasis * ldForce;

    // Output start values
    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = vertexWeights;
      colorings["Force"] = vertexForces;
      for (int i = 0; i < forceBasis.cols(); i++)
        colorings["ForceBasis_" + std::to_string(i)] = forceBasis.col(i);
      saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_start.vtp", colorings );
    }

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Optimization" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // DOF variable
    VectorType combinedVector( numVertices + numDOFs );
    combinedVector.head( numFaces ) = vertexWeights;
    combinedVector.tail( numDOFs ).setZero();

    LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> ES( Topology, refGeometry,
                                                                                  dirichletBoundary,
                                                                                  undefDisplacement,
                                                                                  vertexForces, vertexWeights,
                                                                                  false, true, true,
                                                                                  false, false, false );
    if ( trackingIndices.empty()) {
      trackingIndices.resize( Topology.getNumVertices());
      std::iota( trackingIndices.begin(), trackingIndices.end(), 0 );
    }
    VectorType DesignAndForce( 3 * numVertices + numFaces);
    DesignAndForce.head( 3 * numVertices) = vertexForces;
    DesignAndForce.tail( numFaces ) = vertexWeights;

    VectorType Solution = ES.solve(DesignAndForce);

    VectorType DesignForceDeformation( 3 * numVertices + numFaces +  3 * numVertices );
    DesignForceDeformation.head( 3 * numVertices) = vertexForces;
    DesignForceDeformation.segment(  3 * numVertices, numFaces ) = vertexWeights;
    DesignForceDeformation.tail( 3 * numVertices ) = Solution;

    std::cout << " .. LOFS" << std::endl;
    LinearizedOptimalForceSolver<DefaultConfigurator, MaterialShellDeformationType> OFS( Topology, refGeometry,
                                                                                         dirichletBoundary,
                                                                                         undefDisplacement,
                                                                                         vertexWeights,
                                                                                         forceBasis,
                                                                                         ldForce,
                                                                                         false, true, false, false );

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );
      OFS.Configuration.maxNumIterations = config["OptimalForceSolver"]["maxNumIterations"].as<int>();
      OFS.Configuration.optimalityTolerance = config["OptimalForceSolver"]["optimalityTolerance"].as<RealType>();
      OFS.Configuration.stepsize = config["OptimalForceSolver"]["stepsize"].as<RealType>();
      OFS.Configuration.maximalStepsize = config["OptimalForceSolver"]["maximalStepsize"].as<RealType>();
      OFS.Configuration.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["OptimalForceSolver"]["stepsizeController"].as<int>());
      OFS.Configuration.acceleration = config["OptimalForceSolver"]["acceleration"].as<bool>();
      OFS.Configuration.useL2Constraint = config["OptimalForceSolver"]["useL2Constraint"].as<bool>();
      OFS.Configuration.barrierValue = config["OptimalForceSolver"]["barrierValue"].as<RealType>();
      OFS.Configuration.regularizationWeight = config["OptimalForceSolver"]["regularizationWeight"].as<RealType>();
      OFS.Configuration.objectiveWeight = config["OptimalForceSolver"]["objectiveWeight"].as<RealType>();
      OFS.Configuration.useCylinderBarrier = config["OptimalForceSolver"]["useCylinderBarrier"].as<bool>();
      OFS.Configuration.cylinderHeightFactor = config["OptimalForceSolver"]["cylinderHeightFactor"].as<RealType>();
      OFS.Configuration.cylinderHeightDimension = config["OptimalForceSolver"]["cylinderHeightDimension"].as<int>();
    }


    YAML::Node config = YAML::LoadFile( argv[1] )["OptimalForceSolver"];

    ldForce = OFS.forceSolve( vertexWeights );
    vertexForces = forceBasis * ldForce;

    std::cout << " .. Init OFS: " << OFS.evaluateWithForce(ldForce ) << std::endl;

    int cylinderDim = OFS.Configuration.cylinderHeightDimension;
    std::array<int,2> radialDim{-1,-1};
    if ( cylinderDim == 0 )
      radialDim = { 1, 2 };
    else if ( cylinderDim == 1 )
      radialDim = { 0, 2 };
    else if ( cylinderDim == 2 )
      radialDim = { 0, 1 };

    MatrixType massMatrixH, massMatrixR;

    massMatrixH.resize( 3 * numVertices, 3 * numVertices );
    massMatrixR.resize( 3 * numVertices, 3 * numVertices );
    TripletListType tripletsH, tripletsR;
    for ( int vertexIdx = 0; vertexIdx < numVertices; vertexIdx++ ) {
      tripletsH.emplace_back( cylinderDim * numVertices + vertexIdx, cylinderDim * numVertices + vertexIdx,
                              nodalAreas[vertexIdx] );
      tripletsR.emplace_back( radialDim[0] * numVertices + vertexIdx, radialDim[0] * numVertices + vertexIdx,
                              nodalAreas[vertexIdx] );
      tripletsR.emplace_back( radialDim[1] * numVertices + vertexIdx, radialDim[1] * numVertices + vertexIdx,
                              nodalAreas[vertexIdx] );
    }
    massMatrixH.setFromTriplets( tripletsH.begin(), tripletsH.end());
    massMatrixR.setFromTriplets( tripletsR.begin(), tripletsR.end());

    std::cout << " .. Start forces: " << std::scientific << std::setprecision(6) << ldForce.transpose() << std::endl;
    std::cout << " .. Start forces.norm: " << ldForce.norm() << std::endl;
    std::cout << " .. Start forces.norm: " << std::sqrt(vertexForces.transpose() * massMatrix * vertexForces) << std::endl;
    std::cout << " .. Start forces.norm: " << (massMatrix * vertexForces).sum() << std::endl;
    std::cout << " .. Area: " << nodalAreas.sum() << " vs. " << faceAreas.sum() << std::endl;

    RealType totalMagnitude = 0.;
    RealType totalZMagnitude = 0.;
    RealType totalXYMagnitude = 0.;
    for (int vertexIdx = 0; vertexIdx < numVertices; vertexIdx++) {
      VecType  p;
      getXYZCoord(vertexForces, p, vertexIdx);
      totalZMagnitude += nodalAreas[vertexIdx] * p[1];
      totalXYMagnitude += nodalAreas[vertexIdx] * std::sqrt(p[0] * p[0] + p[2]*p[2]);
      totalMagnitude += nodalAreas[vertexIdx] * p.norm();
    }
    std::cout << " .. totalMagnitude =  " << totalMagnitude<< std::endl;
    std::cout << " .. totalZMagnitude =  " << totalZMagnitude<< std::endl;
    std::cout << " .. totalXYMagnitude =  " << totalXYMagnitude<< std::endl;
    std::cout << " .. Start Pressure: " << totalMagnitude / nodalAreas.sum() << std::endl;
    std::cout << " .. Start Z-Pressure: " << totalZMagnitude / nodalAreas.sum() << std::endl;
    std::cout << " .. Start XY-Pressure: " << totalXYMagnitude / nodalAreas.sum() << std::endl;

    std::cout << " .. Start forces.norm/Area: " << (massMatrix * vertexForces).sum() / nodalAreas.sum() << std::endl;
    if (OFS.Configuration.useCylinderBarrier) {
      RealType L2h = std::sqrt( vertexForces.transpose() * massMatrixH * vertexForces );
      RealType L2r = std::sqrt( vertexForces.transpose() * massMatrixR * vertexForces );
      std::cout << " .. Start forces.norm: " << L2h << " + " << L2r << std::endl;
    }
    else {
      std::cout << " .. Start forces.norm: " << std::sqrt( vertexForces.transpose() * massMatrix * vertexForces )
                << std::endl;
    }

    DesignAndForce.head( 3 * numVertices) = vertexForces;
    DesignAndForce.tail( numFaces ) = vertexWeights;

    Solution = ES.solve(DesignAndForce);
    defGeometry = refGeometry + Solution;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["Material"] = vertexWeights;
      saveAsVTP( Topology, defGeometry, outputPrefix + "sblvl_def.vtp", colorings );
    }

    TrackingTerm<DefaultConfigurator> T( Topology, refGeometry, trackingIndices,
                                         false, true, true, false, false, false );
    TrackingTermGradient<DefaultConfigurator> DT( Topology, refGeometry, trackingIndices, false, true, true,
                                                  false, false, false );

    MaterialMassLogBarrier<DefaultConfigurator> Lg( Topology, refGeometry, BilevelOptimization.MassBarrier,
                                                    false, true, true, false, false, false );
    MaterialMassLogBarrierGradient<DefaultConfigurator> DLg( Topology, refGeometry, BilevelOptimization.MassBarrier,
                                                             false, true, true, false, false, false );

    MaterialPointLogBarrier<DefaultConfigurator> PLg( Topology, refGeometry, BilevelOptimization.LowerPointBarrier, dirichletFaces,
                                                      false, true, true, false, false, false );
    MaterialPointLogBarrierGradient<DefaultConfigurator> DPLg( Topology, refGeometry, BilevelOptimization.LowerPointBarrier, dirichletFaces,
                                                               false, true, true, false, false, false );

    MaterialUpperLogBarrier<DefaultConfigurator> UPLg( Topology, refGeometry, BilevelOptimization.UpperPointBarrier, dirichletFaces,
                                                      false, true, true, false, false, false );
    MaterialUpperLogBarrierGradient<DefaultConfigurator> DUPLg( Topology, refGeometry, BilevelOptimization.UpperPointBarrier, dirichletFaces,
                                                               false, true, true, false, false, false );

    VectorType weights( 4 );
    weights << BilevelOptimization.trackingWeight, BilevelOptimization.MassWeight, BilevelOptimization.LowerPointBarrierWeight, BilevelOptimization.UpperPointBarrierWeight;

    std::cout << " .. weights = " << weights.transpose() << std::endl;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, T, Lg, PLg, UPLg  );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DT, DLg, DPLg, DUPLg   );

    VectorType weights_mat( 3 );
    weights_mat << BilevelOptimization.MassWeight, BilevelOptimization.LowerPointBarrierWeight, BilevelOptimization.UpperPointBarrierWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J_mat( weights_mat, Lg, PLg, UPLg );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ_mat( weights_mat, DLg, DPLg, DUPLg );

    VectorType weights_rnd( 1 );
    weights_rnd << BilevelOptimization.trackingWeight;

    ParametrizedAdditionOp<DefaultConfigurator> J_rnd( weights_rnd, T );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ_rnd( weights_rnd, DT );


//    OFS.testDerivatives();
//    if(testDerivatives)
//      OFS.testMixedDerivatives();

//    OFS.mixedDerivative(vertexWeights);
    std::vector<int> mask;

    if ( testDerivatives ) {
//    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Tred_BLVL( T, ES, OFS, forceBasis );
//    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DTred_BLVL( T, DT, ES, OFS, forceBasis, mask );
//
//    std::cout << " .. F = " << Tred_BLVL( vertexWeights ) << std::endl;
//    std::cout << " .. DF = " << DTred_BLVL( vertexWeights ).transpose() << std::endl;
//
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Tred_BLVL, DTred_BLVL, 1.e-4 ).plotRandomDirections( vertexWeights, 30,
//                                                                                                            outputPrefix + "testTred_BLVL" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Tred_BLVL, DTred_BLVL, 1.e-4 ).testRandomDirections( vertexWeights, 30,
//                                                                                                            true );
//
//
//    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Lgred_BLVL( Lg, ES, OFS, forceBasis );
//    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DLgred_BLVL( Lg, DLg, ES, OFS, forceBasis, mask );
//
//    std::cout << " .. F = " << Lgred_BLVL( vertexWeights ) << std::endl;
//    std::cout << " .. DF = " << DLgred_BLVL( vertexWeights ).transpose() << std::endl;
//
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Lgred_BLVL, DLgred_BLVL, 1.e-4 ).plotRandomDirections( vertexWeights, 30,
//                                                                                                              outputPrefix + "testLgred_BLVL" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Lgred_BLVL, DLgred_BLVL, 1.e-4 ).testRandomDirections( vertexWeights, 30,
//                                                                                                              true );
//
//
//    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> PLgred_BLVL( PLg, ES, OFS, forceBasis );
//    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DPLgred_BLVL( PLg, DPLg, ES, OFS, forceBasis, mask );
//
//    std::cout << " .. F = " << PLgred_BLVL( vertexWeights ) << std::endl;
//    std::cout << " .. DF = " << DPLgred_BLVL( vertexWeights ).transpose() << std::endl;
//
//    ScalarValuedDerivativeTester<DefaultConfigurator>( PLgred_BLVL, DPLgred_BLVL, 1.e-4 ).plotRandomDirections( vertexWeights, 30,
//                                                                                                                outputPrefix + "testPLgred_BLVL" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( PLgred_BLVL, DPLgred_BLVL, 1.e-4 ).testRandomDirections( vertexWeights, 30,
//                                                                                                                true );

//      LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Jred_BLVL( J, ES, OFS,
//                                                                                                   forceBasis );
//      LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DJred_BLVL( J, DJ, ES, OFS,
//                                                                                                  forceBasis, mask );
//
//      std::cout << " .. F = " << Jred_BLVL( vertexWeights ) << std::endl;
//      std::cout << " .. DF = " << DJred_BLVL( vertexWeights ).transpose() << std::endl;
//
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred_BLVL, DJred_BLVL, 1.e-4 ).plotRandomDirections(
//              vertexWeights, 30,
//              outputPrefix + "testJred_BLVL" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred_BLVL, DJred_BLVL, 1.e-4 ).testRandomDirections(
//              vertexWeights, 30,
//              true );
    }
    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Jred( J, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DJred( J, DJ, ES, OFS, forceBasis,
                                                                                           dirichletBoundary );
//
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( vertexWeights, 10,
//                                                                                                 outputPrefix +
//                                                                                                 "testDJred_start" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).testRandomDirections( vertexWeights, 100, true );


    // Stochastic functional
    NormalNoiseGenerator generator( Topology.getNumFaces(), randomPerturbation );
//    NoiseExpectedValue<BaseOp<VectorType, RealType>> EJ( Jred, generator, BilevelOptimization.numSamples );
//    NoiseExpectedValue<BaseOp<VectorType, VectorType>> DEJ( DJred, generator, BilevelOptimization.numSamples );

    StochasticBilevelFunctional<DefaultConfigurator, MaterialShellDeformationType> EJ_ind( J_rnd, J_mat, generator,
                                                                                       BilevelOptimization.numSamples,
                                                                                       Topology, refGeometry,
                                                                                       dirichletBoundary, forceBasis,
                                                                                       undefDisplacement,
                                                                                       vertexWeights, ldForce, config );
    EJ_ind.computeSamples();
//    EJ_ind.setSamples( EJ.getSamples());

//    RealType value = EJ( vertexWeights );
//    std::cout << " .. Init Jred = " << std::scientific << std::setprecision(6) << value << std::endl;

    RealType value_ind = EJ_ind( vertexWeights );
    std::cout << " .. Init Jred_ind = " << std::scientific << std::setprecision(6)  << value_ind << std::endl;


    StochasticBilevelGradient<DefaultConfigurator, MaterialShellDeformationType> DEJ_ind( J_rnd, DJ_rnd, J_mat, DJ_mat, generator,
                                                                                       BilevelOptimization.numSamples,
                                                                                       dirichletBoundary, forceBasis,
                                                                                       EJ_ind.m_ES, EJ_ind.m_OFS );
//    DEJ.setSamples( EJ_ind.getSamples());
    DEJ_ind.setSamples( EJ_ind.getSamples());

//    VectorType grad = DEJ( vertexWeights );
//    std::cout << " .. Init DJred.norm = " << std::scientific << std::setprecision(6)  << grad.norm() << std::endl;

    VectorType grad_ind = DEJ_ind( vertexWeights );
    std::cout << " .. Init DJred_ind.norm = " << std::scientific << std::setprecision(6)  << grad_ind.norm() << std::endl;
//    std::cout << " .. Relative difference = " << std::scientific << std::setprecision(6) << (grad_ind- grad).norm() / grad.norm() << std::endl;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = vertexWeights;
      for (int i = 0; i < EJ_ind.getSamples().size(); i++)
        colorings["Material_" + std::to_string(i)] = vertexWeights.array() * EJ_ind.getSamples()[i].array();
      colorings["Force"] = vertexForces;
      saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_material.vtp", colorings );
    }



    VectorType lowerBounds = VectorType::Constant( numFaces, 1.e-3 );
    VectorType upperBounds = VectorType::Constant( numFaces, std::numeric_limits<RealType>::infinity());


    ldForce = OFS.solve( vertexWeights );
    vertexForces = forceBasis * ldForce;

    std::cout << " .. Start forces: " << std::scientific << std::setprecision(6) << ldForce.transpose() << std::endl;
    std::cout << " .. Start forces.norm: " << ldForce.norm() << std::endl;
    if (OFS.Configuration.useCylinderBarrier) {
      RealType L2h = std::sqrt( vertexForces.transpose() * massMatrixH * vertexForces );
      RealType L2r = std::sqrt( vertexForces.transpose() * massMatrixR * vertexForces );
      std::cout << " .. Start forces.norm: " << L2h << " + " << L2r << std::endl;
    }
    else {
      std::cout << " .. Start forces.norm: " << std::sqrt( vertexForces.transpose() * massMatrix * vertexForces )
                << std::endl;
    }
    DesignAndForce.head( 3 * numVertices ) = vertexForces;
    DesignAndForce.tail( numFaces ) = vertexWeights;

    Solution = ES.solve( DesignAndForce );
    defGeometry = refGeometry + Solution;

    DesignForceDeformation.head( 3 * numVertices ) = vertexForces;
    DesignForceDeformation.segment( 3 * numVertices, numFaces ) = vertexWeights;
    DesignForceDeformation.tail( 3 * numVertices ) = Solution;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["Material"] = vertexWeights;
      saveAsVTP( Topology, defGeometry, outputPrefix + "blvl_start_def.vtp", colorings );
    }

    for (int i = 0; i < EJ_ind.getSamples().size(); i++) {
      VectorType ldForce = OFS.solve( vertexWeights.array() * EJ_ind.getSamples()[i].array() );
      VectorType vertexForces = forceBasis * ldForce;

      VectorType DesignAndForce( 3 * numVertices + numFaces );
      DesignAndForce.head( 3 * numVertices ) = vertexForces;
      DesignAndForce.tail( numFaces ) = vertexWeights.array() * EJ_ind.getSamples()[i].array();

      VectorType Solution = ES.solve( DesignAndForce );
      VectorType defGeometry = refGeometry + Solution;

      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["Material"] = vertexWeights.array() * EJ_ind.getSamples()[i].array();
      saveAsVTP( Topology, defGeometry, outputPrefix + "blvl_material_def_" + std::to_string(i) + ".vtp", colorings );
    }


    VectorType newMaterial = vertexWeights;
    std::cout << " .. Material barrier = " << BilevelOptimization.MassBarrier << std::endl;
    std::cout << " .. Initial material (L2) = " << faceAreas.dot( newMaterial ) << std::endl;
    std::cout << " .. Initial min material  = " << newMaterial.minCoeff() << std::endl;
    std::cout << " .. Initial J = " << J( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Tracking = " << T( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Material-Barrier = " << Lg( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Lower Point-Barrier = " << PLg( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Upper Point-Barrier = " << UPLg( DesignForceDeformation ) << std::endl;


    EJ_ind.computeSamples();
//    EJ_ind.setSamples( EJ.getSamples());
//    DEJ.setSamples( EJ.getSamples());
    DEJ_ind.setSamples( EJ_ind.getSamples());

    if ( testDerivatives ) {
      ScalarValuedDerivativeTester<DefaultConfigurator>( J_rnd, DJ_rnd, 1e-5 ).plotRandomDirections(
              DesignForceDeformation, 20, outputPrefix + "testDJrnd_start" );
      ScalarValuedDerivativeTester<DefaultConfigurator>( J_mat, DJ_mat, 1e-5 ).plotRandomDirections(
              DesignForceDeformation, 20, outputPrefix + "testDJmat_start" );

//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).testRandomDirections( vertexWeights, 10, true );
//
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( vertexWeights, 10,
//                                                                                                       outputPrefix +
//                                                                                                       "testDJred_start" );

      ScalarValuedDerivativeTester<DefaultConfigurator>( EJ_ind, DEJ_ind, 1e-4 ).testRandomDirections( vertexWeights, 10, true );

      ScalarValuedDerivativeTester<DefaultConfigurator>( EJ_ind, DEJ_ind, 1e-4 ).plotRandomDirections( vertexWeights, 30,
                                                                                                       outputPrefix +
                                                                                                       "testDEJ_ind_start" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( EJ, DEJ, 1e-4 ).plotRandomDirections( vertexWeights, 30,
//                                                                                                       outputPrefix +
//                                                                                                       "testDEJ_start" );
    }

    // solve
    StochasticGradientDescent<DefaultConfigurator> Solver( EJ_ind, DEJ_ind, BilevelOptimization.maxNumIterations,
                                                           BilevelOptimization.optimalityTolerance, SHOW_ALL,
                                                           BilevelOptimization.stepsize,
                                                           BilevelOptimization.stepsizeController );
    Solver.setFixedVariables( dirichletFaces );
    Solver.setParameter( "maximal_stepsize", BilevelOptimization.maximalStepsize );
    auto t_start = std::chrono::high_resolution_clock::now();
    Solver.solve( newMaterial, newMaterial );
    auto t_end = std::chrono::high_resolution_clock::now();

    ldForce = OFS.solve( newMaterial );
    vertexForces = forceBasis * ldForce;

    std::cout << " .. End forces: " << std::scientific << std::setprecision(6) << ldForce.transpose() << std::endl;
    std::cout << " .. End forces.norm: " << ldForce.norm() << std::endl;

    if (OFS.Configuration.useCylinderBarrier) {
      RealType L2h = std::sqrt( vertexForces.transpose() * massMatrixH * vertexForces );
      RealType L2r = std::sqrt( vertexForces.transpose() * massMatrixR * vertexForces );
      std::cout << " .. End forces.norm: " << L2h << " + " << L2r << std::endl;
    }
    else {
      std::cout << " .. End forces.norm: " << std::sqrt( vertexForces.transpose() * massMatrix * vertexForces )
                << std::endl;
    }

    DesignAndForce.head( 3 * numVertices ) = vertexForces;
    DesignAndForce.tail( numFaces ) = newMaterial;

    Solution = ES.solve( DesignAndForce );
    defGeometry = refGeometry + Solution;

    DesignForceDeformation.head( 3 * numVertices ) = vertexForces;
    DesignForceDeformation.segment( 3 * numVertices, numFaces ) = newMaterial;
    DesignForceDeformation.tail( 3 * numVertices ) = Solution;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["initMaterial"] = vertexWeights;
      colorings["Material"] = newMaterial;
      colorings["Material_wBND"] = newMaterial;
      for (int faceIdx : dirichletFaces)
        colorings["Material_wBND"][faceIdx] = std::numeric_limits<RealType>::quiet_NaN();
      saveAsVTP( Topology, defGeometry, outputPrefix + "blvl_opt_def.vtp", colorings );
      saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_opt.vtp", colorings );
    }

    EJ_ind.computeSamples();
//    EJ_ind.setSamples( EJ_ind.getSamples());
//    DEJ.setSamples( EJ.getSamples());
    DEJ_ind.setSamples( EJ_ind.getSamples());

//    value = EJ( newMaterial );
//    std::cout << " .. Final Jred = " << std::scientific << std::setprecision(6) << value << std::endl;
    value_ind = EJ_ind( newMaterial );
    std::cout << " .. Final Jred_ind = " << std::scientific << std::setprecision(6)  << value_ind << std::endl;

//    grad = DEJ( newMaterial );
//    std::cout << " .. Final DJred.norm = " << std::scientific << std::setprecision(6)  << grad.norm() << std::endl;

    grad_ind = DEJ_ind( newMaterial );
    std::cout << " .. Final DJred_ind.norm = " << std::scientific << std::setprecision(6)  << grad_ind.norm() << std::endl;

//    std::cout << " .. Relative difference = " << std::scientific << std::setprecision(6) << (grad_ind- grad).norm() / grad.norm() << std::endl;

    std::cout << " .. Final barrier = " << BilevelOptimization.MassBarrier << std::endl;
    std::cout << " .. Final material (L2) = " << faceAreas.dot( newMaterial ) << std::endl;
    std::cout << " .. Final min material  = " << newMaterial.minCoeff() << std::endl;
    {
      VectorType Material = newMaterial;
      applyMaskToVector(dirichletFaces, Material);
      std::cout << " .. Final max material  = " << Material.maxCoeff() << std::endl;
    }
    std::cout << " .. Final J = " << J( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Tracking = " << T( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Material-Barrier = " << Lg( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Lower Point-Barrier = " << PLg( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Upper Point-Barrier = " << UPLg( DesignForceDeformation ) << std::endl;

    for (int i = 0; i < EJ_ind.getSamples().size(); i++) {
      VectorType ldForce = OFS.solve( newMaterial.array() * EJ_ind.getSamples()[i].array() );
      VectorType vertexForces = forceBasis * ldForce;

      VectorType DesignAndForce( 3 * numVertices + numFaces );
      DesignAndForce.head( 3 * numVertices ) = vertexForces;
      DesignAndForce.tail( numFaces ) = newMaterial.array() * EJ_ind.getSamples()[i].array();

      VectorType Solution = ES.solve( DesignAndForce );
      VectorType defGeometry = refGeometry + Solution;

      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["Material"] = newMaterial.array() * EJ_ind.getSamples()[i].array();
      saveAsVTP( Topology, defGeometry, outputPrefix + "blvl_opt_def_" + std::to_string(i) + ".vtp", colorings );
    }

    if ( testDerivatives ) {
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-6 ).testAllDirections( vertexWeights, true );
      ScalarValuedDerivativeTester<DefaultConfigurator>( EJ_ind, DEJ_ind, 1e-4 ).plotRandomDirections( newMaterial, 20,
                                                                                                       outputPrefix +
                                                                                                       "testDEJ_ind_end" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( EJ, DEJ, 1e-4 ).plotRandomDirections( vertexWeights, 10,
//                                                                                                       outputPrefix +
//                                                                                                       "testDEJ_start" );

//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotAllDirections( vertexWeights,
//                                                                                                   outputPrefix +
//                                                                                                   "testDJred_start" );
    }




    std::cout << "===============================================================" << std::endl;

    RealType finalJ = J( DesignForceDeformation );
    RealType finalT = T( DesignForceDeformation );
    RealType finalLg = Lg( DesignForceDeformation );
    RealType finalPLg = PLg( DesignForceDeformation );
    RealType finalUPLg = UPLg( DesignForceDeformation );
    RealType finalBarrier = BilevelOptimization.MassBarrier;
    RealType finalMaterial =  faceAreas.dot( newMaterial );
    RealType finalMinMaterial =  newMaterial.minCoeff() ;
    RealType finalMaxMaterial =  newMaterial.minCoeff() ;
    {
      VectorType Material = newMaterial;
      applyMaskToVector(dirichletFaces, Material);
      finalMaxMaterial = Material.maxCoeff();
    }

    std::cout << " .. End forces: " << std::scientific << std::setprecision(6) << ldForce.transpose() << std::endl;
    std::cout << " .. End forces.norm: " << ldForce.norm() << std::endl;

    std::string finalForceNorm;
    if (OFS.Configuration.useCylinderBarrier) {
      RealType L2h = std::sqrt( vertexForces.transpose() * massMatrixH * vertexForces );
      RealType L2r = std::sqrt( vertexForces.transpose() * massMatrixR * vertexForces );
      finalForceNorm = std::to_string(L2h) + "  + " + std::to_string(L2r);
    }
    else {
      finalForceNorm = std::to_string(std::sqrt( vertexForces.transpose() * massMatrix * vertexForces ));
    }

    std::cout << "\"" << outputFolderName << "\",\""
              << finalJ << "\",\""
              << finalT << "\",\""
              << finalLg << "\",\""
              << finalPLg << "\",\""
              << finalUPLg << "\",\""
              << finalBarrier << "\",\""
              << finalMaterial << "\",\""
              << finalMinMaterial << "\",\""
              << finalMaxMaterial << "\",\""
              << "(" << ldForce.transpose() << ")" << "\",\""
              << ldForce.norm() << "\",\""
              << finalForceNorm << "\""
              << std::endl;

    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}