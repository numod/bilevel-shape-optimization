#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/Core.h"
#include "goast/external/vtkIO.h"

#include <Eigen/PardisoSupport>

//================================================================================


using RealType = DefaultConfigurator::RealType;
using VectorType = DefaultConfigurator::VectorType;
using MatrixType = DefaultConfigurator::SparseMatrixType;
using FullMatrixType = DefaultConfigurator::FullMatrixType;


//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;



    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, argv[1] )) {
      throw std::runtime_error( "Error while reading mesh." );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numVertices = Topology.getNumVertices();
    std::cout << " .. numVertices: " <<  numVertices << std::endl;


    VectorType Geometry;
    getGeometry( inputMesh, Geometry );

    MatrixType stiffnessMatrix;
    auto t_start = std::chrono::high_resolution_clock::now();
    computeStiffnessMatrix<DefaultConfigurator>( Topology, Geometry, stiffnessMatrix, false );
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout << " .. Laplace: " << std::chrono::duration<double, std::milli >( t_end - t_start ).count() << "ms." << std::endl;

    VectorType rhs = VectorType::Random( numVertices );



    // Dirichlet Boundary
    std::vector<int> dirichletBoundary;
    Topology.fillFullBoundaryMask( dirichletBoundary );
    if ( dirichletBoundary.empty())
      dirichletBoundary.push_back( 0 );

    applyMaskToSymmetricMatrixAndVector( dirichletBoundary, stiffnessMatrix, rhs );


    Eigen::PardisoLLT<MatrixType> CholmodSolver;

//    t_start = std::chrono::high_resolution_clock::now();
//    CholmodSolver.analyzePattern( stiffnessMatrix );
//    t_end = std::chrono::high_resolution_clock::now();
//    std::cout << " .. analyzePattern: " << std::chrono::duration<double, std::milli >( t_end - t_start ).count() << "ms." << std::endl;
//
//    t_start = std::chrono::high_resolution_clock::now();
//    CholmodSolver.factorize( stiffnessMatrix );
//    t_end = std::chrono::high_resolution_clock::now();
//    std::cout << " .. factorize: " << std::chrono::duration<double, std::milli >( t_end - t_start ).count() << "ms." << std::endl;

    t_start = std::chrono::high_resolution_clock::now();
    CholmodSolver.compute( stiffnessMatrix );
    t_end = std::chrono::high_resolution_clock::now();
    std::cout << " .. compute: " << std::chrono::duration<double, std::milli >( t_end - t_start ).count() << "ms." << std::endl;

    t_start = std::chrono::high_resolution_clock::now();
    VectorType Solution = CholmodSolver.solve( rhs );
    t_end = std::chrono::high_resolution_clock::now();
    std::cout << " .. Solution: " << std::chrono::duration<double, std::milli >( t_end - t_start ).count() << "ms." << std::endl;

    saveAsVTP( Topology, Geometry, "Solution.vtp", Solution );
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;

    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    return -1;
  }
}
