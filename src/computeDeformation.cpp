/**
 * \brief Compute optimal force for certain material distribution
 */

//==============Includes================================
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/external/vtkIO.h"


#include "ComplianceTerm.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ShapeOptimization/ReducedFunctional.h"
#include "ShapeOptimization/ExpectedValue.h"
#include "ShapeOptimization/ExpectedExcess.h"
#include "Optimization/StochasticGradientDescent.h"
#include "Optimization/StochasticProximalGradientDescent.h"
#include "MaterialGenerators.h"
#include "MaterialDeformationEnergies.h"

#include "2DForces.h"
#include "ReducedForces.h"
//======================================================

//================================================================================

typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

using RealType = DefaultConfigurator::RealType;
using VectorType = DefaultConfigurator::VectorType;
using MatrixType = DefaultConfigurator::SparseMatrixType;
using FullMatrixType = DefaultConfigurator::FullMatrixType;


//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {
  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";

  // Boundary values
  std::vector<int> bndMask{}; // 171
  bool detectBoundary = true;

  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Material
  RealType thickness = 0.03;
  RealType materialFactor = 20.;
  RealType splitOffset = 0.3;
  RealType minAngle = 0;
  RealType maxAngle = M_PI / 2.;

//  RealType minAngle = -0.5* M_PI / 9.;
//  RealType maxAngle = 0.5* M_PI / 9.;

  bool writeExamples = true;

  // Forces
  RealType forceBound = 0.02;
  bool useL2Constraint = true;
  std::vector<RealType> forceInitialization;
  std::string forceSpace = "twistAndCompress";

  // shape optimization
  struct {
    bool useNonlinearElasticity = true;
    int numStochasticSamples = 10;
    int maxNumIterations = 100;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    std::string stochasticMeasure = "ExpectedValue";
  } ShapeOptimization;

  // Additional
  bool testDerivatives = false;

  //</editor-fold>

  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );

      meshPath = config["Geometry"]["mesh"].as<std::string>();
      bndMask = config["Geometry"]["boundaryMask"].as<std::vector<int>>();
      detectBoundary = config["Geometry"]["detectBoundary"].as<bool>();

      outputFilePrefix = config["Output"]["outputFilePrefix"].as<std::string>();
      outputFolder = config["Output"]["outputFolder"].as<std::string>();
      timestampOutput = config["Output"]["timestampOutput"].as<bool>();

      forceSpace = config["Forces"]["space"].as<std::string>();
      forceInitialization = config["Forces"]["initialization"].as<std::vector<RealType>>();

      thickness = config["Material"]["initialThickness"].as<RealType>();
//      thickness = config["Material"]["config"]["baseThickness"].as<RealType>();
//      materialFactor = config["Material"]["config"]["materialFactor"].as<RealType>();
//      splitOffset = config["Material"]["config"]["beamWidth"].as<RealType>();
//      minAngle = config["Material"]["config"]["minAngle"].as<RealType>();
//      maxAngle = config["Material"]["config"]["maxAngle"].as<RealType>();

      ShapeOptimization.useNonlinearElasticity = ( config["ShapeOptimization"]["Elasticity"].as<std::string>() ==
                                                   "Nonlinear" );
      ShapeOptimization.numStochasticSamples = config["ShapeOptimization"]["numSamples"].as<int>();

      writeExamples = config["Additional"]["writeMaterialExamples"].as<bool>();
    }


    //<editor-fold desc="Output folders">
    if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
      outputFolder += "/";

    std::string execName( argv[0] );
    execName = execName.substr( execName.find_last_of( '/' ) + 1 );

    if ( timestampOutput ) {
      std::time_t t = std::time( nullptr );
      std::stringstream ss;
      ss << outputFolder;
      ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss << "_" << execName;
      ss << "/";
      outputFolder = ss.str();
      boost::filesystem::create_directory( outputFolder );
    }

    if ( argc == 2 ) {
      boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                    boost::filesystem::copy_option::overwrite_if_exists );
    }

    std::string outputPrefix = outputFolder + outputFilePrefix;
    //</editor-fold>

    //<editor-fold desc="Output log">
    std::ofstream logFile;
    logFile.open( outputFolder + "/_output.log" );

    typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
    typedef boost::iostreams::stream<TeeDevice> TeeStream;
    TeeDevice tee_cout( logFile, output_cout );
    TeeDevice tee_cerr( logFile, output_cerr );

    TeeStream split_cout( tee_cout );
    TeeStream split_cerr( tee_cerr );

    std::cout.rdbuf( split_cout.rdbuf());
    std::cerr.rdbuf( split_cerr.rdbuf());
    //</editor-fold>


    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    //</editor-fold>

    //<editor-fold desc="Stochastic material config">
    VectorType vertexWeights_const = VectorType::Constant( numVertices, thickness );
    VectorType vertexWeights = VectorType::Constant( numVertices, thickness );

//    SingleBeamGenerator g( thickness, minAngle, maxAngle, refGeometry, splitOffset, materialFactor );
    ConstantGenerator g( vertexWeights_const );
    vertexWeights = g();

    if ( writeExamples ) {
      std::map<std::string, VectorType> colorings;
      for ( int i = 0; i < ShapeOptimization.numStochasticSamples; i++ ) {
        colorings["Material"] = g();
        saveAsVTP( Topology, refGeometry, outputPrefix + "material_examples_" + std::to_string( i ) + ".vtp",
                   colorings );
      }
    }
    //</editor-fold>


    // Deformation energies
    MaterialShellDeformationType W( Topology, vertexWeights );
    MaterialShellDeformationType W_constant( Topology, vertexWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletBoundary;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletBoundary );

    dirichletBoundary.insert( dirichletBoundary.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    extendBoundaryMask( numVertices, dirichletBoundary );

    std::cout << " .. Dirichlet done" << std::endl;


    // Forces
    // Forces
    FullMatrixType forceBasis;
    if ( forceSpace == "Constant" ) {
      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.setZero();
      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ).segment( d * numVertices, numVertices ).setConstant( 1. );
    }
    else if ( forceSpace == "twistAndCompress" ) {
      VectorType horizontalCompressionForce( 3 * numVertices );
      VectorType verticalCompressionForce( 3 * numVertices );
      VectorType twistingForce( 3 * numVertices );

      auto VCFProfile = []( RealType z ) { return z / 4.; };
      auto HCFProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
//      auto TProfile = []( RealType z ) { return ( z - 2. ) / 2.; };
      auto TProfile = []( RealType z ) { return z / 4.; };
//      auto TProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };

      for ( int i = 0; i < Topology.getNumVertices(); i++ ) {
        VectorType p( 3 );
        getXYZCoord( refGeometry, p, i );

        VectorType normal = p;
        normal[2] = 0.;
        normal.normalize();

        VectorType hcf = normal * HCFProfile( p[2] );
        setXYZCoord( horizontalCompressionForce, hcf, i );

        VectorType vcf = VectorType::Zero( 3 );
        vcf[2] = VCFProfile( p[2] );
        setXYZCoord( verticalCompressionForce, vcf, i );

        VectorType twist = normal;
        twist[0] = normal[1];
        twist[1] = -normal[0];
        twist *= TProfile( p[2] );
        setXYZCoord( twistingForce, twist, i );
      }

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = horizontalCompressionForce;
      forceBasis.col( 1 ) = twistingForce;
      forceBasis.col( 2 ) = verticalCompressionForce;
    }

    VectorType ldForce( forceInitialization.size());
    for ( int i = 0; i < forceInitialization.size(); i++ )
      ldForce[i] = forceInitialization[i];

    VectorType vertexForces = forceBasis * ldForce;


    // Output start values
    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = vertexWeights;
      colorings["Force"] = vertexForces;
      saveAsVTP( Topology, refGeometry, outputPrefix + "deformation_start.vtp", colorings );
    }


    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Deformation" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // ========================================================================
    // ============================== Functionals =============================
    // ========================================================================

    // DOF variable
    VectorType combinedVector( 3 * numVertices + numDOFs );
    combinedVector.head( 3 * numVertices ) = vertexForces;
    combinedVector.tail( numDOFs ).setZero();

    //<editor-fold desc="Setup elasticity">
    std::unique_ptr<StateEquationSolverBase<DefaultConfigurator>> SES;

    if ( ShapeOptimization.useNonlinearElasticity ) {
      SES.reset( new NonlinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType>( Topology,
                                                                                                   refGeometry,
                                                                                                   dirichletBoundary,
                                                                                                   undefDisplacement,
                                                                                                   vertexForces,
                                                                                                   vertexWeights,
                                                                                                   false, true, false,
                                                                                                   false, false,
                                                                                                   true ));
    }
    else {
      SES.reset( new LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType>( Topology, refGeometry,
                                                                                                dirichletBoundary,
                                                                                                undefDisplacement,
                                                                                                vertexForces,
                                                                                                vertexWeights,
                                                                                                false, true, false,
                                                                                                false, false, true ));
    }

    VectorType Solution = SES->solve( vertexForces );

    combinedVector.tail( numDOFs ) = Solution;
    //</editor-fold>

    std::map<std::string, VectorType> colorings;
    colorings["Force"] = vertexForces;

    for ( int i = 0; i < ShapeOptimization.numStochasticSamples; i++ ) {
      std::cout << " - Deformation " << i << "... " << std::endl;
      vertexWeights = g();
      SES->updateParameters( vertexWeights );
      Solution = SES->solve( vertexForces );
      defGeometry = refGeometry + Solution;

      colorings["Material"] = vertexWeights;
      saveAsVTP( Topology, defGeometry, outputPrefix + "deformation_" + std::to_string( i ) + ".vtp", colorings );
    }


    std::cout << "===============================================================" << std::endl;


    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}