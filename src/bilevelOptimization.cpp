#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/external/vtkIO.h"
#include "goast/external/ipoptBoxConstraintSolver.h"

#include "Optimization/ProximalGradientDescent.h"
#include "Optimization/ProximalMappings.h"

#include "ShapeOptimization/ReducedFunctional.h"

#include "MaterialDeformationEnergies.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ComplianceTerm.h"
#include "FixedComplianceTerm.h"
#include "TrackingCost.h"
#include "MaterialRegularization.h"
#include "OptimalForceSolver.h"
#include "LinearOptimalForceSolver.h"
#include "BilevelReducedFunctional.h"

#include "ReducedForces.h"

//================================================================================

typedef DefaultConfigurator ConfiguratorType;
typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::FullMatrixType FullMatrixType;
typedef typename ConfiguratorType::VecType VecType;
typedef std::vector<TripletType> TripletListType;


//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {
  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";

  // Boundary values
  std::vector<int> bndMask{}; // 171
  std::vector<int> trackingIndices{}; // 171
  bool detectBoundary = true;

  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Material
  RealType initialThickness = 0.03;
  RealType materialBound = 0.02;
  RealType randomPerturbation = 0.;
  bool useL2Constraint = true;


  // Forces
  std::string forceSpace = "twistAndCompress"; // twistAndCompress, Constant
  std::vector<RealType> forceInitialization;


  // shape optimization
  struct {
    bool useNonlinearElasticity = true;
    int maxNumIterations = 100;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 1e-3;
    TIMESTEP_CONTROLLER stepsizeController = CONST_TIMESTEP_CONTROL;
    bool acceleration = false;
    RealType trackingWeight = 1.;
    RealType dirichletWeight = 0.;
    RealType L2Weight = 0.;
    RealType MassWeight = 0.;
    RealType MassBarrier = 1000.;
    RealType PointBarrierWeight = 0.;
    int preIterations = 0;
  } BilevelOptimization;

  // Additional
  bool testDerivatives = false;


  //</editor-fold>


  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );

      meshPath = config["Geometry"]["mesh"].as<std::string>();
      bndMask = config["Geometry"]["boundaryMask"].as<std::vector<int>>();
      trackingIndices = config["Geometry"]["trackingIndices"].as<std::vector<int>>();
      detectBoundary = config["Geometry"]["detectBoundary"].as<bool>();

      outputFilePrefix = config["Output"]["outputFilePrefix"].as<std::string>();
      outputFolder = config["Output"]["outputFolder"].as<std::string>();
      timestampOutput = config["Output"]["timestampOutput"].as<bool>();

      forceSpace = config["Forces"]["space"].as<std::string>();
      forceInitialization = config["Forces"]["initialization"].as<std::vector<RealType>>();

      initialThickness = config["Material"]["initialThickness"].as<RealType>();
      materialBound = config["Material"]["materialBound"].as<RealType>();
      randomPerturbation = config["Material"]["randomPerturbation"].as<RealType>();
      useL2Constraint = config["Material"]["useL2Constraint"].as<bool>();

      BilevelOptimization.useNonlinearElasticity = ( config["BilevelOptimization"]["Elasticity"].as<std::string>() ==
                                                     "Nonlinear" );
      BilevelOptimization.maxNumIterations = config["BilevelOptimization"]["maxNumIterations"].as<int>();
      BilevelOptimization.optimalityTolerance = config["BilevelOptimization"]["optimalityTolerance"].as<RealType>();
      BilevelOptimization.stepsize = config["BilevelOptimization"]["stepsize"].as<RealType>();
      BilevelOptimization.maximalStepsize = config["BilevelOptimization"]["maximalStepsize"].as<RealType>();
      BilevelOptimization.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["BilevelOptimization"]["stepsizeController"].as<int>());
      BilevelOptimization.acceleration = config["BilevelOptimization"]["acceleration"].as<bool>();
      BilevelOptimization.trackingWeight = config["BilevelOptimization"]["trackingWeight"].as<RealType>();
      BilevelOptimization.dirichletWeight = config["BilevelOptimization"]["dirichletWeight"].as<RealType>();
      BilevelOptimization.L2Weight = config["BilevelOptimization"]["L2Weight"].as<RealType>();
      BilevelOptimization.MassWeight = config["BilevelOptimization"]["MassWeight"].as<RealType>();
      BilevelOptimization.MassBarrier = config["BilevelOptimization"]["MassBarrier"].as<RealType>();
      BilevelOptimization.PointBarrierWeight = config["BilevelOptimization"]["PointBarrierWeight"].as<RealType>();
      BilevelOptimization.preIterations = config["BilevelOptimization"]["preIterations"].as<int>();

      testDerivatives = config["Additional"]["testDerivatives"].as<bool>();
    }


    //<editor-fold desc="Output folders">
    if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
      outputFolder += "/";

    std::string execName( argv[0] );
    execName = execName.substr( execName.find_last_of( '/' ) + 1 );

    if ( timestampOutput ) {
      std::time_t t = std::time( nullptr );
      std::stringstream ss;
      ss << outputFolder;
      ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss << "_" << execName;
      ss << "/";
      outputFolder = ss.str();
      boost::filesystem::create_directory( outputFolder );
      std::cout << " ----------- Output folder: " << outputFolder << " ----------- "<< std::endl;
    }

    if ( argc == 2 ) {
      boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                    boost::filesystem::copy_option::overwrite_if_exists );
    }

    std::string outputPrefix = outputFolder + outputFilePrefix;
    //</editor-fold>

    //<editor-fold desc="Output log">
    std::ofstream logFile;
    logFile.open( outputFolder + "/_output.log" );


    typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
    typedef boost::iostreams::stream<TeeDevice> TeeStream;
    TeeDevice tee_cout( logFile, output_cout );
    TeeDevice tee_cerr( logFile, output_cerr );

    TeeStream split_cout( tee_cout );
    TeeStream split_cerr( tee_cerr );

//    std::cout.rdbuf( split_cout.rdbuf());
//    std::cerr.rdbuf( split_cerr.rdbuf());
    //</editor-fold>

    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();
    const int numFaces = Topology.getNumFaces();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    //</editor-fold>

    //<editor-fold desc="Material">
    VectorType faceWeights_const = VectorType::Constant( numFaces, initialThickness );
    VectorType faceWeights = VectorType::Constant( numFaces, initialThickness );
    faceWeights += VectorType::Random( numFaces ) * initialThickness * randomPerturbation;
    //</editor-fold>

    // Deformation energies
    MaterialShellDeformationType W( Topology, faceWeights );
    MaterialShellDeformationType W_constant( Topology, faceWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletIndices;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletIndices );

    dirichletIndices.insert( dirichletIndices.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    std::vector<int> dirichletBoundary = dirichletIndices;
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;

    std::cout << " - dirichletBoundary = { ";
    for ( auto v : dirichletBoundary )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    // Forces
    FullMatrixType forceBasis;
    if ( forceSpace == "Constant" ) {
      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.setZero();
      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ).segment( d * numVertices, numVertices ).setConstant( 1. );
      for ( int d : { 0, 1, 2 } )
        std::cout << " .. norm = " << forceBasis.col( d ).norm() << std::endl;

      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ) = forceBasis.col( d ).normalized();
    }
    else if ( forceSpace == "twistAndCompress" ) {
      VectorType horizontalCompressionForce( 3 * numVertices );
      VectorType verticalCompressionForce( 3 * numVertices );
      VectorType twistingForce( 3 * numVertices );

      auto VCFProfile = []( RealType z ) { return z / 4.; };
      auto HCFProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
//      auto HCFProfile = []( RealType z ) { return 1.; };
//      auto TProfile = []( RealType z ) { return ( z - 2. ) / 2.; };
//      auto TProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
      auto TProfile = []( RealType z ) { return z / 4.; };

      for ( int i = 0; i < Topology.getNumVertices(); i++ ) {
        VectorType p( 3 );
        getXYZCoord( refGeometry, p, i );

        VectorType normal = p;
        normal[2] = 0.;
        normal.normalize();

        VectorType hcf = normal * HCFProfile( p[2] );
        setXYZCoord( horizontalCompressionForce, hcf, i );

        VectorType vcf = VectorType::Zero( 3 );
        vcf[2] = VCFProfile( p[2] );
        setXYZCoord( verticalCompressionForce, vcf, i );

        VectorType twist = normal;
        twist[0] = normal[1];
        twist[1] = -normal[0];
        twist *= TProfile( p[2] );
        setXYZCoord( twistingForce, twist, i );
      }


      std::cout << " .. hCF.norm = " << horizontalCompressionForce.norm() << std::endl;
      std::cout << " .. twist.norm = " << twistingForce.norm() << std::endl;
      std::cout << " .. vCF.norm = " << verticalCompressionForce.norm() << std::endl;

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = horizontalCompressionForce.normalized();
      forceBasis.col( 1 ) = twistingForce.normalized();
      forceBasis.col( 2 ) = verticalCompressionForce.normalized();
    }
    else if ( forceSpace.rfind( "WindAndSnow", 0 ) == 0 ) {
      std::vector<VecType> vertexNormals;
      computeVertexNormals<DefaultConfigurator>( Topology, refGeometry, vertexNormals, true );

      int snowDirection = 0, windDirectionA = 0, windDirectionB = 0;
      if ( forceSpace.find( "-x" ) != std::string::npos ) {
        snowDirection = 0;
        windDirectionA = 1;
        windDirectionB = 2;
      }
      if ( forceSpace.find( "-y" ) != std::string::npos ) {
        snowDirection = 1;
        windDirectionA = 0;
        windDirectionB = 2;
      }
      if ( forceSpace.find( "-z" ) != std::string::npos ) {
        snowDirection = 2;
        windDirectionA = 0;
        windDirectionB = 1;
      }

      VectorType snowForce( 3 * numVertices );
      VectorType windForceA( 3 * numVertices );
      VectorType windForceB( 3 * numVertices );

      for ( int vertexIdx = 0; vertexIdx < Topology.getNumVertices(); vertexIdx++ ) {
        const VecType &n = vertexNormals[vertexIdx];

        VecType d;
        d[snowDirection] = std::abs( n[snowDirection] );
        setXYZCoord( snowForce, d, vertexIdx );

        d.setZero();
        d[windDirectionA] = std::abs( n[windDirectionA] );
        setXYZCoord( windForceA, d, vertexIdx );

        d.setZero();
        d[windDirectionB] = std::abs( n[windDirectionB] );
        setXYZCoord( windForceB, d, vertexIdx );
      }

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = snowForce.normalized();
      forceBasis.col( 1 ) = windForceA.normalized();
      forceBasis.col( 2 ) = windForceB.normalized();
    }


//    for ( int idx : dirichletBoundary )
//      forceBasis.row( idx ).array() = 0.;

    VectorType ldForce( forceInitialization.size());
    for ( int i = 0; i < forceInitialization.size(); i++ )
      ldForce[i] = forceInitialization[i];

    VectorType vertexForces = forceBasis * ldForce;
//    std::cout << vertexForces << std::endl;

    // Output start values
    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = faceWeights;
      colorings["Force"] = vertexForces;
      saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_start.vtp", colorings );
    }

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Optimization" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // DOF variable
    VectorType combinedVector( numFaces + numDOFs );
    combinedVector.head( numFaces ) = faceWeights;
    combinedVector.tail( numDOFs ).setZero();

//    NonlinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> ES( Topology, refGeometry,
//                                                                                     dirichletBoundary,
//                                                                                     undefDisplacement,
//                                                                                     vertexForces, vertexWeights,
//                                                                                     false, true, true,
//                                                                                     false, false, false );

    LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> ES( Topology, refGeometry,
                                                                                  dirichletBoundary,
                                                                                  undefDisplacement,
                                                                                  vertexForces, faceWeights,
                                                                                  false, true, true,
                                                                                  false, false, false );

    if ( trackingIndices.empty()) {
      trackingIndices.resize( Topology.getNumVertices());
      std::iota( trackingIndices.begin(), trackingIndices.end(), 0 );
    }
    TrackingTerm<DefaultConfigurator> T( Topology, refGeometry, trackingIndices,
                                         false, true, true,
                                         false, false, false );
    TrackingTermGradient<DefaultConfigurator> DT( Topology, refGeometry, trackingIndices, false, true, true,
                                                  false, false, false );

    MaterialMassLogBarrier<DefaultConfigurator> Lg( Topology, refGeometry, BilevelOptimization.MassBarrier,
                                                    false, true, true, false, false, false );
    MaterialMassLogBarrierGradient<DefaultConfigurator> DLg( Topology, refGeometry, BilevelOptimization.MassBarrier,
                                                             false, true, true, false, false, false );

    MaterialPointLogBarrier<DefaultConfigurator> PLg( Topology, refGeometry,
                                                    false, true, true, false, false, false );
    MaterialPointLogBarrierGradient<DefaultConfigurator> DPLg( Topology, refGeometry,
                                                             false, true, true, false, false, false );

    VectorType weights( 3 );
    weights << BilevelOptimization.trackingWeight, BilevelOptimization.MassWeight, BilevelOptimization.PointBarrierWeight; // , BilevelOptimization.L2Weight

    std::cout << " .. weights = " << weights.transpose() << std::endl;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, T,  Lg, PLg );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DT, DLg, DPLg );


//    OptimalForceSolver<DefaultConfigurator, MaterialShellDeformationType> OFS( Topology, refGeometry, dirichletBoundary,
//                                                                               undefDisplacement, vertexWeights,
//                                                                               forceBasis, ldForce.norm(), ldForce,
//                                                                               false, true, false, false );
    LinearizedOptimalForceSolver<DefaultConfigurator, MaterialShellDeformationType> OFS( Topology, refGeometry, dirichletBoundary,
                                                                                         undefDisplacement, faceWeights,
                                                                                         forceBasis, ldForce,
                                                                                         false, true, false, false );

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );
      OFS.Configuration.maxNumIterations = config["OptimalForceSolver"]["maxNumIterations"].as<int>();
      OFS.Configuration.optimalityTolerance = config["OptimalForceSolver"]["optimalityTolerance"].as<RealType>();
      OFS.Configuration.stepsize = config["OptimalForceSolver"]["stepsize"].as<RealType>();
      OFS.Configuration.maximalStepsize = config["OptimalForceSolver"]["maximalStepsize"].as<RealType>();
      OFS.Configuration.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["OptimalForceSolver"]["stepsizeController"].as<int>());
      OFS.Configuration.acceleration = config["OptimalForceSolver"]["acceleration"].as<bool>();
      OFS.Configuration.useL2Constraint = config["OptimalForceSolver"]["useL2Constraint"].as<bool>();
      OFS.Configuration.barrierValue = config["OptimalForceSolver"]["barrierValue"].as<RealType>();
      OFS.Configuration.regularizationWeight = config["OptimalForceSolver"]["regularizationWeight"].as<RealType>();
      OFS.Configuration.objectiveWeight = config["OptimalForceSolver"]["objectiveWeight"].as<RealType>();
    }

    ldForce = OFS.forceSolve( faceWeights);

    std::cout << " .. Init OFS: " << OFS.evaluateWithForce(ldForce ) << std::endl;

//    BilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Jred( J, ES, OFS, forceBasis );
//    BilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DJred( J, DJ, ES, OFS, forceBasis, dirichletBoundary );


    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Jred( J, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DJred( J, DJ, ES, OFS, forceBasis, dirichletBoundary );


//    std::cout << " .. vertexWeights = " << vertexWeights.transpose() << std::endl;
    RealType value = Jred( faceWeights );
    std::cout << " .. Init Jred = " << value << std::endl;
    VectorType grad =  DJred( faceWeights );
//    std::cout << " .. Init DJred = " << grad.transpose() << std::endl;
    std::cout << " .. Init DJred.norm = " << grad.norm() << std::endl;

    if ( testDerivatives ) {
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-6 ).testAllDirections( vertexWeights, true );
      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( faceWeights, 30,
                                                                                                   outputPrefix +
                                                                                                   "testDJred_start" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotAllDirections( vertexWeights,
//                                                                                                   outputPrefix +
//                                                                                                   "testDJred_start" );
    }
//    VectorType rndWeights = vertexWeights;
//    for (int i = 0; i < 3; i++) {
//      rndWeights -= grad * 0.0001;
//      value = Jred( rndWeights );
//      grad = DJred( rndWeights );
//      std::cout << " .. Rnd Jred = " << value << std::endl;
//    }
//
//
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( vertexWeights, 20,
//                                                                                                 outputPrefix +
//                                                                                                 "testDJred_start2" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotAllDirections( vertexWeights,
//                                                                                              outputPrefix +
//                                                                                              "testDJred_" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).testRandomDirections( vertexWeights, 100,
//                                                                                                 true );
//    for ( int i = 0; i < 3; i++ ) {
//      std::cout << " -- Random check " << i << std::endl;
//      VectorType rndWeights = vertexWeights;
//      rndWeights -= i * grad * 0.001;
//
//
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( rndWeights, 50,
//                                                                                                   outputPrefix +
//                                                                                                   "testDJred_" +
//                                                                                                   std::to_string( i ) +
//                                                                                                   "_" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).testRandomDirections( rndWeights, 10,
//                                                                                                   true );
//
//      value = Jred( rndWeights );
//      std::cout << " .. Jred " << i << " = " << std::scientific << std::setprecision(5) << value << std::endl;
//    }


    VectorType lowerBounds = VectorType::Constant( numFaces, 1.e-3 );
    VectorType upperBounds = VectorType::Constant( numFaces, std::numeric_limits<RealType>::infinity());



    ldForce = OFS.solve( faceWeights);
    vertexForces = forceBasis * ldForce;

    std::cout << " .. Start forces: " << ldForce.transpose() << std::endl;
    std::cout << " .. Start forces.norm: " << ldForce.norm() << std::endl;

    VectorType DesignAndForce( 3 * numVertices + numFaces);
    DesignAndForce.head( 3 * numVertices) = vertexForces;
    DesignAndForce.tail( numFaces ) = faceWeights;

    VectorType Solution = ES.solve(DesignAndForce);
    defGeometry = refGeometry + Solution;

    VectorType DesignForceDeformation( 3 * numVertices + numFaces +  3 * numVertices );
    DesignForceDeformation.head( 3 * numVertices) = vertexForces;
    DesignForceDeformation.segment(  3 * numVertices, numFaces ) = faceWeights;
    DesignForceDeformation.tail( 3 * numVertices ) = Solution;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["Material"] = faceWeights;
      saveAsVTP( Topology, defGeometry, outputPrefix + "blvl_start_def.vtp", colorings );
    }

    VectorType faceAreas;
    getFaceAreas<ConfiguratorType>( Topology, refGeometry, faceAreas );
    VectorType newMaterial = faceWeights;
    std::cout << " .. Initial material = " << newMaterial.norm() << std::endl;
    std::cout << " .. Initial material (L2) = " << faceAreas.dot( newMaterial ) << std::endl;
    std::cout << " .. Initial min material  = " <<newMaterial.minCoeff() << std::endl;
    std::cout << " .. Initial J = " << J( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Tracking = " << T( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Material-Barrier = " << Lg( DesignForceDeformation ) << std::endl;
    std::cout << " .. Initial Point-Barrier = " << PLg( DesignForceDeformation ) << std::endl;

    if(testDerivatives) {
//      ScalarValuedDerivativeTester<DefaultConfigurator>( J, DJ, 1e-6 ).plotAllDirections( DesignForceDeformation,
//                                                                                          outputPrefix +
//                                                                                          "testDJ_start" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( T, DT, 1e-6 ).plotAllDirections( DesignForceDeformation,
//                                                                                             outputPrefix +
//                                                                                             "testDT_start" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Lg, DLg, 1e-6 ).plotAllDirections( DesignForceDeformation,
//                                                                                             outputPrefix +
//                                                                                             "testDLg_start" );
//      ScalarValuedDerivativeTester<DefaultConfigurator>( PLg, DPLg, 1e-6 ).plotAllDirections( DesignForceDeformation,
//                                                                                             outputPrefix +
//                                                                                             "testDPLg_start" );
    }

//    if ( testDerivatives ) {
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).testRandomDirections( newMaterial, 10, true);
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( newMaterial, 20,
//                                                                                                   outputPrefix +
//                                                                                                   "testDJred_start" );
//    }

    if (useL2Constraint) {
      std::cout << " .. useL2Constraint" << std::endl;
      auto prox = std::bind( projectionOnBoxBall<RealType, VectorType>, materialBound, lowerBounds, upperBounds,
                             std::placeholders::_1, std::placeholders::_2 );

      // Pre-solve
//      if ( BilevelOptimization.stepsizeController != CONST_TIMESTEP_CONTROL ) {
//        ProximalGradientDescent<DefaultConfigurator> Solver( Jred, DJred, prox, 200,
//                                                             BilevelOptimization.optimalityTolerance, SHOW_ALL );
//        Solver.setParameter( "initial_stepsize", BilevelOptimization.stepsize );
//        Solver.setParameter( "maximal_stepsize", BilevelOptimization.maximalStepsize );
//        Solver.setParameter( "stepsize_control", CONST_TIMESTEP_CONTROL );
//        Solver.setParameter( "acceleration", BilevelOptimization.acceleration );
//        Solver.solve( newMaterial, newMaterial );
//
//        if ( testDerivatives )
//          ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( newMaterial, 20,
//                                                                                                       outputPrefix +
//                                                                                                       "testDJred_mid" );
//      }

      ProximalGradientDescent<DefaultConfigurator> Solver( Jred, DJred, prox, BilevelOptimization.maxNumIterations,
                                                           BilevelOptimization.optimalityTolerance, SHOW_ALL );
      Solver.setParameter( "initial_stepsize", BilevelOptimization.stepsize );
      Solver.setParameter( "maximal_stepsize", BilevelOptimization.maximalStepsize );
      Solver.setParameter( "stepsize_control", BilevelOptimization.stepsizeController );
      Solver.setParameter( "acceleration", BilevelOptimization.acceleration );
//    auto t_start = std::chrono::high_resolution_clock::now();
      Solver.solve( newMaterial, newMaterial );
//    auto t_end = std::chrono::high_resolution_clock::now();
    }
    else {
      auto prox = std::bind( projectionOnBox<RealType, VectorType>, lowerBounds, upperBounds,
                             std::placeholders::_1, std::placeholders::_2 );

      // Pre-solve
//      if ( BilevelOptimization.stepsizeController != CONST_TIMESTEP_CONTROL ) {
//        ProximalGradientDescent<DefaultConfigurator> Solver( Jred, DJred, prox, 100,
//                                                             BilevelOptimization.optimalityTolerance, SHOW_ALL );
//        Solver.setParameter( "initial_stepsize", BilevelOptimization.stepsize );
//        Solver.setParameter( "maximal_stepsize", BilevelOptimization.maximalStepsize );
//        Solver.setParameter( "stepsize_control", CONST_TIMESTEP_CONTROL );
//        Solver.setParameter( "acceleration", BilevelOptimization.acceleration );
//
////        Solver.setFixedVariables( dirichletIndices );
//        Solver.solve( newMaterial, newMaterial );
//        if ( testDerivatives )
//          ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( newMaterial, 20,
//                                                                                                       outputPrefix +
//                                                                                                       "testDJred_mid" );
//      }


    if (BilevelOptimization.preIterations > 0) {
      QuasiNewtonBFGS<DefaultConfigurator> Solver( Jred, DJred, BilevelOptimization.preIterations,
                                                   BilevelOptimization.optimalityTolerance, CONST_TIMESTEP_CONTROL, 50,
                                                   SHOW_ALL, 0.1, 1.e-6, BilevelOptimization.maximalStepsize );
      Solver.solve( newMaterial, newMaterial );
    }

      ldForce = OFS.solve(newMaterial);
      vertexForces = forceBasis * ldForce;

      {
        std::map<std::string, VectorType> colorings;
        colorings["Force"] = vertexForces;
        colorings["Material"] = newMaterial;
        saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_pre.vtp", colorings );
      }


//      GradientDescent<DefaultConfigurator> Solver( Jred, DJred, BilevelOptimization.maxNumIterations,
//                                                   BilevelOptimization.optimalityTolerance, BilevelOptimization.stepsizeController,
//                                                   SHOW_ALL, 0.1, 1.e-6, BilevelOptimization.maximalStepsize );
      QuasiNewtonBFGS<DefaultConfigurator> Solver( Jred, DJred, BilevelOptimization.maxNumIterations,
                                                   BilevelOptimization.optimalityTolerance, BilevelOptimization.stepsizeController, 50,
                                                   SHOW_ALL, 0.1, 1.e-6, BilevelOptimization.maximalStepsize );

      std::vector<RealType> x_l(numVertices, -std::numeric_limits<RealType>::infinity());
      std::vector<RealType> x_u(numVertices, std::numeric_limits<RealType>::infinity());

//      IpoptBoxConstraintFirstOrderSolver<DefaultConfigurator> Solver(Jred, DJred, BilevelOptimization.maxNumIterations,
//                                                                     BilevelOptimization.optimalityTolerance,x_l, x_u );

//      ProximalGradientDescent<DefaultConfigurator> Solver( Jred, DJred, prox, BilevelOptimization.maxNumIterations,
//                                                           BilevelOptimization.optimalityTolerance, SHOW_ALL );
//      Solver.setParameter( "initial_stepsize", BilevelOptimization.stepsize );
//      Solver.setParameter( "maximal_stepsize", BilevelOptimization.maximalStepsize );
//      Solver.setParameter( "stepsize_control", BilevelOptimization.stepsizeController );
//      Solver.setParameter( "acceleration", BilevelOptimization.acceleration );

//      Solver.setFixedVariables( dirichletIndices );
//    auto t_start = std::chrono::high_resolution_clock::now();
      Solver.solve( newMaterial, newMaterial );

      ldForce = OFS.solve(newMaterial);
      vertexForces = forceBasis * ldForce;

      {
        std::map<std::string, VectorType> colorings;
        colorings["Force"] = vertexForces;
        colorings["Material"] = newMaterial;
        saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_mid.vtp", colorings );
      }

      for (int i = 0; i <= 16; i++)
        std::cout << " .. Compliance with " << std::pow(2, i) << "x force: " << OFS.evaluateWithForce( std::pow(2, i) * ldForce ) << std::endl;

//      GradientDescent<DefaultConfigurator> Solver2( Jred, DJred, BilevelOptimization.maxNumIterations,
//                                                   BilevelOptimization.optimalityTolerance, CONST_TIMESTEP_CONTROL,
//                                                   SHOW_ALL, 0.1, 1.e-6, BilevelOptimization.maximalStepsize );
//      Solver2.solve( newMaterial, newMaterial );
    }
    ldForce = OFS.solve(newMaterial);
    vertexForces = forceBasis * ldForce;

    std::cout << " .. End forces: " << ldForce.transpose() << std::endl;
    std::cout << " .. End forces.norm: " << ldForce.norm() << std::endl;

    DesignAndForce.head( 3 * numVertices) = vertexForces;
    DesignAndForce.tail( numFaces ) = newMaterial;

    Solution = ES.solve(DesignAndForce);
    defGeometry = refGeometry + Solution;

    DesignForceDeformation.head( 3 * numVertices) = vertexForces;
    DesignForceDeformation.segment(  3 * numVertices, numFaces ) = newMaterial;
    DesignForceDeformation.tail( 3 * numVertices ) = Solution;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["initMaterial"] = faceWeights;
      colorings["Material"] = newMaterial;
      saveAsVTP( Topology, defGeometry, outputPrefix + "blvl_opt_def.vtp", colorings );
      saveAsVTP( Topology, refGeometry, outputPrefix + "blvl_opt.vtp", colorings );
    }

    std::cout << " .. Final material (L2) = " << faceAreas.dot( newMaterial ) << std::endl;
    std::cout << " .. Final min material  = " << newMaterial.minCoeff() << std::endl;
    std::cout << " .. Final J = " << J( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Tracking = " << T( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Material-Barrier = " << Lg( DesignForceDeformation ) << std::endl;
    std::cout << " .. Final Point-Barrier = " << PLg( DesignForceDeformation ) << std::endl;


//    if (testDerivatives)
//      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( newMaterial, 50,
//                                                                                                   outputPrefix +
//                                                                                                   "testDJred_end" );

//    for ( int i = 0; i < 3; i++ ) {
//      VectorType rndWeights = newMaterial;
//      rndWeights.array() += VectorType::Random( newMaterial.size()).array() * newMaterial.array() * 0.01;
//      value = Jred( rndWeights );
//      std::cout << " .. Rnd Jred = " << value << std::endl;
//    }
//
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-4 ).plotRandomDirections( newMaterial, 20,
//                                                                                                 outputPrefix +
//                                                                                                 "testDJred_end2" );

    std::cout << "===============================================================" << std::endl;


    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}