//
// Created by josua on 03.06.20.
//

//==============Includes================================
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <cstdio>
#include <fstream>
#include <sstream>

#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/Core.h"
#include "goast/Core/FEM.h"

//#define  IPOPTDERIVATIVETEST
#include "goast/external/ipoptBoxConstraintSolver.h"
#include "goast/external/ipoptNonlinearConstraintSolver.h"
#include "goast/Optimization/LineSearchNewton.h"
#include "goast/Optimization/quasiNewton.h"
#include "goast/external/vtkIO.h"

#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ShapeOptimization/ReducedFunctional.h"
#include "EnergyRegularization.h"
#include "ForceRegularization.h"
#include "ComplianceTerm.h"
#include "MaterialDeformationEnergies.h"
//======================================================

//================================================================================

typedef DefaultConfigurator ConfiguratorType;
typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::VecType VecType;
typedef typename ConfiguratorType::MatType MatType;
typedef std::vector<TripletType> TripletListType;

int main( int argc, char *argv[] ) {
  try {
    //<editor-fold desc="Configuration">
    // Mesh
    std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_0.obj";


    RealType thickness = 0.1;

    // Boundary values
    std::vector<int> bndMask;
    bool detectBoundary = true;

    // Forces
    RealType force_x = 0.05;
    RealType force_y = 0.05;
    RealType force_z = -0.05; // -0.05


    // Output folder
    std::string outputFilePrefix;
    std::string outputFolder = "/home/josua/Projects/BiSO/output";
    bool timestampOutput = false;
    //</editor-fold>


    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }

    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    //<editor-fold desc="Output folders">
    if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
      outputFolder += "/";

    std::string execName( argv[0] );
    execName = execName.substr( execName.find_last_of( '/' ) + 1 );

    if ( timestampOutput ) {
      std::time_t t = std::time( nullptr );
      std::stringstream ss;
      ss << outputFolder;
      ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss << "_" << execName;
      ss << "/";
      outputFolder = ss.str();
      boost::filesystem::create_directory( outputFolder );
    }

    if ( argc == 2 ) {
      boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                    boost::filesystem::copy_option::overwrite_if_exists );
    }

    std::string outputPrefix = outputFolder + outputFilePrefix;
    //</editor-fold>

    //<editor-fold desc="Output log">
    std::ofstream logFile;
    logFile.open( outputFolder + "/_output.log" );

    std::ostream output_cout( std::cout.rdbuf());
    std::ostream output_cerr( std::cerr.rdbuf());

    typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
    typedef boost::iostreams::stream<TeeDevice> TeeStream;
    TeeDevice tee_cout( logFile, output_cout );
    TeeDevice tee_cerr( logFile, output_cerr );

    TeeStream split_cout( tee_cout );
    TeeStream split_cerr( tee_cerr );

//    std::cout.rdbuf( split_cout.rdbuf());
//    std::cerr.rdbuf( split_cerr.rdbuf());
    //</editor-fold>


    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================
    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    VectorType undefGeometry = refGeometry + undefDisplacement;


    // Material
    VectorType vertexWeights_const = VectorType::Constant( numVertices, thickness );

    VectorType vertexWeights = VectorType::Constant( numVertices, thickness );

    // Deformation energies
    MaterialShellDeformationType W( Topology, vertexWeights );
    MaterialShellDeformationType W_constant( Topology, vertexWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletBoundary;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletBoundary );

    dirichletBoundary.insert( dirichletBoundary.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;
    std::cout << " - Dirichlet = { ";
    for ( auto v : dirichletBoundary )
      std::cout << v << " ";
    std::cout << "}" << std::endl;


    // Forces
    // TODO: Replace by a proper BaseOp-based force parametrization
    ForceClass<DefaultConfigurator> Forces( numVertices, dirichletBoundary, force_x, force_y, force_z );
//     Forces.printForceVector();

    VectorType x_and_y_force( 2 );
    x_and_y_force[0] = force_x;
    x_and_y_force[1] = force_y;

    VectorType vertexForces;
    Forces.getForceVector( vertexForces );
//    std::cout << vertexForces << std::endl;

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Linear elasticity" << std::endl;
    std::cout << "===============================================================" << std::endl;
    LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> LES( Topology, refGeometry,
                                                                                   dirichletBoundary, undefDisplacement,
                                                                                   vertexForces, vertexWeights,
                                                                                   true, true, false );
    std::cout << " .. LES done" << std::endl;

    VectorType initialControl( 2 * 3 * numVertices );
    initialControl.head( numDOFs ) = undefDisplacement;
    initialControl.tail( numDOFs ) = vertexForces;

    VectorType Solution = LES.solve( initialControl );

    VectorType combinedVector( initialControl.size() + Solution.size());
    combinedVector.head( initialControl.size()) = initialControl;
    combinedVector.tail( Solution.size()) = Solution;

    LinearizedComplianceTerm<DefaultConfigurator, MaterialShellDeformationType> C( Topology, refGeometry,
                                                                                   undefDisplacement,
                                                                                   vertexWeights,
                                                                                   true, true, false,
                                                                                   false, false, false  );
    std::cout << " .. C done" << std::endl;
    LinearizedComplianceTermGradient<DefaultConfigurator, MaterialShellDeformationType> DC( Topology, refGeometry,
                                                                                            undefDisplacement,
                                                                                            vertexWeights,
                                                                                            true, true, false,
                                                                                            false, false, false  );
    std::cout << " .. DC done" << std::endl;

    EnergyRegularizationTerm<DefaultConfigurator> R( Topology, refGeometry, W_constant );
    std::cout << " .. R done" << std::endl;
    EnergyRegularizationTermGradient<DefaultConfigurator> DR( Topology, refGeometry, W_constant );
    std::cout << " .. DR done" << std::endl;

    VectorType functionalWeights = VectorType::Constant( 2, 1. );

    AdditionOp<DefaultConfigurator> J( functionalWeights, C, R );
    std::cout << " .. J done" << std::endl;
    AdditionGradient<DefaultConfigurator> DJ( functionalWeights, DC, DR );
    std::cout << " .. DJ done" << std::endl;

//    ScalarValuedDerivativeTester<DefaultConfigurator>( J, DJ, 1e-6 ).plotRandomDirections( combinedVector,
//                                                                                           100,
//                                                                                           outputPrefix +
//                                                                                           "testDJ_" );

    ReducedPDECFunctional<DefaultConfigurator> Jred( J, LES );
    std::cout << " .. Jred done" << std::endl;
    ReducedPDECGradient<DefaultConfigurator> DJred( J, DJ, LES, dirichletBoundary );
    std::cout << " .. DJred done" << std::endl;


    std::cout << " .. Jred = " << Jred( initialControl ) << std::endl;
    std::cout << " .. J= " << J( combinedVector ) << std::endl;

//    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-8 ).plotRandomDirections( initialControl,
//                                                                                                 100,
//                                                                                                 outputPrefix +
//                                                                                                 "testDJred_" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-8 ).testAllDirections( initialControl,
                                                                                                 true );

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Nonlinear elasticity" << std::endl;
    std::cout << "===============================================================" << std::endl;
    NonlinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> NES( Topology, refGeometry,
                                                                                   dirichletBoundary, undefDisplacement,
                                                                                   vertexForces, vertexWeights,
                                                                                      true, true, true,
                                                                                   false, false, false  );
    std::cout << " .. LES done" << std::endl;

    initialControl.resize( 2 * 3 * numVertices + numVertices );
    initialControl.head( numDOFs ) = undefDisplacement;
    initialControl.segment( numDOFs, numDOFs ) = vertexForces;
    initialControl.tail( numVertices ) = vertexWeights;

    Solution = NES.solve( initialControl );

    combinedVector.resize( initialControl.size() + Solution.size() );
    combinedVector.head( initialControl.size()) = initialControl;
    combinedVector.tail( Solution.size()) = Solution;

    ComplianceTerm<DefaultConfigurator, MaterialShellDeformationType> C_nl( Topology, refGeometry, W,
                                                                            true, true, true,
                                                                            false, false, false );
    std::cout << " .. C done" << std::endl;
    ComplianceTermGradient<DefaultConfigurator, MaterialShellDeformationType> DC_nl( Topology, refGeometry, W,
                                                                                     true, true, true,
                                                                                     false, false, false );
    std::cout << " .. DC done" << std::endl;

    EnergyRegularizationTerm<DefaultConfigurator> R_nl( Topology, refGeometry, W_constant );
    std::cout << " .. R done" << std::endl;
    EnergyRegularizationTermGradient<DefaultConfigurator> DR_nl( Topology, refGeometry, W_constant );
    std::cout << " .. DR done" << std::endl;

    AdditionOp<DefaultConfigurator> J_nl( functionalWeights, C_nl, R_nl );
    std::cout << " .. J done" << std::endl;
    AdditionGradient<DefaultConfigurator> DJ_nl( functionalWeights, DC_nl, DR_nl );
    std::cout << " .. DJ done" << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( C_nl, DC_nl, 1e-6 ).plotAllDirections( combinedVector,
                                                                                           outputPrefix +
                                                                                           "testDJ_nl_" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( C_nl, DC_nl, 1e-6 ).testAllDirections( combinedVector, true );

    ReducedPDECParametrizedFunctional<DefaultConfigurator> Jred_nl( C_nl, NES );
    std::cout << " .. Jred done" << std::endl;
    ReducedPDECParametrizedGradient<DefaultConfigurator> DJred_nl( C_nl, DC_nl, NES, dirichletBoundary );
    std::cout << " .. DJred done" << std::endl;


    std::cout << " .. Jred = " << Jred_nl( initialControl ) << std::endl;
    std::cout << " .. J= " << C_nl( combinedVector ) << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred_nl, DJred_nl, 1e-5 ).plotAllDirections( initialControl,
                                                                                                    outputPrefix +
                                                                                                    "testDJred_nl_" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred_nl, DJred_nl, 1e-5 ).testAllDirections( initialControl, true );

    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    return -1;
  }
}