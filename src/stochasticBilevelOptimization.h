//
// Created by josua on 05.02.21.
//

#ifndef BILEVELSHAPEOPT_STOCHASTICBILEVELOPTIMIZATION_H
#define BILEVELSHAPEOPT_STOCHASTICBILEVELOPTIMIZATION_H

#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/external/vtkIO.h"
#include "goast/external/ipoptBoxConstraintSolver.h"

#include "Optimization/ProximalGradientDescent.h"
#include "Optimization/ProximalMappings.h"

#include "ShapeOptimization/ReducedFunctional.h"

#include "MaterialDeformationEnergies.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ComplianceTerm.h"
#include "FixedComplianceTerm.h"
#include "TrackingCost.h"
#include "MaterialRegularization.h"
#include "OptimalForceSolver.h"
#include "LinearOptimalForceSolver.h"
#include "BilevelReducedFunctional.h"
#include "Optimization/StochasticGradientDescent.h"

#include "ReducedForces.h"

#include <memory>

template<typename FunctionalType>
class NoiseExpectedValue
        : public SampledBaseOp<typename FunctionalType::DomainType, typename FunctionalType::RangeType, typename FunctionalType::DomainType> {
public:
  using SampleType = typename FunctionalType::DomainType;
  using DomainType = typename FunctionalType::DomainType;
  using RangeType = typename FunctionalType::RangeType;
protected:
  // Wrapped function and used generator
  FunctionalType &m_F;
  const std::function<SampleType()> m_Gen;

  const int m_numSamples;

  // Stored samples
  std::vector<SampleType> m_Samples;

public:
  NoiseExpectedValue( FunctionalType &F, const std::function<SampleType()> &Gen, const int numSamples )
          : m_F( F ), m_Gen( Gen ), m_numSamples( numSamples ) {
    computeSamples();
  }

  void computeSamples() override {
    if ( m_Samples.size() != m_numSamples )
      m_Samples.resize( m_numSamples );

    for ( auto &sample : m_Samples )
      sample = m_Gen();
  }

  void setSamples( const std::vector<SampleType> &Samples ) override {
    assert( Samples.size() == m_numSamples && "SampledExpectedValue::setSamples: Wrong number of samples!" );
    m_Samples = Samples;
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Samples;
  }

  void apply( const DomainType &Arg, RangeType &Dest ) const override {
    // first value
    Dest = m_F( Arg.array() * m_Samples[0].array() );

    // other values
    for ( int i = 1; i < m_numSamples; i++ )
      Dest += m_F( Arg.array() * m_Samples[i].array() );

    Dest /= m_numSamples;
  }
};

template<typename ConfiguratorType, typename MaterialShellDeformationType>
class StochasticBilevelFunctional
        : public SampledBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType, typename ConfiguratorType::VectorType> {
public:
  using VectorType = typename ConfiguratorType::VectorType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using RealType = typename ConfiguratorType::RealType;

  using DomainType = VectorType;
  using SampleType = VectorType;
  using RangeType = RealType;
public:
  // Wrapped function and used generator
  BaseOp<DomainType, RangeType> &m_F;
  BaseOp<DomainType, RangeType> &m_G;
  const std::function<SampleType()> m_Gen;

  std::vector<std::unique_ptr<LinearElasticitySolver<ConfiguratorType, MaterialShellDeformationType>>> m_ES;
  std::vector<std::unique_ptr<LinearizedOptimalForceSolver<ConfiguratorType, MaterialShellDeformationType>>> m_OFS;
  std::vector<std::unique_ptr<LinearBilevelReducedFunctional < ConfiguratorType, MaterialShellDeformationType>>> m_Jred;

  const int m_numSamples;

  // Stored samples
  std::vector<SampleType> m_Samples;

public:
  StochasticBilevelFunctional( BaseOp<DomainType, RangeType> &F,
                               BaseOp<DomainType, RangeType> &G,
                               const std::function<SampleType()> &Gen,
                               const int numSamples,
                               const MeshTopologySaver &Topology, const VectorType &refGeometry,
                               const std::vector<int> &dirichletBoundary,
                               const FullMatrixType &forceBasis,
                               const VectorType undefDisplacement,
                               const VectorType vertexMaterial,
                               const VectorType initialForce,
                               const YAML::Node &config )
          : m_F( F ), m_G(G), m_Gen( Gen ), m_numSamples( numSamples ) {
    for ( int i = 0; i < numSamples; i++ ) {
      std::cout << " .. Create ES " << i << std::endl;
      m_ES.push_back(
              std::make_unique<LinearElasticitySolver<ConfiguratorType, MaterialShellDeformationType>>( Topology,
                                                                                                        refGeometry,
                                                                                                        dirichletBoundary,
                                                                                                        undefDisplacement,
                                                                                                        forceBasis *
                                                                                                        initialForce,
                                                                                                        vertexMaterial,
                                                                                                        false, true,
                                                                                                        true, false,
                                                                                                        false,
                                                                                                        false ));

      std::cout << " .. Create OFS " << i << std::endl;
      m_OFS.push_back(
              std::make_unique<LinearizedOptimalForceSolver<ConfiguratorType, MaterialShellDeformationType>>( Topology,
                                                                                                              refGeometry,
                                                                                                              dirichletBoundary,
                                                                                                              undefDisplacement,
                                                                                                              vertexMaterial,
                                                                                                              forceBasis,
                                                                                                              initialForce,
                                                                                                              false,
                                                                                                              true,
                                                                                                              false,
                                                                                                              false ));

      m_OFS[i]->Configuration.maxNumIterations = config["maxNumIterations"].as<int>();
      m_OFS[i]->Configuration.optimalityTolerance = config["optimalityTolerance"].as<RealType>();
      m_OFS[i]->Configuration.stepsize = config["stepsize"].as<RealType>();
      m_OFS[i]->Configuration.maximalStepsize = config["maximalStepsize"].as<RealType>();
      m_OFS[i]->Configuration.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["stepsizeController"].as<int>());
      m_OFS[i]->Configuration.acceleration = config["acceleration"].as<bool>();
      m_OFS[i]->Configuration.useL2Constraint = config["useL2Constraint"].as<bool>();
      m_OFS[i]->Configuration.barrierValue = config["barrierValue"].as<RealType>();
      m_OFS[i]->Configuration.regularizationWeight = config["regularizationWeight"].as<RealType>();
      m_OFS[i]->Configuration.objectiveWeight = config["objectiveWeight"].as<RealType>();
      m_OFS[i]->Configuration.useCylinderBarrier = config["useCylinderBarrier"].as<bool>();
      m_OFS[i]->Configuration.cylinderHeightFactor = config["cylinderHeightFactor"].as<RealType>();
      m_OFS[i]->Configuration.cylinderHeightDimension = config["cylinderHeightDimension"].as<int>();

      std::cout << " .. Create Jred " << i << std::endl;
      m_Jred.push_back(
              std::make_unique<LinearBilevelReducedFunctional<ConfiguratorType, MaterialShellDeformationType>>( F,
                                                                                                                *m_ES[i],
                                                                                                                *m_OFS[i],
                                                                                                                forceBasis ));
    }

    m_ES.push_back(
            std::make_unique<LinearElasticitySolver<ConfiguratorType, MaterialShellDeformationType>>( Topology,
                                                                                                      refGeometry,
                                                                                                      dirichletBoundary,
                                                                                                      undefDisplacement,
                                                                                                      forceBasis *
                                                                                                      initialForce,
                                                                                                      vertexMaterial,
                                                                                                      false, true,
                                                                                                      true, false,
                                                                                                      false,
                                                                                                      false ));

    m_OFS.push_back(
            std::make_unique<LinearizedOptimalForceSolver<ConfiguratorType, MaterialShellDeformationType>>( Topology,
                                                                                                            refGeometry,
                                                                                                            dirichletBoundary,
                                                                                                            undefDisplacement,
                                                                                                            vertexMaterial,
                                                                                                            forceBasis,
                                                                                                            initialForce,
                                                                                                            false,
                                                                                                            true,
                                                                                                            false,
                                                                                                            false ));

    m_OFS[numSamples]->Configuration.maxNumIterations = config["maxNumIterations"].as<int>();
    m_OFS[numSamples]->Configuration.optimalityTolerance = config["optimalityTolerance"].as<RealType>();
    m_OFS[numSamples]->Configuration.stepsize = config["stepsize"].as<RealType>();
    m_OFS[numSamples]->Configuration.maximalStepsize = config["maximalStepsize"].as<RealType>();
    m_OFS[numSamples]->Configuration.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["stepsizeController"].as<int>());
    m_OFS[numSamples]->Configuration.acceleration = config["acceleration"].as<bool>();
    m_OFS[numSamples]->Configuration.useL2Constraint = config["useL2Constraint"].as<bool>();
    m_OFS[numSamples]->Configuration.barrierValue = config["barrierValue"].as<RealType>();
    m_OFS[numSamples]->Configuration.regularizationWeight = config["regularizationWeight"].as<RealType>();
    m_OFS[numSamples]->Configuration.objectiveWeight = config["objectiveWeight"].as<RealType>();
    m_OFS[numSamples]->Configuration.useCylinderBarrier = config["useCylinderBarrier"].as<bool>();
    m_OFS[numSamples]->Configuration.cylinderHeightFactor = config["cylinderHeightFactor"].as<RealType>();
    m_OFS[numSamples]->Configuration.cylinderHeightDimension = config["cylinderHeightDimension"].as<int>();


    m_Jred.push_back(
            std::make_unique<LinearBilevelReducedFunctional<ConfiguratorType, MaterialShellDeformationType>>( G,
                                                                                                              *m_ES[numSamples],
                                                                                                              *m_OFS[numSamples],
                                                                                                              forceBasis ));

    computeSamples();
  }

  void computeSamples() override {
    if ( m_Samples.size() != m_numSamples )
      m_Samples.resize( m_numSamples );

    for ( auto &sample : m_Samples )
      sample = m_Gen();
  }

  void setSamples( const std::vector<SampleType> &Samples ) override {
    assert( Samples.size() == m_numSamples && "StochasticBilevelFunctional::setSamples: Wrong number of samples!" );
    m_Samples = Samples;
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Samples;
  }

  void apply( const VectorType &Arg, RealType &Dest ) const override {
    Dest = 0.;

    // other values
#pragma omp parallel for
    for ( int i = 0; i < m_numSamples; i++ ) {
      RealType localValue = ( *m_Jred[i] )( Arg.array() * m_Samples[i].array() );
//      RealType localValue = ( *m_Jred[i] )( Arg + m_Samples[i] );
#pragma omp critical
      Dest += localValue;
    }

    Dest /= m_numSamples;
    std::cout << " ... EC = " << Dest << std::endl;

    Dest += (*m_Jred[m_numSamples])(Arg);
    std::cout << " ... R = " << (*m_Jred[m_numSamples])(Arg) << std::endl;
  }
};

template<typename ConfiguratorType, typename MaterialShellDeformationType>
class StochasticBilevelGradient
        : public SampledBaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
public:
  using VectorType = typename ConfiguratorType::VectorType;
  using FullMatrixType = typename ConfiguratorType::FullMatrixType;
  using RealType = typename ConfiguratorType::RealType;

  using DomainType = VectorType;
  using SampleType = VectorType;
  using RangeType = VectorType;
protected:
  // Wrapped function and used generator
  BaseOp<DomainType, RangeType> &m_DF;
  BaseOp<DomainType, RangeType> &m_DG;
  const std::function<SampleType()> m_Gen;

  std::vector<std::unique_ptr<LinearElasticitySolver<ConfiguratorType, MaterialShellDeformationType>>> &m_ES;
  std::vector<std::unique_ptr<LinearizedOptimalForceSolver<ConfiguratorType, MaterialShellDeformationType>>> &m_OFS;
  std::vector<std::unique_ptr<LinearBilevelReducedGradient < ConfiguratorType, MaterialShellDeformationType>>> m_Jred;

  const int m_numSamples;

  // Stored samples
  std::vector<SampleType> m_Samples;

public:
  StochasticBilevelGradient( BaseOp<DomainType, RealType> &F,
                             BaseOp<DomainType, RangeType> &DF,
                             BaseOp<DomainType, RealType> &G,
                             BaseOp<DomainType, RangeType> &DG,
                             const std::function<SampleType()> &Gen,
                               const int numSamples,
                               const std::vector<int> &dirichletBoundary,
                               const FullMatrixType &forceBasis,
                             std::vector<std::unique_ptr<LinearElasticitySolver<ConfiguratorType, MaterialShellDeformationType>>> &ES,
                             std::vector<std::unique_ptr<LinearizedOptimalForceSolver<ConfiguratorType, MaterialShellDeformationType>>> &OFS)
          : m_DF( DF ), m_DG( DG ), m_Gen( Gen ), m_numSamples( numSamples ), m_ES(ES), m_OFS(OFS) {

    for (int i = 0; i < numSamples; i++) {
      std::cout << " .. Create DJred " << i << std::endl;
      m_Jred.push_back(
              std::make_unique<LinearBilevelReducedGradient<ConfiguratorType, MaterialShellDeformationType>>( F, DF,
                                                                                                                *m_ES[i],
                                                                                                                *m_OFS[i],
                                                                                                                forceBasis, dirichletBoundary ));
    }

    m_Jred.push_back(
            std::make_unique<LinearBilevelReducedGradient<ConfiguratorType, MaterialShellDeformationType>>( G, DG,
                                                                                                            *m_ES[numSamples],
                                                                                                            *m_OFS[numSamples],
                                                                                                            forceBasis, dirichletBoundary ));

    computeSamples();
  }

  void computeSamples() override {
    if ( m_Samples.size() != m_numSamples )
      m_Samples.resize( m_numSamples );

    for ( auto &sample : m_Samples )
      sample = m_Gen();
  }

  void setSamples( const std::vector<SampleType> &Samples ) override {
    assert( Samples.size() == m_numSamples && "StochasticBilevelFunctional::setSamples: Wrong number of samples!" );
    m_Samples = Samples;
  }

  const std::vector<SampleType> &getSamples() override {
    return m_Samples;
  }

  void apply( const VectorType &Arg, VectorType &Dest ) const override {
    Dest.resize(Arg.size());
    Dest.setZero();

    // other values
#pragma omp parallel for
    for ( int i = 0; i < m_numSamples; i++ ) {
      VectorType localGrad = ( *m_Jred[i] )( Arg.array() * m_Samples[i].array() ).array() * m_Samples[i].array();
//      VectorType localGrad = ( *m_Jred[i] )( Arg + m_Samples[i] ).array();
#pragma omp critical
      Dest += localGrad;
    }

    Dest.array() /= m_numSamples;

    Dest += (*m_Jred[m_numSamples])(Arg);
  }
};

#endif //BILEVELSHAPEOPT_STOCHASTICBILEVELOPTIMIZATION_H
