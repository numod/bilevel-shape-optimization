#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/external/vtkIO.h"

#include "Optimization/ProximalGradientDescent.h"
#include "Optimization/ProximalMappings.h"

#include "ShapeOptimization/ReducedFunctional.h"

#include "MaterialDeformationEnergies.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ComplianceTerm.h"
#include "FixedComplianceTerm.h"
#include "TrackingCost.h"
#include "MaterialRegularization.h"
#include "OptimalForceSolver.h"
#include "LinearOptimalForceSolver.h"
#include "BilevelReducedFunctional.h"

#include "SimpleQuadraticFunctional.h"
#include "ReducedForces.h"

//================================================================================

typedef DefaultConfigurator ConfiguratorType;
typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::FullMatrixType FullMatrixType;
typedef typename ConfiguratorType::VecType VecType;
typedef std::vector<TripletType> TripletListType;


//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================



int main( int argc, char *argv[] ) {
  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";

  // Boundary values
  std::vector<int> bndMask{}; // 171
  std::vector<int> trackingIndices{}; // 171
  std::vector<int> dirichletFaces{};
  bool detectBoundary = true;

  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Material
  RealType initialThickness = 0.03;
  RealType boundaryThickness = 0.03;
  RealType materialBound = 0.02;
  RealType randomPerturbation = 0.;
  bool useL2Constraint = true;


  // Forces
  std::string forceSpace = "twistAndCompress"; // twistAndCompress, Constant
  std::vector<RealType> forceInitialization;


  // shape optimization
  struct {
    bool useNonlinearElasticity = true;
    int maxNumIterations = 100;
    int numSamples = 32;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 1e-3;
    TIMESTEP_CONTROLLER stepsizeController = CONST_TIMESTEP_CONTROL;
    bool acceleration = false;
    RealType trackingWeight = 1.;
    RealType MassWeight = 0.;
    RealType MassBarrier = 1000.;
    RealType LowerPointBarrierWeight = 0.;
    RealType UpperPointBarrierWeight = 0.;
    RealType LowerPointBarrier = 0.;
    RealType UpperPointBarrier = 0.;
    int preIterations = 0;
  } BilevelOptimization;

  // Additional
  bool testDerivatives = false;


  //</editor-fold>


  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );

      meshPath = config["Geometry"]["mesh"].as<std::string>();
      bndMask = config["Geometry"]["boundaryMask"].as<std::vector<int>>();
      trackingIndices = config["Geometry"]["trackingIndices"].as<std::vector<int>>();
      detectBoundary = config["Geometry"]["detectBoundary"].as<bool>();
      dirichletFaces = config["Geometry"]["dirichletFaces"].as<std::vector<int>>();

      outputFilePrefix = config["Output"]["outputFilePrefix"].as<std::string>();
      outputFolder = config["Output"]["outputFolder"].as<std::string>();
      timestampOutput = config["Output"]["timestampOutput"].as<bool>();

      forceSpace = config["Forces"]["space"].as<std::string>();
      forceInitialization = config["Forces"]["initialization"].as<std::vector<RealType>>();

      initialThickness = config["Material"]["initialThickness"].as<RealType>();
      boundaryThickness = config["Material"]["boundaryThickness"].as<RealType>();
      materialBound = config["Material"]["materialBound"].as<RealType>();
      randomPerturbation = config["Material"]["randomPerturbation"].as<RealType>();
      useL2Constraint = config["Material"]["useL2Constraint"].as<bool>();

      BilevelOptimization.useNonlinearElasticity = ( config["BilevelOptimization"]["Elasticity"].as<std::string>() ==
                                                     "Nonlinear" );
      BilevelOptimization.maxNumIterations = config["BilevelOptimization"]["maxNumIterations"].as<int>();
      BilevelOptimization.numSamples = config["BilevelOptimization"]["numSamples"].as<int>();
      BilevelOptimization.optimalityTolerance = config["BilevelOptimization"]["optimalityTolerance"].as<RealType>();
      BilevelOptimization.stepsize = config["BilevelOptimization"]["stepsize"].as<RealType>();
      BilevelOptimization.maximalStepsize = config["BilevelOptimization"]["maximalStepsize"].as<RealType>();
      BilevelOptimization.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["BilevelOptimization"]["stepsizeController"].as<int>());
      BilevelOptimization.acceleration = config["BilevelOptimization"]["acceleration"].as<bool>();
      BilevelOptimization.trackingWeight = config["BilevelOptimization"]["trackingWeight"].as<RealType>();
      BilevelOptimization.MassWeight = config["BilevelOptimization"]["MassWeight"].as<RealType>();
      BilevelOptimization.MassBarrier = config["BilevelOptimization"]["MassBarrier"].as<RealType>();
      BilevelOptimization.LowerPointBarrierWeight = config["BilevelOptimization"]["LowerPointBarrierWeight"].as<RealType>();
      BilevelOptimization.UpperPointBarrierWeight = config["BilevelOptimization"]["UpperPointBarrierWeight"].as<RealType>();
      BilevelOptimization.LowerPointBarrier = config["BilevelOptimization"]["LowerPointBarrier"].as<RealType>();
      BilevelOptimization.UpperPointBarrier = config["BilevelOptimization"]["UpperPointBarrier"].as<RealType>();
      BilevelOptimization.preIterations = config["BilevelOptimization"]["preIterations"].as<int>();


      testDerivatives = config["Additional"]["testDerivatives"].as<bool>();
    }


    //<editor-fold desc="Output folders">
    if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
      outputFolder += "/";

    std::string execName( argv[0] );
    execName = execName.substr( execName.find_last_of( '/' ) + 1 );

    if ( timestampOutput ) {
      std::time_t t = std::time( nullptr );
      std::stringstream ss;
      ss << outputFolder;
      ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss << "_" << execName;
      ss << "/";
      outputFolder = ss.str();
      boost::filesystem::create_directory( outputFolder );
    }

    if ( argc == 2 ) {
      boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                    boost::filesystem::copy_option::overwrite_if_exists );
    }

    std::string outputPrefix = outputFolder + outputFilePrefix;
    //</editor-fold>

    //<editor-fold desc="Output log">
//    std::ofstream logFile;
//    logFile.open( outputFolder + "/_output.log" );
//
//
//    typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
//    typedef boost::iostreams::stream<TeeDevice> TeeStream;
//    TeeDevice tee_cout( logFile, output_cout );
//    TeeDevice tee_cerr( logFile, output_cerr );
//
//    TeeStream split_cout( tee_cout );
//    TeeStream split_cerr( tee_cerr );
//
//    std::cout.rdbuf( split_cout.rdbuf());
//    std::cerr.rdbuf( split_cerr.rdbuf());
    //</editor-fold>

    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();
    const int numFaces = Topology.getNumFaces();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    //</editor-fold>

    //<editor-fold desc="Material">
    VectorType faceAreas,nodalAreas;
    getFaceAreas<DefaultConfigurator>( Topology, refGeometry, faceAreas );
    computeNodalAreas<DefaultConfigurator>( Topology, refGeometry, nodalAreas );

    VectorType faceWeights = VectorType::Constant( numFaces, initialThickness );
    faceWeights += VectorType::Random( numFaces ) * initialThickness * randomPerturbation;

    for ( int faceIdx : dirichletFaces ) {
      faceWeights[faceIdx] = boundaryThickness;
      BilevelOptimization.MassBarrier += faceAreas[faceIdx] * boundaryThickness;
    }
    //</editor-fold>

    // Deformation energies
    MaterialShellDeformationType W( Topology, faceWeights );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletIndices;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletIndices );

    dirichletIndices.insert( dirichletIndices.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    std::vector<int> dirichletBoundary = dirichletIndices;
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;

    // Forces
    FullMatrixType forceBasis;
    if ( forceSpace == "Constant" ) {
      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.setZero();
      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ).segment( d * numVertices, numVertices ).setConstant( 1. );
      for ( int d : { 0, 1, 2 } )
        std::cout << " .. norm = " << forceBasis.col( d ).norm() << std::endl;

      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ) = forceBasis.col( d ).normalized();
    }
    else if ( forceSpace == "twistAndCompress" ) {
      VectorType horizontalCompressionForce( 3 * numVertices );
      VectorType verticalCompressionForce( 3 * numVertices );
      VectorType twistingForce( 3 * numVertices );

      auto VCFProfile = []( RealType z ) { return z / 4.; };
      auto HCFProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
//      auto HCFProfile = []( RealType z ) { return 1.; };
//      auto TProfile = []( RealType z ) { return ( z - 2. ) / 2.; };
//      auto TProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
      auto TProfile = []( RealType z ) { return z / 4.; };

      for ( int i = 0; i < Topology.getNumVertices(); i++ ) {
        VectorType p( 3 );
        getXYZCoord( refGeometry, p, i );

        VectorType normal = p;
        normal[2] = 0.;
        normal.normalize();

        VectorType hcf = normal * HCFProfile( p[2] );
        setXYZCoord( horizontalCompressionForce, hcf, i );

        VectorType vcf = VectorType::Zero( 3 );
        vcf[2] = VCFProfile( p[2] );
        setXYZCoord( verticalCompressionForce, vcf, i );

        VectorType twist = normal;
        twist[0] = normal[1];
        twist[1] = -normal[0];
        twist *= TProfile( p[2] );
        setXYZCoord( twistingForce, twist, i );
      }


      std::cout << " .. hCF.norm = " << horizontalCompressionForce.norm() << std::endl;
      std::cout << " .. twist.norm = " << twistingForce.norm() << std::endl;
      std::cout << " .. vCF.norm = " << verticalCompressionForce.norm() << std::endl;

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = horizontalCompressionForce.normalized();
      forceBasis.col( 1 ) = twistingForce.normalized();
      forceBasis.col( 2 ) = verticalCompressionForce.normalized();
    }
    else if ( forceSpace == "WindAndSnow"  ) {
      std::vector<VecType> vertexNormals;
      computeVertexNormals<DefaultConfigurator>( Topology, refGeometry, vertexNormals, true );

      VectorType snowForce( 3 * numVertices );
      VectorType windForceA( 3 * numVertices );
      VectorType windForceB( 3 * numVertices );

      for ( int vertexIdx = 0; vertexIdx < Topology.getNumVertices(); vertexIdx++ ) {
        const VecType &n = vertexNormals[vertexIdx];

        VecType d;
        d[0] = std::abs( n[0] );
        setXYZCoord( snowForce, d, vertexIdx );

        d.setZero();
        d[1] = std::abs( n[1] );
        setXYZCoord( windForceA, d, vertexIdx );

        d.setZero();
        d[2] = std::abs( n[2] );
        setXYZCoord( windForceB, d, vertexIdx );
      }

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = snowForce.normalized();
      forceBasis.col( 1 ) = windForceA.normalized();
      forceBasis.col( 2 ) = windForceB.normalized();
    }


    for ( int idx : dirichletBoundary )
      forceBasis.row( idx ).array() = 0.;

    VectorType ldForce( forceInitialization.size());
    for ( int i = 0; i < forceInitialization.size(); i++ )
      ldForce[i] = forceInitialization[i];

    std::cout << " .. Init forces: " << ldForce.transpose() << std::endl;
    std::cout << " .. Init forces.norm: " << ldForce.norm() << std::endl;

    VectorType vertexForces = forceBasis * ldForce;
//    std::cout << vertexForces << std::endl;

    // Output start values
    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = faceWeights;
      colorings["Force"] = vertexForces;
      saveAsVTP( Topology, refGeometry, outputPrefix + "OFS_start.vtp", colorings );
    }

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Optimization" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // DOF variable
    VectorType combinedVector( numFaces + numDOFs );
    combinedVector.head( numFaces ) = faceWeights;
    combinedVector.tail( numDOFs ).setZero();

    LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> ES( Topology, refGeometry,
                                                                                  dirichletBoundary,
                                                                                  undefDisplacement,
                                                                                  vertexForces, faceWeights,
                                                                                  false, true, true,
                                                                                  false, false, false );

    if ( trackingIndices.empty()) {
      trackingIndices.resize( Topology.getNumVertices());
      std::iota( trackingIndices.begin(), trackingIndices.end(), 0 );
    }
    VectorType DesignAndForce( 3 * numVertices + numFaces);
    DesignAndForce.head( 3 * numVertices) = vertexForces;
    DesignAndForce.tail( numFaces ) = faceWeights;

    VectorType Solution = ES.solve(DesignAndForce);

    VectorType DesignForceDeformation( 3 * numVertices + numFaces +  3 * numVertices );
    DesignForceDeformation.head( 3 * numVertices) = vertexForces;
    DesignForceDeformation.segment(  3 * numVertices, numFaces ) = faceWeights;
    DesignForceDeformation.tail( 3 * numVertices ) = Solution;

    std::cout << " .. LOFS" << std::endl;
    LinearizedOptimalForceSolver<DefaultConfigurator, MaterialShellDeformationType> OFS( Topology, refGeometry,
                                                                                         dirichletBoundary,
                                                                                         undefDisplacement,
                                                                                         faceWeights,
                                                                                         forceBasis,
                                                                                         ldForce,
                                                                                         false, true, false, false );

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );
      OFS.Configuration.maxNumIterations = config["OptimalForceSolver"]["maxNumIterations"].as<int>();
      OFS.Configuration.optimalityTolerance = config["OptimalForceSolver"]["optimalityTolerance"].as<RealType>();
      OFS.Configuration.stepsize = config["OptimalForceSolver"]["stepsize"].as<RealType>();
      OFS.Configuration.maximalStepsize = config["OptimalForceSolver"]["maximalStepsize"].as<RealType>();
      OFS.Configuration.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["OptimalForceSolver"]["stepsizeController"].as<int>());
      OFS.Configuration.acceleration = config["OptimalForceSolver"]["acceleration"].as<bool>();
      OFS.Configuration.useL2Constraint = config["OptimalForceSolver"]["useL2Constraint"].as<bool>();
      OFS.Configuration.barrierValue = config["OptimalForceSolver"]["barrierValue"].as<RealType>();
      OFS.Configuration.regularizationWeight = config["OptimalForceSolver"]["regularizationWeight"].as<RealType>();
      OFS.Configuration.objectiveWeight = config["OptimalForceSolver"]["objectiveWeight"].as<RealType>();
      OFS.Configuration.useCylinderBarrier = config["OptimalForceSolver"]["useCylinderBarrier"].as<bool>();
      OFS.Configuration.cylinderHeightFactor = config["OptimalForceSolver"]["cylinderHeightFactor"].as<RealType>();
      OFS.Configuration.cylinderHeightDimension = config["OptimalForceSolver"]["cylinderHeightDimension"].as<int>();
    }

//    OFS.testCylinderDerivatives();
    if (testDerivatives) {
      OFS.testDerivatives();
      OFS.testMixedDerivatives();
    }

    ldForce = OFS.forceSolve( faceWeights);
    vertexForces = forceBasis * ldForce;

    std::cout << std::scientific << " .. Start forces: " << ldForce.transpose() << std::endl;
    std::cout << std::scientific << " .. Start forces.norm: " << ldForce.norm() << std::endl;

    return 0;

    DesignAndForce.head( 3 * numVertices) = vertexForces;
    DesignAndForce.tail( numFaces ) = faceWeights;

    Solution = ES.solve(DesignAndForce);
    defGeometry = refGeometry + Solution;

    {
      std::map<std::string, VectorType> colorings;
      colorings["Force"] = vertexForces;
      colorings["Tracking"] = Solution;
      colorings["Material"] = faceWeights;
      saveAsVTP( Topology, defGeometry, outputPrefix + "OFS_def.vtp", colorings );
    }

    SimpleQuadraticFunctional<DefaultConfigurator> F;
    SimpleQuadraticGradient<DefaultConfigurator> DF;

    std::vector<int> mask;


    VectorType testVector( numFaces + 3 );
    testVector.head( numFaces ) = faceWeights;
    testVector.tail( 3 ) = ldForce;

//    ScalarValuedDerivativeTester<DefaultConfigurator>( F, DF, 1.e-4 ).plotRandomDirections( vertexWeights, 30,
//                                                                                                  "testF" );

    ReducedPDECFunctional<DefaultConfigurator> Fred_OFS( F, OFS );
    ReducedPDECGradient<DefaultConfigurator> DFred_OFS( F, DF, OFS, mask );

    std::cout << " .. F = " << Fred_OFS( faceWeights ) << std::endl;
    std::cout << " .. DF = " << DFred_OFS( faceWeights ).transpose() << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_OFS, DFred_OFS, 1.e-4 ).plotRandomDirections( faceWeights, 30,
                                                                                                          outputPrefix + "testFred_OFS" );
//    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_OFS, DFred_OFS, 1.e-4 ).plotAllDirections( faceWeights,
//                                                                                                          outputPrefix + "testFred_OFS" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_OFS, DFred_OFS, 1.e-4 ).testRandomDirections( faceWeights, 30,
                                                                                                  true );


    ReducedPDECFunctional<DefaultConfigurator> Fred_ES( F, ES );
    ReducedPDECGradient<DefaultConfigurator> DFred_ES( F, DF, ES, mask );

    std::cout << " .. F = " << Fred_ES( DesignAndForce ) << std::endl;
    std::cout << " .. DF = " << DFred_ES( DesignAndForce ).transpose() << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_ES, DFred_ES, 1.e-4 ).plotRandomDirections( DesignAndForce, 30,
                                                                                                        outputPrefix + "testFred_ES" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_ES, DFred_ES, 1.e-4 ).testRandomDirections( DesignAndForce, 30,
                                                                                                  true );


    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Fred_BLVL( F, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DFred_BLVL( F, DF, ES, OFS, forceBasis, mask );

    std::cout << " .. F = " << Fred_BLVL( faceWeights ) << std::endl;
    std::cout << " .. DF = " << DFred_BLVL( faceWeights ).transpose() << std::endl;

//    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_BLVL, DFred_BLVL, 1.e-4 ).plotRandomDirections( faceWeights, 30,
//                                                                                                            outputPrefix + "testFred_BLVL" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_BLVL, DFred_BLVL, 1.e-4 ).plotAllDirections( faceWeights,
                                                                                                            outputPrefix + "testFred_BLVL" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Fred_BLVL, DFred_BLVL, 1.e-4 ).testRandomDirections( faceWeights, 30,
                                                                                                            true );

    VectorType bla;
    DFred_BLVL.detailedApply(faceWeights, bla);
    return 0;

    TrackingTerm<DefaultConfigurator> T( Topology, refGeometry, trackingIndices,
                                         false, true, true, false, false, false );
    TrackingTermGradient<DefaultConfigurator> DT( Topology, refGeometry, trackingIndices, false, true, true,
                                                  false, false, false );

    MaterialMassLogBarrier<DefaultConfigurator> Lg( Topology, refGeometry, BilevelOptimization.MassBarrier,
                                                    false, true, true, false, false, false );
    MaterialMassLogBarrierGradient<DefaultConfigurator> DLg( Topology, refGeometry, BilevelOptimization.MassBarrier,
                                                             false, true, true, false, false, false );

    MaterialPointLogBarrier<DefaultConfigurator> PLg( Topology, refGeometry, BilevelOptimization.LowerPointBarrier, dirichletFaces,
                                                      false, true, true, false, false, false );
    MaterialPointLogBarrierGradient<DefaultConfigurator> DPLg( Topology, refGeometry, BilevelOptimization.LowerPointBarrier, dirichletFaces,
                                                               false, true, true, false, false, false );

    MaterialUpperLogBarrier<DefaultConfigurator> UPLg( Topology, refGeometry, BilevelOptimization.UpperPointBarrier, dirichletFaces,
                                                       false, true, true, false, false, false );
    MaterialUpperLogBarrierGradient<DefaultConfigurator> DUPLg( Topology, refGeometry, BilevelOptimization.UpperPointBarrier, dirichletFaces,
                                                                false, true, true, false, false, false );

    VectorType weights( 4 );
    weights << BilevelOptimization.trackingWeight, BilevelOptimization.MassWeight, BilevelOptimization.LowerPointBarrierWeight, BilevelOptimization.UpperPointBarrierWeight;

    std::cout << " .. weights = " << weights.transpose() << std::endl;

    ParametrizedAdditionOp<DefaultConfigurator> J( weights, T, Lg, PLg, UPLg  );
    ParametrizedAdditionGradient<DefaultConfigurator> DJ( weights, DT, DLg, DPLg, DUPLg   );


    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Tred_BLVL( T, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DTred_BLVL( T, DT, ES, OFS, forceBasis, mask );

    std::cout << " .. F = " << Tred_BLVL( faceWeights ) << std::endl;
    std::cout << " .. DF = " << DTred_BLVL( faceWeights ).transpose() << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( Tred_BLVL, DTred_BLVL, 1.e-4 ).plotRandomDirections( faceWeights, 30,
                                                                                                            outputPrefix + "testTred_BLVL" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Tred_BLVL, DTred_BLVL, 1.e-4 ).testRandomDirections( faceWeights, 30,
                                                                                                            true );


    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Lgred_BLVL( Lg, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DLgred_BLVL( Lg, DLg, ES, OFS, forceBasis, mask );

    std::cout << " .. F = " << Lgred_BLVL( faceWeights ) << std::endl;
    std::cout << " .. DF = " << DLgred_BLVL( faceWeights ).transpose() << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( Lgred_BLVL, DLgred_BLVL, 1.e-4 ).plotRandomDirections( faceWeights, 30,
                                                                                                              outputPrefix + "testLgred_BLVL" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Lgred_BLVL, DLgred_BLVL, 1.e-4 ).testRandomDirections( faceWeights, 30,
                                                                                                              true );


    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> PLgred_BLVL( PLg, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DPLgred_BLVL( PLg, DPLg, ES, OFS, forceBasis, mask );

    std::cout << " .. F = " << PLgred_BLVL( faceWeights ) << std::endl;
    std::cout << " .. DF = " << DPLgred_BLVL( faceWeights ).transpose() << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( PLgred_BLVL, DPLgred_BLVL, 1.e-4 ).plotRandomDirections( faceWeights, 30,
                                                                                                                outputPrefix + "testPLgred_BLVL" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( PLgred_BLVL, DPLgred_BLVL, 1.e-4 ).testRandomDirections( faceWeights, 30,
                                                                                                                true );

    LinearBilevelReducedFunctional<DefaultConfigurator, MaterialShellDeformationType> Jred_BLVL( J, ES, OFS, forceBasis );
    LinearBilevelReducedGradient<DefaultConfigurator, MaterialShellDeformationType> DJred_BLVL( J, DJ, ES, OFS, forceBasis, mask );

    std::cout << " .. F = " << Jred_BLVL( faceWeights ) << std::endl;
    std::cout << " .. DF = " << DJred_BLVL( faceWeights ).transpose() << std::endl;

    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred_BLVL, DJred_BLVL, 1.e-4 ).plotRandomDirections( faceWeights, 30,
                                                                                                            outputPrefix + "testJred_BLVL" );
    ScalarValuedDerivativeTester<DefaultConfigurator>( Jred_BLVL, DJred_BLVL, 1.e-4 ).testRandomDirections( faceWeights, 30,
                                                                                                            true );

    std::cout << "===============================================================" << std::endl;


    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}