/**
 * \brief Compute optimal force for certain material distribution
 */

//==============Includes================================
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/Core/FEM.h"
#include "goast/external/vtkIO.h"

#include "ComplianceTerm.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ShapeOptimization/ReducedFunctional.h"
#include "ShapeOptimization/ExpectedValue.h"
#include "ShapeOptimization/ExpectedExcess.h"
#include "Optimization/StochasticGradientDescent.h"
#include "Optimization/StochasticProximalGradientDescent.h"
#include "MaterialGenerators.h"
#include "MaterialDeformationEnergies.h"

#include "3DForces.h"
#include "ReducedForces.h"
//======================================================

//================================================================================

typedef DefaultConfigurator ConfiguratorType;
typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::VecType VecType;
typedef typename ConfiguratorType::FullMatrixType FullMatrixType;
typedef std::vector<TripletType> TripletListType;


//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {
  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";

  // Boundary values
  std::vector<int> bndMask{}; // 171
  bool detectBoundary = true;

  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Material
  RealType thickness = 0.03;
  RealType materialFactor = 20.;
  RealType splitOffset = 0.3;
  RealType minAngle = 0;
  RealType maxAngle = M_PI / 2.;

//  RealType minAngle = -0.5* M_PI / 9.;
//  RealType maxAngle = 0.5* M_PI / 9.;

  bool writeExamples = true;

  // Forces
  RealType force_x = 1e-3;
  RealType force_y = 0.;
  RealType force_z = -0.003;

  RealType forceBound = 0.02;
  bool useL2Constraint = true;

  // shape optimization
  struct {
    bool useNonlinearElasticity = true;
    int numStochasticSamples = 10;
    int maxNumIterations = 100;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    std::string stochasticMeasure = "ExpectedValue";
  } ShapeOptimization;

  // Additional
  bool testDerivatives = false;


  //</editor-fold>

  //<editor-fold desc="Output folders">
  if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
    outputFolder += "/";

  std::string execName( argv[0] );
  execName = execName.substr( execName.find_last_of( '/' ) + 1 );

  if ( timestampOutput ) {
    std::time_t t = std::time( nullptr );
    std::stringstream ss;
    ss << outputFolder;
    ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
    ss << "_" << execName;
    ss << "/";
    outputFolder = ss.str();
    boost::filesystem::create_directory( outputFolder );
  }

  if ( argc == 2 ) {
    boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                  boost::filesystem::copy_option::overwrite_if_exists );
  }

  std::string outputPrefix = outputFolder + outputFilePrefix;
  //</editor-fold>

  //<editor-fold desc="Output log">
  std::ofstream logFile;
  logFile.open( outputFolder + "/_output.log" );

  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
  typedef boost::iostreams::stream<TeeDevice> TeeStream;
  TeeDevice tee_cout( logFile, output_cout );
  TeeDevice tee_cerr( logFile, output_cerr );

  TeeStream split_cout( tee_cout );
  TeeStream split_cerr( tee_cerr );

  std::cout.rdbuf( split_cout.rdbuf());
  std::cerr.rdbuf( split_cerr.rdbuf());
  //</editor-fold>

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );

      meshPath = config["Geometry"]["mesh"].as<std::string>();
      bndMask = config["Geometry"]["boundaryMask"].as<std::vector<int>>();
      detectBoundary = config["Geometry"]["detectBoundary"].as<bool>();

      outputFilePrefix = config["Output"]["outputFilePrefix"].as<std::string>();
      outputFolder = config["Output"]["outputFolder"].as<std::string>();
      timestampOutput = config["Output"]["timestampOutput"].as<bool>();

      forceBound = config["Forces"]["forceBound"].as<RealType>();
      useL2Constraint = config["Forces"]["useL2Constraint"].as<bool>();
      force_x = config["Forces"]["initialization"][0].as<RealType>();
      force_y = config["Forces"]["initialization"][1].as<RealType>();
      force_z = config["Forces"]["initialization"][2].as<RealType>();

      thickness = config["Material"]["config"]["baseThickness"].as<RealType>();
      materialFactor = config["Material"]["config"]["materialFactor"].as<RealType>();
      splitOffset = config["Material"]["config"]["beamWidth"].as<RealType>();
      minAngle = config["Material"]["config"]["minAngle"].as<RealType>();
      maxAngle = config["Material"]["config"]["maxAngle"].as<RealType>();

      ShapeOptimization.useNonlinearElasticity = ( config["ShapeOptimization"]["Elasticity"].as<std::string>() ==
                                                   "Nonlinear" );
      ShapeOptimization.numStochasticSamples = config["ShapeOptimization"]["numSamples"].as<int>();
      ShapeOptimization.maxNumIterations = config["ShapeOptimization"]["maxNumIterations"].as<int>();
      ShapeOptimization.optimalityTolerance = config["ShapeOptimization"]["optimalityTolerance"].as<RealType>();
      ShapeOptimization.stepsize = config["ShapeOptimization"]["stepsize"].as<RealType>();
      ShapeOptimization.stochasticMeasure = config["ShapeOptimization"]["stochasticMeasure"].as<std::string>();

      testDerivatives = config["Additional"]["testDerivatives"].as<bool>();
      writeExamples = config["Additional"]["writeMaterialExamples"].as<bool>();
    }

    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    //</editor-fold>

    //<editor-fold desc="Material">
    VectorType vertexWeights_const = VectorType::Constant( numVertices, thickness );
    VectorType vertexWeights = VectorType::Constant( numVertices, thickness );

    SingleBeamGenerator g( thickness, minAngle, maxAngle, refGeometry, splitOffset, materialFactor );
    vertexWeights = g();
    //</editor-fold>

    // Deformation energies
    MaterialShellDeformationType W( Topology, vertexWeights );
    MaterialShellDeformationType W_constant( Topology, vertexWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletBoundary;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletBoundary );

    dirichletBoundary.insert( dirichletBoundary.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;

    // Forces
    VectorType horizontalCompressionForce( 3 * numVertices );
    VectorType verticalCompressionForce( 3 * numVertices );
    VectorType twistingForce( 3 * numVertices );

    auto VCFProfile = []( RealType z ) { return z / 4.; };
    auto HCFProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
    auto TProfile = []( RealType z ) { return ( z - 2. ) / 2.; };

    for ( int i = 0; i < Topology.getNumVertices(); i++ ) {
      VectorType p( 3 );
      getXYZCoord( refGeometry, p, i );

      VectorType normal = p;
      normal[2] = 0.;
      normal.normalize();

      VectorType hcf = normal * HCFProfile(p[2]);
      setXYZCoord(horizontalCompressionForce, hcf, i);

      VectorType vcf = VectorType::Zero(3);
      vcf[2] = VCFProfile(p[2]);
      setXYZCoord(verticalCompressionForce, vcf, i);

      VectorType twist = normal;
      twist[0] = normal[1];
      twist[1] = -normal[0];
      twist *= TProfile(p[2]);
      setXYZCoord(twistingForce, twist, i);
    }

    FullMatrixType forceBasis( 3 * numVertices, 3);
    forceBasis.col(0) = horizontalCompressionForce;
    forceBasis.col(1) = twistingForce;
    forceBasis.col(2) = verticalCompressionForce;

    VectorType ldForce(3);
    ldForce << force_x, force_y, force_z;

    VectorType vertexForces = forceBasis * ldForce;

    // Output start values
    saveAsVTP( Topology, refGeometry, outputPrefix + "stochastic_start.vtp", vertexWeights, "weights" );

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Optimization" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // DOF variable
    VectorType combinedVector( 3 * numVertices + numDOFs );
    combinedVector.head( 3 * numVertices ) = vertexForces;
    combinedVector.tail( numDOFs ).setZero();

    //<editor-fold desc="Setup elasticity">
    std::unique_ptr<StateEquationSolverBase<DefaultConfigurator>> SES;

    if ( ShapeOptimization.useNonlinearElasticity ) {
      SES.reset( new NonlinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType>( Topology,
                                                                                                   refGeometry,
                                                                                                   dirichletBoundary,
                                                                                                   undefDisplacement,
                                                                                                   vertexForces,
                                                                                                   vertexWeights,
                                                                                                   false, true, false,
                                                                                                   false, false,
                                                                                                   true ));
    }
    else {
      SES.reset( new LinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType>( Topology, refGeometry,
                                                                                                dirichletBoundary,
                                                                                                undefDisplacement,
                                                                                                vertexForces,
                                                                                                vertexWeights,
                                                                                                false, true, false,
                                                                                                false, false, true ));
    }

    VectorType Solution = SES->solve( vertexForces );

    saveAsVTP<VectorType>( Topology, refGeometry + Solution,
                           outputPrefix + "stochastic_start_def.vtp", vertexWeights, "weights" );

    combinedVector.tail( numDOFs ) = Solution;
    //</editor-fold>


    //<editor-fold desc="Functionals">
    std::unique_ptr<ParametrizedBaseOp<VectorType, RealType, VectorType>> C;
    std::unique_ptr<ParametrizedBaseOp<VectorType, VectorType, VectorType>> DC;

    if ( ShapeOptimization.useNonlinearElasticity ) {
      C.reset( new ComplianceTerm<DefaultConfigurator, MaterialShellDeformationType>( Topology, refGeometry, W, false,
                                                                                      true, false, false, false,
                                                                                      true ));
      DC.reset( new ComplianceTermGradient<DefaultConfigurator, MaterialShellDeformationType>( Topology, refGeometry, W,
                                                                                               false, true, false,
                                                                                               false, false, true ));
    }
    else {
      C.reset( new LinearizedComplianceTerm<DefaultConfigurator, MaterialShellDeformationType>( Topology, refGeometry,
                                                                                                undefDisplacement,
                                                                                                vertexWeights,
                                                                                                false, true, false,
                                                                                                false, false, true ));
      DC.reset( new LinearizedComplianceTermGradient<DefaultConfigurator, MaterialShellDeformationType>( Topology,
                                                                                                         refGeometry,
                                                                                                         undefDisplacement,
                                                                                                         vertexWeights,
                                                                                                         false, true,
                                                                                                         false, false,
                                                                                                         false, true ));
    }

    ReducedPDECParametrizedFunctional<DefaultConfigurator> Jred( *C, *SES );
    ReducedPDECParametrizedGradient<DefaultConfigurator> DJred( *C, *DC, *SES, dirichletBoundary );

    if ( testDerivatives ) {
      ScalarValuedDerivativeTester<DefaultConfigurator>( Jred, DJred, 1e-6 ).plotAllDirections( vertexForces,
                                                                                                outputPrefix +
                                                                                                "testDJred_" );
    }

    return 0;

    // Reduce to two-dimensional problem
    ReducedForceFunctional<DefaultConfigurator> J2d( Topology, Jred, forceBasis, dirichletBoundary );
    ReducedForceGradient<DefaultConfigurator> DJ2d( Topology, DJred, forceBasis, dirichletBoundary );

    // Stochastic config
    // Expected value
    SampledExpectedValue<ReducedForceFunctional<DefaultConfigurator>> EJ( J2d, g, ShapeOptimization.numStochasticSamples );
    SampledExpectedValue<ReducedForceGradient<DefaultConfigurator>> DEJ( DJ2d, g, ShapeOptimization.numStochasticSamples );
    DEJ.setSamples( EJ.getSamples());

    NegativeOp<VectorType, RealType, VectorType> nEJ( EJ );
    NegativeOp<VectorType, VectorType, VectorType> nDEJ( DEJ );

    // Expected Excess
    SampledExpectedExcess<DefaultConfigurator> EE( J2d, g, ShapeOptimization.numStochasticSamples, 1e-6, 0.01 );
    SampledExpectedExcessGradient<DefaultConfigurator> DEE( J2d, DJ2d, g, ShapeOptimization.numStochasticSamples, 1e-6,
                                                            0.01 );
    EE.setSamples( EJ.getSamples());
    DEE.setSamples( EE.getSamples());

    NegativeOp<VectorType, RealType, VectorType> nEE( EE );
    NegativeOp<VectorType, VectorType, VectorType> nDEE( DEE );

    // Test
    std::cout << "EV: " << EJ( ldForce ) << "; EE: " << EE( ldForce ) << std::endl;

    if ( testDerivatives ) {
      ScalarValuedDerivativeTester<DefaultConfigurator>( J2d, DJ2d, 1e-6 ).plotAllDirections( ldForce,
                                                                                              outputPrefix +
                                                                                              "testDJ2d_" );
      ScalarValuedDerivativeTester<DefaultConfigurator>( nEE, nDEE, 1e-6 ).plotAllDirections( ldForce,
                                                                                              outputPrefix +
                                                                                              "testnDEE_" );
      ScalarValuedDerivativeTester<DefaultConfigurator>( nEJ, nDEJ, 1e-6 ).plotAllDirections( ldForce,
                                                                                              outputPrefix +
                                                                                              "testnDEJ_" );
    }
    //</editor-fold>

    VectorType lowerBounds = VectorType::Constant( 2, -forceBound );
    VectorType upperBounds = VectorType::Constant( 2, forceBound );

    if ( useL2Constraint ) {
      if ( ShapeOptimization.stochasticMeasure == "ExpectedValue" ) {
        StochasticProximalGradientDescent<DefaultConfigurator> Solver( nEJ, nDEJ, ShapeOptimization.maxNumIterations,
                                                                       ShapeOptimization.optimalityTolerance, SHOW_ALL,
                                                                       ShapeOptimization.stepsize );
        Solver.setMaximalNorm( forceBound );
        auto t_start = std::chrono::high_resolution_clock::now();
        Solver.solve( ldForce, ldForce );
        auto t_end = std::chrono::high_resolution_clock::now();

        std::cout << " - Solution took " << std::fixed
                  << std::chrono::duration<double, std::ratio<1> >( t_end - t_start ).count() << "s." << std::endl;
      }
      else if ( ShapeOptimization.stochasticMeasure == "ExpectedExcess" ) {
        StochasticProximalGradientDescent<DefaultConfigurator> Solver( nEE, nDEE, ShapeOptimization.maxNumIterations,
                                                                       ShapeOptimization.optimalityTolerance, SHOW_ALL,
                                                                       ShapeOptimization.stepsize );
        Solver.setMaximalNorm( forceBound );
        auto t_start = std::chrono::high_resolution_clock::now();
        Solver.solve( ldForce, ldForce );
        auto t_end = std::chrono::high_resolution_clock::now();

        std::cout << " - Solution took " << std::fixed
                  << std::chrono::duration<double, std::ratio<1> >( t_end - t_start ).count() << "s." << std::endl;
      }
    }
    else {
      StochasticGradientDescent<DefaultConfigurator> Solver( nEJ, nDEJ, ShapeOptimization.maxNumIterations,
                                                             ShapeOptimization.optimalityTolerance, SHOW_ALL );
      Solver.setVariableBounds( lowerBounds, upperBounds );
      auto t_start = std::chrono::high_resolution_clock::now();
      Solver.solve( ldForce, ldForce );
      auto t_end = std::chrono::high_resolution_clock::now();

      std::cout << " - Solution took " << std::fixed
                << std::chrono::duration<double, std::ratio<1> >( t_end - t_start ).count() << "s." << std::endl;
    }

    std::cout << " - Solution: ( " << ldForce[0] << ", " << ldForce[1] << ", " << ldForce[2] << " )" << std::endl;
    std::cout << " -- Norm: " << ldForce.norm() << std::endl;
    std::cout << " - Functionals: " << std::endl;
    std::cout << " -- EV: " << std::scientific << EJ( ldForce ) << std::endl;
    std::cout << " -- DEV: " << DEJ( ldForce ).format( Eigen::IOFormat( 4, 0, ", ", ",", "", "", "(", ")" ))
              << std::endl;
    std::cout << " -- EE: " << std::scientific << EE( ldForce ) << std::endl;

    VectorType newForce = forceBasis * ldForce;

    Solution = SES->solve( newForce );

    defGeometry = refGeometry + Solution;
    std::map<std::string, VectorType> colorings;
    colorings["Force"] = newForce;
    colorings["initForce"] = vertexForces;
    colorings["weights"] = vertexWeights;
//    colorings["weights_LES"] = LES.m_vertexMaterial;
    saveAsVTP( Topology, defGeometry, outputPrefix + "stochastic_2dforce_opt.vtp", colorings );

    for ( int i = 0; i < 10; i++ ) {
      vertexWeights = g();
      SES->updateParameters( vertexWeights );
      Solution = SES->solve( newForce );
      defGeometry = refGeometry + Solution;

      colorings["weights"] = vertexWeights;
//      colorings["weights_LES"] = LES.m_vertexMaterial;
      saveAsVTP( Topology, defGeometry, outputPrefix + "stochastic_2dforce_opt_" + std::to_string( i ) + ".vtp",
                 colorings );
    }

    std::cout << std::endl << " - Functionals(1,0,0): " << std::endl;
    ldForce[0] = forceBound;
    ldForce[1] = 0;
    ldForce[2] = 0;
    std::cout << " -- EV: " << std::scientific << EJ( ldForce ) << std::endl;
//    std::cout << " -- DEV: " << DEJ( ldForce ).format( Eigen::IOFormat( 4, 0, ", ", ",", "", "", "(", ")" ))
//              << std::endl;
    std::cout << " -- EE: " << std::scientific << EE( ldForce ) << std::endl;

    std::cout << std::endl << " - Functionals(0,1,0): " << std::endl;
    ldForce[0] = 0;
    ldForce[1] = forceBound;
    ldForce[2] = 0;
    std::cout << " -- EV: " << std::scientific << EJ( ldForce ) << std::endl;
//    std::cout << " -- DEV: " << DEJ( ldForce ).format( Eigen::IOFormat( 4, 0, ", ", ",", "", "", "(", ")" ))
//              << std::endl;
    std::cout << " -- EE: " << std::scientific << EE( ldForce ) << std::endl;


    std::cout << std::endl << " - Functionals(0,0,1): " << std::endl;
    ldForce[0] = 0;
    ldForce[1] = 0;
    ldForce[2] = forceBound;
    std::cout << " -- EV: " << std::scientific << EJ( ldForce ) << std::endl;
//    std::cout << " -- DEV: " << DEJ( ldForce ).format( Eigen::IOFormat( 4, 0, ", ", ",", "", "", "(", ")" ))
//              << std::endl;
    std::cout << " -- EE: " << std::scientific << EE( ldForce ) << std::endl;

    std::cout << "===============================================================" << std::endl;

//    std::cout << newForce << std::endl;

    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}