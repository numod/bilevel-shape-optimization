/**
 * \brief Compute optimal force for certain material distribution
 */

//=====================Includes=========================
#include <iostream>
#include <chrono>
#include <ctime>
#include <string>
#include <random>

#include <yaml-cpp/yaml.h>
#include <boost/filesystem.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include "goast/external/vtkIO.h"

#include "Optimization/ProximalGradientDescent.h"
#include "Optimization/ProximalMappings.h"

#include "ShapeOptimization/ReducedFunctional.h"

#include "MaterialDeformationEnergies.h"
#include "LinearElasticitySolver.h"
#include "NonlinearElasticitySolver.h"
#include "ComplianceTerm.h"
#include "FixedComplianceTerm.h"
#include "TrackingCost.h"
#include "MaterialRegularization.h"
#include "OptimalForceSolver.h"
#include "BilevelReducedFunctional.h"

#include "ReducedForces.h"
//======================================================

//================================================================================

typedef DefaultConfigurator ConfiguratorType;
typedef MaterialShellDeformation<DefaultConfigurator, MaterialNonlinearMembraneDeformation<DefaultConfigurator>, MaterialSimpleBendingDeformation<DefaultConfigurator> > MaterialShellDeformationType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::FullMatrixType FullMatrixType;
typedef std::vector<TripletType> TripletListType;


//====================================================================================================================================
//=================================================== Main ===========================================================================
//====================================================================================================================================


int main( int argc, char *argv[] ) {
  //<editor-fold desc="Configuration">
  // Mesh
  std::string meshPath = "/home/josua/Projects/BiSO/data/Halfsphere/HalfsphereL_3.obj";

  // Boundary values
  std::vector<int> bndMask{}; // 171
  std::vector<int> trackingIndices{}; // 171
  bool detectBoundary = true;

  // Output folder
  std::string outputFilePrefix;
  std::string outputFolder = "/home/josua/Projects/BiSO/output";
  bool timestampOutput = true;

  // Material
  RealType initialThickness = 0.03;
  RealType materialBound = 0.02;
  bool useL2Constraint = true;

  bool writeExamples = true;

  // Forces
  std::string forceSpace = "twistAndCompress"; // twistAndCompress, Constant
  std::vector<RealType> forceInitialization;


  // shape optimization
  struct {
    bool useNonlinearElasticity = true;
    int maxNumIterations = 100;
    RealType optimalityTolerance = 1e-6;
    RealType stepsize = 1e-3;
    RealType maximalStepsize = 1e-3;
    TIMESTEP_CONTROLLER stepsizeController = CONST_TIMESTEP_CONTROL;
    bool acceleration = false;
    RealType dirichletWeight = 0.;
  } ShapeOptimization;

  // Additional
  bool testDerivatives = false;


  //</editor-fold>


  std::ostream output_cout( std::cout.rdbuf());
  std::ostream output_cerr( std::cerr.rdbuf());

  try {
    // ========================================================================
    // ================================= Setup ================================
    // ========================================================================


    // ===== reading and preparing data ===== //
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Input data" << std::endl;
    std::cout << "===============================================================" << std::endl;

    if ( argc == 2 ) {
      YAML::Node config = YAML::LoadFile( argv[1] );

      meshPath = config["Geometry"]["mesh"].as<std::string>();
      bndMask = config["Geometry"]["boundaryMask"].as<std::vector<int>>();
      trackingIndices = config["Geometry"]["trackingIndices"].as<std::vector<int>>();
      detectBoundary = config["Geometry"]["detectBoundary"].as<bool>();

      outputFilePrefix = config["Output"]["outputFilePrefix"].as<std::string>();
      outputFolder = config["Output"]["outputFolder"].as<std::string>();
      timestampOutput = config["Output"]["timestampOutput"].as<bool>();

      forceSpace = config["Forces"]["space"].as<std::string>();
      forceInitialization = config["Forces"]["initialization"].as<std::vector<RealType>>();

      initialThickness = config["Material"]["initialThickness"].as<RealType>();
      materialBound = config["Material"]["materialBound"].as<RealType>();
      useL2Constraint = config["Material"]["useL2Constraint"].as<bool>();

      ShapeOptimization.useNonlinearElasticity = ( config["ShapeOptimization"]["Elasticity"].as<std::string>() ==
                                                   "Nonlinear" );
      ShapeOptimization.maxNumIterations = config["ShapeOptimization"]["maxNumIterations"].as<int>();
      ShapeOptimization.optimalityTolerance = config["ShapeOptimization"]["optimalityTolerance"].as<RealType>();
      ShapeOptimization.stepsize = config["ShapeOptimization"]["stepsize"].as<RealType>();
      ShapeOptimization.maximalStepsize = config["ShapeOptimization"]["maximalStepsize"].as<RealType>();
      ShapeOptimization.stepsizeController = static_cast<TIMESTEP_CONTROLLER>(config["ShapeOptimization"]["stepsizeController"].as<int>());
      ShapeOptimization.acceleration = config["ShapeOptimization"]["acceleration"].as<bool>();
      ShapeOptimization.dirichletWeight = config["ShapeOptimization"]["dirichletWeight"].as<RealType>();

      testDerivatives = config["Additional"]["testDerivatives"].as<bool>();
      writeExamples = config["Additional"]["writeMaterialExamples"].as<bool>();
    }


    //<editor-fold desc="Output folders">
    if ( outputFolder.compare( outputFolder.length() - 1, 1, "/" ) != 0 )
      outputFolder += "/";

    std::string execName( argv[0] );
    execName = execName.substr( execName.find_last_of( '/' ) + 1 );

    if ( timestampOutput ) {
      std::time_t t = std::time( nullptr );
      std::stringstream ss;
      ss << outputFolder;
      ss << std::put_time( std::localtime( &t ), "%Y%m%d_%H%M%S" );
      ss << "_" << execName;
      ss << "/";
      outputFolder = ss.str();
      boost::filesystem::create_directory( outputFolder );
    }

    if ( argc == 2 ) {
      boost::filesystem::copy_file( argv[1], outputFolder + "_parameters.conf",
                                    boost::filesystem::copy_option::overwrite_if_exists );
    }

    std::string outputPrefix = outputFolder + outputFilePrefix;
    //</editor-fold>

    //<editor-fold desc="Output log">
    std::ofstream logFile;
    logFile.open( outputFolder + "/_output.log" );


    typedef boost::iostreams::tee_device<std::ofstream, std::ostream> TeeDevice;
    typedef boost::iostreams::stream<TeeDevice> TeeStream;
    TeeDevice tee_cout( logFile, output_cout );
    TeeDevice tee_cerr( logFile, output_cerr );

    TeeStream split_cout( tee_cout );
    TeeStream split_cerr( tee_cerr );

    std::cout.rdbuf( split_cout.rdbuf());
    std::cerr.rdbuf( split_cerr.rdbuf());
    //</editor-fold>

    //<editor-fold desc="Basic setup">
    TriMesh inputMesh;
    if ( !OpenMesh::IO::read_mesh( inputMesh, meshPath )) {
      throw std::runtime_error( "Error while reading " + meshPath );
    }


    MeshTopologySaver Topology( inputMesh );
    const int numDOFs = 3 * Topology.getNumVertices();
    const int numEdges = Topology.getNumEdges();
    const int numVertices = Topology.getNumVertices();

    std::cout << std::endl << " - Mesh size:" << std::endl;
    std::cout << " -- Number of vertices: " << Topology.getNumVertices() << std::endl;
    std::cout << " -- Number of edges: " << Topology.getNumEdges() << std::endl;
    std::cout << " -- Number of faces: " << Topology.getNumFaces() << std::endl;

    std::cout << " - Boundary Mask = { ";
    for ( auto v : bndMask )
      std::cout << v << " ";
    std::cout << "}" << std::endl;

    VectorType refGeometry;
    getGeometry( inputMesh, refGeometry );

    // Initialize variables
    VectorType undefDisplacement = VectorType::Zero( 3 * numVertices );
    VectorType defDisplacement = VectorType::Zero( 3 * numVertices );

    VectorType defGeometry = refGeometry + defDisplacement;
    //</editor-fold>

    //<editor-fold desc="Material">
    VectorType vertexWeights_const = VectorType::Constant( numVertices, initialThickness );
    VectorType vertexWeights = VectorType::Constant( numVertices, initialThickness ) + VectorType::Random( numVertices ) * initialThickness * 0.01;
    //</editor-fold>

    // Deformation energies
    MaterialShellDeformationType W( Topology, vertexWeights );
    MaterialShellDeformationType W_constant( Topology, vertexWeights_const );
    std::cout << " .. Material done" << std::endl;

    // Dirichlet Boundary
    std::vector<int> dirichletBoundary;
    if ( detectBoundary )
      Topology.fillFullBoundaryMask( dirichletBoundary );

    dirichletBoundary.insert( dirichletBoundary.end(), bndMask.begin(), bndMask.end());

    // Extend the dirichlet boundary in 3 dimensions
    extendBoundaryMask( numVertices, dirichletBoundary );
    std::cout << " .. Dirichlet done" << std::endl;

    // Forces
    FullMatrixType forceBasis;
    if ( forceSpace == "Constant" ) {
      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.setZero();
      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ).segment( d * numVertices, numVertices ).setConstant( 1. );
      for ( int d : { 0, 1, 2 } )
        std::cout << " .. norm = " << forceBasis.col( d ).norm() << std::endl;

      for ( int d : { 0, 1, 2 } )
        forceBasis.col( d ) = forceBasis.col( d ).normalized();
    }
    else if ( forceSpace == "twistAndCompress" ) {
      VectorType horizontalCompressionForce( 3 * numVertices );
      VectorType verticalCompressionForce( 3 * numVertices );
      VectorType twistingForce( 3 * numVertices );

      auto VCFProfile = []( RealType z ) { return z / 4.; };
      auto HCFProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
//      auto HCFProfile = []( RealType z ) { return 1.; };
//      auto TProfile = []( RealType z ) { return ( z - 2. ) / 2.; };
//      auto TProfile = []( RealType z ) { return -z * ( z - 4. ) / 4.; };
      auto TProfile = []( RealType z ) { return z / 4.; };

      for ( int i = 0; i < Topology.getNumVertices(); i++ ) {
        VectorType p( 3 );
        getXYZCoord( refGeometry, p, i );

        VectorType normal = p;
        normal[2] = 0.;
        normal.normalize();

        VectorType hcf = normal * HCFProfile( p[2] );
        setXYZCoord( horizontalCompressionForce, hcf, i );

        VectorType vcf = VectorType::Zero( 3 );
        vcf[2] = VCFProfile( p[2] );
        setXYZCoord( verticalCompressionForce, vcf, i );

        VectorType twist = normal;
        twist[0] = normal[1];
        twist[1] = -normal[0];
        twist *= TProfile( p[2] );
        setXYZCoord( twistingForce, twist, i );
      }


      std::cout << " .. hCF.norm = " << horizontalCompressionForce.norm() << std::endl;
      std::cout << " .. twist.norm = " << twistingForce.norm() << std::endl;
      std::cout << " .. vCF.norm = " << verticalCompressionForce.norm() << std::endl;

      forceBasis.resize( 3 * numVertices, 3 );
      forceBasis.col( 0 ) = horizontalCompressionForce.normalized();
      forceBasis.col( 1 ) = twistingForce.normalized();
      forceBasis.col( 2 ) = verticalCompressionForce.normalized();
    }

    VectorType ldForce( forceInitialization.size());
    for ( int i = 0; i < forceInitialization.size(); i++ )
      ldForce[i] = forceInitialization[i];

    VectorType vertexForces = forceBasis * ldForce;
//    std::cout << vertexForces << std::endl;

    // Output start values
    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = vertexWeights;
      colorings["Force"] = vertexForces;
      saveAsVTP( Topology, refGeometry, outputPrefix + "alt_start.vtp", colorings );
    }

    std::cout << std::endl;
    std::cout << "===============================================================" << std::endl;
    std::cout << "  Optimization" << std::endl;
    std::cout << "===============================================================" << std::endl;

    // DOF variable
    VectorType combinedVector( numVertices + numDOFs );
    combinedVector.head( numVertices ) = vertexWeights;
    combinedVector.tail( numDOFs ).setZero();

    NonlinearElasticitySolver<DefaultConfigurator, MaterialShellDeformationType> ES( Topology, refGeometry,
                                                                                     dirichletBoundary,
                                                                                     undefDisplacement,
                                                                                     vertexForces, vertexWeights,
                                                                                     false, false, true,
                                                                                     false, true, false );

    if ( trackingIndices.empty()) {
      trackingIndices.resize( Topology.getNumVertices());
      std::iota( trackingIndices.begin(), trackingIndices.end(), 0 );
    }
    TrackingTerm<DefaultConfigurator> T( Topology, refGeometry, trackingIndices, false, false, true, false, true,
                                         false );
    TrackingTermGradient<DefaultConfigurator> DT( Topology, refGeometry, trackingIndices, false, false, true, false,
                                                  true, false );

    ReducedPDECParametrizedFunctional<DefaultConfigurator> Jred( T, ES );
    ReducedPDECParametrizedGradient<DefaultConfigurator> DJred( T, DT, ES, dirichletBoundary );

    VectorType lowerBounds = VectorType::Constant( numVertices, 1.e-3 );
    VectorType upperBounds = VectorType::Constant( numVertices, std::numeric_limits<RealType>::infinity());

    VectorType Solution = ES.solve( vertexWeights );

    {
      std::map<std::string, VectorType> colorings;
      colorings["Material"] = vertexWeights;
      colorings["Tracking"] = Solution;
      colorings["Force"] = vertexForces;
      saveAsVTP<VectorType>( Topology, refGeometry + Solution,
                             outputPrefix + "alt_start_def.vtp", vertexWeights, "weights" );
    }

    OptimalForceSolver<DefaultConfigurator, MaterialShellDeformationType> OFS( Topology, refGeometry, dirichletBoundary,
                                                                               undefDisplacement, vertexWeights,
                                                                               forceBasis, 4., ldForce,
                                                                               false, true, false, false );
    ldForce = OFS.solve(vertexWeights);
    for ( int i = 0; i < forceInitialization.size(); i++ )
      ldForce[i] = forceInitialization[i];
    std::cout << " .. Force = " << ldForce.transpose() << std::endl;
    VectorType newForce = forceBasis * ldForce;
    VectorType newMaterial = vertexWeights;

    for (int iter = 0; iter < 10; iter++) {
      std::cout << " ############# Iter " << iter  << " ############# " << std::endl;

      // Then optimize material
      std::cout << " ### Optimizing material ### " << std::endl;
      Jred.setParameters(newForce);
      DJred.setParameters(newForce);
      VectorType nextMaterial = newMaterial;
      auto prox = std::bind( projectionOnBoxBall<RealType, VectorType>, materialBound, lowerBounds, upperBounds,
                             std::placeholders::_1, std::placeholders::_2 );

      ProximalGradientDescent<DefaultConfigurator> Solver( Jred, DJred, prox, ShapeOptimization.maxNumIterations,
                                                           ShapeOptimization.optimalityTolerance, SHOW_ALL );
      Solver.setParameter( "initial_stepsize", ShapeOptimization.stepsize );
      Solver.setParameter( "maximal_stepsize", ShapeOptimization.maximalStepsize );
      Solver.setParameter( "stepsize_control", ShapeOptimization.stepsizeController );
      Solver.setParameter( "acceleration", ShapeOptimization.acceleration );
      auto t_start = std::chrono::high_resolution_clock::now();
      Solver.solve( newMaterial, nextMaterial );
      auto t_end = std::chrono::high_resolution_clock::now();

      std::cout << " - Solution took " << std::fixed
                << std::chrono::duration<double, std::ratio<1> >( t_end - t_start ).count() << "s." << std::endl;
      std::cout << " .. Final L2-norm: " << nextMaterial.norm() << std::endl;
      std::cout << " .. Lower bound: " << nextMaterial.minCoeff() << std::endl;
      RealType materialChange = (newMaterial - nextMaterial).norm();
      std::cout << " .. Change in force: " << materialChange << std::endl;
      newMaterial = nextMaterial;

      OFS.updateParameters(newMaterial);
      Solution = ES.solve(newMaterial);
      {
        defGeometry = refGeometry + Solution;
        std::map<std::string, VectorType> colorings;
        colorings["initForce"] = vertexForces;
        colorings["Force"] = newForce;
        colorings["Tracking"] = Solution;
        colorings["initMaterial"] = vertexWeights;
        colorings["Material"] = newMaterial;
        saveAsVTP( Topology, defGeometry, outputPrefix + "alt_opt_material_" + std::to_string(iter) + ".vtp", colorings );
      }

      // First optimize force:
      std::cout << " ### Optimizing force ### " << std::endl;
      VectorType newLdForce = OFS.solve(newMaterial);
      RealType forceChange = (ldForce - newLdForce).norm();
      std::cout << " .. Force = " << newLdForce.transpose() << std::endl;
      std::cout << " .. Change in force: " << forceChange << std::endl;
      ldForce = newLdForce;
      newForce = forceBasis * ldForce;

      // compute corresponding deformation and save it
      ES.updateParameters(newForce);
      Solution = ES.solve(newMaterial);
      {
        defGeometry = refGeometry + Solution;
        std::map<std::string, VectorType> colorings;
        colorings["initForce"] = vertexForces;
        colorings["Force"] = newForce;
        colorings["Tracking"] = Solution;
        colorings["initMaterial"] = vertexWeights;
        colorings["Material"] = newMaterial;
        saveAsVTP( Topology, defGeometry, outputPrefix + "alt_opt_force_" + std::to_string(iter) + ".vtp", colorings );
      }
    }

    std::cout << "===============================================================" << std::endl;


    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( std::exception &el ) {
    std::cout << std::endl << "ERROR!! CAUGHT FOLLOWING EXCEPTION: " << std::endl << el.what() << std::endl
              << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
  catch ( ... ) {
    std::cout << std::endl << "ERROR!! CAUGHT EXCEPTION. " << std::endl << std::flush;
    std::cout.rdbuf( output_cout.rdbuf());
    std::cerr.rdbuf( output_cerr.rdbuf());
    return -1;
  }
}